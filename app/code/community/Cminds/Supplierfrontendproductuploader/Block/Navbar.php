<?php
class Cminds_Supplierfrontendproductuploader_Block_Navbar extends Mage_Core_Block_Template {
	private $_markedProductIds = null;

	public function getMarkedProductCount() {
		if($this->_markedProductIds == NULL) {
			$this->_markedProductIds = $this->getMarkedProduct();
		}

		return count($this->_markedProductIds);
	}

	public function hasMarkedProducts() {
		return ($this->getMarkedProductCount() > 0);
	}

	public function getMarkedProduct() {
		$count = array();

		$collection = Mage::getModel('catalog/product')
		->getCollection()
		->addAttributeToFilter('creator_id', Mage::helper('supplierfrontendproductuploader')->getSupplierId())
		->addAttributeToFilter('admin_product_note', array('notnull' => true));

		foreach($collection AS $product) {
			$count[] = $product->getId();
		}

		return $count;
	}		
}