<?php

class Cminds_Supplierfrontendproductuploader_LoginController extends Cminds_Supplierfrontendproductuploader_Controller_Action {
    public function preDispatch() {
        parent::preDispatch();
        $this->_getHelper()->validateModule();
    }

    public function indexAction() {
    	$this->loadLayout();
    	$this->getLayout()->getBlock('header')->setTemplate('');
    	$this->getLayout()->getBlock('footer')->setTemplate('');
    	$this->_initLayoutMessages('customer/session');
    	$this->renderLayout();
//      $this->_renderBlocks();
    }

    public function forgetpasswordAction() {
    	$this->loadLayout();
    	$this->getLayout()->getBlock('header')->setTemplate('');
    	$this->getLayout()->getBlock('footer')->setTemplate('');
    	$this->_initLayoutMessages('customer/session');
    	$this->renderLayout();
    	//      $this->_renderBlocks();
    }
    
    
    public function loginAction() {
        if(!Mage::getSingleton('customer/session')->isLoggedIn()) {
            if ($this->getRequest()->isPost()) {
            		$session = Mage::getSingleton('customer/session');
                $login = $this->getRequest()->getPost();
                if (!empty($login['email']) && !empty($login['password'])) {
                    try {
                        $session->login($login['email'], $login['password']);
                        if ($session->getCustomer()->getIsJustConfirmed()) {
                            $this->_redirect('cminds_supplierfrontendproductuploader/');
                        }
                        $session->addError($this->__('Login and password are not correct.'));
                        $this->_redirect('*');
                    } catch (Mage_Core_Exception $e) {
                      $session->addError($e->getMessage());
                      $session->setUsername($login['email']);
                      $this->_redirect('*');
                    } catch (Exception $e) {
                    	$session->addError($e->getMessage());
                      $this->_redirect('*');
                    }
                } else {
                    $session->addError($this->__('Login and password are required.'));
                    $this->_redirect('*');
                }
            }
        } else {
          $this->_redirect('*');
        }
    }    
}
