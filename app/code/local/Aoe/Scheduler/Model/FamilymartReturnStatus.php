<?php

class Aoe_Scheduler_Model_FamilymartReturnStatus {
	public function run() {
		$SLMSG = Mage::helper('logistic/Familymart')->RS4_Action(); //已出貨 RS4
		$SLMSG = $SLMSG."<br/>".Mage::helper('logistic/Familymart')->RS9_Action(); //異常 RS9		
		$SLMSG = $SLMSG."<br/>".Mage::helper('logistic/Familymart')->R08_Action(); //刷退 R08
		//$SLMSG = $SLMSG."<br/>".Mage::helper('logistic/Familymart')->R89_Action(); //退貨處理 已出貨 R89
		$SLMSG = $SLMSG."<br/>".Mage::helper('logistic/Familymart')->R96_Action(); //已取貨 R96
		
		return($SLMSG);
	}

}