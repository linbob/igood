<?php

class Aoe_Scheduler_Model_ReindexPrice {
	public function run() {
		Mage::log("Reindexing", null, 'events.log', true);
		
		$process = Mage::getModel('index/indexer')->getProcessByCode('catalog_product_price');
		$process->reindexAll();
		
		return "Price had been reindexed.";
	}

}