<?php
include_once Mage::getBaseDir('lib').'/ExternalLibs/ChinaTrust/POSAPI.php';

/**
 * Chinatrust payment state flow should be:
 *     state [10/12] => CapRevTransac => state [1] => CapTransac/AuthRev => return success/fail
 */
class ChinaTrust_PaymentChinaTrust_Helper_Refund extends Mage_Core_Helper_Abstract {
	public function doRefund($order) {
		$orderId = $order->getIncrementId();
		$paymentcode = Mage::getModel('paymentchinatrust/paymentMethod')->getCode();
		$orderpayment = $order->getPayment()->getMethodInstance()->getCode();
 		if ($orderpayment === $paymentcode) { //Check if order's payment code is chinatrust credit card
 			Mage::log("Do refund with Chinatrust POS API, order increment id: " . $orderId, null, "refund_chinatrust.log");

 			//- [QUERY] order status from chinatrust
			$masterOrder    = $this->getMasterOrderByOrder($order);
			$masterOrderId  = $this->getMasterOrderIDByOrder($order);
 			$scanorderModel = Mage::getModel('paymentchinatrust/scanOrder');
			$server         = $scanorderModel->GetChinaTrustServer($paymentcode);
			$orderAmount    = round($masterOrder->getOriginalAmount(), 0); //For inquiry, we need the original amount
			$inquiry        = $scanorderModel->GetChinaTrustInquiry($masterOrderId, $orderAmount);
			$inquiryresult  = InquiryTransac($server, $inquiry);

			//- [LOG] inquiry result
			$resultStr = $this->getResponseString($inquiryresult);
			Mage::log("$orderId (master: $masterOrderId) inquiry result : $resultStr", null, "refund_chinatrust.log");

			//- [DO] something according to returned order status
			$resultState = $inquiryresult['CurrentState'];
			switch ($resultState) {
				case "1": { //1: Authorized (授權成功)
					$action = ""; //for logging purpose

					if ($this->shouldAuthRev($order)) { //if order's amount is 0 after refund, then do [AuthRev]
						//: 全額退款
						$action = "AuthRev";

						//- Do [Reversal::AuthRevTransac] so state [1] would become [0]
						$authRev   = $this->getAuthRev($inquiryresult, $order);
						$revresult = AuthRevTransac($server, $authRev);
					} else { //if order's amount after refund > 0, then do [CapTransac]
						//: 部份退款
						$action = "CapTransac";

						//- Do [CapTransact] change the order amount to a new amount (original amount - refunded amount)
						$capTransac = $this->getCapTransac($inquiryresult, $order);
						$revresult  = CapTransac($server, $capTransac);
					}

					//- Log posapi result
					$resultStr = $this->getResponseString($revresult);
					Mage::log("$orderId (master: $masterOrderId) [$action] result : $resultStr", null, "refund_chinatrust.log");

					//- change order status to "退款成功" with memo: "自動退款成功"
					if ($revresult['RespCode']=='00' && $revresult['ErrCode']=='00') {
						$order->setState('order_canceled', 'order_canceled', '自動退款成功');
						$order->save();

						return true; //退款成功, the only place this function will ever return true
					}
					break;
				}
				case "10":   //10: 已請款
				case "12": { //12: 已請款 (請款成功)
					//- Do [CapReverse] so payment state would become [1]
					$capRev = $this->getCapRev($inquiryresult, $order);
					$revresult = CapRevTransac($server, $capRev);

					//- Log result
					$resultStr = $this->getResponseString($revresult);
					Mage::log("$orderId (master: $masterOrderId) CapReverse result : $resultStr", null, "refund_chinatrust.log");

					//- If CapRevTransac is a success, then refund

					if ($revresult['RespCode']=='00' && $revresult['ErrCode']=='00') {
						return $this->doRefund($order);
					}

					break;
				}
				default: {
					//- Do nothing
					break;
				}
			}
 		} else {
			$message = "Requested Chinatrust payment refund, but the master order's payment is not '$paymentcode'";
			Mage::log("order: $orderId : $message", null, "refund_chinatrust.log");
 		}

 		return false;
	}

	/**
	 * Dump response as string
	 *
	 * @param $response - Anything that can be done by var_dump
	 * @return string
	 */
	private function getResponseString($response) {
		ob_start(); var_dump($response); $resultStr = ob_get_clean();
		return $resultStr;
	}

	/**
	 * Array that is needed by Chinatrust POSAPI method : CapTransact
	 *
	 * @param $inquiryresult - TransactInquiry result
	 * @param $order - magento order object
	 * @return array - parameter array for CapTransact
	 */
	public function getCapTransac($inquiryresult, $order) {
		list($grandTotal, $originalAmount, $totalAfterRefund) = $this->getMasterOrderTotalAndRefundedByOrder($order);

		$currency = '901';        //901: NTD
		$orgAmt   = $originalAmount;
		$capAmt   = $totalAfterRefund; //new amount after partial refund
		$exponent = '0';          //0: NTD
		$capTransac = array(
				'MERID'     => Mage::getStoreConfig('payment/paymentchinatrust/pmerid', false),
				'XID'       => $inquiryresult['XID'],
				'AuthRRPID' => $inquiryresult['XID'],
				'currency'  => $currency,
				'orgAmt'    => $orgAmt,
				'capAmt'    => $capAmt,
				'exponent'  => $exponent,
				'AuthCode'  => $inquiryresult['AuthCode'],
				'TermSeq'   => $inquiryresult['TermSeq']
		);
		return $capTransac;
	}

	/**
	 * Array that is needed by Chinatrust POSAPI method : AuthRevTransac
	 *
	 * @param $inquiryresult - TransactInquiry result
	 * @param $order - magento order object
	 * @return array - parameter array for CapTransact
	 */
	public function getAuthRev($inquiryresult, $order) {
		list($grandTotal, $originalAmount, $totalAfterRefund) = $this->getMasterOrderTotalAndRefundedByOrder($order);

		$currency    = '901';        //901: NTD
		$orgAmt      = $originalAmount;
		$authnewAmt  = $totalAfterRefund;
		$exponent    = '0';          //0: NTD
		$authRev = array(
				'MERID'      => Mage::getStoreConfig('payment/paymentchinatrust/pmerid', false),
				'XID'        => $inquiryresult['XID'],
				'AuthRRPID'  => $inquiryresult['XID'],
				'currency'   => $currency,
				'orgAmt'     => $orgAmt,
				'authnewAmt' => $authnewAmt,
				'exponent'   => $exponent,
				'AuthCode'   => $inquiryresult['AuthCode'],
				'TermSeq'    => $inquiryresult['TermSeq']
		);
		return $authRev;
	}

	/**
	 * Array that is needed by Chinatrust POSAPI method : CapRevTransac
	 *
	 * @param $inquiryresult - TransactInquiry result
	 * @param $order - magento order object
	 * @return array - parameter array for CapTransact
	 */
	public function getCapRev($inquiryresult, $order) {
		list($grandTotal, $originalAmount, $totalAfterRefund) = $this->getMasterOrderTotalAndRefundedByOrder($order);

		$currency   = '901';        //901: NTD
		$orgAmt     = $grandTotal;
		$authnewAmt = $totalAfterRefund;
		$exponent   = '0';          //0: NTD
		$capRev = array(
				'MERID'     => Mage::getStoreConfig('payment/paymentchinatrust/pmerid', false),
				'XID'       => $inquiryresult['XID'],
				'AuthRRPID' => $inquiryresult['XID'],
				'currency'  => $currency,
				'orgAmt'    => $orgAmt,
				'exponent'  => $exponent,
				'AuthCode'  => $inquiryresult['AuthCode'],
				'TermSeq'   => $inquiryresult['TermSeq'],
				'BatchID'   => $inquiryresult['BatchID'],
				'BatchSeq'  => $inquiryresult['BatchSeq']
		);
		return $capRev;
	}


	/**
	 * Check if this order should do AuthRevTransac or not
	 * 	+ if total after refund > 0 : no auth rev (cap instead) : 部份退款
	    +    else : auth rev : (全額退款 - 取消)
	 *
	 * @param $order - magento order object
	 * @return boolean
	 *
	 */
	private function shouldAuthRev($order) {
		list($grandTotal, $originalAmount, $totalAfterRefund) = $this->getMasterOrderTotalAndRefundedByOrder($order);
		return !($totalAfterRefund > 0);
	}

	/**
	 * Get master order by an order
	 *
	 * @param $order - magento order object
	 * @return magento master order object
	 */
	public function getMasterOrderByOrder($order) {
		//- Get master order by order entity id (NOTE: not increment id!)
		$orderEntityId = $order->getEntityId();
		$masterOrder = Mage::getModel('splitorder/items')->getMasterOrderByOrderId($orderEntityId);

		return $masterOrder;
	}

	/**
	 * Given an order, get its master order id
	 * @param $order - magento order object
	 * @return magento master order id
	 */
	public function getMasterOrderIDByOrder($order) {
		$masterOrder = $this->getMasterOrderByOrder($order);
		$masterOrderId = $masterOrder->getMasterOrderId();
		if (is_null($masterOrderId)) {
			$masterOrderId = $order->getIncrementId();
		}
		return $masterOrderId;
	}

	/**
	 * Get master order's [grand total], [original amount], and [total after order is refunded]
	 *
	 * @param $order - magento order object
	 * @return list($grandTotal, $originalAmount, $totalAfterRefund)
	 */
	public function getMasterOrderTotalAndRefundedByOrder($order) {
		$masterOrder = $this->getMasterOrderByOrder($order);

		$orderAmount = $order->getGrandTotal();
		$grandTotal  = $masterOrder->getGrandTotal();
		$totalAfterRefund = $grandTotal - $orderAmount;
		$originalAmount   = $masterOrder->getOriginalAmount();

		return array($grandTotal, $originalAmount, $totalAfterRefund);
	}
}