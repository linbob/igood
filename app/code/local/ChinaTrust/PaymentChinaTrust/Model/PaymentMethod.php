<?php

class ChinaTrust_PaymentChinaTrust_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract {
    protected $_code = 'paymentchinatrust';
    protected $_formBlockType = 'paymentchinatrust/dispfields';

	public function assignData($data) {
        $session = Mage::getSingleton('customer/session');
        if (!is_null($session)) {
            $session->setData('order_state', 'cc_processing');
        }

		if (!($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
		$info = $this->getInfoInstance();
		$info->setEtixType($data->getEtixType());

		return $this;
  }
	
	public function getCheckoutRedirectUrl() {
		$redirectUrl = Mage::getUrl("paymentchinatrust/sendcredit");
		
		return $redirectUrl;
	}
}
