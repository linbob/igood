<?php

class ChinaTrust_PaymentChinaTrust_Model_ScanOrder {
	public function run() {
		return($this->TransChinaTrustFailed());
	}

	public function TransChinaTrustFailed()
	{
		$success_orderids = array();
		$failed_orderids  = array();

		$timeoffset = strtotime(Mage::getStoreConfig('payment/paymentchinatrust/expired', false));
		$str = date("Y-m-d H:i:s", $timeoffset);

		$OrderIds = Mage::getResourceModel('sales/order_collection')
		->addFieldToSelect('entity_id')
		->addFieldToSelect('increment_id')
		->addFieldToFilter('state', array(
				array('eq'=>'cc_processing'),
				array('eq'=>'pending_payment')))
				->addFieldToFilter('created_at', array('lt' => $str));

		$OrderIds->getSelect()
		->where("method='paymentchinatrust'")
		->joinLeft('sales_flat_order_payment', 'main_table.entity_id = sales_flat_order_payment.parent_id',array('method'))
		->joinLeft('medium_order_item', 'main_table.entity_id = medium_order_item.order_id', array('master_order_id'));

		
		$OrderIds->getData();

		foreach($OrderIds as $order_Id){
			$lidm  = $order_Id['master_order_id'];
			$orderid = $order_Id['entity_id'];
			$order = Mage::getModel('sales/order')->load($orderid);
			$paymentCode = $order->getPayment()->getMethodInstance()->getCode();
			$orderData   = $order->getData();
			$orderAmount = round($orderData['grand_total'], 0);

			$Server = $this->GetChinaTrustServer($paymentCode);
			if ($Server) {
				$Inquiry = null;

				if ($paymentCode == "paymentchinatrust")
				{
					$Inquiry = $this->GetChinaTrustInquiry($lidm, $orderAmount);
				}
				else if  ($paymentCode == "paymentchinatrustinstallment")
				{
					$customId = Mage::getModel('check/custom_order')->getIdByOrder($order->getId());
					$customModel = Mage::getModel('check/custom_order')->load($customId);
					$customArr = $customModel->getData();
					$recurNum = $customArr["installment"];
					$Inquiry = $this->GetChinaTrustInstallmentInquiry($lidm, $orderAmount, $recurNum);
				}

				if ($Inquiry)
				{
					$resultState = null;
					$chinaTrustResult = $this->GetChinaTrustResult($Server, $Inquiry, $resultState);

					if ($chinaTrustResult === true) //success
					{
						$this->TransSuccessHandler($orderid);
						array_push($success_orderids, $lidm."(".$resultState.")");
					}
					else if ($chinaTrustResult === false) //failed
					{
						$this->TransFailedHandler($orderid);
						array_push($failed_orderids, "[Failed]".$lidm."(".$resultState.")");
					}
					else
					{
						array_push($failed_orderids, "[Waiting]".$lidm."(".$resultState.")");
					}
				}
				else
				{
					$this->TransFailedHandler($orderid);
					array_push($failed_orderids, "[Inquiry Null]".$lidm);
				}
			}
			else
			{
				$this->TransFailedHandler($orderid);
				array_push($failed_orderids, "[Server Null]".$lidm);
			}
		}
		return("[ChinaTrust]Success id(s):".implode(",", $success_orderids).". Failed id(s):".implode(",", $failed_orderids).";");
	}

	public function GetChinaTrustServer($inPaymentCode)
	{
		$cafile = Mage::getBaseDir('base')."/cert/server.cer";
		$posapihost = Mage::getStoreConfig('payment/'.$inPaymentCode.'/posapihost', false);
		$posapiport = Mage::getStoreConfig('payment/'.$inPaymentCode.'/posapiport', false);
		$Server = null;

		if (!empty($posapihost) && !empty($posapiport))
		{
			/* ChinaTrust Server */
			$Server = array(
					'HOST'    => $posapihost,
					'PORT'    => $posapiport,
					'CAFile'  => $cafile,
					'Timeout' => 30
			);
		}
		return $Server;
	}

	public function GetChinaTrustInquiry($inLidm, $inOrderAmount)
	{
		$pmerid = Mage::getStoreConfig('payment/paymentchinatrust/pmerid', false);

		/* ChinaTrust Inquiry */
		$Inquiry = array(
				'TX_ATTRIBUTE'=> 'TX_AUTH',
				'MERID'       => $pmerid,
				'LID-M'       => $inLidm,
				'XID'         => '',
				'PAN'         => '',
				'currency'    => '901',
				'purchAmt'    => $inOrderAmount,
				'RECUR_NUM'   => '0',
				'PRODCODE'    => ''
		);

		return $Inquiry;
	}

	public function GetChinaTrustInstallmentInquiry($inLidm, $inOrderAmount, $inRecurNum)
	{
		$pmerid = Mage::getStoreConfig('payment/paymentchinatrustinstallment/pmerid', false);

		/* ChinaTrust Inquiry */
		$Inquiry = array(
				'TX_ATTRIBUTE'=> 'TX_INSTMT_AUTH',
				'MERID'       => $pmerid,
				'LID-M'       => $inLidm,
				'XID'         => '',
				'PAN'         => '',
				'currency'    => '901',
				'purchAmt'    => $inOrderAmount,
				'RECUR_NUM'   => $inRecurNum,
				'PRODCODE'    => ''
		);

		return $Inquiry;
	}

	public function GetChinaTrustResult($Server, $Inquiry, &$resultState)
	{
		$chinatrustapi = Mage::getBaseDir('lib').'/ExternalLibs/ChinaTrust/POSAPI.php';
		include_once $chinatrustapi;

		$result = InquiryTransac($Server, $Inquiry);

		$resultState = $result['CurrentState'];
		if ($resultState=='11') {
			// =='11' is still being processed
			//DO NOTHING
			return "waiting";
		} else if ($resultState=='1'  ||
				$resultState=='10' ||
				$resultState=='12' ) {
			// =='1'  authorized
			// =='10' Success
			// =='12' Success
			return true;
		} else {
			// =='' Record not found
			// Other: Failed status
			$resultState .= "(QueryCode)".$result['QueryCode'];
			$resultState .= "(RespCode)".$result['RespCode'];
			$resultState .= "(ErrCode)".$result['ErrCode'];

			return false;
		}
	}

	public function TransSuccessHandler($order_Id)
	{	
		$order = Mage::getModel('sales/order')->load($order_Id);	
		$order->sendNewOrderEmail();
		$order->setState('established', true, '')->save();

		//Send mail to supplier
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		Mage::helper('common/common')->emailSupplierByOrder($order, 'New Order to Supplier', $emailTemplateVariables);
	}

	public function TransFailedHandler($inOderId)
	{
		$order = Mage::getModel('sales/order')->load($inOderId);
		$paymentCode = $order->getPayment()->getMethodInstance()->getCode();
		$order->setState('failed', true, '')->save();

		$orderHelper = Mage::helper('common/order');
		$orderHelper->addItemBackToProductInventory($order);

		//Send failed mail
		$email_address = $order->getCustomerEmail();
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$emailTemplateVariables['order_id'] = $order->getIncrementId();
		$items1 = $order->getAllItems();
		foreach ($items1 as $itemId1 => $item1) {
			$emailTemplateVariables['productname'] = $item1->getName();
			$emailTemplateVariables['qty']=$item1->getQtyOrdered()*1;
		}

		Mage::helper('common/common')->sendEmail('CreditCard_Faild', $email_address, $emailTemplateVariables);
	}
}