<?php 
class ChinaTrust_PaymentChinaTrust_SendcreditController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {    	
    	$ExternalLibPath=Mage::getModuleDir('', 'Settv_Checkout') . DS . 'lib' . DS .'auth_mpi_mac.php';
    	require_once ($ExternalLibPath);
    	
    	$master_order_id = Mage::getSingleton('checkout/session')->getLastMasterOrderId();
                
    	//Remove last real order id, flagging this page, in order to prevent multiple form request
    	Mage::getSingleton('checkout/session')->setLastMasterOrderId();
    	
        if (empty($master_order_id)) { //Send user back to home page
            $url = Mage::getBaseUrl();
            Mage::app()->getFrontController()->getResponse()->setRedirect($url);
            return;
        }

        $master_order = Mage::getModel('splitorder/orders')->loadById($master_order_id);
        $ordertotal = round($master_order->getGrandTotal(), 0);

	/* ichannels */
	if ($_COOKIE['ichannelsGid']) {
	    $gid = Mage::getModel('core/cookie')->get('ichannelsGid');
	    $helper = Mage::helper('push');
	    $helper->index($gid, $master_order_id, $ordertotal);
	}
    	
		$lidm=$master_order_id;
		$purchAmt=$ordertotal;
		$txType="0";
		$NumberOfPay="1";

		$MerchantName="igood";
		$AuthResURL= Mage::getUrl("paymentchinatrust/ccresult");
		$Pid = "";
		$OrderDetail="";
		$AutoCap="1";
		$Customize="0";
		$debug="0";
		
        //parameter
        $PostUrl=Mage::getStoreConfig('payment/paymentchinatrust/posturl', false);
        $merID=Mage::getStoreConfig('payment/paymentchinatrust/pmerid', false);
        $MerchantID=Mage::getStoreConfig('payment/paymentchinatrust/pmerchantid', false);
        $TerminalID=Mage::getStoreConfig('payment/paymentchinatrust/pterminalid', false);
        $Key=Mage::getStoreConfig('payment/paymentchinatrust/pkey', false);

        $Option="1";

		$MACString=auth_in_mac($MerchantID,$TerminalID,$lidm,$purchAmt,$txType,$Option,$Key,$MerchantName,$AuthResURL,$OrderDetail,$AutoCap,$Customize,$debug);		
		$URLEnc=get_auth_urlenc($MerchantID,$TerminalID,$lidm,$purchAmt,$txType,$Option,$Key,$MerchantName,$AuthResURL,$OrderDetail,$AutoCap,$Customize,$MACString,$debug);

    	$Html = "
    	<div style='display:none'>
	    	<form id='form1' name='form1' method='post' action='".$PostUrl."'>
				<input type='hidden' name='MerchantID' size='20' value='".$MerchantID."'/>
		    	<input type='hidden' name='TerminalID' size='20' value='".$TerminalID."'/>
		    	<input type='hidden' name='MerchantName' size='20' value='".$MerchantName."'/>
		    	<input type='hidden' name='lidm' size='20' value='".$lidm."'/>
		    	<input type='hidden' name='merID' size='20' value='".$merID."'/>
		    	<input type='hidden' name='InMac' size='20' value='".$MACString."'/>
		    	<input type='hidden' name='URLEnc' size='20' value='".$URLEnc."'/>
		    	<input type='hidden' name='customize' size='20' value='".$Customize."'/>
		    	<input type='hidden' name='purchAmt' size='20' value='".$purchAmt."'/>
		    	<input type='hidden' name='txType' size='20' value='".$txType."'/>
		    	<input type='hidden' name='NumberOfPay' size='20' value='".$NumberOfPay."'/>
		    	<input type='hidden' name='AuthResURL' size='20' value='".$AuthResURL."'/>
		    	<input type='hidden' name='OrderDesc' size='20' value=''/>
		    	<input type='submit' id='sent_btn' value='' style='display:none'/>
    		</form>
    	</div>
    	<script>
    		document.getElementById('sent_btn').click();
    	</script>
    	";
    	echo $Html;
    	
    	$view='<div style="width:100%; text-align:center; padding-top:20%">
    			<dt><img src="'.Mage::getDesign()->getSkinUrl('imgs/logo.png').'" style="background-color:red"/></dt>
    			<dt><img src="'.Mage::getDesign()->getSkinUrl('imgs/ajax-loader.gif').'" /></dt>
    			</div>';
    	echo $view;
    	
    }
}
?>
