<?php
class ChinaTrust_PaymentChinaTrustInstallment_Block_Dispfields extends Mage_Payment_Block_Form {
	protected function _construct() {
		parent::_construct();
		$this->setTemplate('settv/payment_chinatrust_installment_form.phtml');
	}

	public function getPeriods() {
		return Mage::getModel("paymentchinatrustinstallment/paymentMethod")->getAvailablePeriods();
	}

	public function getOrderTotal() {
		return Mage::getModel("paymentchinatrustinstallment/paymentMethod")->getOrderTotal();
	}
}
