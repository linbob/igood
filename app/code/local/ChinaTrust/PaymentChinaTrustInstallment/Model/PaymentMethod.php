<?php

class ChinaTrust_PaymentChinaTrustInstallment_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract {
	protected $_code = 'paymentchinatrustinstallment';
	protected $_formBlockType = 'paymentchinatrustinstallment/dispfields';

	public function assignData($data) {
		$session = Mage::getSingleton('customer/session');
		if (!is_null($session)) {
			$session->setData('order_state', 'cc_processing');
		}

		if (!($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
		$info = $this->getInfoInstance();
		$info->setEtixType($data->getEtixType());

		return $this;
	}

	public function getAvailablePeriods() {
		return array(
				"3" => "2.5"
				,"6" => "3"
				,"12" => "6.5"
				,"24" => "10");
	}

	public function getOrderTotal() {
		$quote = Mage::getModel('checkout/session')->getQuote();
		$cartGrossTotal = 0;
		foreach ($quote->getAllItems() as $item) {
			$cartGrossTotal += $item->getPriceInclTax() * $item->getQty();
		}
		$ordertotal = round($cartGrossTotal, 0);
		return $ordertotal;
	}

	public function getCheckoutRedirectUrl() {
		$redirectUrl = Mage::getUrl("paymentchinatrustinstallment/sendcredit");

		return $redirectUrl;
	}
}
