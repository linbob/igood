<?php
class ChinaTrust_PaymentChinaTrustInstallment_CcresultController extends Mage_Core_Controller_Front_Action{
	public function indexAction(){
		$ExternalLibPath = Mage::getModuleDir('', 'Settv_Checkout') . DS . 'lib' . DS .'auth_mpi_mac.php';
		require_once ($ExternalLibPath);
		$URLResEnc = $_POST['URLResEnc'];
		$EncRes    = $URLResEnc;
		$Key       = Mage::getStoreConfig('payment/paymentchinatrustinstallment/pkey', false);
		$debug     = "0";

		$EncArray  = gendecrypt($EncRes, $Key, $debug);
		$MACString = '';
		$URLEnc    = '';

		$status        = isset($EncArray['status']) ? $EncArray['status'] : "";
		$errCode       = isset($EncArray['errcode']) ? $EncArray['errcode'] : "";
		$authCode      = isset($EncArray['authcode']) ? $EncArray['authcode'] : "";
		$authAmt       = isset($EncArray['authamt']) ? $EncArray['authamt'] : "";
		$lidm          = isset($EncArray['lidm']) ? $EncArray['lidm'] : "";
		$OffsetAmt     = isset($EncArray['offsetamt']) ? $EncArray['offsetamt'] : "";
		$OriginalAmt   = isset($EncArray['originalamt']) ? $EncArray['originalamt'] : "";
		$UtilizedPoint = isset($EncArray['utilizedpoint']) ? $EncArray['utilizedpoint'] : "";
		$Option        = isset($EncArray['numberofpay']) ? $EncArray['numberofpay'] : "";
		$Last4digitPAN = isset($EncArray['last4digitpan']) ? $EncArray['last4digitpan'] : "";
		$MACString     = auth_out_mac($status,$errCode,$authCode,$authAmt,$lidm,$OffsetAmt,$OriginalAmt,$UtilizedPoint,$Option,$Last4digitPAN,$Key,$debug);

		$master_order_id=$lidm;
		$orders = Mage::getModel("splitorder/items")->getOrdersByMasterOrderId($master_order_id);

		if($status=='0'){
			foreach ($orders as $order)
			{
				Mage::getModel("paymentchinatrustinstallment/scanOrder")->TransSuccessHandler($order->getId());				
			}
			$this->getResponse()->setRedirect(Mage::getUrl('checkout/onepage/success'));
		} else {
			Mage::log("CcresultController@status:$status,errCode:$errCode", null, 'ChinaTrust_PaymentChinaTrustInstallment.log');
			foreach ($orders as $order)
			{
				Mage::getModel("paymentchinatrustinstallment/scanOrder")->TransFailedHandler($order->getId());	
			}
			$this->getResponse()->setRedirect(Mage::getUrl('checkout/onepage/ccfailure')."order_id/".$master_order_id);
		}
	}
}
?>
