<?php

class ChinaTrust_PaymentChinaTrustWebATM_Model_ScanOrder {
	public function run() {
		return($this->TransChinaTrustWebATMFailed());
	}

	public function TransChinaTrustWebATMFailed()
	{
		$success_orderids = array();
		$failed_orderids  = array();
		$invalid_orderids = array();

		$timeoffset = strtotime('-35 minutes');
		$str = date("Y-m-d H:i:s", $timeoffset);

		$OrderIds = Mage::getResourceModel('sales/order_collection')
		->addFieldToSelect('entity_id')
		->addFieldToSelect('increment_id')
		->addFieldToFilter('state', array(
				array('eq'=>'cc_processing'),
				array('eq'=>'pending_payment')))
				->addFieldToFilter('created_at', array('lt' => $str));

		$OrderIds->getSelect()
		->where("method='paymentchinatrustwebatm'")
		->joinLeft('sales_flat_order_payment', 'main_table.entity_id = sales_flat_order_payment.parent_id',array('method'))
		->joinLeft('medium_order_item', 'main_table.entity_id = medium_order_item.order_id', array('master_order_id'));

		$OrderIds->getData();

		foreach($OrderIds as $order_Id){
			$lidm  = $order_Id['master_order_id'];
			$orderid = $order_Id['entity_id'];

			$Inquiry = $this->GetChinaTrustWebATMInquiry($lidm);
			$chinaTrustResult = $this->GetChinaTrustWebATMResult($Inquiry);

			if ($chinaTrustResult === true) //success
			{
				$this->TransSuccessHandler($orderid);
				array_push($success_orderids, $lidm);
			}
			else if ($chinaTrustResult === false) //failed
			{
				$this->TransFailedHandler($orderid);
				array_push($failed_orderids, "[Failed]".$lidm);
			} else { //something is wrong!
				array_push($invalid_orderids, "[Invalid]".$lidm);
				Mage::log("Chinatrust WebATM validation error, lidm: " . $lidm);
			}
		}
		return("[ChinaTrust WebvATM]Success id(s):" . implode(",", $success_orderids)
				. ". Failed id(s):" . implode(",", $failed_orderids)
				. ". need to validate id(s):" . implode(",", $invalid_orderids)
				. ";"

		);
	}

	public function GetChinaTrustWebATMInquiry($inLidm)
	{
		$posapihost  = Mage::getStoreConfig('payment/paymentchinatrustwebatm/posapihost', false);
		$pmerid      = Mage::getStoreConfig('payment/paymentchinatrustwebatm/pmerid', false);
		$pmerchantid = Mage::getStoreConfig('payment/paymentchinatrustwebatm/pmerchantid', false);
		$TerminalID  = Mage::getStoreConfig('payment/paymentchinatrustwebatm/pterminalid', false);
		$EncKey      = Mage::getStoreConfig('payment/paymentchinatrustwebatm/pkey', false);

		/* ChinaTrust WebATM Inquiry */
		$Inquiry = array(
				'Server'     => $posapihost,
				'MerID'      => $pmerid,
				'MerchantID' => $pmerchantid,
				'TerminalID' => $TerminalID,
				'EncKey'     => $EncKey,
				'Lidm'       => $inLidm
		);

		return $Inquiry;
	}

	public function GetChinaTrustWebATMResult($Inquiry)
	{
		$chinatrustWebAtmApi = Mage::getBaseDir('lib').'/ExternalLibs/ChinaTrust/WebAtmApi_fix54.php';
		include_once $chinatrustWebAtmApi;

		$result = ATMInquiryTransac($Inquiry);

		if ($result['status'] === '0' && $result['errcode'] === '0000') {
			return true;
		} else if ($result['status'] === '-1') { //Validation error, don't do anything to the order
			return "validation error";
		} else {
			return false;
		}
	}

	public function TransSuccessHandler($order_Id)
	{
		$order = Mage::getModel('sales/order')->load($order_Id);
		$order->sendNewOrderEmail();
		$order->setState('established', true, '')->save();

		//Send mail to supplier
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		Mage::helper('common/common')->emailSupplierByOrder($order, 'New Order to Supplier', $emailTemplateVariables);
	}

	public function TransFailedHandler($orderId)
	{
		$order = Mage::getModel('sales/order')->load($orderId);
		$paymentCode = $order->getPayment()->getMethodInstance()->getCode();

		$order->setState('failed', true, '')->save();

		$orderHelper = Mage::getModel('common/order');
		$orderHelper->addItemBackToProductInventory($order);

		//Send failed mail
		$email_address = $order->getCustomerEmail();
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$emailTemplateVariables['order_id'] = $order->getIncrementId();
		$items1 = $order->getAllItems();
		foreach ($items1 as $itemId1 => $item1) {
			$emailTemplateVariables['productname'] = $item1->getName();
			$emailTemplateVariables['qty']=$item1->getQtyOrdered()*1;
		}

		Mage::helper('common/common')->sendEmail('Chinatrust WebATM failed', $email_address, $emailTemplateVariables);
	}
}