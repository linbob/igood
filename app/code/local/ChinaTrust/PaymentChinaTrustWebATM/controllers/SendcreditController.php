<?php
class ChinaTrust_PaymentChinaTrustWebATM_SendcreditController extends Mage_Core_Controller_Front_Action {
	public function indexAction() {
		$ExternalLibPath=Mage::getModuleDir('', 'Settv_Checkout') . DS . 'lib' . DS .'auth_mpi_mac.php';
		require_once ($ExternalLibPath);

		$master_order_id = Mage::getSingleton('checkout/session')->getLastMasterOrderId();
                
    	//Remove last real order id, flagging this page, in order to prevent multiple form request
    	Mage::getSingleton('checkout/session')->setLastMasterOrderId();
		
		if (empty($master_order_id)) { //Send user back to home page
			$url = Mage::getBaseUrl();
			Mage::app()->getFrontController()->getResponse()->setRedirect($url);
			return;
		}

	 	$master_order = Mage::getModel('splitorder/orders')->loadById($master_order_id);
        $ordertotal = round($master_order->getGrandTotal(), 0);

		$MerchantID   = Mage::getStoreConfig('payment/paymentchinatrustwebatm/pmerchantid', false);
		$TerminalID   = Mage::getStoreConfig('payment/paymentchinatrustwebatm/pterminalid', false);
		$lidm         = $master_order_id;
		$purchAmt     = $ordertotal;
		$txType       = "9";
		$Option       = "";
		$Key          = Mage::getStoreConfig('payment/paymentchinatrustwebatm/pkey', false);
		$MerchantName = "igood";
		$OrderDetail  = "";
		$AutoCap      = "";
		$Customize    = "";
		$debug        = "0";

		$PostUrl      = Mage::getStoreConfig('payment/paymentchinatrustwebatm/posturl', false);
		$AuthResURL   = Mage::getUrl("paymentchinatrustwebatm/ccresult");

		$MACString     = auth_in_mac($MerchantID,$TerminalID,$lidm,$purchAmt,$txType,$Option,$Key,
				$MerchantName,$AuthResURL,$OrderDetail,$AutoCap,$Customize,$debug);
		$storeName     = "igood";
		$billShortDesc = "";
		$WebATMAcct    = Mage::getStoreConfig('payment/paymentchinatrustwebatm/pwebatmaccount', false);
		$note          = "note";

		$ATMEnc        = get_auth_atmurlenc($MerchantID,$TerminalID,$lidm,$purchAmt,$txType,$Option,$Key,
				$storeName,$AuthResURL,$billShortDesc,$WebATMAcct,$note,$MACString,$debug);

		$merID = Mage::getStoreConfig('payment/paymentchinatrustwebatm/pmerid', false);

		$Html = "
				<div style='display:none'>
				<form id='form1' name='form1' method='post' action='".$PostUrl."'>
				<input type='hidden' name='merID' size='20' value='".$merID."'/>
				<input type='hidden' name='URLEnc' size='20' value='".$ATMEnc."'/>
				<input type='submit' id='sent_btn' value='' style='display:none'/>
				</form>
				</div>
				<script>
				document.getElementById('sent_btn').click();
				</script>
				";
		echo $Html;
		 
		$view='<div style="width:100%; text-align:center; padding-top:20%">
			   <dt><img src="'.Mage::getDesign()->getSkinUrl('imgs/logo.png').'" style="background-color:red"/></dt>
			   <dt><img src="'.Mage::getDesign()->getSkinUrl('imgs/ajax-loader.gif').'" /></dt>
			   </div>';
		
		echo $view;
		 
	}
}
?>
