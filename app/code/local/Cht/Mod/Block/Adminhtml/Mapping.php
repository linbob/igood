<?php
class Cht_Mod_Block_Adminhtml_Mapping extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_mapping';
        $this->_blockGroup = 'cht_mod';
        $this->_headerText = Mage::helper('cht')->__('Mod Mapping');
        parent::__construct();
    }
}
