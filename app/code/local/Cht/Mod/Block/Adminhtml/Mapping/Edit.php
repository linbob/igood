<?php
class Cht_Mod_Block_Adminhtml_Mapping_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		$this->_objectId = 'id';
		$this->_blockGroup = 'cht_mod';
		$this->_controller = 'adminhtml_mapping';		
		parent::__construct();
	}

	public function getHeaderText()
	{
		return Mage::helper('cht')->__('Mod Basic Information');
	}
}