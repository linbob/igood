<?php
class Cht_Mod_Block_Adminhtml_Mapping_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$model = Mage::registry('adminhtml_mapping');

		$form = new Varien_Data_Form();

		$fieldset = $form->addFieldset('mapping_form', array('legend'=>Mage::helper('cht')->__('Mod Item information')));

		$fieldset->addField('product_id', 'text', array(
				'label'     => Mage::helper('customer')->__('Product ID'),
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'product_id',
		));

		$fieldset->addField('channel', 'text', array(
				'label'     => Mage::helper('cht')->__('Channel'),
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'channel',
		));
				
		$fieldset->addField('start_time', 'text', array(
				'label'     => Mage::helper('cht')->__('Start Time')."(yyyy-MM-dd HH:mm:ss)",
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'start_time',
		));
		
		$fieldset->addField('end_time', 'text', array(
				'label'     => Mage::helper('cht')->__('End Time')."(yyyy-MM-dd HH:mm:ss)",
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'end_time',
		));
				
		$this->setForm($form);

		return parent::_prepareForm();
	}
}