<?php
class Cht_Mod_Block_Adminhtml_Mapping_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('mapping_tabs');
		$this->setDestElementId('edit_form'); // this should be same as the form id define above
		$this->setTitle(Mage::helper('cht')->__('Mod Information'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('supplier_section', array(
				'label'     => Mage::helper('cht')->__('Mod Basic Information'),
				'title'     => Mage::helper('cht')->__('Mod Basic Information'),
				'content'   => $this->getLayout()->createBlock('cht_mod/adminhtml_mapping_edit_tab_form')->toHtml(),
		));

		return parent::_beforeToHtml();
	}
}