<?php
class Cht_Mod_Block_Adminhtml_Mapping_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('cht_mod_mapping_grid');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('mod/mappings')->getCollection();
		$collection->addProductData();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('mapping_id', array(
				'header'    => Mage::helper('catalog')->__('ID'),
				'index'     => 'mapping_id',
				'type'      => 'text',
				'filter_index' => 'mapping_id',
		));
		
		$this->addColumn('name', array(
				'header'    => Mage::helper('adminhtml')->__('Product Name'),
				'index'     => 'name',
				'type'      => 'text',
				'filter_index' => 'name',
		));
		
		$this->addColumn('channel', array(
				'header'    => Mage::helper('cht')->__('Channel'),
				'index'     => 'channel',
				'type'      => 'text',
				'filter_index' => 'channel',
		));
		
		$this->addColumn('start_time', array(
				'header'    => Mage::helper('cht')->__('Start Time'),
				'index'     => 'start_time',
				'type'      => 'datetime',
				'filter_index' => 'start_time',
		));
		
		$this->addColumn('end_time', array(
				'header'    => Mage::helper('cht')->__('End Time'),
				'index'     => 'end_time',
				'type'      => 'datetime',
				'filter_index' => 'end_time',
		));
		
		$this->addColumn('action', array(
				'header'    =>  Mage::helper('cht')->__('Delete'),
				'width'     => '100',
				'type'      => 'action',
				'getter'    => 'getId',
				'actions'   => array(
						array(
								'caption'   => Mage::helper('cht')->__('Delete'),
								'url'       => array('base'=> '*/*/delete'),
								'field'     => 'id',
								'target'	=>'_self'
						)
				),
				'filter'    => false,
				'sortable'  => false,
				'is_system' => true,
		));
		

		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return false;
	}

	public function getRefreshButtonHtml()
	{
		return $this->getChildHtml('refresh_button');
	}	
}
