<?php
class Cht_Mod_Helper_Customer extends Mage_Core_Helper_Abstract {

	function getCustomerIdByNameAndPhone($mod_username, $mod_cellphone)
	{
		$customerId = null;
		$collection =  Mage::getModel('customer/address')
		->getCollection()
		->addAttributeToSelect("customerId")
		->addFieldToFilter("firstname", $mod_username)
		->addFieldToFilter("cellphone", $mod_cellphone);

		$modUserEmail = Mage::getStoreConfig('cht/mod/useremail', false);

		foreach ($collection as $address)
		{
			$customer = Mage::getModel('customer/customer')->load($address->getCustomerId());
			if ($customer->getEmail() != $modUserEmail)
			{
				//is default billing address
				//if ($customer->getDefaultBillingAddress()->getId() == $address->getId())
				//{
					$customerId = $customer->getId();
					break;
				//}
			}
				
		}
		return $customerId;
	}

	public function AddCustomerAddressByOrder($orderId, $customerId)
	{		
		$order = Mage::getModel('sales/order')->load($orderId);
		//order shipping
		$shippingAddress = $order->getShippingAddress();
		$this->CheckAndAddCustomerAddress($customerId, $shippingAddress);
		
		//order billing
		$billingAddress = $order->getBillingAddress();
		$this->CheckAndAddCustomerAddress($customerId, $billingAddress);
	}
	
	private function CheckAndAddCustomerAddress($customerId, $order_address)
	{
		$customer = Mage::getModel('customer/customer')->load($customerId);
		$firstname = $order_address->getFirstname();
		$cellphone = $order_address->getCellphone();
		$streetFull = $order_address->getStreetFull();
		
		$duplicate = $this->IsCustomerAddressDuplicate($customer
				, $firstname, $cellphone);
		
		if (!$duplicate)
		{
			$customer_address = new ChtCustomerAddressInfo();
			$customer_address->setFirstname($firstname);
			$customer_address->setTelephone($cellphone);
			$customer_address->setCellphone($cellphone);
			$customer_address->setStreet($streetFull);
			$this->AddCustomerAddress($customerId, $customer_address);
		}
	}
	
	private function IsCustomerAddressDuplicate($customer, $firstName, $cellPhone)
	{
		$duplicate = false;
		foreach ($customer->getAddresses() as $address)
		{
			$customer_firstname = $address->getFirstname();
			$customer_cellpohone = $address->getCellphone();
			if ($customer_firstname == $firstName 
				&& $customer_cellpohone == $cellPhone)
				$duplicate = true;
		}
		return $duplicate;
	}
	
	private function AddCustomerAddress($customerId, $customer_address)
	{	
		$address = Mage::getModel('customer/address');
		$address->setFirstname($customer_address->getfirstName());
		$address->setTelephone($customer_address->getTelePhone());
		$address->setCellphone($customer_address->getCellPhone());
		$address->setStreetFull($customer_address->getStreet());
		$address->setCountry($customer_address->getCountry());
		$address->setCustomerId($customerId);
		$address->save();
	}

}

class ChtCustomerAddressInfo {
	private $firstname = "";
	private $lastname = "";
	private $street = "";
	private $city = "";
	private $country = "TW";
	private $postcode = "";
	private $telephone = "";
	private $cellphone = "";
	
	public function getFirstname(){
		return $this->firstname;
	}
	
	public function setFirstname($firstname){
		$this->firstname = $firstname;
	}
	
	public function getLastname(){
		return $this->lastname;
	}
	
	public function setLastname($lastname){
		$this->lastname = $lastname;
	}
	
	public function getStreet(){
		return $this->street;
	}
	
	public function setStreet($street){
		$this->street = $street;
	}
	
	public function getCity(){
		return $this->city;
	}
	
	public function setCity($city){
		$this->city = $city;
	}
	
	public function getCountry(){
		return $this->country;
	}
	
	public function setCountry($country){
		$this->country = $country;
	}
	
	public function getPostcode(){
		return $this->postcode;
	}
	
	public function setPostcode($postcode){
		$this->postcode = $postcode;
	}
	
	public function getTelePhone(){
		return $this->telephone;
	}
	
	public function setTelePhone($phone){
		$this->telephone = $phone;
	}
	
	public function getCellPhone(){
		return $this->cellphone;
	}
	
	public function setCellPhone($phone){
		$this->cellphone = $phone;
	}
}
