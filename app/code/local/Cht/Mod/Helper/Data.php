<?php
class Cht_Mod_Helper_Data extends Mage_Core_Helper_Abstract {

	function validateDate($date, $format = 'Y-m-d H:i:s')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

}