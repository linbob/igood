<?php

class Cht_Mod_Helper_ModOrderParser extends Mage_Core_Helper_Abstract
{

    public function GetOrderByModParser ()
    {
        $default_MOD_user_email = Mage::getStoreConfig('cht/mod/useremail', 
                false);
        try {
            $this->DownloadCsvDataFromMod();
            $scannedDir = $this->getCurrentProcessFiles();
            if (count($scannedDir) == 0)
                return "No order record";
            foreach ($scannedDir as $fileName) {
                
                $csvData = $this->ParseCsvData(
                        $this->processFilePath($fileName));
                $date = $csvData->getDate();
                $time = $csvData->getTime();
                $channel = $csvData->getChannel();
                $phoneNum = $csvData->getPhoneNum();
                $this->addVarLog("fileName", $fileName);
                $this->addVarLog("date", $date);
                $this->addVarLog("time", $time);
                $this->addVarLog("channel", $channel);
                $this->addVarLog("phoneNum", $phoneNum);
                
                $productId = $this->GetProductId($date, $time, $channel);
                $this->addVarLog("productId", $productId);
                
                if (strlen($productId) == 0) {
                    $this->moveFileToErrorDir($fileName);
                    $this->sendModFailNotification($date,$time,$channel,$phoneNum);
                    $this->addFailLogByMethod(
                            "MappingProduct   Date:" . $date . "  Time:" . $time .
                                     "  Channel:" . $channel);
                    continue;
                }
                
                $isInStock = $this->getProductIsInStockByProductId($productId);
                $this->addVarLog("isInStock", $isInStock);
                
                if (! $isInStock) {
                    $key = date("Y-m-d H:i:s", time());
                    $output_SmsService_SendNoInventorySmsMsg;
                    if (! Mage::helper('paymentqrcode/SmsService')->SendNoInventorySmsMsg(
                            $phoneNum, $key, 
                            $output_SmsService_SendNoInventorySmsMsg))
                        $this->addFailLogByMethod("SendNoInventorySmsMsg");
                    $this->addVarLog("output_SmsService_SendNoInventorySmsMsg", 
                            $output_SmsService_SendNoInventorySmsMsg);
                    $this->moveFileToErrorDir($fileName);
                    $this->sendModFailNotification($date,$time,$channel,$phoneNum);
                    continue;
                }
                
                $customerId = $this->getCustomerIdByEmail(
                        $default_MOD_user_email);
                $this->addVarLog("customerId", $customerId);
                
                $ordercreated = $this->createModOrderRecord($customerId, 
                        $productId, $phoneNum);
                
                $orderIncrementId = $ordercreated->getIncrementId();
                $masterOrderId = Mage::getModel("splitorder/items")->getMasterOrderIdByOrderId($ordercreated->getId());
                
                $this->addVarLog("orderIncrementId", $orderIncrementId);
                $this->addVarLog("masterOrderId", $masterOrderId);
                
                $orderAmt = $ordercreated->getGrandTotal() * 100;
                $this->addVarLog("orderAmt", $orderAmt);
                
                if (strlen($orderIncrementId) == 0) {
                    $this->addFailLogByMethod("CreateOrder");
                    $this->moveFileToErrorDir($fileName);
                    $this->sendModFailNotification($date,$time,$channel,$phoneNum);
                    continue;
                }
                
                $qrCode;
                $output_PosService_GetQRCode;
                $tac;
                
                if (! Mage::helper('paymentqrcode/PosService')->GetQRCode(
                        $masterOrderId, $productId, $orderAmt, $qrCode, $tac, 
                        $output_PosService_GetQRCode)) {
                    $this->addFailLogByMethod("GetQRCode");
                    $this->addVarLog("output_PosService_GetQRCode", 
                            $output_PosService_GetQRCode);
                    $this->moveFileToErrorDir($fileName);
                    $this->sendModFailNotification($date,$time,$channel,$phoneNum);
                    continue;
                }
                
                $this->addVarLog("qrCode", $qrCode);
                $this->addVarLog("tac", $tac);
                
                if (! Mage::helper('paymentqrcode/order')->InsertQRCodeToOrder(
                        $orderIncrementId, $qrCode, $tac)) {
                    $this->addFailLogByMethod("InsertQRCodeToOrder");
                    $this->moveFileToErrorDir($fileName);
                    $this->sendModFailNotification($date,$time,$channel,$phoneNum);
                    continue;
                }
                
                $output_SmsService_SendSmsMsgByOrderId;
                if (! Mage::helper('paymentqrcode/SmsService')->SendSmsMsgByOrderId(
                        $phoneNum, $orderIncrementId, 
                        $output_SmsService_SendSmsMsgByOrderId)) {
                    $this->addFailLogByMethod("SendSmsMsgByOrderId");
                    $this->addVarLog("output_SmsService_SendSmsMsgByOrderId", 
                            $output_SmsService_SendSmsMsgByOrderId);
                    $this->moveFileToErrorDir($fileName);
                    $this->sendModFailNotification($date,$time,$channel,$phoneNum);
                    continue;
                }
                
                $this->addSuccessLogByMethod("GetOrderByModParser");
                $this->moveFileToFinishDir($fileName);
            }
        } catch (Exception $e) {
            $this->addFailLogByMethod("GetOrderByModParser");
            $this->addVarLog("exception", $e->getMessage());
            $this->moveFileToErrorDir($fileName);            
        }
    }
    
    // TODO: download csv file from CHT MOD , current always return true for
    // test purpose.
    public function DownloadCsvDataFromMod ()
    {
        try {
            
            $this->checkDirExist();
            
            $path = "ftp_path";
            $username = "ftp_username";
            $password = "ftp_password";
            
            /*
             * $conn_id=ftp_connect($path); $login_result=ftp_login($conn_id,
             * $username, $password); if ((!$conn_id) || (!$login_result)) {
             * //TODO: write log in Magento Mage::log("FTP connection has failed
             * !"); return false; } // turns on FTP passive mode
             * ftp_pasv($conn_id, true); if(count($file_list)>0 and
             * $file_list!=false){ $date=date('Ymd'); }
             */
            
            // Init Dir , in not exists.
            // $folder="cht_Mod";
            
            // $this->CreateFAKEData();
            
            /*
             * //download CSV file to process folder foreach($file_list as
             * $file_name){
             * $process_file[]=$process_path="$folder/process/$file_name";
             * if(ftp_get($conn_id, $process_path, $file_name, FTP_BINARY)) {
             * //TODO: to be confirm is need delete file. //ftp_delete($conn_id,
             * $file_name); } } $process_path="$folder/process/$file_name";
             */
            
            return true;
        } catch (Exception $e) {
            Mage::log(
                    "[Invoke method DownloadOrderRecordFromMod - Exception]" .
                             $e->getMessage(), null, "qrcode.log");
            return false;
        }
    }

    public function GetProductId ($date, $time, $channel)
    {
        $d = strtotime($date . ' ' . $time);
        return Mage::getModel('mod/mappings')->getProductIdByChannelAndTime(
                $channel, $d);
    }

    public function ParseCsvData ($filePath)
    {
        // parse csv file to get date,time,channel,phone,keep1,keep2
        // $array = array("2013/12", "16:30", "40", "0915730309");
        $fields = array();
        
        $rowIdx = 1;
        $realDataRow = 2;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if ($rowIdx == $realDataRow) {
                    // push csv value to array.
                    foreach ($data as $value) {
                        array_push($fields, $value);
                    }
                }
                
                $rowIdx ++;
            }
            fclose($handle);
        }
        
        $data = new CsvData();
        $data->setDate($fields[0]);
        $data->setTime($fields[1]);
        $data->setChannel($fields[2]);
        $data->setPhoneNum($fields[3]);
        return $data;
    }
    
    /*
     * Create FACK data fo demo, name: mod.csv
     */
    public function CreateFAKEData ($date, $time, $channel, $phone)
    {
        $defaultDir = $this->defaultDir();
        $list = array(
                array(
                        'date',
                        'time',
                        'channel',
                        'phone',
                        'keep1',
                        'keep2'
                ),
                // array('2013/12/12', '16:30', '44', '0937088148','','')
                array(
                        $date,
                        $time,
                        $channel,
                        $phone,
                        '',
                        ''
                )
        );
        $fp = fopen("$defaultDir/process/mod.csv", 'w');
        foreach ($list as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
        return true;
    }

    private function defaultDir ()
    {
        return "cht_mod";
    }

    private function processDir ()
    {
        return $this->defaultDir() . "/process";
    }

    private function processFilePath ($fileName)
    {
        return $this->processDir() . "/" . $fileName;
    }

    private function finishDir ()
    {
        return $this->defaultDir() . "/finish";
    }

    private function finishFilePath ($fileName)
    {
    	$milliseconds = round ( microtime ( true ) * 1000 );
    	usleep ( 2000 );
    	$timeStr =  date ('YmdHis',strtotime('+8 hours')).'_'.$milliseconds;
    	
        return $this->finishDir() . "/" .$timeStr.'_'. $fileName;
    }

    private function errorDir ()
    {
        return $this->defaultDir() . "/error";
    }

    private function errorFilePath ($fileName)
    {
    	$milliseconds = round ( microtime ( true ) * 1000 );
    	usleep ( 2000 );
    	$timeStr =  date ('YmdHis',strtotime('+8 hours')).'_'.$milliseconds;
    	
        return $this->errorDir() . "/" . $timeStr.'_'. $fileName;
    }

    private function moveFileToErrorDir ($fileName)
    {    	
        rename($this->processFilePath($fileName), 
                $this->errorFilePath($fileName));
    }

    private function moveFileToFinishDir ($fileName)
    {
        rename($this->processFilePath($fileName), 
                $this->finishFilePath($fileName));
    }

    private function getCustomerIdByEmail ($email)
    {
        $customer = Mage::getModel('customer/customer');
        // HACK: setWebsiteId in Aoc will fail.
        // $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->setWebsiteId(1);
        $customer->loadByEmail($email);
        return $customer->getId();
    }

    private function checkDirExist ()
    {
        $defaultDir = $this->defaultDir();
        if (! is_dir("$defaultDir")) {
            mkdir("$defaultDir");
            chmod($defaultDir, 755);
        }
        
        $processDic = $this->processDir();
        if (! is_dir("$processDic")) {
            mkdir("$processDic");
            chmod($processDic, 0755);
        }
        
        $errorDic = $this->errorDir();
        if (! is_dir("$errorDic")) {
            mkdir("$errorDic");
            chmod($errorDic, 0755);
        }
        
        $finishDir = $this->finishDir();
        if (! is_dir("$finishDir")) {
            mkdir("$finishDir");
            chmod($finishDir, 755);
        }
    }

    private function getCurrentProcessFiles ()
    {
        $processDir = $this->processDir();
        $scannedDir = array_diff(scandir($processDir), 
                array(
                        '..',
                        '.'
                ));
        return $scannedDir;
    }

    private function addVarLog ($name, $value)
    {
        Mage::log($name . ":" . $value, null, "qrcode.log");
    }

    private function addSuccessLogByMethod ($method)
    {
        Mage::log("[SUCCESS] - Invoke method " . $method, null, "qrcode.log");
    }

    private function addFailLogByMethod ($method)
    {
        Mage::log("[FAIL] - Invoke method " . $method, null, "qrcode.log");
    }

    private function getProductIsInStockByProductId ($productId)
    {
        $product = Mage::getModel('catalog/product')->load($productId);
        $isInStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct(
                $product)->getIsInStock();
        return $isInStock;
    }

    private function createModOrderRecord ($customerId, $productId, $phoneNum)
    {
        // Create Order & Init shippingInfo by order.
        $order = new Cht_Mod_Helper_Order();
        $shippingInfo = new ChtOrderShippingInfo();
        $billingInfo = new ChtOrderBillingInfo();
        $billingInfo->setTelePhone($phoneNum);
        $billingInfo->setCellPhone($phoneNum);
        
        $ordercreated = Mage::helper('cht/order')->CreateOrder($customerId, 
                $productId, $shippingInfo, $billingInfo);
        return $ordercreated;
    }
    
    private function sendModFailNotification($date,$time,$channel,$phone) {    	
    	$email_address = Mage::getStoreConfig ( 'sales_email/settv_order_mgmt/manager_email' );
    	$emailTemplateVariables = array ();
    	$emailTemplateVariables ['date'] = $date;
    	$emailTemplateVariables ['time'] = $time;
    	$emailTemplateVariables ['channel'] = $channel;
    	$emailTemplateVariables ['phone'] = $phone;
    	Mage::helper ( 'common/common' )->sendEmail ( "CHT MOD failed", $email_address, $emailTemplateVariables );
    }
}

class CsvData
{

    private $date = "";

    private $time = "";

    private $channel = "";

    private $phoneNum = "";

    public function getDate ()
    {
        return $this->date;
    }

    public function getTime ()
    {
        return $this->time;
    }

    public function getChannel ()
    {
        return $this->channel;
    }

    public function getPhoneNum ()
    {
        return $this->phoneNum;
    }

    public function setDate ($date)
    {
        $this->date = $date;
    }

    public function setTime ($time)
    {
        $this->time = $time;
    }

    public function setChannel ($channel)
    {
        $this->channel = $channel;
    }

    public function setPhoneNum ($phoneNum)
    {
        $this->phoneNum = $phoneNum;
    }
}

