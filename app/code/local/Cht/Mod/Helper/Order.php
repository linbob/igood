<?php
class Cht_Mod_Helper_Order extends Mage_Core_Helper_Abstract {
	function CreateOrder($customerId, $productId, $shippingInfo, $billingInfo) {
		//- Get: customer
		$customer = Mage::getModel('customer/customer')->load($customerId);

		//- Get: store id by product
		$item     = Mage::getModel('catalog/product')->load($productId);
		$storeIds = $item->getStoreIds();
		$storeId  = end($storeIds);

		//- Generate: incremental id
		$orderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);

		//- Create a new order object
		$transaction = Mage::getModel('core/resource_transaction');
		$order = Mage::getModel('sales/order')
		->setIncrementId($orderId)
		->setStoreId($storeId)
		->setQuoteId(0)
		->setGlobal_currency_code('NT')
		->setBase_currency_code('NT')
		->setStore_currency_code('NT')
		->setOrder_currency_code('NT');

		// set Customer data
		$order->setCustomer_email($customer->getEmail())
		->setCustomerFirstname($customer->getFirstname())
		->setCustomerLastname($customer->getLastname())
		->setCustomerGroupId($customer->getGroupId())
		->setCustomer_is_guest(0)
		->setCustomer($customer);

		// set Billing Address
		$billing = $customer->getDefaultBillingAddress();
		$billingAddress = Mage::getModel('sales/order_address')
		->setStoreId($storeId)
		->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
		->setCustomerId($customer->getId())
		->setCustomerAddressId($customer->getDefaultBilling())
		->setCustomer_address_id($billing->getEntityId())
		->setPrefix($billing->getPrefix())
		->setFirstname($billing->getFirstname())
		->setMiddlename($billing->getMiddlename())
		->setLastname($billing->getLastname())
		->setSuffix($billing->getSuffix())
		->setCompany($billing->getCompany())
		->setStreet($billing->getStreet())
		->setCity($billing->getCity())
		->setCountry_id($billing->getCountryId())
		->setRegion($billing->getRegion())
		->setRegion_id($billing->getRegionId())
		->setPostcode($billing->getPostcode())
		->setTelephone($billingInfo->getTelePhone())
		->setCellphone($billingInfo->getCellphone())
		->setFax($billing->getFax());
		$order->setBillingAddress($billingAddress);

		$shipping = $customer->getDefaultShippingAddress();
		$shippingAddress = Mage::getModel('sales/order_address')
		->setStoreId($storeId)
		->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
		->setCustomerId($customer->getId())
		->setCustomerAddressId($customer->getDefaultShipping())
		->setCustomer_address_id($shipping->getEntityId())
		->setFirstname($shippingInfo->getFirstname())
		->setLastname($shippingInfo->getLastname())
		->setStreet($shippingInfo->getStreet())
		->setCity($shippingInfo->getCity())
		->setCountry_id("886")
		->setPostcode($shippingInfo->getPostcode())
		->setTelephone($shippingInfo->getPhone());
		$order->setShippingAddress($shippingAddress);
		
		//you can set your payment method name here as per your need
		$orderPayment = Mage::getModel('sales/order_payment')
		->setStoreId($storeId)
		->setCustomerPaymentId(0)
		->setMethod('paymentqrcode');

		$order->setPayment($orderPayment);

		// let say, we have 1 product
		//check that your products exists
		//need to add code for configurable products if any
		$subTotal = 0;
		$products = array(
				$productId => array(
						'qty' => 1
				)
		);
				

		//- Set order prize
		foreach ($products as $productId=>$product) {
			$_product = Mage::getModel('catalog/product')->load($productId);
			$final_price = $_product->getFinalPrice();
			$rowTotal = $final_price * $product['qty'];			
			$orderItem = Mage::getModel('sales/order_item')
			->setStoreId($storeId)
			->setQuoteItemId(0)
			->setQuoteParentItemId(NULL)
			->setProductId($productId)
			->setProductType($_product->getTypeId())
			->setQtyBackordered(NULL)
			->setTotalQtyOrdered($product['qty'])
			->setQtyOrdered($product['qty'])
			->setName($_product->getName())
			->setSku($_product->getSku())
			->setPrice($final_price)
			->setBasePrice($final_price)
			->setOriginalPrice($final_price)
			->setRowTotal($rowTotal)
			->setBaseRowTotal($rowTotal);

			$subTotal += $rowTotal;
			$order->addItem($orderItem);
		}
		$order->setSubtotal($subTotal)
		->setBaseSubtotal($subTotal)
		->setGrandTotal($subTotal)
		->setBaseGrandTotal($subTotal)
		->setTotalQtyOrdered(1);
		
		//Shipping
		$product = Mage::getModel('catalog/product')->load($productId);
		$logistics_com = $product->getAttributeText('logistics_com');
		$order->setShippingDescription($logistics_com);
		$shipping_method = Mage::helper('ordermanage')->getShippingMethodByCompany($logistics_com);
		$order->setShippingMethod($shipping_method);
		
		//- Save the transaction (order)
		$transaction->addObject($order);
		$transaction->addCommitCallback(array($order, 'place'));
		$transaction->addCommitCallback(array($order, 'save'));
		$transaction->save();

		//- Set supplier for this order
		//  (Save supplier id to custom order model)
		$entityId  = $order->getId();
		$product   = Mage::getModel('catalog/product')->load($productId);
		$supplier_realid = $_product->getSupplier();
		$collection  = Mage::getModel('supplier/supplier')->getCollection()->addFieldToSelect('*')->addFieldToFilter('attribute_id', $supplier_realid);
		$supplier    = current($collection->load()->getIterator());
		$supplier_id = $supplier->getId();

		$model = Mage::getModel('check/custom_order');
		$model->setOrderId($entityId);		
		$model->setShipstatus(Settv_Check_Model_Custom_Order::STATE_NOT_SHIPPED);
		$model->setData('supplier_id', $supplier_id);
		$model->save();

		//- Set state as "Pending payment"
		$order->setState('pending_payment', true, '')->save();

		//Create Master Order		
		$master_order_id = Mage::getModel("splitorder/orders")->createMasterOrderByOrder($order);
		
		//-Return: new order's incremental id
		return $order;
	}

	public function getModOrdersByNameAndPhone($userName, $cellPhone)
	{
		$orderIds = array();
		$orderCollection = $this->getOrderListFromModUser();
		foreach($orderCollection AS $mod_order)
		{
			$billing = $mod_order->getBillingAddress();
			$b_userName = trim($billing->getFirstname());
			$b_cellPhone = $billing->getCellphone();
				
			if ($b_userName == $userName && $b_cellPhone == $cellPhone)
			{
				array_push($orderIds, $mod_order->getId());
			}
		}
		return $orderIds;
	}

	public function moveOrderToCustomer($customerId, $orderIds)
	{
		$customer = Mage::getModel('customer/customer')->load($customerId);
		foreach($orderIds AS $orderId)
		{
			$this->ChangeOrderCustomer($orderId, $customerId);
		}
	}

	private function getOrderListFromModUser()
	{
		$customerEmail = Mage::getStoreConfig('cht/mod/useremail', false);
		$customer = Mage::getModel("customer/customer");
		$customer->setWebsiteId(Mage::app()->getWebsite()->getId());
		$customer->loadByEmail($customerEmail);
		$customerId = $customer->getId();

		$orderCollection = Mage::getModel('sales/order')->getCollection()
		->addFieldToFilter('customer_id', array('eq' => array($customerId)));

		return $orderCollection;
	}

	private function ChangeOrderCustomer($orderId, $customerId)
	{
		$customer = Mage::getModel('customer/customer')->load($customerId);
		$order = Mage::getModel('sales/order')->load($orderId);
		//set order information
		$order->setCustomer_email($customer->getEmail())
		->setCustomerFirstname($customer->getFirstname())
		->setCustomerLastname($customer->getLastname())
		->setCustomerGroupId($customer->getGroupId())
		->setCustomer_is_guest(0)
		->setCustomer($customer);
		$order->save();
		
		Mage::helper ('cht/customer')->AddCustomerAddressByOrder($orderId, $customerId);
	}
}

class ChtOrderShippingInfo {
	private $firstname = "";
	private $lastname = "";
	private $street = "";
	private $city = "";
	private $postcode = "";
	private $phone = "";

	public function getFirstname(){
		return $this->firstname;
	}

	public function setFirstname($firstname){
		$this->firstname = $firstname;
	}

	public function getLastname(){
		return $this->lastname;
	}

	public function setLastname($lastname){
		$this->lastname = $lastname;
	}

	public function getStreet(){
		return $this->street;
	}

	public function setStreet($street){
		$this->street = $street;
	}

	public function getCity(){
		return $this->city;
	}

	public function setCity($city){
		$this->city = $city;
	}

	public function getPostcode(){
		return $this->postcode;
	}

	public function setPostcode($postcode){
		$this->postcode = $postcode;
	}

	public function getPhone(){
		return $this->phone;
	}

	public function setPhone($phone){
		$this->phone = $phone;
	}
}

class ChtOrderBillingInfo {
	
	private $firstname = "";
	private $lastname = "";
	private $street = "";
	private $city = "";
	private $postcode = "";
	private $telephone = "";
	private $cellphone = "";
	
	public function getFirstname(){
		return $this->firstname;
	}
	
	public function setFirstname($firstname){
		$this->firstname = $firstname;
	}
	
	public function getLastname(){
		return $this->lastname;
	}
	
	public function setLastname($lastname){
		$this->lastname = $lastname;
	}
	
	public function getStreet(){
		return $this->street;
	}
	
	public function setStreet($street){
		$this->street = $street;
	}
	
	public function getCity(){
		return $this->city;
	}
	
	public function setCity($city){
		$this->city = $city;
	}
	
	public function getPostcode(){
		return $this->postcode;
	}
	
	public function setPostcode($postcode){
		$this->postcode = $postcode;
	}
	
	public function getTelePhone(){
		return $this->telephone;
	}

	public function setTelePhone($phone){
		$this->telephone = $phone;
	}

	public function getCellPhone(){
		return $this->cellphone;
	}

	public function setCellPhone($phone){
		$this->cellphone = $phone;
	}
}
