<?php
class Cht_Mod_Model_Mappings extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('mod/mappings');
	}

	public function getProductIdByChannelAndTime($channel, $time)
	{	
		$t = date("Y-m-d H:i:s", Mage::getSingleton('core/date')->gmtTimestamp($time));
		
		$product_id = $this->getCollection()
		->addFieldToFilter('start_time', array('lt'=> $t)) //utc
		->addFieldToFilter('end_time', array('gt' => $t)) //utc
		->addFieldToFilter('channel', array('eq' => $channel))
		->getFirstItem()->getProductId();
		
		return $product_id;		
	}
	
	public function createMapping($channel, $product_id, $start_time, $end_time)
	{	
		$utc_now = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s");
		$s_time = Mage::getSingleton('core/date')->gmtTimestamp($start_time);
		$e_time = Mage::getSingleton('core/date')->gmtTimestamp($end_time);
		
		$this->setChannel($channel)
		->setProductId($product_id)
		->setStartTime($s_time)
		->setEndTime($e_time)
		->setCreateTime($utc_now)
		->save();
	}
	
	public function deleteMappings($mapping_id)
	{
		$this->setData('mapping_id', $mapping_id);
		$this->delete();
	}
}
