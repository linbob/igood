<?php
class Cht_Mod_Model_Resource_Mappings_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	protected function _construct()
	{
		$this->_init('mod/mappings');
	}	
	
	public function addProductData()
	{
		/** add particular attribute code to this array */
		$productAttributes = array('name');
		foreach ($productAttributes as $attributeCode) {
			$alias     = $attributeCode . '_table';
			$attribute = Mage::getSingleton('eav/config')
			->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
	
			/** Adding eav attribute value */
			$this->getSelect()->join(
					array($alias => $attribute->getBackendTable()),
					"main_table.product_id = $alias.entity_id AND $alias.attribute_id={$attribute->getId()}",
					array($attributeCode => 'value')
			);
			$this->_map['fields'][$attributeCode] = 'value';
		}
		/** adding catalog_product_entity table fields */
		$this->join(
				'catalog/product',
				'product_id=`catalog/product`.entity_id',
				array('sku' => 'sku', 'type_id' => 'type_id')
		);
		$this->_map['fields']['sku']     = 'sku';
		$this->_map['fields']['type_id'] = 'type_id';
		return $this;
	}
}
