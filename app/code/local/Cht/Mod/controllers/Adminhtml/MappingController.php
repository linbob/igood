<?php

class Cht_Mod_Adminhtml_MappingController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction()
	{
		$this->loadLayout()->_setActiveMenu('cht_mod/adminhtml_mapping');
		return $this;
	}

	public function indexAction() {
		$this->loadLayout();
		$this->_title($this->__('ModMapping'));
		$this->_initAction()->_addContent($this->getLayout()->createBlock('cht_mod/adminhtml_mapping'));
		$this->renderLayout();
	}
	
	public function redirectAction() {
		$url = Mage::getStoreConfig('cht/mod/link', false);		
		Mage::app()->getResponse()->setRedirect($url);
	}


	public function newAction() {
		$this->loadLayout()
		->_addContent($this->getLayout()->createBlock('cht_mod/adminhtml_mapping_edit'))
		->_addLeft($this->getLayout()->createBlock('cht_mod/adminhtml_mapping_edit_tabs'));
		$this->renderLayout();
	}

	public function deleteAction() {		
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('mod/mappings')->load($id)->delete();
		$this->_redirect('*/*/');
	}
	
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {

			$product_id = $data['product_id'];
			$channel = $data['channel'];
			$start_time = $data['start_time'];
			$end_time = $data['end_time'];
				
			//check product
			$product = Mage::getModel('catalog/product')->load($product_id);
			if (!$product->getId())
			{
				Mage::getSingleton('adminhtml/session')->addError($this->__('This product not existed.'));
				$this->_redirect('*/*/new');
				return;
			}
			else if ($product->getTypeId() != 'simple')
			{
				Mage::getSingleton('adminhtml/session')->addError($this->__('Please use simple product only.'));
				$this->_redirect('*/*/new');
				return;
			}
				
			//check time
			if (!(Mage::helper("cht")->validateDate($start_time))) {
				Mage::getSingleton('adminhtml/session')->addError($this->__('Please check start time.'));
				$this->_redirect('*/*/new');
				return;
			}
			if (!(Mage::helper("cht")->validateDate($end_time))) {
				Mage::getSingleton('adminhtml/session')->addError($this->__('Please check end time.'));
				$this->_redirect('*/*/new');
				return;
			}
			
			$model = Mage::getModel('mod/mappings')->createMapping($channel, $product_id, $start_time, $end_time);
		}

		$this->_redirect('*/*/');
	}

	public function printAction() {
		$this->loadLayout();
		$this->renderLayout();
	}

	public function exportCsvAction()
	{
		$fileName   = 'cht_mod.csv';
		$grid       = $this->getLayout()->createBlock('cht_mod/adminhtml_mapping_grid');
		$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		exit(0);
	}

	public function exportExcelAction()
	{
		$fileName   = 'cht_mod.xls';
		$grid       = $this->getLayout()->createBlock('cht_mod/adminhtml_mapping_grid');
		$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		exit(0);
	}

}
