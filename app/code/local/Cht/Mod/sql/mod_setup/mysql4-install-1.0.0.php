<?php

$installer = $this;
$installer->startSetup();

$websiteId = Mage::app()->getWebsite()->getId();
$store = Mage::app()->getStore();

$email = "mod_user@igoodtw.com";

$customer = Mage::getModel("customer/customer")->loadByEmail($email);

if(!$customer->getId()) {
	$customer->setWebsiteId(1); // main website
	$customer->setGroupId(1); // group id
	$customer->setFirstname("MOD_USER");
	$customer->setLastname("MOD_USER");
	$customer->setEmail($email);
	$customer->setPasswordHash(md5("igoodtw01"));
	$customer->save();

	$_custom_address = array (
			'firstname' => '',
			'lastname' => '',
			'street' => array (
					'0' => 'QR扣付款',
					'1' => '',
			),
			'city' => '',
			'region_id' => '',
			'region' => '',
			'postcode' => '',
			'country_id' => '',
			'telephone' => '',
	);
	$customAddress = Mage::getModel('customer/address');
	$customAddress->setData($_custom_address)
	->setCustomerId($customer->getId())
	->setIsDefaultBilling('1')
	->setIsDefaultShipping('1')
	->setSaveInAddressBook('1');

	$customAddress->save();
}

$installer->endSetup();