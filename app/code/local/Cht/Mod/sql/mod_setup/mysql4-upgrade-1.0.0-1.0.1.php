<?php

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('cht_mod_channel_mappings')} (
  `mapping_id` INT NOT NULL AUTO_INCREMENT,
  `product_id` INT(10) UNSIGNED NOT NULL,
  `channel` VARCHAR(10) NOT NULL,
  `start_time` DATETIME NOT NULL,
  `end_time` DATETIME NOT NULL,
  `create_time` DATETIME NOT NULL,
  PRIMARY KEY (`mapping_id`),
  INDEX `product_id_idx` (`product_id` ASC),
  CONSTRAINT `product_id`
    FOREIGN KEY (`product_id`)
    REFERENCES `catalog_product_entity` (`entity_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();