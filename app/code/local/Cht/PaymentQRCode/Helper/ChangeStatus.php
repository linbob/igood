<?php
class Cht_PaymentQRCode_Helper_ChangeStatus extends Mage_Core_Helper_Abstract {
	public function QRCodePaymentExpired()
	{
		$failed_orderids  = array();

		$timeoffset = strtotime(Mage::getStoreConfig('payment/paymentqrcode/expired', false));
		$str = date("Y-m-d H:i:s", $timeoffset);

		$OrderIds = Mage::getResourceModel('sales/order_collection')
		->addFieldToSelect('entity_id')
		->addFieldToSelect('increment_id')
		->addFieldToFilter('state', 'pending_payment')
		->addFieldToFilter('created_at', array('lt' => $str));

		$OrderIds->getSelect()
		->where("method='paymentqrcode'")
		->joinLeft('sales_flat_order_payment', 'main_table.entity_id = sales_flat_order_payment.parent_id',array('method'));

		$OrderIds->getData();

		foreach($OrderIds as $order_Id){
			$lidm  = $order_Id['increment_id'];
			$orderid = $order_Id['entity_id'];
			$order = Mage::getModel('sales/order')->load($orderid);
			$paymentCode = $order->getPayment()->getMethodInstance()->getCode();
			$this->TransFailedHandler($orderid);
			array_push($failed_orderids, $lidm);
		}
		return("[QRCode]Expired id(s):".implode(",", $failed_orderids).";");
	}

	public function TransFailedHandler($inOderId)
	{
		$order = Mage::getModel('sales/order')->load($inOderId);
		$paymentCode = $order->getPayment()->getMethodInstance()->getCode();

		$order->setState('canceled', true, '')->save();

		//Put back items to inventory
		$items = $order->getAllItems();
		foreach ($items as $itemId => $item) {
			$productId=$item->getProductId();
			$qty=$item->getQtyOrdered();
			$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
			$stockItem->addQty($qty);
			$stockItem->save();
		}
		//Send failed mail
		$email_address = $order->getCustomerEmail();
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$emailTemplateVariables['order_id'] = $order->getIncrementId();
		$items1 = $order->getAllItems();
		foreach ($items1 as $itemId1 => $item1) {
			$emailTemplateVariables['productname'] = $item1->getName();
			$emailTemplateVariables['qty']=$item1->getQtyOrdered()*1;
		}
		
		Mage::helper('common/common')->sendEmail('QRCode payment expired', $email_address, $emailTemplateVariables);

	}
}