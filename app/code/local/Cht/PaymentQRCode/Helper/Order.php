<?php
class Cht_PaymentQRCode_Helper_Order extends Mage_Core_Helper_Abstract {
	function InsertQRCodeToOrder($orderID, $qrCode, $tac)
	{
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderID);
		$customId = Mage::getModel('check/custom_order')->getIdByOrder($order->getId());
		if ($customId)
		{
			$customOrder = Mage::getModel('check/custom_order')->load($customId);
			$customOrder->setData('payment_qrcode', $qrCode);
			$customOrder->setData('payment_qrcode_tac', $tac);
			$customOrder->save();
			return true;
		}
		else			
		{
			return false;
		}
	}
}