<?php
class Cht_PaymentQRCode_Helper_PosService extends Mage_Core_Helper_Abstract {
	function GetQRCode($masterOrderID, $productId, $orderAmt, &$qrCode, &$tac, &$output)
	{
		$product_name = urlencode(Mage::getModel('catalog/product')->load($productId)->getName());
		$serviceUrl = Mage::getStoreConfig('payment/paymentqrcode/serviceurl', false);
		$targetRequest = "$serviceUrl?orderId=$masterOrderID&orderAmt=$orderAmt&prodDesc=$product_name";

		// use key 'http' even if you send the request to https://...
		$options = array(
				'http' => array(						
						'method'  => 'GET',
				),
		);

		$context  = stream_context_create($options);
		$output = file_get_contents($targetRequest, false, $context);

		$jobj = json_decode($output, true);
		$successPayLoad = $jobj['successPayLoad'];
		$szRCode = $successPayLoad['szRCode'][0];

		if ($szRCode == "0000")
		{
			$qrCode = $successPayLoad['szQRCode'][0];
			$tac = $successPayLoad['szTAC'][0];
			return true;
		}
		else
			return false;
	}

	function refundQRCode($order, &$output)
	{
		$refundUrl = Mage::getStoreConfig('payment/paymentqrcode/refundurl', false);

		$custom_order = Mage::getModel('check/custom_order')->getByOrder($order->getId());
		// TODO: why does this return an array!?!?!? it should return the model! REFACTOR!
		$TAC = $custom_order[0]["payment_qrcode_tac"];

		$masterOrderId = Mage::getModel("splitorder/items")->getMasterOrderIdByOrderId($order->getId());
		
		$targetRequest = "$refundUrl?orderId=".$masterOrderId."&TAC=$TAC";

		// use key 'http' even if you send the request to https://...
		$options = array(
				'http' => array(
						'method'  => 'GET',
				),
		);

		$context  = stream_context_create($options);
		$output = file_get_contents($targetRequest, false, $context);

		$jobj = json_decode($output, true);
		$successPayLoad = $jobj['successPayLoad'];
		$szRCode = $successPayLoad['szRCode'][0];

		if ($szRCode == "0000")
		{
			return true;
		}
		else
			return false;
	}
}