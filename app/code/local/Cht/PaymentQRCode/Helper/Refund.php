<?php
class Cht_PaymentQRCode_Helper_Refund extends Mage_Core_Helper_Abstract {

	public function doRefund($order) {
		if($order->getPayment()->getMethod() == 'paymentqrcode') {
			try {$output_posservice = null;

			$posserviceResult = Mage::helper('paymentqrcode/PosService')->refundQRCode($order, $output_posservice);
			Mage::log("Invoke method refundQRCode - orderIncrementId:". $order->getIncrementId() ."  MSG:".$output_posservice ,null, "qrcode.log");
			return $posserviceResult;
			}
			catch (Exception $e) {
				Mage::log($e->getMessage(), null, "qrcode.log");
				return false;
			}
		} else {
			// TODO: implement other auto refund services here
			return true;
		}
	}
}