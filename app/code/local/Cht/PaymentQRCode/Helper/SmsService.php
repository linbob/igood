<?php
class Cht_PaymentQRCode_Helper_SmsService extends Mage_Core_Helper_Abstract {
	function SendSmsMsg($mobileNo, $smsMsg, $sourceProdID, $sourceMsgID, $charSet, &$output)
	{
		$result = Mage::helper('yoyo8sms/SMSBridge')->SendSMS($mobileNo, $smsMsg, $sourceProdID, $sourceMsgID, $charSet, $output);		
		return $result;
	}

	function SendSmsMsgByOrderId($mobileNo, $orderId, &$output)
	{
		$serviceUrl = Mage::getStoreConfig('payment/paymentqrcode/smsmsgurl', false);
		$returnURL = "$serviceUrl?orderid=$orderId";
		$msg = Mage::getStoreConfig('payment/paymentqrcode/smsmsg', false);

		if ($this->SendSmsMsg($mobileNo, $returnURL, $orderId, "url", "E", $output)) //第一封
		{
			if ($this->SendSmsMsg($mobileNo, $msg, $orderId, "msg", "U", $output)) //第二封
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function SendNoInventorySmsMsg($mobileNo, $key, &$output)
	{		
		$msg = Mage::getStoreConfig('payment/paymentqrcode/smsnoinventorymsg', false);
		if ($this->SendSmsMsg($mobileNo, $msg, $key, "lackmsg", "U", $output))		
			return true;		
		else
			return false;
	}

}