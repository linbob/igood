<?php
class Cht_PaymentQRCode_QRCodeResultController extends Mage_Core_Controller_Front_Action{
	public function indexAction(){
		extract($_POST, EXTR_SKIP);

		Mage::log("QR Code POST form body;". serialize($_POST), null, "qrcode.log");
		try {
			if ($OrderID != null)
			{
				$orders = Mage::getModel("splitorder/items")->getOrdersByMasterOrderId($OrderID);

				if (count($orders) > 0)
				{
					$order = $orders[0];
					if ($order->getId() != null
					&& $order->getPayment()->getMethodInstance()->getCode() == 'paymentqrcode'
							&& $order->getState() == 'pending_payment')
					{
						if ($PmtResult === "Y") {
							//SUCCESS
							//Mark order as "established" and send mail to supplier
							$order_billing_phone = $order->getBillingAddress()->getCellphone();
							$order->setState('established', true, '')->save();

							$this->setOrderShippingAddress($order, $DeliveryName, $DeliveryAddr, $DeliveryTEL);
							$this->setOrderBillingAddress($order, $InvoiceName, $InvoiceAddr);

							$this->setCustomOrder($order, $TAC, $InvoiceType, $InvoiceTitle, $InvoiceCompNum);

							//Move Mod Order To Customer
							$this->moveModOrdersToUser($InvoiceName, $order_billing_phone);
													
							//Send mail to customer
							$order->sendNewOrderEmail();
							
							//Send mail to supplier
							$emailTemplateVariables = array();
							$emailTemplateVariables['order'] = $order;
							Mage::helper('common/common')->emailSupplierByOrder($order, 'New Order to Supplier', $emailTemplateVariables);

						} else {
							//Failed
							$order->setState('failed', true, '')->save();
						}
					}
					else
					{
						Mage::log("Order id is null or payment != qrcode or state != pending;", null, "qrcode.log");
					}
				}
				else 
				{
					Mage::log("Order count = 0", null, "qrcode.log");
				}
			}
		} catch (Exception $e) {
			Mage::log("An Exception has occurred;". $e->getMessage(), null, "qrcode.log");
			$RCode = "0001";
			$RCodeMsg= "An error has occured while invoking SETTV API.";
		}

		$respBody = "SystemID=".$SystemID."&POSID=".$POSID."&OrderID=".$OrderID."&TAC=".$TAC."&PmtOrderID=".$PmtOrderID."&ResponseDate=".$NotifyDate."&RCode=".$RCode."&RCodeMsg=".$RCodeMsg;
		Mage::log("QR Code respBody for order;".$OrderID."; respBody is;".$respBody, null, "qrcode.log");

		$this->getResponse()->setBody($respBody);
		return;
	}

	public function successAction(){
		$this->loadLayout();
		$this->renderLayout();
	}

	public function failedAction() {
		$this->loadLayout();
		$this->renderLayout();
	}

	private function setOrderShippingAddress($order, $DeliveryName, $DeliveryAddr, $DeliveryTEL) {
		$shippingAddress = $order->getShippingAddress();
		$shippingAddress->setFirstname($DeliveryName);
		$shippingAddress->setName($DeliveryName);
		$shippingAddress->setStreetFull($DeliveryAddr);
		$shippingAddress->setCellphone($DeliveryTEL);
		$shippingAddress->setTelephone($DeliveryTEL);
		$shippingAddress->save();
	}

	private function setOrderBillingAddress($order, $InvoiceName, $InvoiceAddr)
	{
		$billingAddress = $order->getBillingAddress();
		$billingAddress->setFirstname($InvoiceName);
		$billingAddress->setName($InvoiceName);
		$billingAddress->setStreetFull($InvoiceAddr);
		$billingAddress->save();
	}

	private function setCustomOrder($order, $TAC, $InvoiceType, $InvoiceTitle, $InvoiceCompNum) {
		// set shipping status
		$customId = Mage::getModel('check/custom_order')->getIdByOrder($order->getId());
		$custom_order = Mage::getModel('check/custom_order')->load($customId);
		$custom_order->setShipstatus('Not Shipped');

		// set TAC code
		$custom_order->setPaymentQrcodeTac($TAC);

		//TODO: Invoice Type to Enum
		// set Invoice
		$iType = null;
		if ($InvoiceType == "D")//D: 捐贈
		{
			$iType = "5"; //"4"; //捐贈
		}
		else if ($InvoiceType == "P") //P: 紙本發票
		{
			if (empty($InvoiceTitle) || empty($InvoiceCompNum))
				$iType = "5"; //2"; //二聯式
			else
			{
				$iType = "5"; //3"; //三聯式
				$custom_order->setInvoicecom($InvoiceTitle); //抬頭
				$custom_order->setInvoiceubn($InvoiceCompNum); //統編
			}
		}
		else if ($InvoiceType == "E") //E: 電子發票
		{
			$iType = "5";
		}
		$custom_order->setInvoicetype($iType); //抬頭
		$custom_order->save();
	}

	private function moveModOrdersToUser($mod_username, $mod_cellphone)
	{
		try {
			$orderIds = Mage::helper('cht/order')->getModOrdersByNameAndPhone($mod_username, $mod_cellphone);
			$customerId = Mage::helper('cht/customer')->getCustomerIdByNameAndPhone($mod_username, $mod_cellphone);

			if ($orderIds != null && count($orderIds) > 0 && $customerId != null)
			{
				Mage::log($customerId.":".implode(",", $orderIds), null, "moveorder_qrcodepost.log" );
				Mage::helper('cht/order')->moveOrderToCustomer($customerId, $orderIds);
			}
		} catch (Exception $e) {
			$error = $e->getMessage();
			Mage::log("$mod_username:::$mod_cellphone:::$error", null, "error_moveorder.log");
		}
	}
}
?>