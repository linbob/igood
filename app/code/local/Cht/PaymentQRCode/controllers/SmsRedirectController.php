<?php
class Cht_PaymentQRCode_SmsRedirectController extends Mage_Core_Controller_Front_Action{
	public function indexAction(){
		$orderID = $_GET["orderid"];		
		
		if ($orderID != "")
		{
			$order = Mage::getModel('sales/order')->loadByIncrementId($orderID);
			$customId = Mage::getModel('check/custom_order')->getIdByOrder($order->getId());
			$custom_order = Mage::getModel('check/custom_order')->load($customId);
			$qrcode = $custom_order["payment_qrcode"];
			$successurl = urlencode(Mage::getStoreConfig('payment/paymentqrcode/successurl', false));
			$failedurl = urlencode(Mage::getStoreConfig('payment/paymentqrcode/failedurl', false));
			$packagename = urlencode(Mage::getStoreConfig('payment/paymentqrcode/packagename', false));
			$redirectURL = 'intent://MobilePayment/AppInPay?source=Browser&success='.$successurl.'&error='.$failedurl.'&QRCodeImage='.$qrcode.'&p=#Intent;scheme=chtqrpay;package='.$packagename.';end';
						 
			echo "<script>window.location='".$redirectURL."'</script>";
		}
		else
		{
			echo "There is no order id!";
		}
			
	}
}