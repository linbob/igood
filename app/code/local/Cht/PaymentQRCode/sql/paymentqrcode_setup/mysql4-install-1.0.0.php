<?php

$installer = $this;
$installer->startSetup();

$connection = $this->getConnection();
$connection->addColumn($this->getTable('check/custom_order'), 'payment_qrcode', "varchar(10000)");

$installer->endSetup();