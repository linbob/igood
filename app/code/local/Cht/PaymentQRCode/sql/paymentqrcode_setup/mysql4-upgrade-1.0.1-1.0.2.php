<?php

$installer = $this;
$installer->startSetup();

$connection = $this->getConnection();
$connection->addColumn($this->getTable('check/custom_order'), 'payment_qrcode_tac', "varchar(100)");

$installer->endSetup();