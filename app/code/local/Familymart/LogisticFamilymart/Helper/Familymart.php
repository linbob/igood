<?php
class Familymart_LogisticFamilymart_Helper_Familymart extends Mage_Core_Helper_Abstract {

	public function deliverOrder($increment_id) {

		$xmlInput = new stdClass();
		$xmlInput->order_id = $increment_id;

		$order = Mage::getModel('sales/order')->loadByIncrementId($increment_id);
		if ($order->getIncrementId())
		{
			$xmlInput->order_receiver = $order->getShippingAddress()->getName();
			$xmlInput->order_receiver_cellphone = $order->getShippingAddress()->getCellphone();
			$xmlInput->order_receiver_telephone = $order->getShippingAddress()->getTelephone();
			$items = $order->getAllItems();
			foreach ($items as $itemId => $item) {
				$xmlInput->productName = $item->getName();
				$unitPrice = round($item->getPrice(),0);
				$sku       = $item->getSku();
				$productId = $item->getProductId();
				$qty       = $item->getQtyOrdered();
			}
			$xmlInput->totalPrice = round($order->getGrandTotal(),0);
		}
		else
		{
			die("can not find order increment id:".$increment_id);
		}

		//讀取訂單
		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order->getId());
		$Custommodel = Mage::getModel('check/custom_order')->load($CustomId);
		if($CustomId && $Custommodel){
			$CustomArr = $Custommodel->getData();
			//取得出貨地點
			$fminfo = unserialize($CustomArr["shippingdescription"]);
			$xmlInput->fmstorecode =  $fminfo["fmstorecode"];
			$xmlInput->fmstorename = $fminfo["fmstorename"];

			//Get XML String
			$fm_ID = "R02";
			$local_folder = "shipping";
			$xmlInput->fm_ID = $fm_ID;
			$xmlInput->store_name = Mage::getStoreConfig('carriers/familymart/sellername', false);
			$xmlStr = $this->createXml($xmlInput);

			$this->saveStrToXML($fm_ID, $local_folder, $xmlStr);
			//Mage::log("[R02 deliverOrder]".$order->getIncrementId(), null, 'Logistic_Familymart_Info.log');				
		}
		else
		{
			die("can not find custom order id:".$CustomId.":".$increment_id);
		}

		$logistics = Mage::getModel('check/custom_logistics')->createLogistics();
		Mage::getModel('check/custom_order')->setLogisticsData($order->getId(), $logistics->getId());
	}

	//出貨 R02
	public function shipping()
	{
		$fm_ID = "R02";
		$local_folder = "shipping";

		//yesterday 跨天問題
		$date=date("Ymd", strtotime('-16 hours')); //+8 -> -24 = -16
		$folder = $this->createDateFolder($local_folder, $date);
		$message = $this->uploadZipToFtp($fm_ID, $folder);

		//today
		$folder = $this->createFolder($local_folder);
		$message = $message.$this->uploadZipToFtp($fm_ID, $folder);

		Mage::log($fm_ID.":".$message, null, 'Logistic_Familymart.log');

		return $fm_ID.":".$message;
	}

	//通知顧客取貨 RS4
	public function RS4_Action()
	{
		$fm_ID = "RS4";
		$message = $this->doAction($fm_ID);

		Mage::log($fm_ID.":".$message, null, 'Logistic_Familymart.log');

		return $fm_ID.":".$message;
	}

	//顧客未取退回物流中心 R08
	public function R08_Action()
	{
		$fm_ID = "R08";
		$message = $this->doAction($fm_ID);

		Mage::log($fm_ID.":".$message, null, 'Logistic_Familymart.log');

		return $fm_ID.":".$message;
	}

	//退貨處理 已出貨
	public function R89_Action()
	{
		$fm_ID = "R89";
		$message = $this->doAction($fm_ID);

		Mage::log($fm_ID.":".$message, null, 'Logistic_Familymart.log');

		return $fm_ID.":".$message;
	}

	//已送達或已取 R96
	public function R96_Action()
	{
		$fm_ID = "R96";
		$message = $this->doAction($fm_ID);

		Mage::log($fm_ID.":".$message, null, 'Logistic_Familymart.log');

		return $fm_ID.":".$message;
	}

	//刷退 RS9
	public function RS9_Action()
	{
		$fm_ID = "RS9";
		$message = $this->doAction($fm_ID);

		Mage::log($fm_ID.":".$message, null, 'Logistic_Familymart.log');

		return $fm_ID.":".$message;
	}

	public function setRS4($tag)
	{
		$message = "";
		$order_incrementid = $tag->getElementsByTagName("ShipmentNo")->item(0)->nodeValue;
		$order = Mage::getModel('sales/order')->loadByIncrementId($order_incrementid);
		if ($order->getIncrementId())
		{
			$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order->getId());
			$Custommodel = Mage::getModel('check/custom_order')->load($CustomId);
			if ($CustomId && $Custommodel){
				$CustomArr = $Custommodel->getData();
				//取得出貨地點
				$fminfo = unserialize($CustomArr["shippingdescription"]);
				$fmstore = new stdClass();
				$fmstore->fmstorecode =  $fminfo["fmstorecode"];
				$fmstore->fmstorename = $fminfo["fmstorename"];
				$fmstore->fmaddr = $fminfo["fmstoreaddr"];
				$email_address = $order->getCustomerEmail();
				$this->sendOrderInfoMail($order, 'Shipping Advice', $email_address, $fmstore);
				$message = $message."[".$order->getIncrementId()."]";
			}
			else
			{
				$this->logError("RS4 cutoms_order was not existed:".$order_incrementid.":".$CustomId);
				$message = $message."[".$order->getIncrementId()."(c)]";
			}
		}
		else
		{
			$this->logError("RS4 order_incrementid was not existed:".$order_incrementid);
			$message = $message."[".$order_incrementid."(x)]";
		}
		return $message;
	}

	public function setR08($tag)
	{
		$message = "";
		$order_incrementid = $tag->getElementsByTagName("ShipmentNo")->item(0)->nodeValue;
		$order = Mage::getModel('sales/order')->loadByIncrementId($order_incrementid);
		if ($order->getIncrementId())
		{
			$order->setState('return_processing', true, '')->save();
			$email_address = Mage::getStoreConfig('sales_email/settv_order_mgmt/manager_email');
			$this->sendOrderInfoMail($order, 'Return Of Goods', $email_address, null);
			$message = $message."[".$order->getIncrementId()."]";
		}
		else
		{
			$this->logError("R08 order_incrementid was not existed:".$order_incrementid);
			$message = $message."[".$order_incrementid."(x)]";
		}
		return $message;
	}

	public function setR96($tag)
	{
		$message = "";
		$order_incrementid = $tag->getElementsByTagName("ShipmentNo")->item(0)->nodeValue;
		$arrivate_date = $tag->getElementsByTagName("SPDate")->item(0)->nodeValue;
		$flow_type = $tag->getElementsByTagName("FlowType")->item(0)->nodeValue;
		$order = Mage::getModel('sales/order')->loadByIncrementId($order_incrementid);
		if ($order->getIncrementId())
		{
			$order_id = $order->getId();
			$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
			$customOrder = Mage::getModel('check/custom_order')->load($CustomId);
			if($order_id && $customOrder)
			{
				if ($flow_type == "N") //顧客取走
				{
					$customOrder->setShipstatus('Arrived', '系統');
					$customOrder->setData("arrived_at", strtotime($arrivate_date));
					$customOrder->save();
					//Mage::log("[R08 hasPickup flow_type:N]".$order->getIncrementId().":".$email_address, null, 'Logistic_Familymart_Info.log');
					$message = $message."[".$order->getIncrementId()."(N)]";
				}
				else if ($flow_type == "R") //R 三立取走
				{
					$customOrder->setShipstatus('Return Received', '系統');
					$customOrder->save();
					$message = $message."[".$order->getIncrementId()."(R)]";
				}
				else
				{
					$message = $message."[".$order->getIncrementId()."(".$flow_type.")]";
				}
			}
			else
			{
				$this->logError("R96 Custom Order ID was not existed:".$order_incrementid.":".$order_id.":".$CustomId.":".$customOrder);
				$message = $message."[".$order->getIncrementId()."(c)]";
			}
		}
		else
		{
			$this->logError("R96 order_incrementid was not existed:".$order_incrementid);
			$message = $message."[".$order_incrementid."(x)]";
		}

		return $message;
	}

	public function setR89($tag)
	{
		$message = "";
		$order_incrementid = $tag->getElementsByTagName("ShipmentNo")->item(0)->nodeValue;
		$order = Mage::getModel('sales/order')->loadByIncrementId($order_incrementid);
		$order_id = $order->getId();
		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$customOrder = Mage::getModel('check/custom_order')->load($CustomId);

		if ($customOrder)
		{
			$customOrder->setShipstatus('Shipped', '系統')->save();
		}
		else
		{
			$this->logError("R89 order_incrementid was not existed:".$order_incrementid);
			$message = $message."[".$order_incrementid."(x)]";
		}
	}

	public function setRS9($tag)
	{
		$message = "";
		$email_address = Mage::getStoreConfig('sales_email/settv_order_mgmt/manager_email');

		$order_incrementid = $tag->getElementsByTagName("ShipmentNo")->item(0)->nodeValue;
		$statusRemark = $tag->getElementsByTagName("StatusRemark")->item(0)->nodeValue;
		$statusDetails = $tag->getElementsByTagName("StatusDetails")->item(0)->nodeValue;

		$order = Mage::getModel('sales/order')->loadByIncrementId($order_incrementid);
		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order->getId());
		$customOrder = Mage::getModel('check/custom_order')->load($CustomId);

		if ($customOrder)
		{
			$moreMsg = "";

			if ($statusDetails == "D04" || $statusDetails == "S07" || $statusDetails == "T01" || $statusDetails == "T05")
			{
				$order->setState('return_processing', true, '')->save();
				$moreMsg = "進行退貨";
			}
			else if ($statusDetails == "999")
			{
				$moreMsg = "商品尚未至物流中心";
			}
			else if ($statusDetails == "XXX")
			{
				if ($customOrder->getShipstatus() != 'Arrived')
				{
					$customOrder->setShipstatus('Not Shipped', '系統 :商品期限內未至物流中心，物流單已被取消')->save();
					$moreMsg = "商品期限內未至物流中心，物流單已被取消";
				}
				else 
					$moreMsg = "商品已到達，物流單取消訊息將被忽略不處理";
			}
			else //S03 N05 S06 T03
			{
				$moreMsg = "請與日翊聯絡";
			}

			$emailTemplateVariables = array();
			$emailTemplateVariables['order'] = $order;
			$emailTemplateVariables['comment'] = $moreMsg.":".$statusRemark;
			Mage::helper('common/common')->sendEmail("Admin Order Comment", $email_address, $emailTemplateVariables);

			$message = $message."[".$order_incrementid."][".$statusDetails."]";
		}
		else
		{
			$this->logError("RS9 order_incrementid was not existed:".$order_incrementid);
			$message = $message."[".$order_incrementid."(x)]";
		}

		return $message;
	}

	function sendOrderInfoMail($order,  $email_code, $email_address, $fmstore)
	{
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$emailTemplateVariables['order_id'] = $order->getIncrementId();
		$emailTemplateVariables['totalPrice'] = round($order->getGrandTotal(),0);

		if ($fmstore)
		{
			$emailTemplateVariables['fmstorename'] = $fmstore->fmstorename;
		}

		$items = $order->getAllItems();
		$itemIndex = 0;
		foreach ($items as $itemId => $item)
		{
			if ($itemIndex == 0)
			{
				$emailTemplateVariables['productname'] = $item->getName();
				$emailTemplateVariables['qty']=intval($item->getQtyOrdered()*1);
				$emailTemplateVariables['unitPrice'] = round($item->getPrice(),0);
			}
			$emailTemplateVariables['sku'] =$item->getSku();
			$itemIndex++;
		}
		Mage::helper('common/common')->sendEmail($email_code, $email_address, $emailTemplateVariables);
	}

	function logError($comment)
	{
		Mage::log($comment, null, 'Logistic_Familymart_Error.log');
	}

	function getSpecificTag($folder, $file, $tagName)
	{
		$tags = array();

		if ($this->endsWith($file, ".xml"))
		{
			try
			{
				$doc = new DOMDocument('1.0', 'utf-8');
				$doc->load($folder->path.$file);
				$elements = $doc->getElementsByTagName($tagName);
				if (count($elements) > 0)
				{
					if (rename($folder->path.$file, str_replace("/process/","/finish/", $folder->path).$file))
					{
						foreach ($elements as $element)
						{
							$xml = new DOMDocument('1.0', 'utf-8');
							$node = $xml->importNode($element, true);
							$xml->appendChild($node);
							array_push($tags, $xml);
						}
					}
					else
					{
						$this->logError("Move XML file to finish fail:".$folder->path.$file);
					}
				}
				else
				{
					$this->logError("Can't find ".$tagName.":".$file);
				}
			} catch (Exception $e) {
				$this->logError("[getSpecificTag]Get XML Exception:".$file.":".$e->getMessage());
			}
		}
		else
		{
			$this->logError("not XML file".$folder->path.$file);
		}

		return $tags;
	}

	function connToFtp()
	{
		$path     = Mage::getStoreConfig('carriers/familymart/ftp', false);
		$username = Mage::getStoreConfig('carriers/familymart/fusername', false);
		$password = Mage::getStoreConfig('carriers/familymart/fpassword', false);
		$port     = Mage::getStoreConfig('carriers/familymart/ftpport', false);
		$conn_id=ftp_connect($path, $port);
		if ($conn_id)
		{
			$login_result=ftp_login($conn_id, $username, $password);

			// Check open
			if ($login_result)
			{
				ftp_pasv($conn_id, true);
				return $conn_id;
			}
			else
			{
				$this->logError("Unable to login to familymart FTP.");
				return  null;
			}
		}
		else
		{
			$this->logError("Unable to connect to familymart FTP.");
		}
	}

	function doAction($fm_ID)
	{
		$message = "";
		$conn_id = $this->connToFtp();
		if ($conn_id)
		{
			if (ftp_chdir($conn_id, "./".$fm_ID."/WORK"))
			{
				$contents = ftp_nlist($conn_id, ".");
				$folder = $this->createFolder($fm_ID);
				$folder = dir("$folder/process/");
				foreach ($contents as $remote_file)
				{
					$message = $message."-".$remote_file;

					if ($this->endsWith($remote_file, ".zip"))
					{
						$localZipFile = $folder->path.$remote_file;

						if (ftp_get($conn_id, $localZipFile, $remote_file, FTP_BINARY)) //download
						{
							$user_name = "apache";
							chmod($localZipFile, 0755);
							chown($localZipFile, $user_name);
							chgrp($localZipFile, $user_name);

							$result = false;
							$message = $message."-get";

							if ($this->unzip($folder, $remote_file))
							{
								$message = $message."-unzip";
								$xml_file = str_replace(".zip","", $remote_file);
								$tags = $this->getSpecificTag($folder, $xml_file, $fm_ID);
								if (count($tags) > 0)
								{
									$message = $message."-action:";
									foreach ($tags as $tag)
									{
										if ($fm_ID == "RS4")
										{
											$message = $message.$this->setRS4($tag);
										}
										else if ($fm_ID == "R08")
										{
											$message = $message.$this->setR08($tag);
										}
										else if ($fm_ID == "R96")
										{
											$message = $message.$this->setR96($tag);
										}
										else if ($fm_ID == "RS9")
										{
											$message = $message.$this->setRS9($tag);
										}
										else if ($fm_ID == "R89")
										{
											$message = $message.$this->setR89($tag);
										}
									}
								}
								else
								{
									$message = $message."-count:0";
								}
								$result = true;

							}
							else
							{
								$message = $message."-unzip(x)";
							}

							if ($result)
							{
								if (!ftp_rename($conn_id, $remote_file, "../OK/".$remote_file))
								{
									$this->logError("Move to OK Folder Failed:".$remote_file);
									$message = $message."-ok(x)";
								}
								else
								{
									$message = $message."-ok";
								}
							}
							else
							{
								if (!ftp_rename($conn_id, $remote_file, "../ERR/".$remote_file))
								{
									$this->logError("Move to ERROR Folder Failed:".$remote_file);
									$message = $message."-error(x)";
								}
								else
								{
									$message = $message."-error";
								}
							}
						}
						else
						{
							$this->logError("Download File to Local Failed:".$localZipFile);
							$message = $message."-get(x)";
						}
					}
				}
			}
			else
			{
				$this->logError("Change FTP Dir Failed:".$ftp_folder_name);
				$message = $message."-chftpdir(x)";
			}

			ftp_close($conn_id);
		}
		return $message;
	}

	function saveStrToXML($ftp_folder_name, $local_folder_name, $xmlStr)
	{
		//precreate folders
		$folder = $this->createFolder($local_folder_name);
		//Send file to ftp
		$datenow   = date("YmdHi", strtotime('+8 hours'));
		$file_name = $ftp_folder_name."DFM893" . $datenow . ".XML";
		$process_folder=dir("$folder/process/");
		if(!is_file($process_folder->path.$file_name)){
			$handle=fopen($process_folder->path.$file_name, "x+");
		}else{
			//Get Node
			$newXml = new DOMDocument('1.0', 'utf-8');
			$newXml->loadXML($xmlStr);
			$newElement = $newXml->getElementsByTagName($ftp_folder_name)->item(0);
			//Load XML
			$doc = new DOMDocument('1.0', 'utf-8');
			$doc->load($process_folder->path.$file_name);
			//Append Node
			$node = $doc->importNode($newElement, true);
			$docElement = $doc->getElementsByTagName("BODY")->item(0);
			$docElement->appendChild($node);
			//Item Count
			$RDCNT = $doc->getElementsByTagName("RDCNT")->item(0);
			$RDCNT->nodeValue = $doc->getElementsByTagName($ftp_folder_name)->length;
			//Order Amount
			/*
			$AMT = $doc->getElementsByTagName("AMT")->item(0);
			$amountAry = array();
			$order_amounts = $doc->getElementsByTagName("OrderAmount");
			foreach ($order_amounts as $order_amount) {
				$amount = $order_amount->nodeValue;
				array_push($amountAry, (int)$amount);
			}
			$AMT->nodeValue = array_sum($amountAry);
			*/
			//Save
			$xmlStr = $doc->saveXml();
			$handle=fopen($process_folder->path.$file_name, "w+");
		}
		fwrite($handle, $xmlStr);
		fclose($handle);
	}

	function uploadZipToFtp($ftp_folder_name, $folder)
	{
		$message = "";
		//Mage::log("[R02 uploadZipToFtp folder]".$ftp_folder_name.":".$folder, null, 'Logistic_Familymart_Info.log');
		$message = $message."(".$folder.")";

		$process_folder = dir("$folder/process/");
		$this->zipFile($process_folder);

		$files = scandir($process_folder->path);
		foreach ($files as $file)
		{
			if ($this->endsWith($file, ".zip"))
			{
				//upload file to familymart ftp
				$remote_file_name = $ftp_folder_name."/WORK/".$file;
				$this->putFileToFtp($folder, $file, $remote_file_name);
				$message = $message."[".$file."]";
				//Mage::log("[R02 uploadZipToFtp]".$file, null, 'Logistic_Familymart_Info.log');
			}
		}
		return $message;
	}

	function putFileToFtp($folder, $file_name, $remote_file_name)
	{
		$conn_id = $this->connToFtp();
		$result = true;
		if ($conn_id)
		{
			$process_path = $folder."/process/".$file_name;
			$finish_path = $folder."/finish/".$file_name;
			if(ftp_put($conn_id, $remote_file_name, $process_path, FTP_BINARY)){
				if (rename($process_path, $finish_path))
				{
					return true;
				}
				else
				{
					$result = false;
					$this->logError("Move File To Finsih Folder Failed:".$process_path.":".$finish_path);
					if (!ftp_delete($conn_id, $remote_file_name))
					{
						$this->logError("Delete File From FTP Failed (Because Move Failed):".$remote_file_name);
					}
				}
			}
			else
			{
				$this->logError("Put File To FTP Failed:".$process_path.":".$remote_file_name);
				$result = false;
			}
		}
		else
		{
			$result = false;
		}

		ftp_close($conn_id);
		return $result;
	}

	function createFolder($folderName)
	{
		$date=date("Ymd", strtotime('+8 hours'));
		return $this->createDateFolder($folderName, $date);
	}

	function createDateFolder($folderName, $date)
	{
		Mage::helper('common/common')->chdirToMageBase();

		//Mage::log("[createDateFolder]".getcwd().":".$folderName.":".$date, null, 'Logistic_Familymart_Info.log');

		$user_name = "apache";

		$folder = "logistic_fm";
		if(!is_dir("$folder")){
			mkdir("$folder");
			chmod($folder, 0755);
			chown($folder, $user_name);
			chgrp($folder, $user_name);
		}
		$folder="$folder/".$folderName;
		if(!is_dir("$folder")){
			mkdir("$folder");
			chmod($folder, 0755);
			chown($folder, $user_name);
			chgrp($folder, $user_name);
		}
		$folder="$folder/$date";
		if(!is_dir("$folder")){
			mkdir("$folder");
			chmod($folder, 0755);
			chown($folder, $user_name);
			chgrp($folder, $user_name);
		}
		if (!is_dir("$folder/process"))
		{
			mkdir("$folder/process");
			chmod("$folder/process", 0755);
			chown("$folder/process", $user_name);
			chgrp("$folder/process", $user_name);
		}
		if (!is_dir("$folder/finish"))
		{
			mkdir("$folder/finish");
			chmod("$folder/finish", 0755);
			chown("$folder/finish", $user_name);
			chgrp("$folder/finish", $user_name);
		}

		return $folder;
	}

	function unzip($folder, $file)
	{
		$zippassword = Mage::getStoreConfig('carriers/familymart/zippassword', false);
		$result = false;
		$currentDir = getcwd();
		chdir($folder->path);

		if ($this->endsWith($file, ".zip"))
		{
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') { //Windows
				$cmd = "c:\\Progra~1\\7-Zip\\7z.exe e -p$zippassword -y $file";
			} else { //non-Windows
				$cmd = "unzip -o -P $zippassword $file";
			}
			$outputs = null;
			exec($cmd, $outputs);

			$xml_file = str_replace(".zip","", $file);
			$outputStr = implode(",",$outputs);
			if (strpos($outputStr,"Fail") === false && is_file($xml_file) && filesize($xml_file) > 0)
			{
				//Move Zip to Finish
				$targetPath = "../finish/".$file;
				if (!rename($file, $targetPath))
				{
					$this->logError("Move ZIP to Finish Folder Failed:".$file.":".$targetPath);
				}
				else
				{
					$result = true;
				}
			}
			else
			{
				$this->logError("Unzip Failed:".$cmd.":".$outputStr);
			}
		}
		chdir($currentDir);
		return $result;
	}

	function zipFile($folder)
	{
		//zip with password
		$zippassword = Mage::getStoreConfig('carriers/familymart/zippassword', false);
		$files = scandir($folder->path);
		$currentDir = getcwd();
		chdir($folder->path);
		foreach ($files as $file)
		{
			if ($this->endsWith($file, ".XML"))
			{
				$datenow   = date("YmdHi", strtotime('+8 hours'));
				$datefile =  substr($file, 9, 12);
				$minDiff = $this->minDiff($datefile, $datenow);
				if ($minDiff > 0) //one hour ago
				{
					$zipfile = $file.".zip";
					if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') { //Windows
						$cmd = "c:\\Progra~1\\7-Zip\\7z.exe a -r -p$zippassword $zipfile $file";
					} else { //non-Windows
						$cmd = "zip -P $zippassword $zipfile $file";
					}
					exec($cmd);

					if (is_file($zipfile))
					{
						//Move Xml to Finish
						$targetPath = "../finish/".$file;
						if (!rename($file, $targetPath))
						{
							$this->logError("Move XML to Finish Folder Failed:".$file.":".$targetPath);
							//Delete Unzip File
							if (unlink($zipfile))
							{
								$this->logError("Delete Zip File Successful:".$zipfile);
							}
							else
							{
								$this->logError("Delete Zip File Failed:".$zipfile);
							}
						}
					}
					else
					{
						$this->logError("Zip Failed:".$cmd);
					}
				}
			}
		}
		chdir($currentDir);
	}

	function endsWith($haystack, $needle)
	{
		$haystack = strtoupper($haystack);
		$needle =  strtoupper($needle);
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}

		return (substr($haystack, -$length) === $needle);
	}

	function createXml($xmlInput) {
		$datenow  = date("Y-m-d", strtotime('+8 hours'));
		$shipDateStr = date("Y-m-d", strtotime('+3 day 8 hours'));

		$doc = new DOMDocument('1.0', 'utf-8');
		$doc->formatOutput = true;

		$root = $doc->createElement('doc');
		$header = $doc->createElement('HEADER');
		$RDFMT = $doc->createElement('RDFMT', '1');
		$SNCD  = $doc->createElement('SNCD', '893'); //TODO: config file
		$PRDT  = $doc->createElement('PRDT', $datenow);
		$header->appendChild($RDFMT);
		$header->appendChild($SNCD);
		$header->appendChild($PRDT);

		$body = $doc->createElement('BODY');
		$RDFMT = $doc->createElement('RDFMT', '2');
		$R02   = $doc->createElement($xmlInput->fm_ID);
		$ParentId            = $doc->createElement('ParentId', '893'); //TODO: config file
		$EshopId             = $doc->createElement('EshopId', '0001');
		$OPMode              = $doc->createElement('OPMode', '1');
		$BuyerPO             = $doc->createElement('BuyerPO', $xmlInput->order_id);
		$EshopOrderDate      = $doc->createElement('EshopOrderDate', $datenow);
		$ServiceType         = $doc->createElement('ServiceType', '3');
		$ShopperName         = $doc->createElement('ShopperName', mb_substr($xmlInput->order_receiver, 0, 15, 'UTF-8')); //30 bytes
		$ShopperPhone        = $doc->createElement('ShopperPhone');
		$ShopperMobilePhone  = $doc->createElement('ShopperMobilePhone', substr($xmlInput->order_receiver_cellphone, 0, 20));
		$ShopperEmail        = $doc->createElement('ShopperEmail');
		$SutkNm              = $doc->createElement('SutkNm', mb_substr($xmlInput->store_name, 0, 10, 'UTF-8')); //20 bytes
		$SutkTl              = $doc->createElement('SutkTl');
		$SutkZe              = $doc->createElement('SutkZe');
		$SutkAs              = $doc->createElement('SutkAs');
		$ReceiverName        = $doc->createElement('ReceiverName', mb_substr($xmlInput->order_receiver, 0, 15, 'UTF-8')); //30 bytes
		$ReceiverPhone       = $doc->createElement('ReceiverPhone',substr($xmlInput->order_receiver_telephone,0,20));
		$ReceiverMobilePhone = $doc->createElement('ReceiverMobilePhone', substr($xmlInput->order_receiver_cellphone, 0, 20));
		$ReceiverEmail       = $doc->createElement('ReceiverEmail');
		$ReceiverZipCode     = $doc->createElement('ReceiverZipCode');
		$ReceiverAddress     = $doc->createElement('ReceiverAddress');
		$SellerName          = $doc->createElement('SellerName',  mb_substr($xmlInput->store_name, 0, 15, 'UTF-8')); //30 bytes
		$OrderAmount         = $doc->createElement('OrderAmount', $xmlInput->totalPrice);
		$ProductId           = $doc->createElement('ProductId');
		$ProductName         = $doc->createElement('ProductName', mb_substr($xmlInput->productName, 0, 15, 'UTF-8')); //30 bytes
		$Quantity            = $doc->createElement('Quantity');
		$ShipmentNo          = $doc->createElement('ShipmentNo', $xmlInput->order_id);
		$ShipDate            = $doc->createElement('ShipDate', $shipDateStr);
		$ReturnDate          = $doc->createElement('ReturnDate');
		$ReturnStoreId       = $doc->createElement('ReturnStoreId');
		$LastShipment        = $doc->createElement('LastShipment');
		$ShipmentAmount      = $doc->createElement('ShipmentAmount', "0");
		$StoreId             = $doc->createElement('StoreId', substr($xmlInput->fmstorecode, 1));
		$ProdNm              = $doc->createElement('ProdNm', "0");
		$DisType             = $doc->createElement('DisType', "1");
		$Invoice             = $doc->createElement('Invoice');
		$BagNum              = $doc->createElement('BagNum');
		$Weight              = $doc->createElement('Weight');
		$PackageFlag         = $doc->createElement('PackageFlag');
		$R02->appendChild($ParentId);
		$R02->appendChild($EshopId);
		$R02->appendChild($OPMode);
		$R02->appendChild($BuyerPO);
		$R02->appendChild($EshopOrderDate);
		$R02->appendChild($ServiceType);
		$R02->appendChild($ShopperName);
		$R02->appendChild($ShopperPhone);
		$R02->appendChild($ShopperMobilePhone);
		$R02->appendChild($ShopperEmail);
		$R02->appendChild($SutkNm);
		$R02->appendChild($SutkTl);
		$R02->appendChild($SutkZe);
		$R02->appendChild($SutkAs);
		$R02->appendChild($ReceiverName);
		$R02->appendChild($ReceiverPhone);
		$R02->appendChild($ReceiverMobilePhone);
		$R02->appendChild($ReceiverEmail);
		$R02->appendChild($ReceiverZipCode);
		$R02->appendChild($ReceiverAddress);
		$R02->appendChild($SellerName);
		$R02->appendChild($OrderAmount);
		$R02->appendChild($ProductId);
		$R02->appendChild($ProductName);
		$R02->appendChild($Quantity);
		$R02->appendChild($ShipmentNo);
		$R02->appendChild($ShipDate);
		$R02->appendChild($ReturnDate);
		$R02->appendChild($ReturnStoreId);
		$R02->appendChild($LastShipment);
		$R02->appendChild($ShipmentAmount);
		$R02->appendChild($StoreId);
		$R02->appendChild($ProdNm);
		$R02->appendChild($DisType);
		$R02->appendChild($Invoice);
		$R02->appendChild($BagNum);
		$R02->appendChild($Weight);
		$R02->appendChild($PackageFlag);
		$body->appendChild($RDFMT);
		$body->appendChild($R02);

		$header = $doc->createElement('HEADER');
		$RDFMT = $doc->createElement('RDFMT', '1');
		$SNCD  = $doc->createElement('SNCD', '893'); //TODO: config file
		$PRDT  = $doc->createElement('PRDT', $datenow);
		$header->appendChild($RDFMT);
		$header->appendChild($SNCD);
		$header->appendChild($PRDT);

		$footer = $doc->createElement('FOOTER');
		$RDFMT = $doc->createElement('RDFMT', '3');
		$RDCNT = $doc->createElement('RDCNT', '1');
		$AMT   = $doc->createElement('AMT', '0');
		$footer->appendChild($RDFMT);
		$footer->appendChild($RDCNT);
		$footer->appendChild($AMT);
		$root->appendChild($header);
		$root->appendChild($body);
		$root->appendChild($footer);

		$root = $doc->appendChild($root);

		$fstring = $doc->saveXml();

		return $fstring;
	}

	function minDiff($startTime, $endTime) {
		$start = strtotime($startTime);
		$end = strtotime($endTime);
		$timeDiff = $end - $start;
		return floor($timeDiff / 60);
	}

	function dateDiff($startTime, $endTime) {
		$start = strtotime($startTime);
		$end = strtotime($endTime);
		$timeDiff = $end - $start;
		return floor($timeDiff / (60 * 60 * 24));
	}

}
