<?php
class Familymart_LogisticFamilymart_FamilymartController extends Mage_Core_Controller_Front_Action{

    //Callback for familymart
    //Sample QueryString:
    //cvsid=1111&cvsspot=F002770&cvstemp=&cvsname=www%2Eigood%2Etw&name=%A5%FE%AEa%AAO%BE%F4%B7s%A5%C1%A9%B1&tel=02%2D22570742&addr=%B7s%A5%5F%A5%AB%AAO%BE%F4%B0%CF%A4%E5%A4%C6%B8%F4%A4%40%ACq188%AB%D142%B8%B91%BC%D3&cvsnum=WN10
    public function indexAction() {
        $this->loadLayout();
        $block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'familymart_block_name',
            array('template' => 'settv/logistic_familymart.phtml')
        );
        
        $str  = $this->getRequest()->getParam("name");

        $data = array(
            "cvsspot" => $this->getRequest()->getParam('cvsspot'),
            "name"    => iconv('big-5', 'utf-8', $this->getRequest()->getParam('name')),
            "addr"    => iconv('big-5', 'utf-8', $this->getRequest()->getParam('addr'))
        );
        $block->setData("data", $data);

        $this->getLayout()->getBlock('content')->append($block);
        $this->renderLayout();
    }
}
?>