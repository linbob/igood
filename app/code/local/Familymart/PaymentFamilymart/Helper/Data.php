<?php
class Familymart_PaymentFamilymart_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getPaymentEndTime($order)
	{
		$order_created_at = $order->getCreatedAt();
		$dateTime = new DateTime($order_created_at);
		$dateTime->add(date_interval_create_from_date_string('8 hours'));
		$dateTime->add(date_interval_create_from_date_string('3 days'));
		return $dateTime->format("Y-m-d H:i:s");
	}
}
