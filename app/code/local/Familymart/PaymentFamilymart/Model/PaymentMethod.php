<?php

class Familymart_PaymentFamilymart_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract {
    protected $_code = 'paymentfamilymart';
    protected $_formBlockType = 'paymentfamilymart/dispfields';

	public function assignData($data) {
        $session = Mage::getSingleton('customer/session');
        if (!is_null($session)) {
            $session->setData('order_state', 'pending_payment');
        }
        
		
		
		if (!($data instanceof Varien_Object)) {
			$data = new Varien_Object($data);
		}
		$info = $this->getInfoInstance();
		$info->setEtixType($data->getEtixType());

		return $this;
    }
	
	public function getCheckoutRedirectUrl() {
		$redirectUrl = Mage::getUrl("paymentfamilymart/familymartprint");
		
		return $redirectUrl;
	}
}
