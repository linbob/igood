<?php

class Familymart_PaymentFamilymart_Model_ScanOrder {
	public function run() {
		return($this->TransFamilyMartFailed());
	}

	public function TransFamilyMartFailed()
	{
		$success_orderids = array();
		$failed_orderids  = array();

		$timeoffset = strtotime(Mage::getStoreConfig('payment/paymentfamilymart/expired', false));
		$str = date("Y-m-d H:i:s", $timeoffset);

		$OrderIds = Mage::getResourceModel('sales/order_collection')
		->addFieldToSelect('entity_id')
		->addFieldToSelect('increment_id')
		->addFieldToFilter('state', 'pending_payment')
		->addFieldToFilter('created_at', array('lt' => $str));

		$OrderIds->getSelect()
		->where("method='paymentfamilymart'")
		->joinLeft('sales_flat_order_payment', 'main_table.entity_id = sales_flat_order_payment.parent_id',array('method'));

		$OrderIds->getData();

		foreach($OrderIds as $order_Id){
			$lidm  = $order_Id['increment_id'];
			$orderid = $order_Id['entity_id'];
			$order = Mage::getModel('sales/order')->load($orderid);
			$paymentCode = $order->getPayment()->getMethodInstance()->getCode();
			$this->TransFailedHandler($orderid);
			array_push($failed_orderids, $lidm);
		}
		return("[FamilyMart]Failed id(s):".implode(",", $failed_orderids).";");
	}
	
	public function TransSuccessHandler($inOderId)
	{
		$order = Mage::getModel('sales/order')->load($inOderId);
		$order->sendNewOrderEmail();
		$order->setState('established', true, '')->save();
	
		//Send mail to supplier
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		Mage::helper('common/common')->emailSupplierByOrder($order, 'New Order to Supplier', $emailTemplateVariables);
	}

	public function TransFailedHandler($inOderId, $state = 'canceled')
	{
		$order = Mage::getModel('sales/order')->load($inOderId);
		$paymentCode = $order->getPayment()->getMethodInstance()->getCode();

		$order->setState($state, true, '')->save();

		$orderHelper = Mage::helper('common/order');
		$orderHelper->addItemBackToProductInventory($order);

		//Send failed mail
		$email_address = $order->getCustomerEmail();
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$emailTemplateVariables['order_id'] = $order->getIncrementId();
		$items1 = $order->getAllItems();
		foreach ($items1 as $itemId1 => $item1) {
			$emailTemplateVariables['productname'] = $item1->getName();
			$emailTemplateVariables['qty']=$item1->getQtyOrdered()*1;
		}

		Mage::helper('common/common')->sendEmail('Familymart payment failed', $email_address, $emailTemplateVariables);
	}
}