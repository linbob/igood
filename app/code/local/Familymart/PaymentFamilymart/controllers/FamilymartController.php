<?php
class Familymart_PaymentFamilymart_FamilymartController extends Mage_Core_Controller_Front_Action{
	public function indexAction(){
        ob_start();
        var_dump($_POST);
        $text = ob_get_clean();
        $enc  = mb_detect_encoding($text, "UTF-8,big-5");
        $payload = iconv($enc, "UTF-8", $_POST["d"]);

        $xml = simplexml_load_string($payload);
        $statcode = trim($xml->HEADER->STATCODE);
        $order_no = trim($xml->AP->ORDER_NO);        
        $result = $statcode;
        
        $master_order_id = $order_no;
        $orders = Mage::getModel("splitorder/items")->getOrdersByMasterOrderId($master_order_id);
        
        if ($statcode === "0000") {
            //SUCCESS
        	foreach ($orders as $order)
        	{
            	Mage::getModel("paymentfamilymart/scanOrder")->TransSuccessHandler($order->getId());            	
        	}            
        	
        } else {
        	Mage::log("FamilymartController@statcode:$statcode", null, 'Familymart_PaymentFamilymart.log');
            //Failed
        	foreach ($orders as $order)
        	{
        		Mage::getModel("paymentfamilymart/scanOrder")->TransFailedHandler($order->getId());
        	}
        }
        
        $this->getResponse()->setBody($result);
        return;
	}	
}
?>
