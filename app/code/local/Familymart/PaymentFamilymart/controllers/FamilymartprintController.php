<?php
class Familymart_PaymentFamilymart_FamilymartprintController extends Mage_Core_Controller_Front_Action {
	public function indexAction() {
		$ExternalLibPath = Mage::getModuleDir('', 'Familymart_PaymentFamilymart') . DS . 'lib' . DS .'FamilymartPort.php';
		require_once ($ExternalLibPath);

		$master_order_id = Mage::getSingleton('checkout/session')->getLastMasterOrderId();
		//Remove last real order id, flagging this page, in order to prevent multiple form request
		Mage::getSingleton('checkout/session')->setLastMasterOrderId();
		if (empty($master_order_id)) { //Send user back to home page
			$url = Mage::getBaseUrl();
			Mage::app()->getFrontController()->getResponse()->setRedirect($url);
			return;
		}

		//1. __init__
		$params = new FamilymartPort();

		$dateTimeZoneTaipei   = new DateTimeZone("Asia/Taipei");
		$dateTimeTaipei       = new DateTime("now", $dateTimeZoneTaipei);

		$params->url          = Mage::getStoreConfig('payment/paymentfamilymart/posturl', false); //EC Web Service
		$params->m_date       = $dateTimeTaipei->format("Ymd");; //日期 YYYYMMDD
		$params->m_time       = $dateTimeTaipei->format("His"); //時間 HHMMSS
		$dateTimeTaipei->add(date_interval_create_from_date_string('3 days'));
		$params->m_endDate    = $dateTimeTaipei->format("Ymd");; //日期 YYYYMMDD
		$params->m_endTime    = $dateTimeTaipei->format("His"); //時間 HHMMSS

		$params->m_payType    = htmlspecialchars("現金"); //繳款類別
		$params->m_payCompany = htmlspecialchars(Mage::getStoreConfig('payment/paymentfamilymart/ppaycompany', false)); //付款廠商
		$params->m_tradeType  = "1";     //1:要號, 3:二次列印
		$params->m_desc1      = "";      //備註1
		$params->m_desc2      = "";      //備註2
		$params->m_desc3      = "";      //備註3
		$params->m_desc4      = "";      //備註4
		$params->m_pinCode    = "";

		$params->m_taxID      = Mage::getStoreConfig('payment/paymentfamilymart/ptaxid', false);
		$params->m_termino    = Mage::getStoreConfig('payment/paymentfamilymart/ptermino', false);
		$params->m_accountNo  = Mage::getStoreConfig('payment/paymentfamilymart/paccountno', false);  //帳號
		$params->m_password   = Mage::getStoreConfig('payment/paymentfamilymart/ppassword', false);   //密碼

		$master_order = Mage::getModel('splitorder/orders')->loadById($master_order_id);
		$ordertotal = round($master_order->getGrandTotal(), 0);

		/* ichannels */
	if ($_COOKIE['ichannelsGid']) {
	   $gid = Mage::getModel('core/cookie')->get('ichannelsGid');
	   $helper = Mage::helper('push');
	   $helper->index($gid, $master_order_id, $ordertotal);
	}

		$params->m_orderNo    = $master_order_id;
		$params->m_amount     = $ordertotal;
		$params->m_prdDesc    = ""; //htmlspecialchars($items[0]->getName());    //TODO:商品簡述

		//2. Get PINCODE for printing
		$response = $params->getResponseFromFamilyPort();
		$orders = Mage::getModel("splitorder/items")->getOrdersByMasterOrderId($master_order_id);

		if (empty($response))
		{
			$this->failureHandler($master_order_id, $orders, $statuscode, $pincode);
		} 
		else 
		{
			$xml = simplexml_load_string($response);
			$xmlbody = $xml->children('soap', true)->Body->children();
			$statuscode = trim($xmlbody
					->NewOrderResponse
					->TX_WEB
					->HEADER
					->STATCODE
			);
			$pincode = trim($xmlbody
					->NewOrderResponse
					->TX_WEB
					->AP
					->PIN_CODE
			);
				
			if ($statuscode === "0000" && !empty($pincode))  //SUCCESS
			{
				foreach ($orders as $order)
				{
					//3. send user for printing pin code
					$customOrder = Mage::getModel('check/custom_order')->loadByOrderId($order->getId());
					$customOrder->setData('familymart_pincode', $pincode)->save();
				}

				$postUrl = Mage::getStoreConfig('payment/paymentfamilymart/barcodeprintposturl', false);

				$data = array(
						"postUrl"   => $postUrl,
						"accountNo" => $params->m_accountNo,
						"orderNo"   => $params->m_orderNo,
						"pincode"   => $pincode
				);

				$this->loadLayout();
				$this->getLayout()->getBlock("checkout.success")->setData("data", $data);
				$this->renderLayout();

			} 
			else 
			{
				$this->failureHandler($master_order_id, $orders, $statuscode, $pincode);
			}
		}
	}
	
	private function failureHandler($master_order_id, $orders, $statuscode, $pincode)
	{
		foreach ($orders as $order)
		{
			Mage::getModel("paymentfamilymart/scanOrder")->TransFailedHandler($order->getId(), 'failed');
		}
		
		if (empty($pincode)) { //The server didn't return anything at all
			Mage::log("Familymart Checkout Fail, Familymart didn't response." );
		} else {
			Mage::log("Familymart Checkout Fail, Master Order ID = ". $master_order_id .", Status Code = " .  $statuscode .", PinCode = ". $pincode );
		}
		
		$this->getResponse()->setRedirect(Mage::getUrl('checkout/onepage/failure')."order_id/".$master_order_id);
	}
}
?>
