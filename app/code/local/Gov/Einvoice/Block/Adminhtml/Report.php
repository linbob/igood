<?php
class Gov_Einvoice_Block_Adminhtml_Report extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_report';
        $this->_blockGroup = 'gov_einvoice';
        $this->_headerText = Mage::helper('goveinvoice')->__('Print Einvoice');
        parent::__construct();
        $this->_removeButton('add');
    }
}
