<?php
class Gov_Einvoice_Block_Adminhtml_Report_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('gov_einvoice_report_grid');
		$this->setDefaultSort('invoice_duration_end');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('einvoice/einvoices')->getCollection();
		$collection->getSelect()->joinLeft('settv_sales_order_custom as oc', 'main_table.order_id = oc.order_id', array('invoicetype', 'invoiceubn', 'invoicecom', 'order_id', 'invoice_donateorg'));
		$collection->getSelect()->joinLeft('sales_flat_order as o', 'oc.order_id = o.entity_id', array('increment_id', 'total_qty_ordered', 'grand_total', 'order_currency_code'));
		$collection->getSelect()->joinLeft('sales_flat_order_address as oa', 'main_table.order_id = oa.parent_id',array('firstname','postcode','city','region','street'))->where("oa.address_type = 'billing'");
		$collection->getSelect()->joinLeft('customer_entity as c', 'c.entity_id = o.customer_id',array('email'));
		$collection->getSelect()->joinLeft('gov_einvoice_source as i', 'i.invoice_id = main_table.invoice_id',array('invoice_number','invoice_duration_end'));
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('increment_id', array(
				'header'    => Mage::helper('sales')->__('Order #'),
				'index'     => 'increment_id',
				'type'      => 'text',
				'filter_index' => 'o.increment_id',
		));		
		
		$this->addColumn('invoice_duration_end', array(
				'header'    => Mage::helper('goveinvoice')->__('Invoice Time'),
				'index'     => 'invoice_duration_end'
		));

		$this->addColumn('invoice_number', array(
				'header'    => Mage::helper('goveinvoice')->__('Invoice Numer'),
				'index'     => 'invoice_number'
		));
		
		$this->addColumn('invoice_status', array(
				'header'    => Mage::helper('goveinvoice')->__('Invoice Status'),
				'index'     => 'invoice_status',
				'type'      => 'options',
				'options'   => array(0 => Mage::helper('goveinvoice')->__('Invoice Created'),1 => Mage::helper('goveinvoice')->__('Invoice Cancel')),
		));
		
		$this->addColumn('grand_total', array(
				'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
				'index' => 'grand_total',
				'filter_index' => 'o.grand_total',
				'type'  => 'currency',
				'currency' => 'order_currency_code',
		));		

		$this->addColumn('lottery_result', array(
				'header'    => Mage::helper('goveinvoice')->__('Lottery Result'),
				'index'     => 'lottery_result',
				'type'      => 'options',
				'options'   => array(1 => Mage::helper('adminhtml')->__('Yes'),0 => Mage::helper('adminhtml')->__('No')),
		));		
		
		
		$this->addColumn('user_request', array(
				'header'    => Mage::helper('goveinvoice')->__('User Request'),
				'index'     => 'user_request',
				'type'      => 'options',
				'options'   => array(1 => Mage::helper('adminhtml')->__('Yes'),0 => Mage::helper('adminhtml')->__('No')),
		));
		
		$this->addColumn('invoice_donateorg', array(
				'header'    => Mage::helper('goveinvoice')->__('Invoice Donateorg'),
				'index'     => 'invoice_donateorg',
		));

		/*
		$this->addColumn('invoiceubn', array(
				'header'    => Mage::helper('sales')->__('Invoice UBN'),
				'index'     => 'invoiceubn',
		));

		$this->addColumn('invoice_title', array(
				'header'=> Mage::helper('sales')->__('Invoice Title'),
				'type'  => 'text',
				'index' => 'invoicecom',
		));
		*/
		
		$this->addColumn('billing_name', array(
				'header'    => Mage::helper('sales')->__('Bill to Name'),
				'index'     => 'firstname',
				'type'      => 'text',
		));

		$this->addColumn('billing_address', array(
				'header'    => Mage::helper('sales')->__('Billing Address'),
				'index'     => 'street',
				'type'      => 'text',
				'renderer' => 'Settv_Common_Block_Render_RenderAddress',
				'filter_condition_callback' => array($this, '_addressFilter'),
		));

		$this->addColumn('email', array(
				'header'    => Mage::helper('customer')->__('Email'),
				'width'     => '150',
				'index'     => 'email'
		));
		
		
		$this->addColumn('action', array(
				'header'    =>  Mage::helper('goveinvoice')->__('Print Einvoice'),
				'width'     => '100',
				'type'      => 'action',
				'getter'    => 'getId',
				'actions'   => array(
						array(
								'caption'   => Mage::helper('goveinvoice')->__('Print Einvoice'),
								'url'       => array('base'=> '*/*/print'),
								'field'     => 'id',
								'target'	=>'_blank'
						)
				),
				'filter'    => false,
				'sortable'  => false,				
				'is_system' => true,
		));
		

		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return false;
	}

	public function getRefreshButtonHtml()
	{
		return $this->getChildHtml('refresh_button');
	}	
}
