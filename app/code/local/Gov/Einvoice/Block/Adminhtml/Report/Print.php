<?php
class Gov_Einvoice_Block_Adminhtml_Report_Print extends Mage_Adminhtml_Block_Template
{
	public function getEinvoiceObj()
	{
		$eid = $this->getRequest()->getParam('id');
		$einvoice = Mage::getModel("einvoice/einvoices")->load($eid);
		$einvoice_source = Mage::getModel("einvoice/sources")->load($einvoice->getInvoiceId());
		$order = Mage::getModel("sales/order")->load($einvoice->getOrderId());

		$company_name = Mage::getStoreConfig('gov/einvoice/sellername', false);
		$company_tax_id = $einvoice_source->getCompanyTaxId();
		$random_code = $einvoice->getInvoiceRandomCode();
		$AESKey = Mage::getStoreConfig('gov/einvoice/aeskey', false);

		$invoice_number = $einvoice_source->getInvoiceNumber();
		$barcode = $this->getBarcode($einvoice, $einvoice_source, $random_code);
		$qrcode = $this->getQrcode($einvoice, $einvoice_source, $order, $random_code, $AESKey);

		$year = substr($einvoice_source->getInvoiceDurationStart(), 0, 3);
		$start_month = substr($einvoice_source->getInvoiceDurationStart(), 3, 2);
		$end_month = substr($einvoice_source->getInvoiceDurationEnd(), 3, 2);

		$einvoiceObj = new Varien_Object();
		$einvoiceObj->setCompanyName($company_name);
		$einvoiceObj->setInvoiceNumer($invoice_number);
		$einvoiceObj->setYear($year);
		$einvoiceObj->setStartMonth($start_month);
		$einvoiceObj->setEndMonth($end_month);
		$einvoiceObj->setCreateDate($einvoice->getInvoiceDate());
		$einvoiceObj->setCreateTime($einvoice->getInvoiceTime());
		$einvoiceObj->setRandomCode($random_code);
		$einvoiceObj->setTotal(round($order->getGrandTotal(),0));
		$einvoiceObj->setCompanyTaxId($company_tax_id);
		$einvoiceObj->setBarcode($barcode);
		$einvoiceObj->setQrcode($qrcode);

		return $einvoiceObj;
	}

	private function getBarcode($einvoice, $einvoice_source, $random_code)
	{
		/*
		 一維條碼應以三九碼 (Code 39) 記載，記載事項應含發票期別、發票字軌號碼及隨機碼總計十九碼。詳細內容如下：
		1.發票期別 (5)：記錄發票開立當期後月之三碼民國年份二碼月份。
		2.發票字軌號碼 (10)：記錄發票完整十碼號碼。
		3.隨機碼 (4)：記錄發票上四碼隨機碼。
		*/
		$invoice_num = $einvoice->getInvoiceNumber();
		$end_month = $einvoice_source->getInvoiceDurationEnd();
		return $end_month.$invoice_num.$random_code;
	}

	private function getQrcode($einvoice, $einvoice_source, $order, $random_code, $AESKey)
	{
		$invoice_source = Mage::getModel("einvoice/sources")->load($einvoice->getInvoiceId());
		$invoice_num = $invoice_source->getInvoiceNumber();
		$invoice_date = Mage::Helper("goveinvoice")->getTaiwanDate($einvoice->getInvoiceDate());
		$invoice_amount = round($order->getGrandTotal(),0);
		$seller_Identifier = $einvoice_source->getCompanyTaxId();

		$qrcode = $this->getQRCodeINV($invoice_num, $invoice_date, null, $random_code
				, "00000000", null, $invoice_amount, "00000000"
				, null, $seller_Identifier, null, $AESKey);
		$productInfo = $this->getQrCodeAdditionInfo($order);

		return "$qrcode:$productInfo";
	}

	private function getQRCodeINV($InvoiceNumber, $InvoiceDate, $InvoiceTime, $RandomNumber
			, $SalesAmount, $TaxAmount, $TotalAmount, $BuyerIdentifier
			, $RepresentIdentifier, $SellerIdentifier, $BusinessIdentifier, $AESKey)
	{
		$TotalAmount = str_pad(dechex(round($TotalAmount, 0)), 8, "0", STR_PAD_LEFT);
		$edata = $this->getEncrypt($InvoiceNumber, $RandomNumber, $AESKey);

		//(一) 左方二維條碼記載事項：
		if (strlen($InvoiceNumber) != 10) //$InvoiceNumber 發票字軌 (10位)：記錄發票完整十碼號碼。
			throw new Exception("InvoiceNumber: $InvoiceNumber incorrent");
		if (strlen($InvoiceDate) != 7) //發票開立日期 (7位)：記錄發票三碼民國年、二碼月份、二碼日期。
			throw new Exception("InvoiceDate: $InvoiceDate incorrent");
		if (strlen($RandomNumber) != 4)// 隨機碼 (4位)：記錄發票上隨機碼四碼。
			throw new Exception("RandomNumber: $RandomNumber incorrent");
		if (strlen($SalesAmount) != 8) //$SalesAmount 銷售額 (8位)：記錄發票上未稅之金額總計八碼，將金額轉換以十六進位方式記載。若營業人銷售系統無法順利將稅項分離計算，則以00000000記載。
			throw new Exception("SalesAmount: $SalesAmount incorrent");
		if (strlen($TotalAmount) != 8) //$TotalAmount 總計額 (8位)：記錄發票上含稅總金額總計八碼，將金額轉換以十六進位方式記載。
			throw new Exception("TotalAmount: $TotalAmount incorrent");
		if (strlen($BuyerIdentifier) != 8) //$BuyerIdentifier 買方統一編號 (8位)：記錄發票上買受人統一編號，若買受人為一般消費者則以 00000000記載。
			throw new Exception("BuyerIdentifier: $BuyerIdentifier incorrent");
		if (strlen($SellerIdentifier) != 8)// 賣方統一編號 (8位)：記錄發票上賣方統一編號。
			throw new Exception("SellerIdentifier: $SellerIdentifier incorrent");
		if (empty($AESKey)) //加密驗證資訊 (24位)：將發票字軌十碼及隨機碼四碼以字串方式合併後使用 AES 加密並採用 Base64 編碼轉換。
			throw new Exception("AESKey empty");
		if (strlen($edata) != 24)
			throw new Exception("Encrypted incorrent");
			
		/*以上欄位總計77碼。下述資訊為接續以上資訊繼續延伸記錄，且每個欄位前皆以間隔符號“:”(冒號)區隔各記載事項，若左方二維條碼不敷記載，則繼續記載於右方二維條碼。*/

		return $InvoiceNumber.$InvoiceDate.$RandomNumber.$SalesAmount.$TotalAmount.$BuyerIdentifier.$SellerIdentifier.$edata;
	}

	private function getQrCodeAdditionInfo($order)
	{
		$customize_area = "**********"; //營業人自行使用區 (10位)：提供營業人自行放置所需資訊，若不使用則以10個“*”符號呈現。
		$product_invoice_count = 1; //二維條碼記載完整品目筆數：記錄左右兩個二維條碼記載消費品目筆數，以十進位方式記載。
		$product_item_count = 1; //該張發票交易品目總筆數：記錄該張發票記載總消費品目筆數，以十進位方式記載。
		$product_item_encode = 1; //中文編碼參數 (1 位)：定義後續資訊的編碼規格，若以：	(1) Big5 編碼，則此值為0	(2) UTF-8 編碼，則此值為1	(3) Base64 編碼，則此值為2

		$product_name = ""; //品名：商品名稱，請避免使用間隔符號“:”(冒號)於品名。
		$product_count = ""; //數量：商品數量，在中文編碼前，以十進位方式記載。
		$product_price = ""; //單價：商品單價，在中文編碼前，以十進位方式記載。
		$addtional_info; //補充說明：非必要使用資訊，營業人可自行選擇是否運用，於左右兩個二維條碼已記載所有品目資訊後，始可使用此空間。長度不限。

		$items = $order->getAllItems();
		$product_str = "";
		$i = 0;
		foreach ($items as $itemId => $item)
		{
			if ($i == 0)
			{
				$product_name = utf8_encode(str_replace(":","-",$item->getName()));
				$product_count= $item->getQtyToInvoice();
				$product_price= round($item->getPrice(), 0);
				$product_str = 	"$product_name:$product_count:$product_price";
			}
			$i++;
		}

		return "$customize_area:$product_invoice_count:$product_item_count:$product_item_encode:$product_str";
	}

	private function getEncrypt($invoice_number, $random_number, $AESKey)
	{
		$ivKey = Mage::getStoreConfig('gov/einvoice/ivkey', false);

		$key = pack('H*', $AESKey);
		$iv = pack('H*', $ivKey);

		$encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128
				, $key, $invoice_number.$random_number.chr(2).chr(2), MCRYPT_MODE_CBC
				, $iv);

		$edata = base64_encode($encrypted);

		return $edata;
	}

}
