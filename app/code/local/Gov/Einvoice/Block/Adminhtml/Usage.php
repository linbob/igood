<?php
class Gov_Einvoice_Block_Adminhtml_Usage extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_usage';
        $this->_blockGroup = 'gov_einvoice';
        $this->_headerText = Mage::helper('goveinvoice')->__('Einvoice Usage');
        parent::__construct();
        $this->_removeButton('add');
        
        /*
        $this->_addButton('upload_einvoice', array(
        		'label' => $this->__('Upload Einvoice List'),
        		'onclick' => 'setLocation(\'' . $this->getUrl('einvoice/usage/upload') . '\')',
        ));
        */
    }
}
