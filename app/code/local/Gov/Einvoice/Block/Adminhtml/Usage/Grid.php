<?php
class Gov_Einvoice_Block_Adminhtml_Usage_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('gov_einvoice_usage_admihtml_grid');
		$this->setDefaultSort('invoice_number');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('einvoice/sources')->getCollection();		
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{

		$this->addColumn('invoice_number', array(
				'header'    => Mage::helper('goveinvoice')->__('Invoice Numer'),				
				'index'     => 'invoice_number',
				'width'		=> 200
		));
		
		$this->addColumn('invoice_duration_start', array(
				'header'    => Mage::helper('goveinvoice')->__('Invoice Start Month'),				
				'index'     => 'invoice_duration_start',				
				"type"		=> 'number',			
				'width'		=> 200
		));
		
		$this->addColumn('invoice_duration_end', array(
				'header'    => Mage::helper('goveinvoice')->__('Invoice End Month'),
				'index'     => 'invoice_duration_end',
				"type"		=> 'number',
				'width'		=> 200
		));
		
		$this->addColumn('invoice_usage', array(
				'header'    => Mage::helper('goveinvoice')->__('Invoice Usage'),
				'index'     => 'invoice_usage',	
				'type'      => 'options',
				'options'   => array(1 => Mage::helper('adminhtml')->__('Yes')
						,0 => Mage::helper('adminhtml')->__('No')),
				'width'		=> 200
		));
		
		$this->addColumn('invoice_create_at', array(
				'header'    => Mage::helper('goveinvoice')->__('Create Date'),
				'index'     => 'invoice_create_at',
				'type'		=> 'datetime',
		));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return false;
	}

	public function getRefreshButtonHtml()
	{
		return $this->getChildHtml('refresh_button');
	}
}
