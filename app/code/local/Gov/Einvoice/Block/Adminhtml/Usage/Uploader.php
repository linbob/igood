<?php

class Gov_Einvoice_Block_Adminhtml_Usage_Uploader extends Mage_Adminhtml_Block_Media_Uploader
{
	public function __construct()
	{
		parent::__construct();
		$this->getConfig()->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/*/upload'));
		$this->getConfig()->setFileField('file');		
		$this->getConfig()->setFilters(array(
				'csv' => array(
						'label' => 'csv',
						'files' => array('*.csv')
				)
		));		
	}
}
