<?php
class Gov_Einvoice_Helper_Data extends Mage_Core_Helper_Abstract {

	public function getTaiwanDate($ad_date) //yyyyMMdd or yyyyMM
	{
		$ad_year = substr($ad_date, 0, 4);
		$month = "";
		$day = "";

		if (strlen($ad_date) >= 6)
			$month = substr($ad_date, 4, 2);

		if (strlen($ad_date) >= 8)
			$day = substr($ad_date, 6, 2);

		$taiwan_year = (intval($ad_year) - 1911);
		$taiwan_date = $taiwan_year.$month.$day;

		return $taiwan_date;
	}

	public function getTaipeiNow()
	{
		date_default_timezone_set("UTC");
		$now = strtotime("+8 hours", time());
		return $now;
	}

	public function getNow()
	{
		date_default_timezone_set("UTC");
		return time();
	}

	public function createInvoice($order)
	{	
		$invoice_existed = $this->checkInvoiceExisted($order);

		if (!$invoice_existed)
		{
			$order_id = $order->getId();
			$custom_order = Mage::getModel('check/custom_order')->loadByOrderId($order_id);
			$invoice_type = $custom_order->getInvoicetype();
			//Gov einvoice
			if($invoice_type == "5" || $invoice_type == "4" )
			{
				if(Mage::helper('goveinvoice/message')->C0401_Action($order))
				{
					Mage::log ( "[C0401] Create einvoice C0401 action SUCCESS.  Order IncrementId:" . $order->getIncrementId (),null, 'einvoice.log' );
				}
				else
				{
					Mage::log ( "[C0401] Create einvoice C0401 action FAIL.  Order IncrementId:" . $order->getIncrementId (),null, 'einvoice.log' );
				}
			}
		}		
	}
	
	private function checkInvoiceExisted($order)
	{
		$order_id = $order->getId();
		$einvoice_id = Mage::getModel("einvoice/einvoices")->loadByOrderId($order_id)->getId();
		if ($einvoice_id)
		{
			Mage::log ("Einvoice existed:".$order_id.":".$einvoice_id, null, 'einvoice.log');
			return true;
		}
		else
		{
			$parent_id = $order->getRelationParentId();
			if ($parent_id)
			{
				$parent_order = Mage::getModel("sales/order")->load($parent_id);
				return $this->checkInvoiceExisted($parent_order);
			} 
			else			
				return false; //no parent order
		}
	}

}