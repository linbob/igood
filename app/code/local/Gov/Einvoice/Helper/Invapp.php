<?php
class Gov_Einvoice_Helper_Invapp extends Mage_Core_Helper_Abstract {

	public function isValidBarCode($barCode)
	{		
		$host = Mage::getStoreConfig('gov/einvoice/invappurl', false);
		$method = 'BIZAPIVAN/biz?';
		$version = "1.0";
		$action = "bcv";
		$barCode = urlencode($barCode);			
		$txID = $this->getRandomNumber(7);
		$appID = Mage::getStoreConfig('gov/einvoice/appid', false);
		
		$url = $host.$method.'version='.$version.'&action='.$action.'&barCode='.$barCode.'&TxID='.$txID.'&appId='.$appID;
				
		$options = array(
				'http' => array(
						'header'  => "Content-type: application/x-www-form-urlencoded",
						'method'  => 'GET',					
				),
		);				
		
		$context  = stream_context_create($options);
		$output = file_get_contents($url, false, $context);
		$jsonObj = json_decode($output);
				
		$response_code = $jsonObj->code;
	
		Mage::log ("Method - isValidBarCode() - url:".$url ,null,"invapp.log" );
		
		if ($response_code != "200")			
		{
			Mage::log ("Method - isValidBarCode() , response_code != 200 , barCode:".$barCode ,null,"invapp.log" );
			return false;			
		}							
					
		if($jsonObj->isExist == 'Y')
		{			
			return true;
		}
		else
		{			
			return false;
		}				
	}
	
	public function lottery()
	{
		$msg = "";
		$target_year_month = $this->getLastMonth();
		$winningList = $this->getWinningList($target_year_month);
		$lottery_list = $this->getLotteryList($target_year_month);

		foreach ($lottery_list as $invoice) {
			$isWinning = false;
			$lottery_title = null;
			
			$invoice_id = $invoice->getInvoiceId();
			$invoice_source = Mage::getModel("einvoice/sources")->load($invoice_id);			
			$invoice_number = $invoice_source->getInvoiceNumber();
					
			$lottery_wins = $this->checkWinning($winningList, $invoice_number, $lottery_title);
			
			if ($lottery_wins > 0)
			{
				$isWinning = true;
				$msg .= "($invoice_number:::$lottery_wins)";
				
				$this->sendWinningNotification($invoice_id, $lottery_title, $lottery_wins, $invoice_number);
			}
			
			Mage::getModel("einvoice/einvoices")->updateByLotteryResult($invoice_id, $isWinning, $lottery_title, $lottery_wins);					
		}
		
		return $msg;
	}
	
	private function sendWinningNotification($invoice_id, $lottery_title, $lottery_wins, $invoice_number)
	{	
		//load var
		$order_id = Mage::getModel("einvoice/einvoices")->loadByInvoiceId($invoice_id)->getOrderId();
		$order = Mage::getModel('sales/order')->load($order_id);
		$email_address = $order->getCustomerEmail();
		$billing =  $order->getBillingAddress();
		$billing_address = $billing->getData('postcode').' '.$billing->getData('city').$billing->getData('region').$billing->getData('street');
				
		//set var
		$emailTemplateVariables = array();
		$emailTemplateVariables['billing_address'] = $billing_address;
		$emailTemplateVariables['lottery_title'] = $lottery_title;
		$emailTemplateVariables['lottery_wins'] = $lottery_wins;
		$emailTemplateVariables['invoice_number'] = $invoice_number;
		
		Mage::log ("$invoice_number:::$lottery_title:::$lottery_wins:::$order_id:::$email_address", null, 'einvoice_mail_list.log' );		
		Mage::helper('common/common')->sendEmail('Einvoice Winning Notification', $email_address, $emailTemplateVariables);
	}
	
	private function getLastMonth() //return last yyyyMM
	{
		$now = Mage::Helper("goveinvoice")->getTaipeiNow();
		$now = strtotime("-1 months", $now);		
		$target_year_month = date("Ym", $now);		
		return $target_year_month;
	}
	
	private function checkWinning($winningList, $invoice_number, &$lottery_title)
	{
		if (strlen($invoice_number) != 10)
			throw new Exception("InvoiceNumber: $invoice_number incorrent");
		
		$invoice_number = substr($invoice_number, 2, 8); //get number
		
		$superPrizeNo = $winningList->superPrizeNo; //千萬特獎號碼;
		$spcPrizeNo = $winningList->spcPrizeNo; //特獎號碼
		$spcPrizeNo2 = $winningList->spcPrizeNo2; //特獎號碼
		$spcPrizeNo3 = $winningList->spcPrizeNo3; //特獎號碼2		
		$firstPrizeNo1 = $winningList->firstPrizeNo1; //頭獎號碼1
		$firstPrizeNo2 = $winningList->firstPrizeNo2; //頭獎號碼2
		$firstPrizeNo3 = $winningList->firstPrizeNo3; //頭獎號碼3
		$firstPrizeNo4 = $winningList->firstPrizeNo4; //頭獎號碼4
		$firstPrizeNo5 = $winningList->firstPrizeNo5; //頭獎號碼5
		$firstPrizeNo6 = $winningList->firstPrizeNo6; //頭獎號碼6
		$firstPrizeNo7 = $winningList->firstPrizeNo7; //頭獎號碼7
		$firstPrizeNo8 = $winningList->firstPrizeNo8; //頭獎號碼8
		$firstPrizeNo9 = $winningList->firstPrizeNo9; //頭獎號碼9
		$firstPrizeNo10 = $winningList->firstPrizeNo10; //頭獎號碼10
		$sixthPrizeNo1 = $winningList->sixthPrizeNo1; //六獎號碼1
		$sixthPrizeNo2 = $winningList->sixthPrizeNo2; //六獎號碼2
		$sixthPrizeNo3 = $winningList->sixthPrizeNo3; //六獎號碼3
		
		$superPrizeAmt = intval($winningList->superPrizeAmt); //千萬特獎金額		
		$spcPrizeAmt = intval($winningList->spcPrizeAmt); //特獎金額	
		
		$lottery_title = "";
		$lottery_wins = 0;
		
		if ($invoice_number == $superPrizeNo)
		{		
			$lottery_title = "千萬特獎";
			$lottery_wins = $superPrizeAmt;
		}
		else if ($invoice_number == $spcPrizeNo
			|| $invoice_number == $spcPrizeNo2
			|| $invoice_number == $spcPrizeNo3)
		{		
			$lottery_title = "特獎";
			$lottery_wins = $spcPrizeAmt;
		}
		else
		{	
			if ($lottery_wins == 0 && !empty($firstPrizeNo1))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo1, $lottery_title);			
			if ($lottery_wins == 0 && !empty($firstPrizeNo2))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo2, $lottery_title);
			if ($lottery_wins == 0 && !empty($firstPrizeNo3))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo3, $lottery_title);
			if ($lottery_wins == 0 && !empty($firstPrizeNo4))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo4, $lottery_title);
			if ($lottery_wins == 0 && !empty($firstPrizeNo5))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo5, $lottery_title);
			if ($lottery_wins == 0 && !empty($firstPrizeNo6))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo6, $lottery_title);
			if ($lottery_wins == 0 && !empty($firstPrizeNo7))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo7, $lottery_title);
			if ($lottery_wins == 0 && !empty($firstPrizeNo8))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo8, $lottery_title);
			if ($lottery_wins == 0 && !empty($firstPrizeNo9))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo9, $lottery_title);
			if ($lottery_wins == 0 && !empty($firstPrizeNo10))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number, $firstPrizeNo10, $lottery_title);
			
			$invoice_number_last_3 = substr($invoice_number, 5, 3);
			
			if ($lottery_wins == 0 && !empty($sixthPrizeNo1))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number_last_3, $sixthPrizeNo1, $lottery_title);
			if ($lottery_wins == 0 && !empty($sixthPrizeNo2))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number_last_3, $sixthPrizeNo2, $lottery_title);
			if ($lottery_wins == 0 && !empty($sixthPrizeNo3))
				$lottery_wins = $this->checkFirstPrize($winningList, $invoice_number_last_3, $sixthPrizeNo3, $lottery_title);			
		}
		
		return $lottery_wins;
	}
	
	private function checkFirstPrize($winningList, $invoice_number, $winning_no, &$lottery_title)
	{		
		$lottery_title = "";
		$lottery_wins = 0;
		
		$firstPrizeAmt = intval($winningList->firstPrizeAmt); //頭獎金額
		$secondPrizeAmt = intval($winningList->secondPrizeAmt); //二獎金額
		$thirdPrizeAmt = intval($winningList->thirdPrizeAmt); //三獎金額
		$fourthPrizeAmt = intval($winningList->fourthPrizeAmt); //四獎金額
		$fifthPrizeAmt = intval($winningList->fifthPrizeAmt); //五獎金額
		$sixthPrizeAmt = intval($winningList->sixthPrizeAmt); //六獎金額
		
		do
		{
			$invoice_len = strlen($invoice_number);
			
			//echo "checkFirstPrize:::$invoice_number:::$winning_no".PHP_EOL;
			
			if ($invoice_number == $winning_no)
			{	
				if ($invoice_len == 8)
				{
					$lottery_title = "頭獎";
					$lottery_wins = $firstPrizeAmt;
				}
				else if ($invoice_len == 7)
				{
					$lottery_title = "二獎";
					$lottery_wins = $secondPrizeAmt;
				}
				else if ($invoice_len == 6)
				{
					$lottery_title = "三獎";
					$lottery_wins = $thirdPrizeAmt;
				}
				else if ($invoice_len == 5)
				{
					$lottery_title = "四獎";
					$lottery_wins = $fourthPrizeAmt;
				}
				else if ($invoice_len == 4)
				{
					$lottery_title = "五獎";
					$lottery_wins = $fifthPrizeAmt;
				}
				else if ($invoice_len == 3)
				{
					$lottery_title = "六獎";
					$lottery_wins = $sixthPrizeAmt;
				}
				
				break;
			}
			$invoice_number = substr($invoice_number, 1, $invoice_len - 1);
			$winning_no = substr($winning_no, 1, strlen($winning_no)- 1);
			
		} while (strlen($invoice_number) >= 3);	
		
		return $lottery_wins;
	}

	private function getLotteryList($year_month) //yyyyMM, 只會有後月
	{
		$year = substr($year_month, 0, 4);
		$month_end = substr($year_month, 4, 2);
		$month_start = str_pad(intval($month_end)-1, 2, "0", STR_PAD_LEFT); //減一個月補0, 無跨年問題

		$collection = Mage::getModel("einvoice/einvoices")->getCollection();
		$collection->addFieldToFilter('lottery_result', array('null' => true));
		$collection->addFieldToFilter('invoice_date', array(
				array('like'=> $year.$month_start.'%'),
				array('like'=> $year.$month_end.'%')
		));

		return $collection;		
	}

	private function getWinningList($year_month) //yyyyMM
	{
		$host = Mage::getStoreConfig('gov/einvoice/invappurl', false);
		$url = $host."PB2CAPIVAN/invapp/InvApp";
		$version = "0.2";
		$action = "QryWinningList";
		$appID = Mage::getStoreConfig('gov/einvoice/appid', false);

		$invTerm = Mage::Helper("goveinvoice")->getTaiwanDate($year_month);

		$data = array('version' => $version  //版本號碼
				, 'action' => $action //API行為
				, 'invTerm' => $invTerm //查詢開獎期別，年分為民國年，月份必頇為雙數月
				, 'appID' => $appID //透過財政資訊中心大平台申請之軟體 ID
				, 'UUID' => null //行動工具 Unique ID
		);
		
		$qry = http_build_query($data);

		$options = array(
				'http' => array(
						'header'  => "Content-type: application/x-www-form-urlencoded",
						'method'  => 'POST',
						'content' => $qry,
				),
		);

		$context  = stream_context_create($options);
		$output = file_get_contents($url, false, $context);
		$jsonObj = json_decode($output);

		Mage::log ("$url:::$qry:::$output", null, 'einvoice_winning_list.log' );
		
		$response_code = $jsonObj->code;
		if ($response_code != "200")
			throw new Exception("get winning list error code: ".$response_code);

		return $jsonObj;
	}

	private function getRandomNumber($length)
	{
		$_num = '';
		for($j = 0; $j < $length; $j ++) {
			$_num .= mt_rand ( 0, 9 );
		}
		return $_num;
	}
}