<?php
class Gov_Einvoice_Helper_Message extends Mage_Core_Helper_Abstract {
	
	// 平台存證開立發票訊息
	public function C0401_Action($order) {
		$orderId = $order->getId ();
		$orderIncrementId = $order->getIncrementId ();
		$customOrderId = Mage::getModel ( 'check/custom_order' )->getIdByOrder ( $orderId );
		$customOrderModel = Mage::getModel ( 'check/custom_order' )->load ( $customOrderId );
		$invoice_id;
		
		if (is_null ( $orderId )) {
			$this->WriteErrorLog ( "[C0401] Can not find any Order by orderId:" . $orderId );
			return false;
		}
		
		$C0401Obj = new GovC0401Invoice ();
		
		$C0401Obj->setRandomNumber ( $this->GetRandomNumber ( 4 ) );
		
		$C0401Obj->setBuyerName ( $order->getBillingAddress ()->getName () );
		$items = $order->getAllItems ();
		
		if (count ( $items ) == 0) {
			$this->WriteErrorLog ( "[C0401] Can not find any Product by orderId:" . $orderId );
			return false;
		}
		
		$productItems = array ();
		$index = 0;
		foreach ( $items as $itemId => $item ) {
			$productItem = new ProductItem ();
			$productItem->setDescription ( $item->getName () );
			$productItem->setUnitPrice ( round ( $item->getPrice (), 0 ) );
			$productItem->setQuantity ( round ( $item->getQtyOrdered (), 0 ) );
			$productItem->setSequenceNumber ( $index );
			array_push ( $productItems, $productItem );
			$index ++;
		}
		
		$C0401Obj->setProductItemArray ( $productItems );
		$C0401Obj->setTotalAmount ( round ( $order->getGrandTotal (), 0 ) );
		
		$C0401Obj->setInvoiceType ( Mage::getStoreConfig('gov/einvoice/defaultInvoiceType', false));
							
		$hasCarrierId = !is_null( $customOrderModel->getinvoice_carriernum1 () );
		
		if ($hasCarrierId) {
			//  電子發票營業人應用 API 說明v1 1_(公告版).pdf - P4
			// 	a. 3J0002 - 手機條碼  ,  手機條碼格式 必須 以 "/" 開頭，總長度為8碼 e.g. "/RZR1DHQ" or "/E.N1-0Q"
			// 	b. 1K0001 - 悠遊卡
			// 	c. 2G0001 - iCash			
			// Phase 2014-Q1 , settv only support 手機條碼(3J0002). 
			$C0401Obj->setCarrierType("3J0002");
			$C0401Obj->setCarrierId1 ( $customOrderModel->getinvoice_carriernum1 () );
			$C0401Obj->setCarrierId2 ( $customOrderModel->getinvoice_carriernum2 () );
			//電子發票上傳標準規格MIG 3.1版.pdf - P76 , 若 CarrierId1 & CarrierId2  非空白，則紙本電子發票已列印註記必須為N。
			$C0401Obj->setPrintMark ( 'N' );
		} else {
			//電子發票上傳標準規格MIG 3.1版.pdf - P76 , PrintMark為 Y 時載具類別號碼(CarrierType),載具顯碼 ID(CarrierId1),載具隱碼 ID (CarrierId2)必須為空白，
			$C0401Obj->setPrintMark ( 'Y' );
		}				
		
		$isDonate = $customOrderModel->getinvoice_isdonate ();
		if ($isDonate) {
			//電子發票上傳標準規格MIG 3.1版.pdf - P76 , 是捐贈-> PrintMark 註記必為  N。
			$C0401Obj->setPrintMark ( 'N' );
			$C0401Obj->setDonateMark ( 1 );
			//電子發票上傳標準規格MIG 3.1版.pdf - P75 , 若捐贈註記為 1，發票捐贈對象不應為空白。
			$C0401Obj->setNPOBAN ( $customOrderModel->getinvoice_donateorgnum () );
		} else {
			$C0401Obj->setDonateMark ( 0 );
		}
		
		try {
			$invoiceNumber = Mage::getModel ( 'einvoice/sources' )->getNextInvoiceNumberWithLock ( $invoice_id );
			$C0401Obj->setInvoiceNumber ( $invoiceNumber );
		} catch ( Exception $e ) {
			$this->UpdateCreateInvoiceIsUploadStatus ( $orderId, false );
			$this->SendCreateInvoiceFailNotification ( $orderId );
			$this->WriteErrorLog ( "[C0401] Order Increment ID:" . $orderIncrementId . "  Can not get einvoice number from current einvoice source table.   Exception MSG:" . $e->getMessage () );
			return false;
		}
		
		$C0401xmlStr = $this->CreateC0401Xml ( $C0401Obj );
		
		$date = new DateTime ();
		
		Mage::getModel ( 'einvoice/einvoices' )->createNewEinvoice ( $orderId, $invoice_id, $C0401Obj->getInvoiceDate (), $C0401Obj->getInvoiceTime (), $C0401Obj->getInvoiceType (), $C0401Obj->getRandomNumber (), $C0401xmlStr );
		$folderPath = Mage::getStoreConfig ( 'gov/einvoice/turnkeyfolder', false ) . "B2CSTORAGE/C0401/SRC/";
		$this->CreateFolderIfNotExist ( $folderPath );
		$milliseconds = round ( microtime ( true ) * 1000 );
		usleep ( 2000 );
		$xmlfilePath = $folderPath . date ( 'YmdHis', strtotime ( '+8 hours' ) ) . '_' . $milliseconds . ".xml";
		$this->WriteInfoLog ( $xmlfilePath );
		
		if (file_put_contents ( $xmlfilePath, ($C0401xmlStr) )) {
			$this->UpdateCreateInvoiceIsUploadStatus ( $orderId, true );
			return true;
		} else {
			$this->UpdateCreateInvoiceIsUploadStatus ( $orderId, false );
			$this->WriteErrorLog ( "[C0401] Can not write xml file to Turnkey folder.   Order IncrementId:" . $orderIncrementId );
			$this->SendCreateInvoiceFailNotification ( $orderId );
			return false;
		}
	}
	
	// 平台存證作廢發票訊息
	public function C0501_Action($order) {
		$orderId = $order->getId ();
		$orderIncrementId = $order->getIncrementId ();
		
		if (is_null ( $orderId )) {
			$this->writeErrorLog ( "[C0501] Can not find any invoice record from database by Order IncrementId:" . $orderIncrementId );
			return false;
		}
		
		$CustomId = Mage::getModel ( 'check/custom_order' )->getIdByOrder ( $orderId );
		$customReturnId = Mage::getModel ( 'check/custom_order' )->load ( $CustomId )->getReturnreason ();
		$customReturnReasonMsg = Mage::getModel ( 'check/custom_returnreason' )->load ( $customReturnId )->getReturnreason ();
		
		$createdinvoice = Mage::getModel ( 'einvoice/einvoices' )->loadByOrderId ( $orderId );
		
		if ($createdinvoice->getInvoice_id () == null) {
			$this->UpdateCancelInvoiceIsUploadStatus ( $orderId, false );
			$this->WriteErrorLog ( "[C0501] Can not get einvoice number due to this order waiting for re-create einovice .   Order IncrementId:" . $orderIncrementId );
			$this->SendCancelInvoiceFailNotification ( $orderId );
			return false;
		}
		
		$invoiceSourceById = Mage::getModel ( 'einvoice/sources' )->load ( $createdinvoice->getInvoice_id () );
		
		$C0501Obj = new GovC0501Invoice ();
		$C0501Obj->setCancelInvoiceNumber ( $invoiceSourceById->getInvoice_number () );
		$C0501Obj->setInvoiceDate ( $createdinvoice->getInvoiceDate () );
		$C0501Obj->setCancelReason ( $customReturnReasonMsg );
		
		$C0501xmlStr = $this->CreateC0501Xml ( $C0501Obj );
		
		$date = new DateTime ();
		
		$createdinvoice->setInvoiceCancelXml ( $C0501xmlStr )->setInvoiceCancelAt ( $date->getTimestamp () )->setInvoiceStatus ( '1' )->save ();
		
		$folderPath = Mage::getStoreConfig ( 'gov/einvoice/turnkeyfolder', false ) . "B2CSTORAGE/C0501/SRC/";
		$this->CreateFolderIfNotExist ( $folderPath );
		$milliseconds = round ( microtime ( true ) * 1000 );
		usleep ( 2000 );
		$xmlfilePath = $folderPath . date ( 'YmdHis', strtotime ( '+8 hours' ) ) . '_' . $milliseconds . ".xml";
		$this->WriteInfoLog ( $xmlfilePath );
		
		if (file_put_contents ( $xmlfilePath, ($C0501xmlStr) )) {
			return true;
		} else {
			$this->UpdateCancelInvoiceIsUploadStatus ( $orderId, false );
			$this->WriteErrorLog ( "[C0501] Can not write xml file to Turnkey folder.   Order IncrementId:" . $orderIncrementId );
			$this->SendCancelInvoiceFailNotification ( $orderId );
			return false;
		}
	}
	public function ReCreateC0401Action($order) {
		$order_id = $order->getId ();
		$createdinvoice = Mage::getModel ( 'einvoice/einvoices' )->loadByOrderId ( $order_id );
		if (is_null ( $createdinvoice->getId () )) {
			if (Mage::helper ( 'goveinvoice/message' )->C0401_Action ( $order )) {
				Mage::log ( "[C0401] Re-Create einvoice C0401 action SUCCESS.  Order IncrementId:" . $order->getIncrementId (), null, 'einvoice.log' );
			} else {
				Mage::log ( "[C0401] Re-Create einvoice C0401 action FAIL.  Order IncrementId:" . $order->getIncrementId (), null, 'einvoice.log' );
			}
		} else {
			if (Mage::helper ( 'goveinvoice/message' )->WriteC0401FromDb_Action ( $order )) {
				Mage::log ( "[C0401] Re-Write einvoice C0401 from DB SUCCESS.  Order IncrementId:" . $order->getIncrementId (), null, 'einvoice.log' );
			} else {
				Mage::log ( "[C0401] Re-Write einvoice C0401 from DB FAIL.  Order IncrementId:" . $order->getIncrementId (), null, 'einvoice.log' );
			}
		}
	}
	public function ReCreateC0501Action($order) {
		if (Mage::helper ( 'goveinvoice/message' )->WriteC0501FromDb_Action ( $order )) {
			Mage::log ( "[C0501] Re-Write einvoice C0501 from DB SUCCESS.  Order IncrementId:" . $order->getIncrementId (), null, 'einvoice.log' );
		} else {
			Mage::log ( "[C0501] Re-Write einvoice C0501 from DB FAIL.  Order IncrementId:" . $order->getIncrementId (), null, 'einvoice.log' );
		}
	}
	function CreateC0401Xml($C0401Obj) {
		$doc = new DOMDocument ( '1.0', 'utf-8' );
		$doc->formatOutput = true;
		
		$root = $doc->createElementNS ( 'urn:GEINV:eInvoiceMessage:C0401:3.1', 'Invoice' );
		$doc->appendChild ( $root );
		$root->setAttributeNS ( 'http://www.w3.org/2000/xmlns/', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance' );
		$root->setAttributeNS ( 'http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation', 'urn:GEINV:eInvoiceMessage:C0401:3.1 C0401.xsd' );
		
		$main = $doc->createElement ( 'Main' );
		$invoiceNumber = $doc->createElement ( 'InvoiceNumber', $C0401Obj->getInvoiceNumber () );
		$invoiceDate = $doc->createElement ( 'InvoiceDate', $C0401Obj->getInvoiceDate () );
		$invoiceTime = $doc->createElement ( 'InvoiceTime', $C0401Obj->getInvoiceTime () );
		$seller = $doc->createElement ( 'Seller' );
		
		$seller->appendChild ( $doc->createElement ( 'Identifier', $C0401Obj->getSellerIdentifier () ) );
		$seller->appendChild ( $doc->createElement ( 'Name', $C0401Obj->getSellerName () ) );
		$buyer = $doc->createElement ( 'Buyer' );
		$buyer->appendChild ( $doc->createElement ( 'Identifier', $C0401Obj->getBuyerIdentifier () ) );
		$buyer->appendChild ( $doc->createElement ( 'Name', $C0401Obj->getBuyerName () ) );
		
		$main->appendChild ( $invoiceNumber );
		$main->appendChild ( $invoiceDate );
		$main->appendChild ( $invoiceTime );
		$main->appendChild ( $seller );
		$main->appendChild ( $buyer );
		$main->appendChild ( $doc->createElement ( 'InvoiceType', $C0401Obj->getInvoiceType () ) );
		
		$main->appendChild ( $doc->createElement ( 'DonateMark', $C0401Obj->getDonateMark () ) );

		
		if($C0401Obj->getCarrierType () == "3J0002")
		{
			// 消費者使用手機條碼(3J0002) 索取含買方統編發票，則"不論"是否已列印紙本，其載具類別號碼、載具顯碼 ID和載具隱碼 ID皆為必填。
			$main->appendChild ( $doc->createElement ( 'CarrierType', $C0401Obj->getCarrierType () ) );
			$main->appendChild ( $doc->createElement ( 'CarrierId1', $C0401Obj->getCarrierId1 () ) );
			$main->appendChild ( $doc->createElement ( 'CarrierId2', $C0401Obj->getCarrierId2 () ) );						
		}
		else
		{		
			//電子發票上傳標準規格MIG 3.1版.pdf - P76 , PrintMark為 Y 時載具類別號碼(CarrierType),載具顯碼 ID(CarrierId1),載具隱碼 ID (CarrierId2 )必須為空白，
			if ($C0401Obj->getPrintMark () == 'N') {		
				$main->appendChild ( $doc->createElement ( 'CarrierType', $C0401Obj->getCarrierType () ) );
				$main->appendChild ( $doc->createElement ( 'CarrierId1', $C0401Obj->getCarrierId1 () ) );
				$main->appendChild ( $doc->createElement ( 'CarrierId2', $C0401Obj->getCarrierId2 () ) );
			}
		}
		
		$main->appendChild ( $doc->createElement ( 'PrintMark', $C0401Obj->getPrintMark () ) );
		if ($C0401Obj->getNPOBAN () != "") {
			$main->appendChild ( $doc->createElement ( 'NPOBAN', $C0401Obj->getNPOBAN () ) );
		}
		
		$main->appendChild ( $doc->createElement ( 'RandomNumber', $C0401Obj->getRandomNumber () ) );
		
		$details = $doc->createElement ( 'Details' );
		
		foreach ( $C0401Obj->getProductItemArray () as $productObj ) {
			$productItem = $doc->createElement ( 'ProductItem' );
			$productItem->appendChild ( $doc->createElement ( 'Description', $productObj->getDescription () ) );
			$productItem->appendChild ( $doc->createElement ( 'Quantity', $productObj->getQuantity () ) );
			$productItem->appendChild ( $doc->createElement ( 'UnitPrice', $productObj->getUnitPrice () ) );
			$productItem->appendChild ( $doc->createElement ( 'Amount', $productObj->getAmount () ) );
			$productItem->appendChild ( $doc->createElement ( 'SequenceNumber', $productObj->getSequenceNumber () ) );
			
			$details->appendChild ( $productItem );
		}
		
		$amount = $doc->createElement ( 'Amount' );
		$amount->appendChild ( $doc->createElement ( 'SalesAmount', $C0401Obj->getSalesAmount () ) );
		$amount->appendChild ( $doc->createElement ( 'FreeTaxSalesAmount', $C0401Obj->getFreeTaxSalesAmount () ) );
		$amount->appendChild ( $doc->createElement ( 'ZeroTaxSalesAmount', $C0401Obj->getZeroTaxSalesAmount () ) );
		$amount->appendChild ( $doc->createElement ( 'TaxType', $C0401Obj->getTaxType () ) );
		$amount->appendChild ( $doc->createElement ( 'TaxRate', $C0401Obj->getTaxRate () ) );
		$amount->appendChild ( $doc->createElement ( 'TaxAmount', $C0401Obj->getTaxAmount () ) );
		$amount->appendChild ( $doc->createElement ( 'TotalAmount', $C0401Obj->getTotalAmount () ) );
		
		$root->appendChild ( $main );
		$root->appendChild ( $details );
		$root->appendChild ( $amount );
		
		return $doc->savexml ();
	}
	function CreateC0501Xml($C0501Obj) {
		$doc = new DOMDocument ( '1.0', 'utf-8' );
		$doc->formatOutput = true;
		
		$root = $doc->createElementNS ( 'urn:GEINV:eInvoiceMessage:C0501:3.1', 'CancelInvoice' );
		$doc->appendChild ( $root );
		$root->setAttributeNS ( 'http://www.w3.org/2000/xmlns/', 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance' );
		$root->setAttributeNS ( 'http://www.w3.org/2001/XMLSchema-instance', 'xsi:schemaLocation', 'urn:GEINV:eInvoiceMessage:C0401:3.1 C0501.xsd' );
		
		$root->appendChild ( $doc->createElement ( 'CancelInvoiceNumber', $C0501Obj->getCancelInvoiceNumber () ) );
		$root->appendChild ( $doc->createElement ( 'InvoiceDate', $C0501Obj->getInvoiceDate () ) );
		$root->appendChild ( $doc->createElement ( 'BuyerId', $C0501Obj->getBuyerId () ) );
		$root->appendChild ( $doc->createElement ( 'SellerId', $C0501Obj->getSellerId () ) );
		$root->appendChild ( $doc->createElement ( 'CancelDate', $C0501Obj->getCancelDate () ) );
		$root->appendChild ( $doc->createElement ( 'CancelTime', $C0501Obj->getCancelTime () ) );
		$root->appendChild ( $doc->createElement ( 'CancelReason', $C0501Obj->getCancelReason () ) );
		
		return $doc->savexml ();
	}
	private function WriteC0401FromDb_Action($order) {
		$orderId = $order->getId ();
		$createdinvoice = Mage::getModel ( 'einvoice/einvoices' )->loadByOrderId ( $order->getId () );
		
		$C0401xmlStr = $createdinvoice->getinvoice_create_xml ();
		
		$folderPath = Mage::getStoreConfig ( 'gov/einvoice/turnkeyfolder', false ) . "B2CSTORAGE/C0401/SRC/";
		$this->CreateFolderIfNotExist ( $folderPath );
		$xmlfilePath = $folderPath . date ( 'YmdHis', strtotime ( '+8 hours' ) ) . ".xml";
		
		if (file_put_contents ( $xmlfilePath, ($C0401xmlStr) )) {
			$this->UpdateCreateInvoiceIsUploadStatus ( $orderId, true );
			return true;
		} else {
			$this->UpdateCreateInvoiceIsUploadStatus ( $orderId, false );
			$this->WriteErrorLog ( "[C0401] Can not write xml file to Turnkey folder.   Order IncrementId:" . $orderIncrementId );
			$this->SendCreateInvoiceFailNotification ( $orderId );
			return false;
		}
	}
	private function WriteC0501FromDb_Action($order) {
		$orderId = $order->getId ();
		$createdinvoice = Mage::getModel ( 'einvoice/einvoices' )->loadByOrderId ( $order->getId () );
		
		$C0501xmlStr = $createdinvoice->getinvoice_cancel_xml ();
		
		$folderPath = Mage::getStoreConfig ( 'gov/einvoice/turnkeyfolder', false ) . "B2CSTORAGE/C0501/SRC/";
		$this->CreateFolderIfNotExist ( $folderPath );
		$xmlfilePath = $folderPath . date ( 'YmdHis', strtotime ( '+8 hours' ) ) . ".xml";
		
		if (file_put_contents ( $xmlfilePath, ($C0501xmlStr) )) {
			$this->UpdateCancelInvoiceIsUploadStatus ( $orderId, true );
			return true;
		} else {
			$this->UpdateCancelInvoiceIsUploadStatus ( $orderId, false );
			$this->WriteErrorLog ( "[C0501] Can not write xml file to Turnkey folder.   Order IncrementId:" . $orderIncrementId );
			$this->SendCancelInvoiceFailNotification ( $orderId );
			return false;
		}
	}
	public function GetInvoiceNumberByOrder($orderId) {
		$customOrderId = Mage::getModel ( 'check/custom_order' )->getIdByOrder ( $orderId );
		$customOrderModel = Mage::getModel ( 'check/custom_order' )->load ( $customOrderId );
		if ($customOrderModel->getInvoice_isdonate ()) {
			return "";
		}
		
		$createdinvoice = Mage::getModel ( 'einvoice/einvoices' )->loadByOrderId ( $orderId );
		$invoice_id = $createdinvoice->getInvoice_id ();
		$invoice_number = Mage::getModel ( 'einvoice/sources' )->getInvoiceNumberById ( $invoice_id );
		return $invoice_number;
	}
	public function GetInvoiceStrResult($orderId) {
		$result = "";
		
		$customOrderId = Mage::getModel ( 'check/custom_order' )->getIdByOrder ( $orderId );
		$customOrderModel = Mage::getModel ( 'check/custom_order' )->load ( $customOrderId );
		
		if ($customOrderModel->getInvoice_isdonate ()) {
			$result = $this->__ ( "donate" ) . $customOrderModel->getInvoice_donateorg ();			
		} else if (! is_null ( $customOrderModel->getInvoice_carriertype () )) {
			$result = $this->__ ( "Phone barcode" ) . ":" . $customOrderModel->getInvoice_carriernum1 ();
		} else {
			$createdinvoice = Mage::getModel ( 'einvoice/einvoices' )->loadByOrderId ( $orderId );
			$invoice_id = $createdinvoice->getInvoice_id ();
			$invoice_number = Mage::getModel ( 'einvoice/sources' )->getInvoiceNumberById ( $invoice_id );
			
			if (! is_null ( $invoice_number )) {
				if ($createdinvoice->getLottery_at () != '0000-00-00 00:00:00' && $createdinvoice->getInvoice_cancel_at () == '0000-00-00 00:00:00') {
					if ($createdinvoice->getLottery_result ()) {
						$result = "(" . $createdinvoice->getLottery_title () . ")";
					} else {
						$result = "(" . $this->__ ( "Not winning" ) . ")";
					}
				}
			}
		}
		
		return $result;
	}
	public function GetRequestInvoiceLink($orderId) {
		$result = "";
		
		$customOrderId = Mage::getModel ( 'check/custom_order' )->getIdByOrder ( $orderId );
		$customOrderModel = Mage::getModel ( 'check/custom_order' )->load ( $customOrderId );
		
		$createdinvoice = Mage::getModel ( 'einvoice/einvoices' )->loadByOrderId ( $orderId );
		$invoice_id = $createdinvoice->getInvoice_id ();
		$invoice_number = Mage::getModel ( 'einvoice/sources' )->getInvoiceNumberById ( $invoice_id );
		
		// Not donate & and carrier type is null. && invoice number not null && einvoice not cancel.
		if (! $customOrderModel->getInvoice_isdonate () && is_null ( $customOrderModel->getInvoice_carriertype () ) && ! is_null ( $invoice_number ) && $createdinvoice->getInvoice_cancel_at () == '0000-00-00 00:00:00') {
			if ($createdinvoice->getUser_request () != '1') {
				$order = Mage::getModel ( 'sales/order' )->load ( $orderId );
				$customerAddressId = Mage::getSingleton ( 'customer/session' )->getCustomer ()->getDefaultBilling ();
				$strBillingAddress = '';
				if ($customerAddressId) {
					$_BA = Mage::getModel ( 'customer/address' )->load ( $customerAddressId );
					$strBillingAddress = $_BA->getPostcode () . $_BA->getCity () . $_BA->getRegion () . $_BA->getStreetFull ();
				}
				
				$requestInvoiceUrl = Mage::getUrl ( 'sales/order/requireEeinvoice/' ) . 'order_id/' . $orderId;
				$onclickConfirm = "return window.confirm('" . $this->__ ( 'Default invoice addresses' ) . "：\\n";
				$onclickConfirm .= $strBillingAddress;
				$onclickConfirm .= "\\n" . $this->__ ( 'If want to change address message' );
				$onclickConfirm .= "\\n" . $this->__ ( 'Are your sure request invoice' ) . "');";
				
				$result .= "<a onclick=\"" . $onclickConfirm . "\" href=\"" . $requestInvoiceUrl . "\">";
				$result .= $this->__ ( 'Request invoice' ) . "</a>";
			} else {
				$result .= "<div>" . $this->__ ( 'Have been requested' ) . "</div>";
			}
		}
		
		return $result;
	}
	function WriteErrorLog($log) {
		Mage::log ( "[Error]" . $log, null, 'einvoice.log' );
	}
	function WriteInfoLog($log) {
		Mage::log ( $log, null, 'einvoice.log' );
	}
	function CreateFolderIfNotExist($path) {
		if (! is_dir ( $path )) {
			$user_name = "apache";			
			mkdir ( $path, 0755, true );						
			chown ( $path, $user_name );
			chgrp ( $path, $user_name );
		}
	}
	private function GetRandomNumber($length) {
		$_num = '';
		for($j = 0; $j < $length; $j ++) {
			$_num .= mt_rand ( 0, 9 );
		}
		return $_num;
	}
	private function SendCreateInvoiceFailNotification($order_id) {
		$order = Mage::getModel ( 'sales/order' )->load ( $order_id );
		$email_address = Mage::getStoreConfig ( 'sales_email/settv_order_mgmt/manager_email' );
		$emailTemplateVariables = array ();
		$emailTemplateVariables ['order'] = $order;
		Mage::helper ( 'common/common' )->sendEmail ( "Invoice_create_fail_notification", $email_address, $emailTemplateVariables );
	}
	private function SendCancelInvoiceFailNotification($order_id) {
		$order = Mage::getModel ( 'sales/order' )->load ( $order_id );
		$email_address = Mage::getStoreConfig ( 'sales_email/settv_order_mgmt/manager_email' );
		$emailTemplateVariables = array ();
		$emailTemplateVariables ['order'] = $order;
		Mage::helper ( 'common/common' )->sendEmail ( "Invoice_cancel_fail_notification", $email_address, $emailTemplateVariables );
	}
	private function UpdateCreateInvoiceIsUploadStatus($orderId, $status) {
		$customOrderId = Mage::getModel ( 'check/custom_order' )->getIdByOrder ( $orderId );
		$customOrderModel = Mage::getModel ( 'check/custom_order' )->load ( $customOrderId );
		$customOrderModel->setInvoice_create_isupload ( $status );
		$customOrderModel->save ();
	}
	private function UpdateCancelInvoiceIsUploadStatus($orderId, $status) {
		$customOrderId = Mage::getModel ( 'check/custom_order' )->getIdByOrder ( $orderId );
		$customOrderModel = Mage::getModel ( 'check/custom_order' )->load ( $customOrderId );
		$customOrderModel->setInvoice_cancel_isupload ( $status );
		$customOrderModel->save ();
	}
}
class ProductItem {
	private $description = "";
	private $quantity = "";
	private $unitPrice = "";
	private $sequenceNumber = "";
	public function getDescription() {
		return $this->description;
	}
	public function setDescription($description) {
		$this->description = mb_substr ( $description, 0, 255 );
	}
	public function getQuantity() {
		return $this->quantity;
	}
	public function setQuantity($quantity) {
		$this->quantity = round ( $quantity );
	}
	public function getUnitPrice() {
		return $this->unitPrice;
	}
	public function setUnitPrice($unitPrice) {
		$this->unitPrice = $unitPrice;
	}
	public function getAmount() {
		return ($this->unitPrice * $this->quantity);
	}
	public function setSequenceNumber($sequenceNumber) {
		$this->sequenceNumber = $sequenceNumber;
	}
	public function getSequenceNumber() {
		return $this->sequenceNumber;
	}
}
class GovC0401Invoice {
	private $invoiceNumber = "";
	private $invoiceDate = "";
	private $invoiceTime = "";
	private $buyerName = "";
	private $invoiceType = "";
	private $donateMark = "";
	private $printMark = "";
	private $randomNumber = "";
	private $productItem = array ();
	private $taxType = 1;
	private $totalAmount = 0;
	private $taxRate = 0.05;
	private $npoban = "";
	private $carrierType = "";
	private $carrierId1 = "";
	private $carrierId2 = "";
	public function getInvoiceNumber() {
		return $this->invoiceNumber;
	}
	public function setInvoiceNumber($invoiceNumber) {
		$this->invoiceNumber = $invoiceNumber;
	}
	public function getInvoiceDate() {
		return date ( "Ymd", strtotime ( '+8 hours' ) );
	}
	public function getInvoiceTime() {
		return date ( 'H:i:s', strtotime ( '+8 hours' ) );
	}
	public function getBuyerName() {
		return $this->buyerName;
	}
	public function setBuyerName($buyerName) {
		$this->buyerName = mb_substr ( trim ( $buyerName ), 0, 60 );
	}
	public function getBuyerIdentifier() {
		return Mage::getStoreConfig ( 'gov/einvoice/b2cbuyerid', false );
	}
	public function getSellerIdentifier() {
		return Mage::getStoreConfig ( 'gov/einvoice/sellerid', false );
	}
	public function getSellerName() {
		return Mage::getStoreConfig ( 'gov/einvoice/sellername', false );
	}
	public function getInvoiceType() {
		return $this->invoiceType;
	}
	public function setInvoiceType($invoiceType) {
		$this->invoiceType = str_pad ( ( int ) $invoiceType, 2, "0", STR_PAD_LEFT );
	}
	public function getDonateMark() {
		return $this->donateMark;
	}
	public function setDonateMark($donateMark) {
		$this->donateMark = $donateMark;
	}
	public function getPrintMark() {
		return $this->printMark;
	}
	public function setPrintMark($printMark) {
		$this->printMark = $printMark;
	}
	public function setRandomNumber($randomNumber) {
		$this->randomNumber = $randomNumber;
	}
	public function getRandomNumber() {
		return $this->randomNumber;
	}
	public function setProductItemArray($productItem) {
		$this->productItem = $productItem;
	}
	public function getProductItemArray() {
		return $this->productItem;
	}
	public function getFreeTaxSalesAmount() {
		return 0;
	}
	public function getZeroTaxSalesAmount() {
		return 0;
	}
	public function setTaxType($taxType) {
		$this->taxType = $taxType;
	}
	public function getTaxType() {
		return $this->taxType;
	}
	public function setTaxRate($taxRate) {
		$this->taxRate = $taxRate;
	}
	public function getTaxRate() {
		return $this->taxRate;
	}
	public function getTaxAmount() {
		//return round ( $this->totalAmount * $this->taxRate );  ,By Ricahrd.
		return 0;
	}
	public function getSalesAmount() {
		return round ( $this->totalAmount );
	}
	public function setTotalAmount($totalAmount) {
		$this->totalAmount = $totalAmount;
	}
	public function getTotalAmount() {
		return $this->totalAmount;
	}
	public function setNPOBAN($npoban) {
		// 愛心馬 發票捐贈對象 , length 3-7
		$this->npoban = $npoban;
	}
	public function getNPOBAN() {
		return $this->npoban;
	}	
	public function setCarrierType($carrierType){
		$this->carrierType = $carrierType;
	}
	public function getCarrierType() {
		return $this->carrierType;
	}
	public function setCarrierId1($carrierId1) {
		$this->carrierId1 = $carrierId1;
	}
	public function getCarrierId1() {
		return $this->carrierId1;
	}
	public function setCarrierId2($carrierId2) {
		$this->carrierId2 = $carrierId2;
	}
	public function getCarrierId2() {
		return $this->carrierId2;
	}
}
class GovC0501Invoice {
	private $cancelInvoiceNumber = "";
	private $invoiceDate = "";
	private $sellerId = "";
	private $cancelDate = "";
	private $cancelTime = "";
	private $cancelReason = "";
	public function setCancelInvoiceNumber($cancelInvoiceNumber) {
		$this->cancelInvoiceNumbe = $cancelInvoiceNumber;
	}
	public function getCancelInvoiceNumber() {
		return $this->cancelInvoiceNumbe;
	}
	public function setInvoiceDate($invoiceDate) {
		$this->invoiceDate = $invoiceDate;
	}
	public function getInvoiceDate() {
		return $this->invoiceDate;
	}
	public function getBuyerId() {
		return Mage::getStoreConfig ( 'gov/einvoice/b2cbuyerid', false );
	}
	public function getSellerId() {
		return Mage::getStoreConfig ( 'gov/einvoice/sellerid', false );
	}
	public function getCancelDate() {
		return date ( "Ymd", strtotime ( '+8 hours' ) );
	}
	public function getCancelTime() {
		return date ( 'H:i:s', strtotime ( '+8 hours' ) );
	}
	public function setCancelReason($cancelReason) {
		$this->cancelReason = mb_substr ( $cancelReason, 0, 20 );
		;
	}
	public function getCancelReason() {
		if (strlen ( $this->cancelReason ) == 0) {
			return "Cancel by default";
		} else {
			return $this->cancelReason;
		}
	}
}