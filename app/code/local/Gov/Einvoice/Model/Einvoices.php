<?php
class Gov_Einvoice_Model_Einvoices extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('einvoice/einvoices');
	}
	
	public function loadByOrderId($orderId)
	{
		return $this->loadByAttribute('order_id', $orderId);
	}
	
	public function loadByInvoiceId($invoiceId)
	{
		return $this->loadByAttribute('invoice_id', $invoiceId);
	}
	
	public function loadByAttribute($attribute, $value)
	{
		$this->load($value, $attribute);
		return $this;
	}
	
	public function createNewEinvoice($order_id, $invoice_id
			, $invoice_date, $invoice_time, $invoice_type
			, $invoice_random_code, $invoice_xml)
	{	
		$now = Mage::Helper("goveinvoice")->getNow();
		$this->setOrderId($order_id)			
			->setInvoiceId($invoice_id)
			->setInvoiceDate($invoice_date)
			->setInvoiceTime($invoice_time)
			->setInvoiceType($invoice_type)
			->setInvoiceRandomCode($invoice_random_code)
			->setInvoiceStatus(0)
			->setInvoiceCreateXml($invoice_xml)
			->setInvoiceCreateAt($now)
			->save();
	}
	
	public function updateByLotteryResult($invoice_id, $isWinning, $lottery_title, $lottery_wins)
	{	
		$now = Mage::Helper("goveinvoice")->getNow();
		$invoice = $this->loadByInvoiceId($invoice_id);
		$invoice->setLotteryResult($isWinning);
		$invoice->setLotteryTitle($lottery_title);
		$invoice->setLotteryWins($lottery_wins);
		$invoice->setLotteryAt($now);
		$invoice->save();	
	}
}
