<?php
class Gov_Einvoice_Model_Resource_Sources extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {    	
        $this->_init('einvoice/sources', 'invoice_id');     
    }
}
