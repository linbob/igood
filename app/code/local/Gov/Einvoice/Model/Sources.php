<?php
class Gov_Einvoice_Model_Sources extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('einvoice/sources');
	}

	public function getInvoiceNumberById($inovice_id)
	{
		return Mage::getModel('einvoice/sources')->load($inovice_id, 'invoice_id')->getInvoiceNumber();
	}

	public function getNextInvoiceNumberWithLock(&$invoice_id)
	{
		$invoice_id = $this->getNextInvoiceIdWithLock();
		return $this->getInvoiceNumberById($invoice_id);
	}

	public function getNextInvoiceIdWithLock()
	{
		$current_month = date("Ym", Mage::Helper("goveinvoice")->getTaipeiNow());
		$taiwan_month = Mage::Helper("goveinvoice")->getTaiwanDate($current_month);

		$invoice_id = null;
		$einvoice_source = Mage::getSingleton('core/resource')->getTableName('gov_einvoice_source');
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$conn->beginTransaction();
		$result = $conn->fetchAll("
				SELECT @update_id := invoice_id
				FROM {$einvoice_source}
		WHERE invoice_usage = 0
		AND invoice_duration_start <= {$taiwan_month}
		AND invoice_duration_end >= {$taiwan_month}
		ORDER BY invoice_number
		LIMIT 1 FOR UPDATE;
		UPDATE {$einvoice_source} SET invoice_usage = 1 WHERE invoice_id = @update_id;
		");
		$conn->commit();

		$this->checkInvoiceCount($taiwan_month);

		if ($result != null && count($result) > 0)
			$invoice_id = $result[0]["@update_id := invoice_id"];
		else
			throw new Exception("Invoice id is empty or null.");
			
		return $invoice_id;
	}

	public function createNewInvoice($company_tax_id, $invoice_class_id
			, $invoice_number, $invoice_duration_start, $invoice_duration_end)
	{
		$invoice_unique = substr($invoice_duration_start,0,3).$invoice_number;
		$now = Mage::Helper("goveinvoice")->getNow();

		Mage::getModel("einvoice/sources")
		-> setCompanyTaxId($company_tax_id)
		-> setInvoiceUnique($invoice_unique)
		-> setInvoiceClassId($invoice_class_id)
		-> setInvoiceNumber($invoice_number)
		-> setInvoiceDurationStart($invoice_duration_start)
		-> setInvoiceDurationEnd($invoice_duration_end)
		-> setInvoiceCreateAt($now)
		-> save();
	}

	private function checkInvoiceCount($taiwan_month)
	{
		try {
			$collection = Mage::getModel("einvoice/sources")
			->getCollection()
			->addFieldToFilter('invoice_usage', 0)
			->addFieldToFilter('invoice_duration_start', array('lteq' => $taiwan_month))
			->addFieldToFilter('invoice_duration_end', array('gteq' => $taiwan_month));
			
			$count = count($collection);
			
			if ($count <= 10)
			{
				$email_address = Mage::getStoreConfig ( 'sales_email/settv_order_mgmt/manager_email' );
				$emailTemplateVariables = array ();
				$emailTemplateVariables ['count'] = $count;
				Mage::helper('common/common')->sendEmail("Invoice_not_enough_notification", $email_address, $emailTemplateVariables);
			}
		} catch (Exception $e) {
			Mage::log ('exception checkInvoiceCount:'.$e->getMessage(), null, 'einvoice.log' );
		}
		
	}
}
