<?php

abstract class Gov_Einvoice_Adminhtml_AbstractController extends Mage_Adminhtml_Controller_Action {

	public function indexAction() {
		$this->loadLayout();
		$this->_setActiveMenu('report');
		$this->renderLayout();
	}
}

