<?php
require_once Mage::getModuleDir('controllers', 'Gov_Einvoice').'/Adminhtml/AbstractController.php';

class Gov_Einvoice_Adminhtml_ReportController extends Gov_Einvoice_Adminhtml_AbstractController
{
	protected function _initAction()
	{
		$this->loadLayout()->_setActiveMenu('gov_einvoice/report');
		return $this;
	}

	public function indexAction() {
		$this->_title($this->__('Einvoices'));
		$this->_initAction()
		->_addContent($this->getLayout()->createBlock('gov_einvoice/adminhtml_report'));
		$this->renderLayout();
	}	
	
	public function printAction() {
		$this->loadLayout();
		$this->renderLayout();
	}
	
	/**
	 * Export invoice grid to CSV format
	 */
	public function exportCsvAction()
	{
		$fileName   = 'einvoice_Report.csv';
		$grid       = $this->getLayout()->createBlock('gov_einvoice/adminhtml_report_grid');
		$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		exit(0);
	}
	
	/**
	 *  Export invoice grid to Excel XML format
	 */
	public function exportExcelAction()
	{
		$fileName   = 'einvoice_report.xls';
		$grid       = $this->getLayout()->createBlock('gov_einvoice/adminhtml_report_grid');
		$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		exit(0);
	}
	
}
