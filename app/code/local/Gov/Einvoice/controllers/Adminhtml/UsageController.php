<?php
require_once Mage::getModuleDir('controllers', 'Gov_Einvoice').'/Adminhtml/AbstractController.php';

class Gov_Einvoice_Adminhtml_UsageController extends Gov_Einvoice_Adminhtml_AbstractController
{
	protected function _initAction()
	{
		$this->loadLayout()->_setActiveMenu('goveinvoice/usage');
		return $this;
	}

	public function indexAction()
	{
		$this->_title($this->__('Einvoices'));
		$this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('gov_einvoice/adminhtml_usage'));
		$this->_addContent($this->getLayout()->createBlock('gov_einvoice/adminhtml_usage_uploader'));
		$this->renderLayout();
	}

	public function uploadAction()
	{
		$result = array();
		try {
			$uploader = new Mage_Core_Model_File_Uploader("file"); //file need to mapppig with uploader.php
			$uploader->setAllowedExtensions(array('xls','csv'));
			$uploader->setAllowRenameFiles(true);
			$uploader->setFilesDispersion(false);
			$folderPath = $this->createEinvoiceFolder();
			$result = $uploader->save($folderPath);
			$filename = $result['file'];
			$filePath = $result['path']."/".$filename;			
			$ext = end(explode('.', $filename));
			
			if ($ext == 'xls')
				$this->saveExcelFileToDB($filePath);
			else 
				$this->saveCsvFileToDB($filePath);
			
			$result['tmp_name'] = str_replace(DS, "/", $result['tmp_name']);
			$result['path'] = str_replace(DS, "/", $result['path']);
			$result['url'] = '';
			$result['file'] = $result['file'] . '.tmp';
			$result['cookie'] = array(
					'name'     => session_name(),
					'value'    => $this->_getSession()->getSessionId(),
					'lifetime' => $this->_getSession()->getCookieLifetime(),
					'path'     => $this->_getSession()->getCookiePath(),
					'domain'   => $this->_getSession()->getCookieDomain()
			);
			Mage::log("upload:".$filePath, null, 'einvoice.log');
			
		} catch (Exception $e) {
			Mage::log("error:".$e->getMessage(), null, 'einvoice.log');			
			$result = array(
					'error' => $e->getMessage(),
					'errorcode' => $e->getCode());
		}
		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
	}
	
	private function createEinvoiceFolder()
	{	
		$date = date("Ymd", strtotime('+8 hours'));
		Mage::helper('common/common')->createFolderAndSetPermission("einovice");
		Mage::helper('common/common')->createFolderAndSetPermission("einovice/$date");
		Mage::helper('common/common')->createFolderAndSetPermission("einovice/$date/finish");
		$folder = Mage::helper('common/common')->createFolderAndSetPermission("einovice/$date/process");
		return $folder;
	}

	private function saveExcelFileToDB($file)
	{
		set_include_path(get_include_path().PATH_SEPARATOR.'phpexcel/Classes/');
		require_once("phpexcel/Classes/PHPExcel.php");
		require_once("phpexcel/Classes/PHPExcel/IOFactory.php");
		require_once("phpexcel/Classes/PHPExcel/Reader/Excel5.php");
		$objReader = PHPExcel_IOFactory::createReader('Excel5');
		
		$objPHPExcel = $objReader->load($file);
		$currentSheet = $objPHPExcel->getSheet(0);
		$allLine = $currentSheet->getHighestRow();
		for ($row = 2; $row <= $allLine; $row++) {
			$val=array();			
			for ($column = 0; $column <= 6; $column++) {
				$val[$column] = $currentSheet->getCellByColumnAndRow($column, $row)->getValue();
			}			
			$this->saveRowToDB($val);
		}
		unset($objReader);
		unset($objPHPExcel);
		rename($file, str_replace("/process/", "/finish/", $file));
	}

	private function saveCsvFileToDB($filePath)
	{	
		
		$row = 1;
		if (($handle = fopen($filePath, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {				
				if ($row != 1) {					
					$this->saveRowToDB($data);
				}		
				$row++;
			}
			fclose($handle);
		}		
	}
	
	private function saveRowToDB($val)
	{
		$company_tax_id = str_replace("　","", $val[0]);//營業人統編
		$invoice_class_id = str_replace("　","", $val[1]);//發票類別代號
		$invoice_class = str_replace("　","", $val[2]);//發票類別
		$invoice_duration = str_replace("　","", $val[3]);//發票期別
		$invoice_name = str_replace("　","", $val[4]);//發票字軌名稱
		$invoice_start_no = intval(ltrim(str_replace("　","", $val[5]), '0'));//發票起號
		$inovice_end_no	= intval(ltrim(str_replace("　","", $val[6]), '0'));//發票迄號
		Mage::log("import:".$invoice_name.":".$invoice_start_no.":".$inovice_end_no, null, 'einvoice.log');
			
		$this -> getInvoiceDuration($invoice_duration, $inovice_duration_start, $invoice_duration_end);
		for ($inum = $invoice_start_no; $inum <= $inovice_end_no; $inum++)
		{
			$inovice_number = $invoice_name.sprintf("%08s", $inum);
			Mage::getModel("einvoice/sources")->createNewInvoice($company_tax_id
			, $invoice_class_id, $inovice_number
			, $inovice_duration_start, $invoice_duration_end);
			
		}
	}
	
	private function getInvoiceDuration($invoice_duration, &$inovice_duration_start, &$invoice_duration_end)
	{
		$invoice_duration_ary = preg_split("/ ~ /", $invoice_duration);		
		$start_ary = preg_split("/\//", $invoice_duration_ary[0]);
		$end_ary = preg_split("/\//", $invoice_duration_ary[1]);
		$inovice_duration_start = $start_ary[0].$start_ary[1];
		$invoice_duration_end = $end_ary[0].$end_ary[1];
	}


	/**
	 * Export invoice grid to CSV format
	 */
	public function exportCsvAction()
	{
		$fileName   = 'einvoice_usage.csv';
		$grid       = $this->getLayout()->createBlock('gov_einvoice/adminhtml_report_grid');
		$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		exit(0);
	}


	/**
	 *  Export invoice grid to Excel XML format
	 */
	public function exportExcelAction()
	{
		$fileName   = 'einvoice_usage.xls';
		$grid       = $this->getLayout()->createBlock('gov_einvoice/adminhtml_usage_grid');
		$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		exit(0);
	}

}

