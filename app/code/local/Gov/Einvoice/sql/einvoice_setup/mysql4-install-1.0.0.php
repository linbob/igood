<?php

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('gov_einvoice_source')} (
  `invoice_number` varchar(10) NOT NULL DEFAULT '',
  `company_tax_id` int(10) NOT NULL,
  `invoice_duration_start` int(6) NOT NULL,
  `invoice_duration_end` int(6) NOT NULL,
  `invoice_usage` tinyint(1) NOT NULL DEFAULT '0',
  `invoice_create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `invoice_class_id` int(2) NOT NULL,  
  PRIMARY KEY (`invoice_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('gov_einvoice')} (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `invoice_number` varchar(10) NOT NULL,
  `invoice_date` text,
  `invoice_time` text,
  `invoice_type` text,  
  `invoice_status` int(1) DEFAULT 0,
  `invoice_create_xml` text,
  `invoice_create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoice_cancel_xml` text,
  `invoice_cancel_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lottery_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lottery_result` tinyint(1) DEFAULT NULL,
  `lottery_title` text,
  `lottery_wins` int(11) DEFAULT NULL,
  `user_request` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `order_id_idx` (`order_id`),
  KEY `invoice_number` (`invoice_number`),
  CONSTRAINT `invoice_number` FOREIGN KEY (`invoice_number`) REFERENCES `gov_einvoice_source` (`invoice_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_id` FOREIGN KEY (`order_id`) REFERENCES `sales_flat_order` (`entity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;		
");

$installer->endSetup();