<?php

$installer = $this;
$installer->startSetup();

$connection = $this->getConnection();
$connection->addColumn($this->getTable('einvoice/einvoices'), 'invoice_random_code', "VARCHAR(4)");

$installer->endSetup();