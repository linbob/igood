<?php

$installer = $this;
$installer->startSetup();

$installer->run("		
ALTER TABLE {$this->getTable('gov_einvoice')}
DROP FOREIGN KEY `invoice_number`,
DROP COLUMN `invoice_number`,
DROP INDEX `invoice_number`,
CHANGE COLUMN `invoice_random_code` `invoice_random_code` VARCHAR(4) NULL DEFAULT NULL AFTER `invoice_status`,
ADD COLUMN `invoice_id` INT NULL AFTER `order_id`;
");

$installer->run("
ALTER TABLE {$this->getTable('gov_einvoice_source')}
ADD COLUMN `invoice_id` INT NOT NULL AUTO_INCREMENT FIRST,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`invoice_id`);
");

$installer->run("
ALTER TABLE {$this->getTable('gov_einvoice')}
ADD CONSTRAINT `invoice_id`
FOREIGN KEY (`invoice_id`)
REFERENCES {$this->getTable('gov_einvoice_source')} (`invoice_id`)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
");

$installer->endSetup();