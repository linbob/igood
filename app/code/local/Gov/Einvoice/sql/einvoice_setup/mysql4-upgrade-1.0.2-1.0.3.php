<?php

$installer = $this;
$installer->startSetup();

$installer->run("		
ALTER TABLE {$this->getTable('gov_einvoice_source')}
ADD COLUMN `invoice_unique` VARCHAR(13) NOT NULL AFTER `invoice_id`,
ADD UNIQUE INDEX `invoice_unique_UNIQUE` (`invoice_unique` ASC);		
");


$installer->endSetup();