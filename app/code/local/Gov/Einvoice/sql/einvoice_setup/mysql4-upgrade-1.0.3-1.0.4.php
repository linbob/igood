<?php

$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('gov_einvoice')}
DROP FOREIGN KEY `invoice_id`;
ALTER TABLE {$this->getTable('gov_einvoice')}
CHANGE COLUMN `invoice_id` `invoice_id` INT(11) NOT NULL ,
ADD UNIQUE INDEX `order_id_UNIQUE` (`order_id` ASC),
ADD UNIQUE INDEX `invoice_id_UNIQUE` (`invoice_id` ASC);
ALTER TABLE {$this->getTable('gov_einvoice')}
ADD CONSTRAINT `invoice_id`
FOREIGN KEY (`invoice_id`)
REFERENCES {$this->getTable('gov_einvoice_source')} (`invoice_id`)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
");

$installer->endSetup();