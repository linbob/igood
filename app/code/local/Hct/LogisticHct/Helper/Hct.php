﻿<?php
class Hct_LogisticHct_Helper_Hct extends Mage_Core_Helper_Abstract {

	public function deliverOrder($input_order, $orders)
	{

		$logistics = Mage::getModel('check/custom_logistics')->createLogistics();
		$logistics_id = $logistics->getId();

		if ($logistics_id != null)
		{
			$this->CreateTransactionFile($logistics_id, $input_order, $orders);

			foreach ($orders as $order)
			{
				Mage::getModel('check/custom_order')->setLogisticsData($order->getId(), $logistics_id);
			}
		}
		else
		{
			die("can't get logistics number.");
		}
			
	}

	//託運單
	public function CreateTransactionFile($logistics_id, $order, $orders)
	{
		//loading Order Details Data
		$order_increment_id = $order->getIncrementId();
		$master_order = Mage::helper("ordermanage")->setSummaryOfOrders($orders);;
		$qty = $master_order->getTotalQtyOrdered();		
		$SAddress = $order->getShippingAddress();
		$ChargeMemo = "";

		$ChargeAmt = $master_order->getChargeAmt();
		if ($ChargeAmt > 0)
			$ChargeMemo = "貨到付款";

		$supplier = Mage::helper('splitorder/supplier')->getSupplierByOrder($order);			
		$edelno	= Mage::getModel('check/custom_logistics')->getlogisticsnum($logistics_id); //查貨號碼
		$logistics = Mage::getModel('check/custom_logistics')->load($logistics_id);
		$logistics->setLogisticsNum($edelno)->save();
		
		$epino	=$logistics_id; //客戶清單編號
		$escsno	=$SAddress->getCustomerId(); //客戶代號
		$ecust	=""; //收貨人代號
		$ercsig	=$SAddress->getName(); //收貨人名稱
		$ertel1	=$SAddress->getTelephone(); //收貨人電話1
		$ertel2	=$SAddress->getCellphone(); //收貨人電話2
		$eraddr	=$SAddress->getPostcode().$SAddress->getCity().$SAddress->getRegion().$SAddress->getStreetFull(); //收貨人地址
		$eraddr =str_replace(",","", $eraddr);
		$eqmny	=$ChargeAmt; //代收貨款
		$egamt	=""; //報值金額
		$esdate	=""; //發送日期
		$esstno	=""; //發送站
		$erstno	=""; //到著站
		$ekamt	=""; //個數
		$ejamt	="1"; //件數: 無論幾樣都是1
		$eqamt	=""; //重量
		$ebamt	=""; //本款
		$eramt	=""; //接費
		$esamt	=""; //送費
		$edamt	=""; //代款
		$elamt	=""; //聯運費
		$eprdct	=""; //傳票區分
		$eprdcl	=""; //商品區分
		$eprdc1	=""; //商品區分
		$eddate	=""; //指配日期
		$edtime	=""; //指配時間
		$etcsno	=$supplier->getSupplierCode(); //供貨人代號
		$etcsig	=$supplier->getContactName(); //供貨人名稱
		$ettel1	=$supplier->getContactTelephone(); //供貨人電話1
		$ettel2	=$supplier->getTelephone(); //供貨人電話2
		$etaddr	=$supplier->getPostcode().$supplier->getRegion().$supplier->getCity().$supplier->getStreet(); //供貨人地址
		$etaddr =str_replace(",","", $etaddr);
		$emark	=$ChargeMemo; //備註
		$esel	=""; //是否列印
		$eprint	=""; //已列印過
		$epost	=""; //郵遞區號
		$essen	=""; //自送站止
		$edlcde	=""; //特殊配送

		//create transaction file and input data
		$header_list="查貨號碼,客戶清單編號,客戶代號,收貨人代號,收貨人名稱,收貨人電話1,收貨人電話2,收貨人地址,代收貨款,報值金額,發送日期,發送站,到著站,個數,件數,重量,本款,接費,送費,代款,聯運費,傳票區分,商品區分,商品區分,指配日期,指配時間,供貨人代號,供貨人名稱,供貨人電話1,供貨人電話2,供貨人地址,備註,是否列印,已列印過,郵遞區號,自送站止,特殊配送\n";
		$content_list="$edelno,$epino,$escsno,$ecust,$ercsig,$ertel1,$ertel2,$eraddr,$eqmny,$egamt,$esdate,$esstno,$erstno,$ekamt,$ejamt,$eqamt,$ebamt,$eramt,$esamt,$edamt,$elamt,$eprdct,$eprdcl,$eprdc1,$eddate,$edtime,$etcsno,$etcsig,$ettel1,$ettel2,$etaddr,$emark,$esel,$eprint,$epost,$essen,$edlcde";
		$file_name="SETTV$edelno.csv";

		$this->sendToHct('shipping', $file_name, $header_list.$content_list);
	}

	//逆物流單
	public function CreateReverseTransactionFile($order_id)
	{
		$order = Mage::getModel('sales/order')->load($order_id);

		$datatype=""; //資料類別
		$cstid=""; //客戶代號
		$cstname=""; //客戶名稱
		$cstaddr=""; //客戶地址
		$cstphone=""; //客戶電話
		$esstno=""; //通知站代號
		$erdate=""; //取貨通知日期
		$ertime=""; //取貨時段
		$erstno=""; //取貨站代號
		$ername=""; //退貨人名稱
		$eraddr=""; //退貨人地址
		$erphone=""; //退貨人電話
		$ejamt=""; //件數
		$emark=""; //備註
		$edekbi=""; //查貨號碼

		$order = Mage::getModel('sales/order')->load($order_id);
		$order_incrementId = $order->getIncrementId();
		if($order){
			$items = $order->getAllItems();
			foreach ($items as $itemId => $item)
			{
				$ejamt = $item->getQtyOrdered();
			}
			$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
			$Custommodel = Mage::getModel('check/custom_order')->load($CustomId);
			if($Custommodel){
				$CustomArr = $Custommodel->getData();
				//Mage::log(print_r($CustomArr,true));
				$cstname  = "三立電視台"; //客戶名稱
				$cstaddr  = "台北市內湖區舊宗路一段159號"; //客戶地址
				$cstphone = "02-8792-8888"; //客戶電話
				$erdate   = now(); //取貨通知日期
				$edekbi = $CustomArr['returnnum']; //查貨號碼

				$address = Mage::getModel("customer/address")->load($CustomArr['returnaddressid']);
				if($address){
					$ername  = $address->getName(); //退貨人名稱
					$eraddr  = $address->getPostcode().$address->getCity().$address->getRegion().$address->getStreetFull(); //退貨人地址
					$eraddr = str_replace(",","", $eraddr);
					$erphone = $address->getCellphone(); //退貨人電話
				}
			}

		}

		$header_list="資料類別,客戶代號,客戶名稱,客戶地址,客戶電話,通知站代號,取貨通知日期,取貨時段,取貨站代號,退貨人名稱,退貨人地址,退貨人電話,件數,備註,客戶清單編號,查貨號碼\n";
		$content_list="$datatype,$cstid,$cstname,$cstaddr,$cstphone,$esstno,$erdate,$ertime,$erstno,$ername,$eraddr,$erphone,$ejamt,$emark,$order_incrementId,$edekbi\n";
		$file_name="RT_".$order->getIncrementId().".csv";
		$this->sendToHct('reverse', $file_name, $header_list.$content_list);
	}

	//物流託運單回覆
	public function ProcessReplyConsign(){
		$path     = Mage::getStoreConfig('carriers/hct/ftp', false);
		$username = Mage::getStoreConfig('carriers/hct/fusername', false);
		$password = Mage::getStoreConfig('carriers/hct/fpassword', false);
		$port     = Mage::getStoreConfig('carriers/hct/ftpport', false);
		$conn_id=ftp_connect($path, $port); //or die("Unable to connect to HCT FTP.");
		$login_result=ftp_login($conn_id, $username, $password);

		if ((!$conn_id) || (!$login_result)) {
			return ("FTP connection has failed !");
		}
		ftp_pasv($conn_id, true);
		ftp_chdir($conn_id, "DATA");
		$file_list=ftp_nlist($conn_id, "./ZI_NEW_*.txt");

		if(count($file_list)>0 and $file_list!=false){

			$folder = $this->createHctFolder('reply_consign');

			//下載檔案至 process 資料夾
			foreach($file_list as $file_name){
				$process_file[]=$process_path="$folder/process/$file_name";
				if(ftp_get($conn_id, $process_path, $file_name, FTP_BINARY))
					ftp_delete($conn_id, $file_name);
			}

			//處理下載的檔案
			foreach($process_file as $file){
				//取出資料
				$handle=fopen($file, "r");
				$content=fread($handle, filesize($file));
				$content=explode("\n", $content);
				foreach($content as $data){
					if(strlen($data)>=2){
						$edelno		=substr($data, 0, 10)	;//查貨號碼
						$ibivpu		=substr($data, 10, 1)	;//重複次號
						$ibivpc		=substr($data, 11, 1)	;//重複次號2
						$esstno		=substr($data, 12, 4)  	;//發送站
						$esstig		=substr($data, 16, 6)  	;//發送站名稱
						$erstno		=substr($data, 22, 4)  	;//到著站
						$erstig		=substr($data, 26, 6)  	;//到著站名稱
						$escsno		=substr($data, 32, 11)  ;//客戶代號
						$epino		=substr($data, 43, 20)  ;//清單編號
						$esdate		=substr($data, 63, 8)  	;//發送日期
						$ercsig		=substr($data, 71, 12)  ;//收貨人名稱
						$ejamt		=substr($data, 83, 5)  	;//總件數
						$eqamt		=substr($data, 88, 6)  	;//總重量
						$ebamt		=substr($data, 94, 8)  	;//本款
						$eramt		=substr($data, 102, 8)  ;//接費
						$esamt		=substr($data, 110, 8)  ;//送費
						$edamt		=substr($data, 118, 8)  ;//代款
						$elamt		=substr($data, 126, 8)  ;//聯運費
						$eoamt		=substr($data, 132, 8)  ;//其他附加費
						$etotal		=substr($data, 140, 8)  ;//總運費
						$eqmny		=substr($data, 148, 8)  ;//代收貸款
						$egamt		=substr($data, 156, 8)  ;//報值金額

						//---BIG5 CONVERT TO UTF-8---
						$edelno		= mb_convert_encoding($edelno, "utf-8", "big5");
						$ibivpu		= mb_convert_encoding($ibivpu, "utf-8", "big5");
						$ibivpc		= mb_convert_encoding($ibivpc, "utf-8", "big5");
						$esstno		= mb_convert_encoding($esstno, "utf-8", "big5");
						$esstig		= mb_convert_encoding($esstig, "utf-8", "big5");
						$erstno		= mb_convert_encoding($erstno, "utf-8", "big5");
						$erstig		= mb_convert_encoding($erstig, "utf-8", "big5");
						$escsno		= mb_convert_encoding($escsno, "utf-8", "big5");
						$epino		= mb_convert_encoding($epino, "utf-8", "big5");
						$esdate		= mb_convert_encoding($esdate, "utf-8", "big5");
						$ercsig		= mb_convert_encoding($ercsig, "utf-8", "big5");
						$ejamt		= mb_convert_encoding($ejamt, "utf-8", "big5");
						$eqamt		= mb_convert_encoding($eqamt, "utf-8", "big5");
						$ebamt		= mb_convert_encoding($ebamt, "utf-8", "big5");
						$eramt		= mb_convert_encoding($eramt, "utf-8", "big5");
						$esamt		= mb_convert_encoding($esamt, "utf-8", "big5");
						$edamt		= mb_convert_encoding($edamt, "utf-8", "big5");
						$elamt		= mb_convert_encoding($elamt, "utf-8", "big5");
						$eoamt		= mb_convert_encoding($eoamt, "utf-8", "big5");
						$etotal		= mb_convert_encoding($etotal, "utf-8", "big5");
						$eqmny		= mb_convert_encoding($eqmny, "utf-8", "big5");
						$egamt		= mb_convert_encoding($egamt, "utf-8", "big5");

						$ercsig		=str_replace("　","", $ercsig);

						//------將資料寫入DB------
						$logistics_id = trim($epino);
						$logisticsmodel = Mage::getModel('check/custom_logistics')->load($logistics_id);

						if ($logisticsmodel->getId() != null)
						{
							$DateFormat = strtotime(substr($esdate, 0, 4)."-".substr($esdate, 4, 2)."-".substr($esdate, 6, 2));
							$logisticsmodel->setData("forward_at",$DateFormat);
							$logisticsmodel->setData("sendee",$ercsig);
							$logisticsmodel->save();
						}
					}
				}
				fclose($handle);

				//Finish Process
				rename($file, str_replace("process", "finish", $file));

			}
			return ("Job done");
		}
		else{
			return ("No any file update");
		}
	}

	//物流貨物抵達
	public function ProcessReplyDispose(){
		$path     = Mage::getStoreConfig('carriers/hct/ftp', false);
		$username = Mage::getStoreConfig('carriers/hct/fusername', false);
		$password = Mage::getStoreConfig('carriers/hct/fpassword', false);
		$port     = Mage::getStoreConfig('carriers/hct/ftpport', false);
		$conn_id=ftp_connect($path, $port);// or die("Unable to connect to HCT FTP.");
		$login_result=ftp_login($conn_id, $username, $password);

		if ((!$conn_id) || (!$login_result)) {
			return ("FTP connection has failed !");
		}

		ftp_pasv($conn_id, true);

		ftp_chdir($conn_id, "DATA");
		$file_list=ftp_nlist($conn_id, "./ZS_NEW_*.txt");

		if(count($file_list)>0 and $file_list!=false){

			$folder = $this->createHctFolder('reply_dispose');

			//下載檔案至 process 資料夾
			foreach($file_list as $file_name){
				$process_file[]=$process_path="$folder/process/$file_name";
				if(ftp_get($conn_id, $process_path, $file_name, FTP_BINARY))
					ftp_delete($conn_id, $file_name);
			}

			//處理下載的檔案
			$handlestr = "";

			foreach($process_file as $file){
				//取出資料
				$handle=fopen($file, "r");
				$content=fread($handle, filesize($file));
				$content=explode("\n", $content);
				foreach($content as $data){
					if(strlen($data)>=2){
						$edelno		=substr($data, 0, 10);		//查貨號碼
						$sbivpu		=substr($data, 10, 1);		//重覆次號
						$erstno		=substr($data, 11, 4);		//到著站
						$erstig		=substr($data, 15, 6);		//到著站名稱
						$escsno		=substr($data, 21, 11);		//客戶代號
						$esname		=substr($data, 32, 12);		//客戶名稱
						$epino		=substr($data, 44, 20);		//清單編號
						$esdate		=substr($data, 64, 8);		//發送日期
						$ercsig		=substr($data, 72, 12);		//收貨人名稱
						$ebamt		=substr($data, 84, 8);		//本款
						$eramt		=substr($data, 92, 8);		//接費
						$esamt		=substr($data, 100, 8);		//送費
						$edamt		=substr($data, 108, 8);		//代款
						$elamt		=substr($data, 116, 8);		//聯運費
						$eoamt		=substr($data, 124, 8);		//其它附加費
						$etotal		=substr($data, 132, 8);		//總運費
						$eqmny		=substr($data, 140, 8);		//代收貨款
						$eadate		=substr($data, 148, 8);		//銷號日期
						$eajamt		=substr($data, 156, 5);		//銷號件數
						$edcode		=substr($data, 161, 14);		//銷號狀況
						$ecode		=substr($data, 175, 2);		//銷號代碼
						$sign_code	=substr($data, 177, 1);		//簽收代碼
						$sign_cust	=substr($data, 178, 10);		//簽收人
						$eatime		=substr($data, 188, 6);		//銷號時間

						//---BIG5 CONVERT TO UTF-8---
						$edelno		= mb_convert_encoding($edelno	, "utf-8", "big5");	//查貨號碼
						$sbivpu		= mb_convert_encoding($sbivpu	, "utf-8", "big5");	//重覆次號
						$erstno		= mb_convert_encoding($erstno	, "utf-8", "big5");	//到著站
						$erstig		= mb_convert_encoding($erstig	, "utf-8", "big5");	//到著站名稱
						$escsno		= mb_convert_encoding($escsno	, "utf-8", "big5");	//客戶代號
						$esname		= mb_convert_encoding($esname	, "utf-8", "big5");	//客戶名稱
						$epino		= mb_convert_encoding($epino	, "utf-8", "big5");	//清單編號
						$esdate		= mb_convert_encoding($esdate	, "utf-8", "big5");	//發送日期
						$ercsig		= mb_convert_encoding($ercsig	, "utf-8", "big5");	//收貨人名稱
						$ebamt		= mb_convert_encoding($ebamt	, "utf-8", "big5");	//本款
						$eramt		= mb_convert_encoding($eramt	, "utf-8", "big5");	//接費
						$esamt		= mb_convert_encoding($esamt	, "utf-8", "big5");	//送費
						$edamt		= mb_convert_encoding($edamt	, "utf-8", "big5");	//代款
						$elamt		= mb_convert_encoding($elamt	, "utf-8", "big5");	//聯運費
						$eoamt		= mb_convert_encoding($eoamt	, "utf-8", "big5");	//其它附加費
						$etotal		= mb_convert_encoding($etotal	, "utf-8", "big5");	//總運費
						$eqmny		= mb_convert_encoding($eqmny	, "utf-8", "big5");	//代收貨款
						$eadate		= mb_convert_encoding($eadate	, "utf-8", "big5");	//銷號日期
						$eajamt		= mb_convert_encoding($eajamt	, "utf-8", "big5");	//銷號件數
						$edcode		= mb_convert_encoding($edcode	, "utf-8", "big5");		//銷號狀況
						$ecode		= mb_convert_encoding($ecode	, "utf-8", "big5");	//銷號代碼
						$sign_code	= mb_convert_encoding($sign_code, "utf-8", "big5");	//簽收代碼
						$sign_cust	= mb_convert_encoding($sign_cust, "utf-8", "big5");		//簽收人
						$eatime		= mb_convert_encoding($eatime	, "utf-8", "big5");	//銷號時間

						$ercsig		=str_replace("　","", $ercsig);
						$sign_cust	=str_replace("　","", $sign_cust);
						$edcode		=str_replace("　","", $edcode);

						$ForwardDate = strtotime(substr($esdate, 0, 4)."-".substr($esdate, 4, 2)."-".substr($esdate, 6, 2));
						$ArriveDate = strtotime(substr($eadate, 0, 4)."-".substr($eadate, 4, 2)."-".substr($eadate, 6, 2)." ".substr($eatime, 0, 2).":".substr($eatime, 2, 2).":".substr($eatime, 4, 2));

						$logistics_id = trim($epino);

						$custom_orders = Mage::getModel('check/custom_order')->loadByLogisticsId($logistics_id);

						if (count($custom_orders) > 0)
						{
							foreach ($custom_orders as $custom_order)
							{
								if($ecode == "01" || $ecode == "05"){
									$custom_order->setArrivedAt($ArriveDate);
									$custom_order->setShipstatus(Settv_Check_Model_Custom_Order::STATE_ARRIVED, '系統');
									$custom_order->save();
								}else if($ecode == "02"){
									$custom_order->setShipstatus(Settv_Check_Model_Custom_Order::STATE_REJECTED, '系統');
									$custom_order->save();
								}
							}

							$logisticsmodel = Mage::getModel('check/custom_logistics')->load($logistics_id);
							$logisticsmodel->setData("forward_at",$ForwardDate);
							$logisticsmodel->setData("arrived_at",$ArriveDate);
							$logisticsmodel->setData("sendee",$ercsig);
							$logisticsmodel->setData("sign_cust",$sign_cust);
							$logisticsmodel->setData("charge_amt",$eqmny);
							$logisticsmodel->setData("ship_code",$ecode);
							$logisticsmodel->setData("ship_memo",$edcode);
							$logisticsmodel->save();
						}
					}
				}
				fclose($handle);

				//---Finish Process--
				rename($file, str_replace("process", "finish", $file));
				$handlestr .= $file . "<br>";

			}
			return ("Job Done : ".$handlestr);
		}
		else{
			return ("No any file update");
		}
	}

	//逆物流回覆
	public function ProcessReplyReverse(){
		$path     = Mage::getStoreConfig('carriers/hct/ftp', false);
		$username = Mage::getStoreConfig('carriers/hct/fusername', false);
		$password = Mage::getStoreConfig('carriers/hct/fpassword', false);
		$port     = Mage::getStoreConfig('carriers/hct/ftpport', false);
		$conn_id=ftp_connect($path, $port);// or die("Unable to connect to HCT FTP.");
		$login_result=ftp_login($conn_id, $username, $password);

		if ((!$conn_id) || (!$login_result)) {
			return ("FTP connection has failed !");
		}

		ftp_pasv($conn_id, true);

		ftp_chdir($conn_id, "DATA");
		$file_list=ftp_nlist($conn_id, "./settv*.xls");

		if(count($file_list)>0 and $file_list!=false){

			$folder = $this->createHctFolder('reply_reverse');

			//下載檔案至 process 資料夾
			foreach($file_list as $file_name){
				$process_file[]=$process_path="$folder/process/$file_name";
				if(ftp_get($conn_id, $process_path, $file_name, FTP_BINARY))
					ftp_delete($conn_id, $file_name);
			}

			//處理下載的檔案
			set_include_path(get_include_path().PATH_SEPARATOR.'phpexcel/Classes/');
			require_once("phpexcel/Classes/PHPExcel.php");
			require_once("phpexcel/Classes/PHPExcel/IOFactory.php");
			require_once("phpexcel/Classes/PHPExcel/Reader/Excel5.php");
			$objReader = PHPExcel_IOFactory::createReader('Excel5');

			foreach($process_file as $file){
				//取出資料
				$val=array();
				$objPHPExcel = $objReader->load($file);
				$currentSheet = $objPHPExcel->getSheet(0);
				$allLine = $currentSheet->getHighestRow();
				for ($row = 2; $row <= $allLine; $row++) {
					for ($column = 0; $column <= 15; $column++) {
						$val[$row][] = $currentSheet->getCellByColumnAndRow($column, $row)->getValue();
					}

					$esdate		=str_replace("　","", $val[$row][0]);//退貨日期
					$escsno		=str_replace("　","", $val[$row][1]);//客戶代號
					$ercsig		=str_replace("　","", $val[$row][2]);//收貨人
					$eraddr		=str_replace("　","", $val[$row][3]);//收貨地址
					$etaddr		=str_replace("　","", $val[$row][4]);//取貨地址
					$ettel		=str_replace("　","", $val[$row][5]);//取貨電話
					$etcsig		=str_replace("　","", $val[$row][6]);//取貨人
					$emark		=str_replace("　","", $val[$row][7]);//備註
					$ejamt		=str_replace("　","", $val[$row][8]);//件數
					$epino		=str_replace("　","", $val[$row][9]);//退貨單編號
					$edelno		=str_replace("　","", $val[$row][10]);//查貨號碼
					$rtype		=str_replace("　","", $val[$row][11]);//取貨狀況
					$rdate		=str_replace("　","", $val[$row][12]);//取貨回覆
					$asdate		=str_replace("　","", $val[$row][13]);//發送日期
					$ascdate	=str_replace("　","", $val[$row][14]);//入庫日期
					$asccode	=str_replace("　","", $val[$row][15]);//配送狀態

					//將資料寫入DB

					$ForwardDate = strtotime(substr($esdate, 0, 4)."-".substr($esdate, 4, 2)."-".substr($esdate, 6, 2));
					$ArriveDate = strtotime(substr($rdate, 0, 4)."-".substr($rdate, 4, 2)."-".substr($rdate, 6, 2));

					$order_Increment_id = trim($epino);
					$order = Mage::getModel('sales/order')->loadByIncrementId($order_Increment_id);
					$custom_order = Mage::getModel('check/custom_order')->loadByOrderId($order->getId());
						
					if ($custom_order->getId() != null)
					{
						if($rtype == "已取"){
							$custom_order->setArrivedAt($ArriveDate);
							$custom_order->setShipstatus(Settv_Check_Model_Custom_Order::STATE_RETURN_RECEIVED, '系統');
							$custom_order->save();
						}
						else //if($rtype == "不退")
						{
							$custom_order->setShipstatus(Settv_Check_Model_Custom_Order::STATE_RETURN_REJECTED, '系統');
							$custom_order->save();
						}

						$logisticsmodel = Mage::getModel('check/custom_logistics')->load($custom_order->getLogisticsId());

						if ($logisticsmodel->getId() != null)
						{
							$logisticsmodel->setData("forward_at",$ForwardDate);
							$logisticsmodel->setData("arrived_at",$ArriveDate);
							$logisticsmodel->setData("sendee",$ercsig);
							if($rtype == "已取"){
								$logisticsmodel->setShipCode("01");
							}
							else if($rtype == "不退"){
								$logisticsmodel->setShipCode("02");
							}
							else{
								$logisticsmodel->setShipCode("");
							}
							$logisticsmodel->setShipMemo($asccode);
							$logisticsmodel->save();
						}
					}
				}
				unset($objReader);
				unset($objPHPExcel);
				//Finish Process
				rename($file, str_replace("process", "finish", $file));
			}
			return ("Job Done");
		}
		else{
			return ("No any file update");
		}
	}

	private function createHctFolder($folder_name)
	{
		Mage::helper('common/common')->chdirToMageBase();
		$user_name = "apache";

		$folder="hct";
		if(!is_dir("$folder")){
			mkdir("$folder");
			chmod($folder, 755);
			chown($folder, $user_name);
			chgrp($folder, $user_name);
		}

		$folder="$folder/$folder_name";
		if(!is_dir("$folder")){
			mkdir("$folder");
			chmod($folder, 0755);
			chown($folder, $user_name);
			chgrp($folder, $user_name);
		}

		$dateTimeZoneTaipei = new DateTimeZone("Asia/Taipei");
		$dateTimeTaipei = new DateTime("now", $dateTimeZoneTaipei);
		$date = date_format($dateTimeTaipei, 'Ymd');
		$folder="$folder/$date";

		if(!is_dir("$folder")){
			mkdir("$folder");
			chmod($folder, 0755);
			chown($folder, $user_name);
			chgrp($folder, $user_name);
			mkdir("$folder/process");
			chmod("$folder/process", 0755);
			chown($folder, $user_name);
			chgrp($folder, $user_name);
			mkdir("$folder/finish");
			chmod("$folder/finish", 0755);
			chown($folder, $user_name);
			chgrp($folder, $user_name);
		}

		return $folder;
	}

	private function sendToHct($foler_name, $file_name, $csv_data)
	{
		$folder = $this->createHctFolder($foler_name);
		$process_path="$folder/process/$file_name";
		$finish_path="$folder/finish/$file_name";

		$folder=dir("$folder/process/");
		if(!is_file($folder->path.$file_name)){
			$handle=fopen($folder->path.$file_name, "x+");
		}else{
			$handle=fopen($folder->path.$file_name, "w+");
		}

		fwrite($handle, mb_convert_encoding($csv_data, "big5", "utf-8"));
		fclose($handle);

		$this->uploadFileToHctFtp($process_path,$finish_path, $file_name);
	}

	private function uploadFileToHctFtp($process_path, $finish_path, $remote_file_name)
	{
		//upload file to HCT ftp space
		if(is_file($process_path)){
			$path     = Mage::getStoreConfig('carriers/hct/ftp', false);
			$username = Mage::getStoreConfig('carriers/hct/fusername', false);
			$password = Mage::getStoreConfig('carriers/hct/fpassword', false);
			$port     = Mage::getStoreConfig('carriers/hct/ftpport', false);
			$conn_id = ftp_connect($path, $port) or die("Unable to connect to HCT FTP.");
			$login_result = ftp_login($conn_id, $username, $password);
			ftp_pasv($conn_id, true);
			if (ftp_put($conn_id, $remote_file_name, $process_path, FTP_ASCII))
			{
				rename($process_path, $finish_path);
			}
			ftp_close($conn_id);
		}
	}


}

