<?php
class Hct_LogisticHct_Helper_Order extends Mage_Core_Helper_Abstract
{
	public function isShippingByHct($order)
	{
		$shipping_title = Mage::helper('ordermanage')->getShippingTitleByOrder($order);
		if ($shipping_title == Mage::getStoreConfig("carriers/hct/title"))
			return true;
		else
			return false;
	}
}