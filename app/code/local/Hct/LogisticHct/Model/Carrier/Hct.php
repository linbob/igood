<?php
class Hct_LogisticHct_Model_Carrier_Hct extends Mage_Shipping_Model_Carrier_Abstract {
	protected $_code = 'hct';
	public $optValue = 400;
	
    public function collectRates(Mage_Shipping_Model_Rate_Request $request) {
        // skip if not enabled
        if (!Mage::getStoreConfig('carriers/'.$this->_code.'/active')) {
        	return false;
        }
 
        $result = Mage::getModel('shipping/rate_result');
        $handling = 0;
 
        $method = Mage::getModel('shipping/rate_result_method');
        $method->setCarrier($this->_code);
        $method->setCarrierTitle(Mage::getStoreConfig('carriers/'.$this->_code.'/title'));
        $method->setCost($handling);
        $method->setPrice($handling);
        $result->append($method);
        return $result; 
    }

	/**
	 * This method is used when viewing / listing Shipping Methods with Codes programmatically
	 */
	public function getAllowedMethods() {
	    return array($this->_code => $this->getConfigData('name'));
	}
}
?>