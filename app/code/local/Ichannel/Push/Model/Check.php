<?php

class Ichannel_Push_Model_Check {
    public function order() {
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "SELECT * FROM ichannels_order WHERE created_time BETWEEN ".(strtotime("now")-86400*15)." AND ".strtotime("now");
        $result = $readConnection->fetchAll($sql);

        foreach ($result as $row) {
            //讀取訂單編號
            $query = "SELECT order_id  FROM medium_order_item WHERE master_order_id = ".(int)$row['master_order_id'];
            $orders = $readConnection->fetchAll($query);
            foreach ($orders as $order) {
                $orderId[] = $order['order_id'];
                $master_order_id[$order['order_id']] = $row['master_order_id'];
            }
        }

        $client = new SoapClient(Mage::getBaseUrl().'api/?wsdl');

        $session = $client->login('sanlih', '1QAZ2wsx');

        $filter = array(array(
            'order_id' => array(
                array(
                    'in' => $orderId
                )
            )
        ));

        $results = $client->call($session, 'order.list', $filter);

        foreach ($results as $order) {
            if ($order['status'] == "order_established" && array_key_exists($order['order_id'], $master_order_id)) {
                $established_master_id[] = $master_order_id[$order['order_id']];
            } elseif ($order['status'] != "pending_payment") {
            	$canceled_master_id[] = $master_order_id[$order['order_id']];
            }
        }
        $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');

        $establishedmasterIds = join(',',$established_master_id);
        $canceledmasterIds = join(',', $canceled_master_id);

        if (!empty($established_master_id)) {
            $sql = 'UPDATE ichannels_order SET status = "established" WHERE master_order_id IN ('.$establishedmasterIds.') AND created_time BETWEEN '.(strtotime("now")-86400*15)." AND ".strtotime("now");
            $result = $writeConnection->query($sql);
            $sql = 'UPDATE ichannels_order SET status = "canceled" WHERE master_order_id IN ('.$canceledmasterIds.') AND created_time BETWEEN '.(strtotime("now")-86400*15)." AND ".strtotime("now");
            $result = $writeConnection->query($sql);
        } else {
            $sql = 'UPDATE ichannels_order SET status = "canceled" WHERE master_order_id IN ('.$canceledmasterIds.') AND created_time BETWEEN '.(strtotime("now")-86400*15)." AND ".strtotime("now");
            $result = $writeConnection->query($sql);
        }

    }
}
?>
