<?php
class Magetools_Catalog_Model_Observer {

	public function lowStock($observer)
	{
		$stockItem = $observer->getEvent()->getData('data_object');

		//ini_set('display_errors', 1);
    $product = Mage::getModel('catalog/product')->load($stockItem->getProductId());
    $type = $product->getTypeId();

    $qty = $stockItem->getQty();
    if($type == "configurable"){
      $sub_prods = $product->getTypeInstance(true)->getUsedProducts(null,$product);
      foreach($sub_prods as $product){
        $qty +=  intval(Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty());
      }
    }
		if($qty < $stockItem->getNotifyStockQty() && $qty != 0){
			$product = Mage::getModel('catalog/product')->load($stockItem->getProductId());
			$body = "{$product->getName()} :: {$product->getSku()} just Ran out of stock:\n\n";
			$body .= "Current Qty: {$stockItem->getQty()}\n";
			$body .= "Low Stock Date: {$stockItem->getLowStockDate()}\n";

			$mail = new Zend_Mail('UTF-8');
			$mail->setType(Zend_Mime::MULTIPART_RELATED);
			$mail->setBodyHtml($body);
			$mail->setFrom('lowstock@igood.tw', 'Low Stock Notifier');
			//$mail->addTo('tailiang.kuo@gmail.com', 'test');

			$supplier_id = $product->getSupplier();
			
			$collection = Mage::getModel('supplier/supplier')
				->getCollection()
				->addFieldToSelect('*')
				->addFieldToFilter('attribute_id', $supplier_id);
			
			$supplier = current($collection->load()->getIterator());
			$supplier_name = $supplier->getName();
			$mail->addTo($supplier->getContactEmail(), $supplier_name);
			$creator = Mage::getModel('customer/customer')->load($product->getCreatorId());
			if($creator->getId()){
				$mail->addTo($creator->getEmail(), $creator->getName());
			}
			$mail->setSubject('[Notice] '."{$product->getName()} :: {$product->getSku()} just Ran out of stock:\n\n");
			//exit();
			$mail->send();
		}
	}
	
	public function changeProductListener($observer)
	{
		$product = $observer->getEvent()->getData('data_object');
		if($product->getId()){
			$osp = $product->getData('supplier_percentage');
			$cost = $product->getProductCost();
			$price = $product->getFinalPrice();
			$sp = round(($price-$cost)/$price,2)*100;
			if( $osp !== $sp ){
				$product->setSupplierPercentage($sp);
			}
		}
	}
	
	
}