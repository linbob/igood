<?php 
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn(
		$this->getTable('supplier/supplier'),
		'supplier_group',
		'int(1) NOT NULL DEFAULT 0'
);
$entity = $this->getEntityTypeId('catalog_product');
$this->addAttribute($entity, 'supplier_supervisor_id', array(
		'type' => 'int',
		'attribute_set' =>  'supplierfrontendproductuploader_product_attributes',
		'backend' => '',
		'frontend' => '',
		'label' => 'Supplier Supervisor',
		'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
		'visible' => false,
		'required' => false,
		'user_defined' => true,
		'default' => 0,
		'searchable' => false,
		'filterable' => false,
		'comparable' => false,
		'visible_on_front' => false,
		'visible_in_advanced_search' => false,
));

$installer->updateAttribute('catalog_product','supplier_supervisor_id','frontend_input','select');
$installer->updateAttribute('catalog_product','supplier_supervisor_id','source_model','supplierfrontendproductuploader/source_supervisors');
$installer->updateAttribute('catalog_product','supplier_supervisor_id','frontend_label','Supplier Supervisor');
$installer->updateAttribute('catalog_product','supplier_supervisor_id','is_visible',1);

$installer->endSetup();