﻿<?php

require_once('HctShippingOrder.php');

class Medium_Eai_Hct_Adaptor  {
	private $hctShippingOrder;

	function __construct($hctShippingOrder) {
		$this->hctShippingOrder = $hctShippingOrder;
	}

	public function to_json() {
		return json_encode(array(
				array(
						'id' => $this->hctShippingOrder->getId(),
						'customerId' => $this->hctShippingOrder->getCustomerId(),
						'customerName' => $this->hctShippingOrder->getCustomerName(),
						'recepientName' => $this->hctShippingOrder->getRecepientName(),
						'recepientPhone1' => $this->hctShippingOrder->getRecepientPhone1(),
						'recepientPhone2' => $this->hctShippingOrder->getRecepientPhone2(),
						'recepientAddress' => $this->hctShippingOrder->getRecepientAddress(),
						'paymentAmount' => $this->hctShippingOrder->getPaymentAmount(),
						'numberOfItem' => $this->hctShippingOrder->getNumberOfItem(),
						'supplierId' => $this->hctShippingOrder->getSupplierId(),
						'supplierName' => $this->hctShippingOrder->getSupplierName(),
						'supplierPhone1' => $this->hctShippingOrder->getSupplierPhone1(),
						'supplierPhone2' => $this->hctShippingOrder->getSupplierPhone2(),
						'supplierAddress' => $this->hctShippingOrder->getSupplierAddress(),
						'note' => $this->hctShippingOrder->getNote()
				)
		));
	}
}
