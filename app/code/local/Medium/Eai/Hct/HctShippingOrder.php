<?php
class Medium_Eai_Hct_Shipping_Order {
	private $id;
	private $customerId;
	private $customerName;
	private $recepientName;
	private $recepientPhone1;
	private $recepientPhone2;
	private $recepientAddress;
	private $paymentAmount = "0";
	private $numberOfItem;
	private $supplierId;
	private $supplierName;
	private $supplierPhone1;
	private $supplierPhone2;
	private $supplierAddress;
	private $note;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getCustomerId(){
		return $this->customerId;
	}

	public function setCustomerId($customerId){
		$this->customerId = $customerId;
	}

	public function getCustomerName(){
		return $this->customerName;
	}

	public function setCustomerName($customerName){
		$this->customerName = $customerName;
	}

	public function getRecepientName(){
		return $this->recepientName;
	}

	public function setRecepientName($recepientName){
		$this->recepientName = $recepientName;
	}

	public function getRecepientPhone1(){
		return $this->recepientPhone1;
	}

	public function setRecepientPhone1($recepientPhone1){
		$this->recepientPhone1 = $recepientPhone1;
	}

	public function getRecepientPhone2(){
		return $this->recepientPhone2;
	}

	public function setRecepientPhone2($recepientPhone2){
		$this->recepientPhone2 = $recepientPhone2;
	}

	public function getRecepientAddress(){
		return $this->recepientAddress;
	}

	public function setRecepientAddress($recepientAddress){
		$this->recepientAddress = $recepientAddress;
	}

	public function getPaymentAmount(){
		return $this->paymentAmount;
	}

	public function setPaymentAmount($paymentAmount){
		$this->paymentAmount = $paymentAmount;
	}

	public function getNumberOfItem(){
		return $this->numberOfItem;
	}

	public function setNumberOfItem($numberOfItem){
		$this->numberOfItem = $numberOfItem;
	}

	public function getSupplierId(){
		return $this->supplierId;
	}

	public function setSupplierId($supplierId){
		$this->supplierId = $supplierId;
	}

	public function getSupplierName(){
		return $this->supplierName;
	}

	public function setSupplierName($supplierName){
		$this->supplierName = $supplierName;
	}

	public function getSupplierPhone1(){
		return $this->supplierPhone1;
	}

	public function setSupplierPhone1($supplierPhone1){
		$this->supplierPhone1 = $supplierPhone1;
	}

	public function getSupplierPhone2(){
		return $this->supplierPhone2;
	}

	public function setSupplierPhone2($supplierPhone2){
		$this->supplierPhone2 = $supplierPhone2;
	}

	public function getSupplierAddress(){
		return $this->supplierAddress;
	}

	public function setSupplierAddress($supplierAddress){
		$this->supplierAddress = $supplierAddress;
	}

	public function getNote(){
		return $this->note;
	}

	public function setNote($note){
		$this->note = $note;
	}
}
