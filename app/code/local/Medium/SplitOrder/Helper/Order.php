<?php
class Medium_SplitOrder_Helper_Order extends Mage_Core_Helper_Abstract {

	public function getSimpleItemByOrder($order)
	{
		$return_item = null;
		$real_name = null;
		$price = 0;
		foreach ($order->getAllItems() as $item)
		{
			if($item->getParentItem())
			{		
				$real_name = $item->getName();
			}
			else 
			{
				$return_item = $item;
				$price = round($item->getPrice(), 0);
			}
		}
		
		if ($real_name != null)
			$return_item->setRealName($real_name);
		else
			$return_item->setRealName($return_item->getName());
		
		$return_item->setRealPrice($price);
		
		return $return_item;
	}

	public function isDisplayRefOrders($order)
	{
		$orders = Mage::getModel('splitorder/items')->getOrdersOfSameMasterByOrder($order);
		if (count($orders) > 1)
			return true;
		else 
			return false;
	}

	public function getLogisticCompany($order)
	{
		$productId = $this->getSimpleItemByOrder($order)->getProductId();
		$product = Mage::getModel('catalog/product')->load($productId);
		$logistics_com = $product->getAttributeText('logistics_com');		
		return $logistics_com;
	}
}