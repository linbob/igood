<?php
class Medium_SplitOrder_Helper_Supplier extends Mage_Core_Helper_Abstract {

	public function getSupplierIdByOrder($order)
	{
		$product_id = Mage::helper('splitorder/order')->getSimpleItemByOrder($order)->getProductId();
		$product = Mage::getModel('catalog/product')->load($product_id);
		$supplier_id = Mage::getModel('supplier/supplier')->getSupplierByProduct($product)->getId();

		return $supplier_id;
	}
	
	public function getSupplierByOrder($order)
	{
		$product_id = Mage::helper('splitorder/order')->getSimpleItemByOrder($order)->getProductId();
		$product = Mage::getModel('catalog/product')->load($product_id);
		$supplier = Mage::getModel('supplier/supplier')->getSupplierByProduct($product);
	
		return $supplier;
	}
}