<?php

class Medium_SplitOrder_Model_Adminhtml_Sales_Order_Create extends Mage_Adminhtml_Model_Sales_Order_Create
{  
    public function createOrder()
    {
        $this->_prepareCustomer();
        $this->_validate();
        $quote = $this->getQuote();
        $this->_prepareQuoteItems();

        $service = Mage::getModel('sales/service_quote', $quote);
        if ($this->getSession()->getOrder()->getId()) {
            $oldOrder = $this->getSession()->getOrder();
            $originalId = $oldOrder->getOriginalIncrementId();
            if (!$originalId) {
                $originalId = $oldOrder->getIncrementId();
            }
            $orderData = array(
                'original_increment_id'     => $originalId,
                'relation_parent_id'        => $oldOrder->getId(),
                'relation_parent_real_id'   => $oldOrder->getIncrementId(),
                'edit_increment'            => $oldOrder->getEditIncrement()+1,
                'increment_id'              => $originalId.'-'.($oldOrder->getEditIncrement()+1)
            );
            $quote->setReservedOrderId($orderData['increment_id']);
            
            //set master order id
            $master_order_id = Mage::getModel('splitorder/items')->getMasterOrderIdByOrderId($oldOrder->getId());           
            if ($master_order_id != null 
            	&& $master_order_id != $oldOrder->getId()) //no master id
            	$quote->setReservedMasterOrderId($master_order_id);
            
            $service->setOrderData($orderData);
        }

        $master_order = $service->submit();
             
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderData['increment_id']);        
        $this->insertAddtionalData($oldOrder->getId(), $order);
        
        if ((!$quote->getCustomer()->getId() || !$quote->getCustomer()->isInStore($this->getSession()->getStore()))
            && !$quote->getCustomerIsGuest()
        ) {
            $quote->getCustomer()->setCreatedAt($order->getCreatedAt());
            $quote->getCustomer()
                ->save()
                ->sendNewAccountEmail('registered', '', $quote->getStoreId());;
        }
        if ($this->getSession()->getOrder()->getId()) {
            $oldOrder = $this->getSession()->getOrder();

            $this->getSession()->getOrder()->setRelationChildId($order->getId());
            $this->getSession()->getOrder()->setRelationChildRealId($order->getIncrementId());
            $this->getSession()->getOrder()->cancel()
                ->save();
            $order->save();
        }
        if ($this->getSendConfirmation()) {
            $order->sendNewOrderEmail();
        }

        Mage::dispatchEvent('checkout_submit_all_after', array('order' => $order, 'quote' => $quote));

        return $order;
    }
    
    private function insertAddtionalData($original_order_id, $order)
    {
    	$origin_custom_order = Mage::getModel('check/custom_order')->loadByOrderId($original_order_id);    	
    	$order_id = $order->getId();
    	$custom_order = Mage::getModel('check/custom_order')->loadByOrderId($order_id);    	
    	$shipping_method = $this->getShippingMethod();
    	$order->setShippingMethod($shipping_method);
    	$order->setState('established', true, '修改訂單');
    	
    	$shipping_title = Mage::helper('ordermanage')->getShippingTitleByOrder($order);

		if ($shipping_method == 'familymart_') {
			
			$fm_shipname = $this->getFmshipname();
			$fm_name = $this->getFmstorename();
			$fm_code = $this->getFmstorecode();
			$fm_addr = $this->getFmstoreaddr();
			
			$shipping_title = "全家取貨<br/>取貨門市:".$fm_name."<br/>";	
			$order->setShippingDescription($shipping_title);		
			$shipping_address = $order->getShippingAddress();
			$shipping_address
			->setFirstname($fm_shipname)
			->setLastname('')
			->setCity('')
			->setPostcode('')
			->setStreet($fm_addr)
			->setCountry_id()
			->setRegion('')
			->setCollectShippingrates(true);
			$order->setShippingAddress($shipping_address);
						
			$fmarray = array('fmstorename' => $fm_name, 'fmstoreaddr'=> $fm_addr, 'fmstorecode' => $fm_code);
			$fm_desc = serialize($fmarray);			
			$custom_order->setShippingdescription($fm_desc);
			$custom_order->setShippingtype("familymart");
		}
		else
		{
			$order->setShippingDescription($shipping_title);
			$custom_order->setShippingtype("frontdoor");			
		}
		
		$order->save();
		
		//Invoice
		$custom_order->setInvoicetype($origin_custom_order->getInvoicetype());
		$custom_order->setInvoiceubn($origin_custom_order->getInvoiceubn());
		$custom_order->setInvoicecom($origin_custom_order->getInvoicecom());
		$custom_order->setInvoiceCarriertype($origin_custom_order->getInvoiceCarriertype());
		$custom_order->setInvoiceCarriernum1($origin_custom_order->getInvoiceCarriernum1());
		$custom_order->setInvoiceCarriernum2($origin_custom_order->getInvoiceCarriernum2());
		$custom_order->setInvoiceDonateorg($origin_custom_order->getInvoiceDonateorg());
		$custom_order->setInvoiceDonateorgnum($origin_custom_order->getInvoiceDonateorgnum());
		$custom_order->setInvoiceIsdonate($origin_custom_order->getInvoiceIsdonate());
		$custom_order->setFamilymartPincode($origin_custom_order->getFamilymartPincode());
		
		$custom_order->save();
    }

}

