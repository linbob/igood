<?php
class Medium_SplitOrder_Model_Items extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('splitorder/items');
	}

	public function getOrdersOfSameMasterByOrder($order)
	{
		$order_id = $order->getId();
		$master_order_id = $this->getMasterOrderIdByOrderId($order_id);
		$orders = $this->getOrdersByMasterOrderId($master_order_id);
		return $orders;
	}

	public function getMasterOrderIdByOrderId($order_id)
	{
		$master_order_id = $this->load($order_id, 'order_id')->getMasterOrderId();
		
		if ($master_order_id == null)
			$master_order_id = $order_id;
		
		return $master_order_id;
	}

	public function getMasterOrderByOrderId($order_id)
	{
		$master_id = $this->getMasterOrderIdByOrderId($order_id);
		$master_order = Mage::getModel('splitorder/orders')->loadById($master_id);
		return $master_order;
	}

	//$master_order_id => master_order_id or order_id
	public function getOrdersByMasterOrderId($master_order_id)
	{
		//TODO: add ship status

		$sales_orders = array();

		$collection = $this->getCollection()
		->addFieldToFilter('master_order_id', array('eq' => $master_order_id));

		if (count($collection) > 0)
		{
			foreach ($collection as $order)
			{
				$sales_order = Mage::getModel('sales/order')->load($order->getOrderId());
				array_push($sales_orders, $sales_order);
			}
		}
		else
		{
			$order = Mage::getModel('sales/order')->load($master_order_id);
			if ($order->getId() != null)
			{
				array_push($sales_orders, $order);
			}
		}	

		return $sales_orders;
	}

}
