<?php
class Medium_SplitOrder_Model_Orders extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('splitorder/orders');
	}
	
	public function loadById($master_order_id)
	{
		$master_order = $this->load($master_order_id);
		
		if ($master_order->getId() == null)		
			$master_order->setId($master_order_id);
		
		$orders = Mage::getModel("splitorder/items")->getOrdersByMasterOrderId($master_order_id);				
		$master_order = Mage::helper("ordermanage")->setSummaryOfOrders($orders, $master_order);
	
		return $master_order;
	}
	
	public function createMasterOrderByOrder($order)
	{
		$create_time = Mage::getSingleton('core/date')->gmtdate();
		$master_order = Mage::getModel("splitorder/orders")->setCreateTime($create_time)->save();;
		$master_order_id = $master_order->getId();
		$order_item = Mage::getModel("splitorder/items")
		->setMasterOrderId($master_order_id)
		->setOrderId($order->getId())
		->setCreateTime($create_time)
		->save();		
		return $master_order_id;
	}
	
	
}
