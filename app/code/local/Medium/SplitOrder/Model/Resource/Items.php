<?php
class Medium_SplitOrder_Model_Resource_Items extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {    	
        $this->_init('splitorder/items', 'order_item_id');
    }
}
