<?php
class Medium_SplitOrder_Model_Resource_Orders extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {    	
        $this->_init('splitorder/orders', 'master_order_id');
    }
}
