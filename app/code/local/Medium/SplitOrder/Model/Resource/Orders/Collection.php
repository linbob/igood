<?php
class Medium_SplitOrder_Model_Resource_Einvoices_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	protected function _construct()
	{
		$this->_init('splitorder/orders');
	}	
}
