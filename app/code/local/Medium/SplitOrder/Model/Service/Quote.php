<?php
class Medium_SplitOrder_Model_Service_Quote extends Mage_Sales_Model_Service_Quote
{
	protected function _validate()
	{
		$helper = Mage::helper('sales');
		if (!$this->getQuote()->isVirtual()) {
			$address = $this->getQuote()->getShippingAddress();
			$addressValidation = $address->validate();
			if ($addressValidation !== true) {
				Mage::throwException(
				$helper->__('Please check shipping address information. %s', implode(' ', $addressValidation))
				);
			}
		}

		$addressValidation = $this->getQuote()->getBillingAddress()->validate();
		if ($addressValidation !== true) {
			Mage::throwException(
			$helper->__('Please check billing address information. %s', implode(' ', $addressValidation))
			);
		}

		if (!($this->getQuote()->getPayment()->getMethod())) {
			Mage::throwException($helper->__('Please select a valid payment method.'));
		}

		return $this;
	}

	public function submitOrder()
	{
		$this->_deleteNominalItems();
		$this->_validate();
		$quote = $this->_quote;
		$isVirtual = $quote->isVirtual();

		$transaction = Mage::getModel('core/resource_transaction');
		if ($quote->getCustomerId()) {
			$transaction->addObject($quote->getCustomer());
		}
		$transaction->addObject($quote);

		$orders = array();

		foreach ($quote->getAllItems() as $item) {

			if ($item->getProduct()->getTypeId() != 'simple')
				continue;

			//Address
			if ($isVirtual) {
				$order = $this->_convertor->addressToOrder($quote->getBillingAddress());
			} else {
				$order = $this->_convertor->addressToOrder($quote->getShippingAddress());
			}
			$order->setBillingAddress($this->_convertor->addressToOrderAddress($quote->getBillingAddress()));
			if ($quote->getBillingAddress()->getCustomerAddress()) {
				$order->getBillingAddress()->setCustomerAddress($quote->getBillingAddress()->getCustomerAddress());
			}
			if (!$isVirtual) {
				$order->setShippingAddress($this->_convertor->addressToOrderAddress($quote->getShippingAddress()));
				if ($quote->getShippingAddress()->getCustomerAddress()) {
					$order->getShippingAddress()->setCustomerAddress($quote->getShippingAddress()->getCustomerAddress());
				}
			}

			//Payment
			$order->setPayment($this->_convertor->paymentToOrderPayment($quote->getPayment()));

			foreach ($this->_orderData as $key => $value) {
				$order->setData($key, $value);
			}

			$orderItem = $this->_convertor->itemToOrderItem($item);

			//Parent product
			if ($item->getParentItem()) {
				//use sample product to find configurable product
				$parentItem = $item->getParentItem();
				$parentOrderItem = $this->_convertor->itemToOrderItem($parentItem);
				$order->addItem($parentOrderItem);

				//reset amount
				$total = ($parentItem->getQty())*($parentItem->getPrice());
				$order->setTotalQtyOrdered($parentItem->getQty());
				$order->setSubtotal($total);
				$order->setGrandTotal($total);

				$orderParentItem = $order->getItemByQuoteItemId($item->getParentItem()->getId());
				$orderItem->setParentItem($orderParentItem);
			}
			else
			{
				//reset amount
				$total = ($item->getQty())*($item->getPrice());
				$order->setTotalQtyOrdered($item->getQty());
				$order->setSubtotal($total);
				$order->setGrandTotal($total);
			}
				
			$increment_id = "";
			if ($quote->getReservedOrderId() != null) //edit functon
				$increment_id = $quote->getReservedOrderId();
			else
				$increment_id = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($quote->getStoreId());
				
			$order->setIncrementId($increment_id);
			$order->addItem($orderItem);
			$order->setQuote($quote);

			$transaction->addCommitCallback(array($order, 'place'));
			$transaction->addCommitCallback(array($order, 'save'));

			array_push($orders, $order);
			$transaction->addObject($order);

			$order = null;
		}

		//Master Order
		$master_order = null;
		$master_order_id = $quote->getReservedMasterOrderId();
		
		if ($master_order_id == null) //edit functon
		{
			$create_time = Mage::getSingleton('core/date')->gmtdate();
			$master_order = Mage::getModel("splitorder/orders")->setCreateTime($create_time);
			$transaction->addObject($master_order);
		}
		else
		{
			$master_order = Mage::getModel("splitorder/orders")->loadById($master_order_id);
		}

		Mage::dispatchEvent('checkout_type_onepage_save_order', array('quote'=>$quote));
		Mage::dispatchEvent('sales_model_service_quote_submit_before', array('quote'=>$quote));

		try {
			$transaction->save();
			$this->_inactivateQuote();

			Mage::dispatchEvent('sales_model_service_quote_submit_success', array('quote'=>$quote)); //Inventory submit

		} catch (Exception $e) {

			if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
				// reset customer ID's on exception, because customer not saved
				$quote->getCustomer()->setId(null);
			}

			foreach ($orders as $sub_order) {
				//reset order ID's on exception, because order not saved
				$sub_order->setId(null);
				/** @var $item Mage_Sales_Model_Order_Item */
				foreach ($sub_order->getItemsCollection() as $item) {
					$item->setOrderId(null);
					$item->setItemId(null);
				}
			}

			Mage::dispatchEvent('sales_model_service_quote_submit_failure', array('quote'=>$quote)); //Inventory back
			throw $e;
		}

		$this->saveSubOrder($master_order, $orders);
		$this->saveOrderAfter($orders);

		$this->_order = $master_order;

		return $master_order;
	}

	private function saveSubOrder($master_order, $orders)
	{
		foreach ($orders as $order) {
			$order_item = Mage::getModel("splitorder/items")
			->setMasterOrderId($master_order->getId())
			->setOrderId($order->getId())
			->setCreateTime(Mage::getSingleton('core/date')->gmtdate())
			->save();
		}
	}

	private function saveOrderAfter($orders) {

		$PostData = Mage::getSingleton('checkout/session')->getOnepagedata();
		if($PostData){

			foreach ($orders as $order) {
				$this->createCustomOrder($order, $PostData);
				$this->stateHandler($order);
				$this->shippingHandler($order, $PostData);
				$this->paymentHandler($order, $PostData);
			}

			Mage::getSingleton('checkout/session')->setOnepagedata();
		}

	}

	private function stateHandler($order)
	{
		$session  = Mage::getSingleton('customer/session');
		$order_state = $session->getData('order_state');
		if (!empty($order_state))
		{
			$order->setState($order_state, true, '')->save();
		}
	}

	private function paymentHandler($order, $PostData)
	{
		$data = Mage::getSingleton('checkout/session')->getQuote()->getPayment();

		//Send mail to Supplier
		$PayMethod = $order->getPayment()->getMethodInstance()->getCode();
		if ($PayMethod == "cashondelivery")
		{
			$order->sendNewOrderEmail();
				
			//Send mail to supplier
			$emailTemplateVariables = array();
			$emailTemplateVariables['order'] = $order;
			Mage::helper('common/common')->emailSupplierByOrder($order, 'New Order to Supplier', $emailTemplateVariables);
		}

		//Payment: Chinatrust installment
		if (is_array($PostData) && array_key_exists("installment", $PostData) && $PostData["installment"]!=="") { //set data period (installment)
			$period = $PostData["installment"];
			$order->getPayment()->setAdditionalData($period)->save();
		}
	}

	private function createCustomOrder($order, $PostData)
	{
		//save to custom order
		$model = Mage::getModel('check/custom_order');
		$model->setOrderId($order->getId());
		foreach($PostData as $index => $value){
			$model->setData($index,$value);
		}
		$supplier_id = Mage::helper('splitorder/supplier')->getSupplierIdByOrder($order);
		$model->setData('supplier_id', $supplier_id);
		$model->setData('shipstatus', 'Not Shipped');
		$model->save();
	}

	private function shippingHandler($order, $PostData)
	{
		$session  = Mage::getSingleton('customer/session');

		// Shipping
		if ($session->getLogistic() == 'familymart') {
			$order->setShippingMethod("familymart_");
			$order->setShippingDescription($session->getLogisticDescription());

			$_shippingAddress = $order->getShippingAddress();
			$fminfo = unserialize($PostData["shippingdescription"]);
			$fmshipname = $fminfo['fmshipname'];
			$fmstoreaddr = $fminfo['fmstoreaddr'];
			$_shippingAddress
			->setFirstname($fmshipname)
			->setLastname('')
			//->setTelephone('')
			//->setCellphone('')
			->setCity('')
			->setPostcode('')
			->setStreet($fmstoreaddr)
			->setCountry_id()
			->setRegion('')
			->setCollectShippingrates(true);
			$order->setShippingAddress($_shippingAddress);

		} else {			
			$logistics_com = Mage::helper('splitorder/order')->getLogisticCompany($order);			
			$order->setShippingDescription($logistics_com);	
			$shipping_method = Mage::helper('ordermanage')->getShippingMethodByCompany($logistics_com);			
			$order->setShippingMethod($shipping_method);
		}
		$order->save();
	}


}
?>