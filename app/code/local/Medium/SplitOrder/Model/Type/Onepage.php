<?php
class Medium_SplitOrder_Model_Type_Onepage extends Mage_Checkout_Model_Type_Onepage
{
	protected function validateOrder()
	{
		$helper = Mage::helper('checkout');
		if ($this->getQuote()->getIsMultiShipping()) {
			Mage::throwException($helper->__('Invalid checkout type.'));
		}

		$addressValidation = $this->getQuote()->getBillingAddress()->validate();
		if ($addressValidation !== true) {
			Mage::throwException($helper->__('Please check billing address information.'));
		}

		if (!($this->getQuote()->getPayment()->getMethod())) {
			Mage::throwException($helper->__('Please select valid payment method.'));
		}
	}


	//20120627 Disable CreditCard to Send New-Order-Mail before checkout by Sean
	public function saveOrder($SendFlag = true)
	{
		$this->validate();
		$isNewCustomer = false;
		switch ($this->getCheckoutMethod()) {
			case self::METHOD_GUEST:
				$this->_prepareGuestQuote();
				break;
			case self::METHOD_REGISTER:
				$this->_prepareNewCustomerQuote();
				$isNewCustomer = true;
				break;
			default:
				$this->_prepareCustomerQuote();
				break;
		}

		$service = Mage::getModel('sales/service_quote', $this->getQuote());
		$service->submitAll();

		if ($isNewCustomer) {
			try {
				$this->_involveNewCustomer();
			} catch (Exception $e) {
				Mage::logException($e);
			}
		}

		$this->_checkoutSession->setLastQuoteId($this->getQuote()->getId())
		->setLastSuccessQuoteId($this->getQuote()->getId())
		->clearHelperData();

		$master_order = $service->getOrder();
		$orders = Mage::getModel("splitorder/items")->getOrdersByMasterOrderId($master_order->getId());
		//$redirectUrl = $this->getQuote()->getPayment()->getOrderPlaceRedirectUrl();

		/*
		 if (!$redirectUrl && $SendFlag) {
		try {
		//$order->sendNewOrderEmail(); //TODO: send mail
		} catch (Exception $e) {
		Mage::logException($e);
		}
		}*/

		// add order information to the session
		$master_order_id = $master_order->getId();
		$this->_checkoutSession
		->setLastOrderId($master_order_id)
		->setLastMasterOrderId($master_order_id);
		//->setRedirectUrl($redirectUrl);
			
		foreach ($orders as $order)
		{
			$this->orderHandler($order, $service);
		}

		return $this;
	}

	private function orderHandler($order, $service)
	{
		if ($order) {
			Mage::dispatchEvent('checkout_type_onepage_save_order_after',
			array('order'=>$order, 'quote'=>$this->getQuote()));

			// as well a billing agreement can be created
			$agreement = $order->getPayment()->getBillingAgreement();
			if ($agreement) {
				$this->_checkoutSession->setLastBillingAgreementId($agreement->getId());
			}
		}
			
		// add recurring profiles information to the session
		$profiles = $service->getRecurringPaymentProfiles();
		if ($profiles) {
			$ids = array();
			foreach ($profiles as $profile) {
				$ids[] = $profile->getId();
			}
			$this->_checkoutSession->setLastRecurringProfileIds($ids);
			// TODO: send recurring profile emails
		}

		Mage::dispatchEvent('checkout_submit_all_after',array('order' => $order, 'quote' => $this->getQuote(), 'recurring_profiles' => $profiles));

	}

}

?>