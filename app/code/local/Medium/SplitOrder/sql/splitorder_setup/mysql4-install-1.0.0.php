<?php

$installer = $this;
$installer->startSetup();

$installer->run("
	CREATE TABLE IF NOT EXISTS {$this->getTable('medium_order')} (
	`master_order_id` INT(10) NOT NULL AUTO_INCREMENT,	
	`create_time` DATETIME NOT NULL,
	PRIMARY KEY (`master_order_id`)
	);
");

$installer->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('medium_order_item')} (
  `order_item_id` INT(10) NOT NULL AUTO_INCREMENT,
  `master_order_id` INT(10) NOT NULL,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `create_time` DATETIME NOT NULL,
  PRIMARY KEY (`order_item_id`),
  UNIQUE INDEX `order_id_UNIQUE` (`order_id` ASC),
  CONSTRAINT `ref_order_id`
    FOREIGN KEY (`order_id`)
    REFERENCES `sales_flat_order` (`entity_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ref_master_order_id`
    FOREIGN KEY (`master_order_id`)
    REFERENCES `medium_order` (`master_order_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
	);			
");

$installer->endSetup();