<?php

$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE {$this->getTable('medium_order')} AUTO_INCREMENT = 900000001;
");

$installer->endSetup();