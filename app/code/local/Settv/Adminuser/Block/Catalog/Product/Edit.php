<?php
class Settv_Adminuser_Block_Catalog_Product_Edit extends Mage_Adminhtml_Block_Catalog_Product_Edit
{

	public function getHeader()
	{
		$header = parent::getHeader();

		$product = $this->getProduct();
		if ($product->getId()) {
			$parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
			$url = Mage::getUrl($product->getUrlPath());

			// jeff: this only works when default product have one configurable parent
			if(!empty($parentIds)) {
				$parentProduct = Mage::getModel('catalog/product')->load($parentIds[0]);
					
				if($parentProduct->getId()) {
					$url = Mage::getUrl($parentProduct->getUrlPath());
				}
			}

			$header .= "&nbsp&nbsp<a href='$url"  . "?preview=true'" . "target='_blank'>(" . Mage::helper('adminhtml')->__('Preview Product') . ")</a>";
			
			$user = Mage::getSingleton('admin/session')->getUser();
			$is_supplier = $user->getIsSupplier();
			if($is_supplier){
				$header .= "<script type='text/javascript'
						>disableAllProductInput()</script>";
			}
		}

		return $header;
	}

	protected function _prepareLayout()
	{
		$this->getLayout()->getBlock('head')->addItem("skin_js", "settvjs/jquery-1.9.0.min.js");
		$this->getLayout()->getBlock('head')->addItem("skin_js", "settvjs/product.js");

		$return =  parent::_prepareLayout();

		if($this->getChild("save_button")) {
			$this->setChild('save_button',
					$this->getLayout()->createBlock('adminhtml/widget_button')
					->setData(array(
							'label'     => Mage::helper('catalog')->__('Save'),
							'onclick'   => 'if(productApprovalConfirm()){productForm.submit();}',
							'class' => 'save'
					))
			);
		}

		if($this->getChild("save_and_edit_button")) {
			$this->setChild('save_and_edit_button',
					$this->getLayout()->createBlock('adminhtml/widget_button')
					->setData(array(
							'label'     => Mage::helper('catalog')->__('Save and Continue Edit'),
							'onclick'   => 'if(productApprovalConfirm()){saveAndContinueEdit(\''.$this->getSaveAndContinueUrl().'\');}',
							'class' => 'save'
					))
			);
		}
		
		$user = Mage::getSingleton('admin/session')->getUser();
		$is_supplier = $user->getIsSupplier();	
		if($is_supplier){		
			$this->unsetChild('reset_button');
			$this->unsetChild('save_button');
			$this->unsetChild('save_and_edit_button');
			$this->unsetChild('delete_button');
			$this->unsetChild('duplicate_button');
		}

		return $return;
	}


}