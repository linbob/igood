<?php 

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$installer->run("
		ALTER TABLE {$this->getTable('admin/user')} ADD COLUMN `is_supplier` smallint(6) DEFAULT 0;
		");

$installer->endSetup();
?>