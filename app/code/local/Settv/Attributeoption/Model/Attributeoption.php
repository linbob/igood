<?php
class Settv_Attributeoption_Model_Attributeoption extends Mage_Core_Model_Abstract
{
	public function _construct() {
		parent::_construct();
		$this->_init('attributeoption/attributeoption'); // this is location of the resource file.
	}

	public function loadByField($field, $value) {
		$id = $this->getResource()->loadByField($field, $value);
		$this->load($id);
	}
}