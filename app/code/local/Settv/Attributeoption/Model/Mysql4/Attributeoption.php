<?php
class Settv_Attributeoption_Model_Mysql4_Attributeoption extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
	{
		$this->_init('attributeoption/attributeoption', 'value_id');
	}

	public function loadByField($field, $value) {
		$table = $this->getMainTable();
		$where = $this->_getReadAdapter()->quoteInto("$field = ?", $value);
		$select = $this->_getReadAdapter()->select()->from($table, array('value_id'))->where($where);
		$id = $this->_getReadAdapter()->fetchOne($sql);
		return $id;
	}
}