<?php
class Settv_Cart_IndexController extends Mage_Core_Controller_Front_Action{
    public function indexAction(){
        $productId = $this->getRequest()->getParam('id');
//	$qty = $this->getRequest()->getParam('qty');
        $key = Mage::getSingleton('core/session')->getFormKey();;
	$product = Mage::getModel('catalog/product')
		->setStoreId(Mage::app()->getStore()->getId())
		->load($productId);
	
	$cart = Mage::getSingleton('checkout/cart/add');
	$cart->init();
	//echo 'Id: '.$product->getId();
	$count = 1;
        if (empty($product->getId())){ //if no product id, redirect to homepage
            $this->_redirect('');
        }
        else{ //redirect to the add to cart with the form key
		if($product->getId()) {
//			$cart = Mage::getSingleton('checkout/cart/add');
			//$cart->truncate();			
//			$cart->init();
			$param = array(
			    'product' => $productId,
			    'form_key' => $key
			    //'qty' => $qty
			);
		$cart->addProduct($product,$param);
		$cart->save();
		if (!$cart->getQuote()->getHasError()){
//                $this->_redirectUrl($product->getProductUrl());
		$this->_redirect('checkout/cart'); 
                }
	    }
        }
    }
}

