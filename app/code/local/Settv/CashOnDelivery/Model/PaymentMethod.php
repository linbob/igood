<?php
class Settv_CashOnDelivery_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract {
    protected $_code = 'cashondelivery';
    protected $_formBlockType = 'cashondelivery/dispfields';
    protected $_isInitializeNeeded = true;

        public function assignData($data) {
            $session = Mage::getSingleton('customer/session');
            if (!is_null($session)) {
                $session->setData('order_state', 'established');
            }

            if (!($data instanceof Varien_Object)) {
                $data = new Varien_Object($data);
            }

            $info = $this->getInfoInstance();
            $info->setEtixType($data->getEtixType());

            return $this;
      }

        public function getCheckoutRedirectUrl() {
                $redirectUrl = Mage::getUrl("checkout/onepage/success");

                return $redirectUrl;
        }
}
