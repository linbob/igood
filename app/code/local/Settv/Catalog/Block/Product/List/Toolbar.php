<?php
class Settv_Catalog_Block_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{
	
	/**
	 * Init Toolbar
	 *
	 */
	protected function _construct()
	{
		parent::_construct();
		$this->_orderField = 'created_at';
		$this->_direction  = 'desc';

	}

	/**
	 * Retrieve available limits for specified view mode
	 *
	 * @return array
	 */
	public function getAvailableOrders()
	{
		// return $this->_availableOrder;

		$t = array(
				'created_at' => $this->__('Create Date'),             // sort by Date option (default)
				'price' => $this->__('Price Sort')
				// 'position'      => $this->__('Best Value'),
				// 'name'          => $this->__('Name')
		);

		return $t;
	}
}