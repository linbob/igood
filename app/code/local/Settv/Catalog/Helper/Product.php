<?php

class Settv_Catalog_Helper_Product extends Mage_Catalog_Helper_Product
{
	public function canShow($product, $where = 'catalog')
	{
		$canShow = parent::canShow($product);
		
		if($this->_getRequest()->getParam("preview") == true) {
			$canShow = true;
		}
		
		return $canShow;
	}
}
