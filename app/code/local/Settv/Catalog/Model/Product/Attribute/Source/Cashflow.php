﻿<?php

class Settv_Catalog_Model_Product_Attribute_Source_Cashflow extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	public function getAllOptions()
	{
		if (!$this->_options) {
			$payments = Mage::getSingleton('payment/config')->getActiveMethods();
			foreach ($payments as $paymentCode=>$paymentModel) {
				//Skip QR Code payment due to not available for current design. By Ricahrd 2014.01.06
				if($paymentCode == 'paymentqrcode') continue;
				$paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
				$methods[$paymentCode] = array(
						'label'   => $paymentTitle,
						'value' => $paymentCode,
				);
			}

			$this->_options = $methods;
		}
		return $this->_options;
	}
}