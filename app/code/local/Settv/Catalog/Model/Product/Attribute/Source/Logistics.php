<?php

class Settv_Catalog_Model_Product_Attribute_Source_Logistics extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	public function getAllOptions()
	{
		if (!$this->_options) {
			$methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
			
			$options = array();
			
			foreach($methods as $_code => $_method)
			{
				if(!$_title = Mage::getStoreConfig("carriers/$_code/title"))
					$_title = $_code;
			
				if(isset($_method->optValue)) {
					$_code = $_method->optValue;
				}
				
				$options[] = array('value' => $_code, 'label' => $_title);
			}
			
			$this->_options = $options;
		}
		return $this->_options;
	}
}