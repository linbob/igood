<?php
class Settv_Check_Model_Custom_Logistics extends Mage_Core_Model_Abstract{
	public function _construct() {
		parent::_construct();
		$this->_init('check/custom_logistics');
	}

	public function createLogistics()
	{
		$create_time = Mage::getSingleton('core/date')->gmtdate();
		$this->setCreatedAt($create_time);
		$this->save();			
		return $this;
	}
	
	public function getlogisticsnum($logistics_id) {
		$Lnum = $logistics_id % 10000 + 178251581 ;
		$Cnum = $Lnum % 7;
		$Lnum = $Lnum * 10 + $Cnum;
		return (String)$Lnum;
	}

}