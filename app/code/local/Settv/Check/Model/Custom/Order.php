<?php
class Settv_Check_Model_Custom_Order extends Mage_Core_Model_Abstract
{
	protected $_eventPrefix = 'check_custom_order';
	protected $_eventObject = 'custom_order';
	
	/** Ship status */
	const STATE_NOT_SHIPPED     = 'Not Shipped';
	const STATE_SHIPPED			= 'Shipped';
	const STATE_ARRIVED			= 'Arrived';
	const STATE_REJECTED		= 'Rejected';
	const STATE_RETURN_RECEIVED	= 'Return Received';
	const STATE_RETURN_REJECTED = 'Return Rejected';
	
	public function _construct() {
		parent::_construct();
		$this->_init('check/custom_order');
	}

	public function deleteByOrder($order_id) {
		$this->_getResource()->deleteByOrder($order_id);
	}

	public function getByOrder($order_id) {
		return $this->_getResource()->getByOrder($order_id);
	}
	
	public function loadByOrderId($order_id) {
		return $this->load($order_id, 'order_id');
	}

	public function getIdByOrder($order_id) {
		$OrderArr =  $this->_getResource()->getByOrder($order_id);

		if($OrderArr){
			return $OrderArr['0']['id'];
		}else{
			return '';
		}
	}

	public function canDeliver($order) {
		if($this->getShipstatus() === self::STATE_NOT_SHIPPED && $order->getState() === Settv_Check_Model_Sales_Order::STATE_ESTABLISHED){
			return true;
		}
		return false;
	}

	public function canArrive() {
		if($this->getShipstatus() === self::STATE_SHIPPED) {
			return true;
		}
		return false;
	}

	public function canReturn($order) {
		if($this->getShipstatus() === self::STATE_RETURN_RECEIVED && $order->getState() === Settv_Check_Model_Sales_Order::STATE_RETURN_PROCESSING){
			return true;
		}
		return false;
	}

	public function setShipstatus($state, $comment = "")
	{
		$order = Mage::getModel('sales/order')->load($this->OrderId);

		$translate = Mage::getSingleton('core/translate')->init("adminhtml");
		$state_str = Mage::helper('sales')->__($state);

		$order->addStatusHistoryComment($state_str . ' - ' . $comment, false); // no sense to set $status again
		$order->save();

		$this->setData('shipstatus', $state);

		return $this;
	}
	
	public function setLogisticsData($order_id, $logistics_id)
	{
		$custom_order = $this->loadByOrderId($order_id);
		$custom_order->setLogisticsId($logistics_id);
		
		//TODO: hct-> should remove this and change phtml for get hct link 
		//\app\design\adminhtml\default\settv_default\template\sales\order\view\tab\info.phtml
		
		$logistics_num = Mage::getModel('check/custom_logistics')->getlogisticsnum($logistics_id);
		$custom_order->setLogisticsNum($logistics_num);
		
		$custom_order->setShipstatus(Settv_Check_Model_Custom_Order::STATE_SHIPPED);
		$custom_order->save();
	}

	public function loadByLogisticsId($logistics_id)
	{
		$custom_orders = $collection = Mage::getModel("check/custom_order")
		->getCollection()
		->addFieldToFilter('logistics_id', $logistics_id);
		
		return $custom_orders;
	}
}