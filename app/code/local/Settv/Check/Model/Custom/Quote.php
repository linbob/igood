<?php
class Settv_Check_Model_Custom_Quote extends Mage_Core_Model_Abstract{
	public function _construct() {
		parent::_construct();
		$this->_init('check/custom_quote');
	}
	
	public function deleteByQuote($quote_id) {
		$this->_getResource()->deleteByQuote($quote_id);
	}
	
	public function getByQuote($quote_id) {
		return $this->_getResource()->getByQuote($quote_id);
	}	
}