<?php
class Settv_Check_Model_Custom_Returnorder extends Mage_Core_Model_Abstract{
	public function _construct() {
		parent::_construct();
		$this->_init('check/custom_returnorder');
	}
	
	public function getreturnnum($return_id) {
		//$Lnum = $return_id + 310000000 ;
		//$Cnum = $Lnum % 8;
		$Lnum = $return_id % 5000 + 211191900 ;
		$Cnum = $Lnum % 7;
		$Lnum = $Lnum * 10 + $Cnum;
		
		return (String)$Lnum;
	}
}