<?php
class Settv_Check_Model_Mysql4_Custom_Order extends Mage_Core_Model_Mysql4_Abstract{
	public function _construct()
	{
		$this->_init('check/custom_order', 'id');
	}
	public function deleteByOrder($order_id) {
		$table = $this->getMainTable();
		$where = $this->_getWriteAdapter()->quoteInto('order_id = ?', $order_id);
		$this->_getWriteAdapter()->delete($table,$where);
	}
	
	//TODO: return array ? wtf ?
	public function getByOrder($order_id) {
		$table = $this->getMainTable();
		$where = $this->_getReadAdapter()->quoteInto('order_id = ?', $order_id);
		
		$sql = $this->_getReadAdapter()->select()->from($table)->where($where);
		$rows = $this->_getReadAdapter()->fetchAll($sql);
		$return = array();
		foreach($rows as $key => $value){
			$return[$key] = $value;
		}
		return $return;
	}
}