<?php
class Settv_Check_Model_Observer {

	public function saveQuoteBefore($evt) {
		$quote = $evt->getQuote();
		$post = Mage::app()->getFrontController()->getRequest()->getPost();

		$PostData = Mage::getSingleton('checkout/session')->getOnepagedata();
		if(!$PostData){
			$PostData = array();
		}
		if(isset($post['check']['invoicetype'])) {
			$var = $post['check']['invoicetype'];
			$PostData['invoicetype'] = $post['check']['invoicetype'];

			if($var == "3"){
				if(isset($post['check']['invoiceubn'])) {
					$PostData['invoiceubn'] = $post['check']['invoiceubn'];
				}
				if(isset($post['check']['invoicecom'])) {
					$PostData['invoicecom'] = $post['check']['invoicecom'];
				}
			}

			//donate invoice
			if($var == "4"){								
				$PostData["invoice_isdonate"] = 1;
				if(isset($post["donateorgLOV"])) {
					$donateCodeAndDisName = explode("|", $post["donateorgLOV"]);														
					$PostData["invoice_donateorgnum"] = $donateCodeAndDisName[0];
					$PostData["invoice_donateorg"] = $donateCodeAndDisName[1];
				}
				else{
					$defaultDonateCodeAndDisName = Mage::getStoreConfig ( 'gov/einvoice/donateOrgDefault', false );
					$donateCodeAndDisName = explode ("|", $defaultDonateCodeAndDisName);
					$PostData["invoice_donateorgnum"] = $donateCodeAndDisName[0];
					$PostData["invoice_donateorg"] = $donateCodeAndDisName[1];
					//$PostData["invoice_donateorgnum"] = '919';
					//$PostData["invoice_donateorg"] = '財團法人創世社會福利基金會';
				}								
			}
			
			//e-invoice
			if($var == "5"){
				{
					if($post['check']['hascarriernumber'])
					{
						if(isset($post["carriertype"])) {
							//$PostData["invoice_carriertype"] = $post["carriertype"]; // can be extended in future
							$PostData["invoice_carriertype"] = "3J0002";
						}
						if(isset($post["carriernumber"])) {														
							$isValidBarCode = Mage::helper('goveinvoice/invapp')->isValidBarCode($post["carriernumber"]);													
							if($isValidBarCode)
							{
								$PostData["invoice_carriernum1"] = $post["carriernumber"];
								$PostData["invoice_carriernum2"] = $post["carriernumber"];
							}
							else
							{
								Mage::log ("Invalid carrier code number : ". $post["carriernumber"],null,"onepage.log" );
								throw new Exception("Invalid carrier code number : " . $post["carriernumber"]);								
							}								
						}						
					}
				}
			}			
		}
		
		$PostData['shipstatus'] = "Not Shipped";
		
		if(isset($post['payment']['method'])) {
			$var = $post['payment']['method'];
			if($var == "cashondelivery"){
				$PostData['cashdelivery'] = 0;
			} else {
				$PostData['cashdelivery'] = null;

				if ($var == "paymentchinatrustinstallment") //分期
				{
					if (is_numeric($post['payment']['period']))
					{
						$PostData['installment'] = $post['payment']['period'];
					}
				}
			}
		}
		//billing[logistic]
		if(isset($post['billing']['logistic'])) {
			$var = $post['billing']['logistic'];
			$PostData['shippingtype'] = $var;
			if($var == "familymart"){
				$fmarray = array('fmstorename' => $post['billing']['fmstorename'],
						'fmstoreaddr'=> $post['billing']['fmstoreaddr'],
						'fmstorecode' => $post['billing']['fmstorecode'],
						'fmshipname' => $post['billing']['fmshipname']);
				
				$PostData['shippingdescription'] = serialize($fmarray);
			}
		}
		Mage::getSingleton('checkout/session')->setOnepagedata($PostData);
	}

	public function saveQuoteAfter($evt) {

	}

	/**
	 *
	 * When load() function is called on the quote object,
	 * we read our custom fields value from database and put them back in quote object.
	 * @param unknown_type $evt
	 */
	public function loadQuoteAfter($evt) {

	}

	/**
	 *
	 * This function is called when $order->load() is done.
	 * Here we read our custom fields value from database and set it in order object.
	 * @param unknown_type $evt
	 */
	public function loadOrderAfter($evt) {
	}
}