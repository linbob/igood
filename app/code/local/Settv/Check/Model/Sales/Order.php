<?php
class Settv_Check_Model_Sales_Order extends Mage_Sales_Model_Order 
{	
	const STATE_ESTABLISHED = 'established';
	const STATE_RETURN_PROCESSING = 'return_processing';
	const STATE_RETURN_SUCCEEDED = 'return_succeeded';
	const STATE_FAILED = 'failed';
	const STATE_CANCELED = 'canceled';
	
	public function formatPrice($price, $addBrackets = false)
	{
		return $this->formatPricePrecision($price, 0, $addBrackets);
	}

	public function getTaiwanCreatedAtFormated()
	{
		return $this->getCreatedAtStoreDate()->toString("Y-M-d H:m:s");
	}
	
	public function canEdit()
	{
		if ($this->canUnhold()) {
			return false;
		}
	
		$state = $this->getState();
		if ($this->isCanceled() || $this->isPaymentReview()
		|| $state === self::STATE_COMPLETE || $state === self::STATE_CLOSED) {
			return false;
		}
	
		if (!$this->getPayment()->getMethodInstance()->canEdit()) {
			return false;
		}
	
		if ($this->getActionFlag(self::ACTION_FLAG_EDIT) === false) {
			return false;
		}
		
		$order_id = $this->getId();		
		$custom_order = Mage::getModel('check/custom_order')->loadByOrderId($order_id);
		if (!$custom_order->canDeliver($this)) {
			return false;
		}
	
		return true;
	}
	
	public function canCancel()
	{
		$curstom_order = Mage::getModel('check/custom_order')->loadByOrderId($this->getId());
		if ($this->getState() == self::STATE_ESTABLISHED
			&& $curstom_order->getShipstatus() == Settv_Check_Model_Custom_Order::STATE_NOT_SHIPPED)		
			return true;
		else
			return false;	
	}
}