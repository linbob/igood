<?php 
/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

$this->addAttribute('customer_address', 'cellphone', array(
		'type' => 'varchar',
		'input' => 'text',
		'label' => 'Cellphone',
		'global' => 1,
		'visible' => 1,
		'required' => 1,
		'user_defined' => 1,
		'visible_on_front' => 1
));
Mage::getSingleton('eav/config')->getAttribute('customer_address', 'cellphone')->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address'))->save();

$installer->run("
		ALTER TABLE {$this->getTable('sales/quote_address')} ADD COLUMN `cellphone` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `fax`;
		ALTER TABLE {$this->getTable('sales/order_address')} ADD COLUMN `cellphone` VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL AFTER `fax`;
		");


// CREATE TABLE IF NOT EXISTS {$this->getTable('settv_sales_quote_custom')} (
// `id` int(11) unsigned NOT NULL auto_increment,
// `quote_id` int(11) unsigned NOT NULL,
// `key` varchar(255) NOT NULL,
// `value` text NOT NULL,
// PRIMARY KEY (`id`)
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
// CREATE TABLE IF NOT EXISTS {$this->getTable('settv_sales_order_custom')} (
// `id` int(11) unsigned NOT NULL auto_increment,
// `order_id` int(11) unsigned NOT NULL,
// `key` varchar(255) NOT NULL,
// `value` text NOT NULL,
// PRIMARY KEY (`id`)
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

$installer->run("
			CREATE TABLE IF NOT EXISTS {$this->getTable('settv_sales_order_custom')} (
					`id` int(11) unsigned NOT NULL auto_increment,
					`order_id` int(11) unsigned NOT NULL,
					`invoicetype` text,
					`invoiceubn` text,
					`invoicecom` text,
					`shipstatus` text,
					`cashdelivery` text,
					`returnid` int(11),
					`returnnum` text NULL,
					`returnreason` text,
					`returnaddressid` text,
					`returnallowflag` BOOLEAN,
					`logistics_id` int(11),
					`logistics_num` text NULL,
					`arrived_at` timestamp,
					`supplier_id` int(11),	
					PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			CREATE TABLE IF NOT EXISTS {$this->getTable('settv_sales_returnorder_custom')} (
					`id` int(11) unsigned NOT NULL auto_increment,
					`return_num` text NULL,
					`order_id` int(11) unsigned NOT NULL,
					`active` BOOLEAN DEFAULT TRUE NOT NULL,
					`created_at` timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
					`update_at` timestamp NULL,
					PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;	
			CREATE TABLE IF NOT EXISTS {$this->getTable('settv_sales_returnreason_custom')} (
					`id` int(11) unsigned NOT NULL auto_increment,
					`returnreason` text,
					PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;	
			CREATE TABLE IF NOT EXISTS {$this->getTable('settv_sales_logistics_custom')} (
					`id` int(11) unsigned NOT NULL auto_increment,
					`logistics_num` text NULL,
					`order_id` int(11) unsigned NULL,
					`order_incrementid` int(11) unsigned NULL,
					`forward_at` timestamp NULL,
					`arrived_at` timestamp NULL,
					`sendee` text NULL,
					`sign_cust` text NULL,
					`ship_code` text NULL,
					`ship_memo` text NULL,
					`charge_amt` int(11) unsigned NULL,
					`created_at` timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
					PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;						
");

$installer->endSetup();
?>