<?php 
require_once 'Mage/Checkout/controllers/CartController.php';

class Settv_Checkout_CartController extends Mage_Checkout_CartController
{
	public function updatePostAndCheckoutAction() {
		$hasError = false;
		try {
			$cartData = $this->getRequest()->getParam('cart');
			if (is_array($cartData)) {
				$filter = new Zend_Filter_LocalizedToNormalized(
						array('locale' => Mage::app()->getLocale()->getLocaleCode())
				);
				foreach ($cartData as $index => $data) {
					if (isset($data['qty'])) {
						$cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
					}
				}
				$cart = $this->_getCart();
				if (! $cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
					$cart->getQuote()->setCustomerId(null);
				}

				$cartData = $cart->suggestItemsQty($cartData);
				$cart->updateItems($cartData)
				->save();
			}
			$this->_getSession()->setCartWasUpdated(true);
		} catch (Mage_Core_Exception $e) {
			$this->_getSession()->addError($e->getMessage());
			$hasError = true;
			$this->_goBack();
		} catch (Exception $e) {
			$this->_getSession()->addException($e, $this->__('Cannot update shopping cart.'));
			Mage::logException($e);
			$hasError = true;
			$this->_goBack();
		}
		if(!$hasError) {
			$this->_redirect('checkout/onepage');
		}
	}
	
	public function addAction() {
		//clear session messages
		$this->_getSession()->getMessages(true);

		//ajax shopping cart feature.
		$cart   = $this->_getCart();
		$params = $this->getRequest()->getParams();
		
		if($params['isAjax'] == 1){
			$response = array();
			try {
				if (isset($params['qty'])) {
					$filter = new Zend_Filter_LocalizedToNormalized(
							array('locale' => Mage::app()->getLocale()->getLocaleCode())
					);
					$params['qty'] = $filter->filter($params['qty']);
				}
			
				$product = $this->_initProduct();
				$related = $this->getRequest()->getParam('related_product');
			
				/**
				 * Check product availability
				*/
				if (!$product) {
					$response['status'] = 'ERROR';
					$response['message'] = $this->__('Unable to find Product ID');
				}
			
				$cart->addProduct($product, $params);
				if (!empty($related)) {
					$cart->addProductsByIds(explode(',', $related));
				}
			
				$cart->save();
			
				$this->_getSession()->setCartWasUpdated(true);
			
				/**
				 * @todo remove wishlist observer processAddToCart
				*/
				Mage::dispatchEvent('checkout_cart_add_product_complete',
				array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
				);
			
				if (!$this->_getSession()->getNoCartRedirect(true)) {
					if (!$cart->getQuote()->getHasError()){
						$message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
						$response['status'] = 'SUCCESS';
						$response['message'] = $message;
					}
				}
			} catch (Mage_Core_Exception $e) {
				$msg = "";
				if ($this->_getSession()->getUseNotice(true)) {
					$msg = $e->getMessage();
				} else {
					$messages = array_unique(explode("\n", $e->getMessage()));
					foreach ($messages as $message) {
						$msg .= $message.'<br/>';
					}
				}
			
				$response['status'] = 'ERROR';
				$response['message'] = $msg;
			} catch (Exception $e) {
				$response['status'] = 'ERROR';
				$response['message'] = $this->__('Cannot add the item to shopping cart.');
				Mage::logException($e);
			}
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;			
		}
		else
		{			
			parent::addAction();
		}
	}
}