<?php 

require_once 'Mage/Checkout/controllers/OnepageController.php';

class Settv_Checkout_OnepageController extends Mage_Checkout_OnepageController
{

	public function preDispatch()
	{
		parent::preDispatch();

		if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
			$this->_redirect('customer/account/login/');
			return;
		}

		$this->_preDispatchValidateCustomer();

		$checkoutSessionQuote = Mage::getSingleton('checkout/session')->getQuote();
		if ($checkoutSessionQuote->getIsMultiShipping()) {
			$checkoutSessionQuote->setIsMultiShipping(false);
			$checkoutSessionQuote->removeAllAddresses();
		}

		return $this;
	}

	public function saveShippingAction()
	{
		if ($this->_expireAjax()) {
			return;
		}
		if ($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getPost('shipping', array());
			$customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
			$result = $this->getOnepage()->saveShipping($data, $customerAddressId);

			if (!isset($result['error'])) {
				$result['goto_section'] = 'payment';
				$result['update_section'] = array(
						'name' => 'payment-method',
						'html' => $this->_getPaymentMethodsHtml()
				);
			}
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
		}
	}

	public function saveBillingAction()
	{
		if ($this->_expireAjax()) {
			return;
		}

		if ($this->getRequest()->isPost()) {
			$data = $this->getRequest()->getPost('billing', array());
			$customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

			// shipping
			$logistic = "";
			$logistic_description = "";
			if (! empty($data)) {
				$session  = Mage::getSingleton('customer/session');
				if ($data['logistic'] == 'familymart') {
					$logistic = "familymart";
					$logistic_description = '全家取貨<br/>取貨門市:'.$data['fmstorename'].'<br/>';
				}

				$session -> setLogistic($logistic);
				$session -> setLogisticDescription($logistic_description);
			}

			$result = $this->getOnepage()->saveBilling($data, $customerAddressId);

			if (!isset($result['error'])) {
				/* check quote for virtual */
				if ($this->getOnepage()->getQuote()->isVirtual()) {
					$result['goto_section'] = 'payment';
					$result['update_section'] = array(
							'name' => 'payment-method',
							'html' => $this->_getPaymentMethodsHtml()
					);
				}
				elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {

					$result['goto_section'] = 'payment';
					$result['update_section'] = array(
							'name' => 'payment-method',
							'html' => $this->_getPaymentMethodsHtml()
					);
				}
				else {
					//save shipping by Sean S
					//$result['goto_section'] = 'shipping';
					$data = $this->getRequest()->getPost('shipping', array());
					$customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
					$result = $this->getOnepage()->saveShipping($data, $customerAddressId);
					if (!isset($result['error'])) {
						$result['goto_section'] = 'payment';
					}
					//save shipping by Sean E
				}
			}

			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
		}
	}

	// 20120531 delete review step by Sean
	public function savePaymentAction()
	{
		if ($this->_expireAjax()) {
			return;
		}
		try {
			if (!$this->getRequest()->isPost()) {
				$this->_ajaxRedirectResponse();
				return;
			}

			// set payment to quote
			$result = array();
			$data = $this->getRequest()->getPost('payment', array());
			if($data['method'] == 'webatm'){
				Mage::getSingleton('checkout/session')->setRedirectUrl(Mage::getUrl('webatm/processing/redirect'));
				//return parent::savePaymentAction();
			}
			if($data['method'] == 'credit'){
				$_SESSION['credit-period'] = $this->getRequest()->getPost('credit-period');
				Mage::getSingleton('checkout/session')->setRedirectUrl(Mage::getUrl('credit/processing/redirect'));
				//return parent::savePaymentAction();
			}
			$result = $this->getOnepage()->savePayment($data);

			$this->getOnepage()->saveOrder();

			// get section and redirect data
			$redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl();
			if($data['method'] == 'webatm'){
				$redirectUrl = Mage::getUrl('webatm/processing/redirect');
			}
			if($data['method'] == 'credit'){
				$redirectUrl = Mage::getUrl('credit/processing/redirect');
			}
			
				
			if (empty($result['error']) && !$redirectUrl) {
				if ($data = $this->getRequest()->getPost('payment', false)) {
					$this->getOnepage()->getQuote()->getPayment()->importData($data);
				}
				$result = $this->getRequest()->getPost('check', array());

				$storeId = Mage::app()->getStore()->getId();
				$paymentHelper = Mage::helper("payment");
				$zeroSubTotalPaymentAction = $paymentHelper->getZeroSubTotalPaymentAutomaticInvoice($storeId);
				if ($paymentHelper->isZeroSubTotal($storeId)
						&& $this->_getOrder()->getGrandTotal() == 0
						&& $zeroSubTotalPaymentAction == Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE
						&& $paymentHelper->getZeroSubTotalOrderStatus($storeId) == 'pending') {
					$invoice = $this->_initInvoice();
					$invoice->getOrder()->setIsInProcess(true);
					$transactionSave = Mage::getModel('core/resource_transaction')
					->addObject($invoice)
					->addObject($invoice->getOrder());
					$transactionSave->save();
				}
				$result['success'] = true;
				$result['error']   = false;
			}
			if ($redirectUrl) {
				$result['redirect'] = $redirectUrl;
			}
		} catch (Mage_Payment_Exception $e) {
			if ($e->getFields()) {
				$result['fields'] = $e->getFields();
			}
			$result['error'] = $e->getMessage();
		}
		catch (Mage_Payment_Model_Info_Exception $e) {
			$message = $e->getMessage();
			if( !empty($message) ) {
				$result['error_messages'] = $message;
			}
			$result['goto_section'] = 'payment';
			$result['update_section'] = array(
					'name' => 'payment-method',
					'html' => $this->_getPaymentMethodsHtml()
			);
		} catch (Mage_Core_Exception $e) {
			Mage::logException($e);
			Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
			$result['success'] = false;
			$result['error'] = true;
			$result['error_messages'] = $e->getMessage();

			if ($gotoSection = $this->getOnepage()->getCheckout()->getGotoSection()) {
				$result['goto_section'] = $gotoSection;
				$this->getOnepage()->getCheckout()->setGotoSection(null);
			}

			if ($updateSection = $this->getOnepage()->getCheckout()->getUpdateSection()) {
				if (isset($this->_sectionUpdateFunctions[$updateSection])) {
					$updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
					$result['update_section'] = array(
							'name' => $updateSection,
							'html' => $this->$updateSectionFunction()
					);
				}
				$this->getOnepage()->getCheckout()->setUpdateSection(null);
			}
		}

		catch (Exception $e) {
			//Add from saveOrderAction() Exception by Sean S
			//Mage::logException($e);
			//$result['error'] = $this->__('Unable to set Payment Method.');

			Mage::logException($e);
			Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
			$result['success']  = false;
			$result['error']    = true;
			$result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
			//Add from saveOrderAction() Exception by Sean E
		}
			
		//Add from saveOrderAction() End by Sean S
		$this->getOnepage()->getQuote()->save();

		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
	}

	public function validateCarrierBarcodeAction()
	{
		$isValidBarCode = '';
		if ($this->getRequest()->isPost()) {
			$barCode = $this->getRequest()->getPost('carriernumber', false);
			if(Mage::helper('goveinvoice/invapp')->isValidBarCode($barCode))
			{
				$this->getResponse()->setBody('true');
			}
			else 
			{
				$this->getResponse()->setBody('false');
			}			
		}
		else 
		{
			$this->getResponse()->setBody('false');
		}					
	}
	
	function ccfailureAction()
	{
		$this->failureHandler();
	}

	function failureAction()
	{
		$this->failureHandler();
	}	
	
	private function failureHandler()
	{
		$this->loadLayout();
		$this->renderLayout();
	} 
	
	
}

?>
