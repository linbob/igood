<?php

class  Settv_Common_Block_Render_ProductPrice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Currency
{
	public function render(Varien_Object $row)
	{
		$orderId = $row->getData($this->getColumn()->getIndex());

		if (empty($orderId))
			return Mage::helper('core')->currency(0,true,false);

		$order = Mage::getModel('sales/order')->load($orderId);
		$orderItems = $order->getItemsCollection();

		foreach ($orderItems as $item){
			if ($data = $item->getPrice()) {
				return Mage::helper('core')->currency($data,true,false);
			}
		}
	}
}