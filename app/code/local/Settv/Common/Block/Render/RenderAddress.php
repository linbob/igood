<?php

class  Settv_Common_Block_Render_RenderAddress extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value = $row->getData('postcode').' '.
				$row->getData('city') .
				$row->getData('region').
				$row->getData('street');

		return $value;
	}
}

?>