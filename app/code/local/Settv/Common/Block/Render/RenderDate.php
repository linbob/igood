<?php

class Settv_Common_Block_Render_RenderDate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$data = "";
		$date = $row->getData($this->getColumn()->getIndex());
		if ($date!=null){
			$data = $this->formatDate($date, 'short', true);
		}
		return $data;
	}
}

?>