<?php

class  Settv_Common_Block_Render_RenderPayment extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$method = Mage::helper('payment')->getMethodInstance($row->getData($this->getColumn()->getIndex()))->getTitle();

		return $method;
	}
}

?>