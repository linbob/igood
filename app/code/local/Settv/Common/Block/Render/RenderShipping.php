<?php

class Settv_Common_Block_Render_RenderShipping extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

	public function render (Varien_Object $row)
	{
		$_code = $row->getData($this->getColumn()->getIndex());
		$_code = substr($_code, 0, -1);
		$_title = Mage::getStoreConfig("carriers/$_code/title");

		if (! empty($_title))
			return $_title;
		else
			return "";
	}
}

?>