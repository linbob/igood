<?php

class  Settv_Common_Block_Render_RenderSupplier extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$supplierId = $row->getData($this->getColumn()->getIndex());
		
		$productModel = Mage::getModel('catalog/product');
		$attr = $productModel->getResource()->getAttribute("supplier");
		if ($attr->usesSource()) {
			return $attr->getSource()->getOptionText($supplierId);
		}
	}
}

?>