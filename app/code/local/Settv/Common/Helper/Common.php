<?php
class Settv_Common_Helper_Common extends Mage_Core_Helper_Abstract {

	/**
	 * @param string $template_code
	 * @param string $email
	 * @param string $email_subject
	 * @param array $emailTemplateVariables
	 */
	public function sendEmail($template_code, $email, array $emailTemplateVariables = array()) {
		$emailTemplate  = Mage::getModel('core/email_template')->loadByCode($template_code);

		$senderName = Mage::getStoreConfig('trans_email/ident_general/name');
		$emailTemplate->setSenderName($senderName);

		$senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
		$emailTemplate->setSenderEmail($senderEmail);

		$emailTemplate->send($email, null, $emailTemplateVariables );

		Mage::log("Send mail:".$template_code.";To:".$email, null, 'Mail.log');
	}

	public function emailSupplierByOrder($order, $template_code, array $emailTemplateVariables = array()) {
		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order->getId());
		$Custommodel = Mage::getModel('check/custom_order')->load($CustomId);
		if($Custommodel){
			$CustomArr = $Custommodel->getData();
			$supplierId = $CustomArr['supplier_id'];
			$SupplierModel = Mage::getModel('supplier/supplier')->load($supplierId);
			if($SupplierModel){
				$SupplierArr = $SupplierModel->getData();
				$email_address = $SupplierArr['contact_email'];
				Mage::helper('common/common')->sendEmail($template_code, $email_address, $emailTemplateVariables);
				Mage::log("Send mail to supplier:".$template_code.";To:".$email_address, null, 'Mail.log');
			}
		}
	}

	public function chdirToMageBase()
	{
		$mageDir = Mage::getBaseDir();
		$currentDir = getcwd();

		if ($mageDir != $currentDir)
		{
			//Mage::log("[chdirToMageBase Before]".$mageDir.":".$currentDir, null, 'Common_Info.log');
			chdir($mageDir);
			//Mage::log("[chdirToMageBase After]".$mageDir.":".getcwd(), null, 'Common_Info.log');
		}
	}

	public function createFolderAndSetPermission($folderPath)
	{
		$this->chdirToMageBase();
		$user_name = "apache";
		if (!is_dir("$folderPath"))
		{
			mkdir("$folderPath");
			chmod("$folderPath", 0755);
			chown("$folderPath", $user_name);
			chgrp("$folderPath", $user_name);
		}
		$currentPath = getcwd().'/'.$folderPath;
		return $currentPath;
	}

	/**
	 * @return true if product not (low temp logistic and ezcat)
	 */
	public function shouldProductUseHct($productId) {
		$NowProduct         = Mage::getModel('catalog/product')->load($productId);
		$LogisticsCom       = $NowProduct->getAttributeText('logistics_com');
		$isEzcat            = ($LogisticsCom == Mage::getStoreConfig('carriers/ezcat/title', false));
		$isLowTempLogistics = ($NowProduct->getLowTempLogistics() == True);

		return !($isLowTempLogistics && $isEzcat);
	}
}