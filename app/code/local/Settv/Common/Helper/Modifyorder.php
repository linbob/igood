<?php
class Settv_Common_Helper_Modifyorder extends Mage_Core_Helper_Abstract {
	
	public function TransModifyorder() {
		//$LMin10 = strtotime('-24 hour');
		//$str = date("Y-m-d H:i:s", $LMin10);
		
		//Set Modifdy_Order to established
		$OrderIds = Mage::getResourceModel('sales/order_collection')
          ->addFieldToSelect('entity_id')
          ->addFieldToSelect('relation_parent_id')
          ->addFieldToSelect('increment_id')
          ->addFieldToSelect('original_increment_id')
          ->addFieldToFilter('state', 'new')
          ->addFieldToFilter('original_increment_id', array('neq' => null))
          //->addFieldToFilter('created_at', array('gt' => $str))
          ->getData();
		$OrderCount = 0;
		$OrderList = "";
        foreach($OrderIds as $order_Id){	
        	//Mage::log("*******************".$order_Id['entity_id']);
        	//Mage::log("*******************".$order_Id['relation_parent_id']);
        	
        	$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_Id['entity_id']);
        	
        	
        	if(!$CustomId){
        		//Mage::log("******************* no custom record ".$order_Id['relation_parent_id']);
        		
        		$NowOrder = Mage::getModel('sales/order')->load($order_Id['entity_id']);
        		$NowOrder->setState('established', true, '')->save();
        		
        		$original_order = Mage::getModel('sales/order')->loadByIncrementId($order_Id['original_increment_id']);
        		$origial_order_id = $original_order->getId();
        		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($origial_order_id);
        		if($CustomId){
        			//Mage::log("******************* old custom record found".$order_Id['relation_parent_id']);
        			
	        		$Old_Order_Data = Mage::getModel('check/custom_order')->load($CustomId)->getdata();
	        		$Newmodel = Mage::getModel('check/custom_order');
	        		foreach($Old_Order_Data as $index => $value){
	        			//Mage::log("******************* ".$index." => ".$value);
	        			if($index != 'id' && $index != 'order_id') $Newmodel->setData($index,$value);
	        		}
	        		$Newmodel->setData('order_id',$order_Id['entity_id']);
	        		$Newmodel->save();
	        		$OrderList .= $order_Id['increment_id']."  ";
	        		$OrderCount++;
        		}
        	}
        	else{
        		$NewOrderId = $order_Id['entity_id'];
        		$original_order = Mage::getModel('sales/order')->loadByIncrementId($order_Id['original_increment_id']);
        		$OldOrderId = $original_order->getId();
				//Mage::log("******************* no custom record ".$order_Id['relation_parent_id']);
		
				$NowOrder = Mage::getModel('sales/order')->load($NewOrderId);
				$NowOrder->setState('established', true, '')->save();
		
				$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($OldOrderId);
				if($CustomId){
					//Mage::log("******************* old custom record found".$order_Id['relation_parent_id']);
					 
					$Old_Order_Data = Mage::getModel('check/custom_order')->load($CustomId)->getdata();
					
					$NewCustomId = Mage::getModel('check/custom_order')->getIdByOrder($NewOrderId);
					$Newmodel = Mage::getModel('check/custom_order')->load($NewCustomId);
					foreach($Old_Order_Data as $index => $value){
						//Mage::log("******************* ".$index." => ".$value);
						if($index != 'id' && $index != 'order_id') $Newmodel->setData($index,$value);
					}
					$Newmodel->save();
					$OrderList .= $OldOrderId." => ".$NewOrderId ." ; ";
					$OrderCount++;
				}
        	}	
        }
        
        $OrderIds1 = Mage::getResourceModel('sales/order_collection')
        ->addFieldToSelect('entity_id')
        ->addFieldToSelect('original_increment_id')
        ->addFieldToFilter('state', 'new')
        //->addFieldToFilter('created_at', array('gt' => $str))
        ->getData();
        $OrderCount1 = 0;
        $OrderList1 = "";
        foreach($OrderIds1 as $order_Id1){
        	if($order_Id1['original_increment_id'] == null){
        		$NowOrder1 = Mage::getModel('sales/order')->load($order_Id1['entity_id']);
        		$NowOrder1->setState('failed', true, '')->save();
        		
        		$OrderCount1++;
        		$OrderList1 .= $order_Id1['entity_id'] ." ; ";
        	}
        }
        $errstr = "Set established ".$OrderCount." Custom Orders :".$OrderList . "***** Set failed " . $OrderCount1 ." Orders : " .$OrderList1;
        
        return($errstr);
	}

}