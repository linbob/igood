<?php
class Settv_Common_Helper_Order extends Mage_Core_Helper_Abstract {

	public function addItemBackToProductInventory($order) {
		$items = $order->getAllItems();
		foreach ($items as $itemId => $item) {
			$productId=$item->getProductId();
			$qty=$item->getQtyOrdered();
			$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
			$stockItem->addQty($qty);
			$stockItem->save();
		}
	}

}