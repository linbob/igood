<?php
class Settv_Common_Helper_Payment extends Mage_Core_Helper_Abstract {
	/**
	 * Refund according to order's payment method
	 */
	public function refundOrder($order) {
		$isRefunded = false;

		//Refund order by payment method
		try {
			$paymentMethod = $order->getPayment()->getMethod();
			switch ($paymentMethod) {
				case 'paymentqrcode': {
					$isRefunded = Mage::helper('paymentqrcode/refund')->doRefund($order);
					break;
				}
				case 'paymentchinatrust': {
					$isRefunded = Mage::helper('paymentchinatrust/refund')->doRefund($order);
					break;
				}
				default: {
					$orderId = $order->getIncrementId();
					Mage::log("Unable to refund order : $orderId, because payment method: $paymentMethod refund function hasn't been defined.", null, "payment_exception.log");
					break;
				}
			}
		} catch (Exception $ex) { //Any other exception?
			$orderId = $order->getIncrementId();
			Mage::log("Exception thrown while trying to refund order : $orderId", null, "payment_exception.log");
			Mage::log($ex->getMessage(), null, "payment_exception.log");
		}

		/**
		 * If refund failed for whatever reason, then log to order's 異常回報
		 */
		if (!$isRefunded) {
			$order->setState('return_processing', 'return_processing', '無法自動退款');
			$order->save();
		}

		return $isRefunded;
	}
}