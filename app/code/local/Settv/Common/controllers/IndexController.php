<?php
class Settv_Xml_IndexController extends Mage_Core_Controller_Front_Action {

	public function indexAction() {
		$_tmpString = $this->_request->getQuery('store');
		// echo '<dt><strong>Param: </strong>'.$key.'</dt>';
		// echo '<dt><strong>Value: </strong>'.$value.'</dt>';

		echo '<FEED>'.'<Store>'.$_tmpString.'</Store>'.
				'<entry><name>商品名稱</name><price>NT$100</price>'.
				'<image>http://igood.tw/images/product/111.jpg</image>'.
				'<url>http://igood.tw/product/123.php</url><description>商品介紹商品介紹商品介紹商品介紹</description>'.
				'</entry><entry><name>商品名稱</name><price>NT$100</price>'.
				'<image>http://igood.tw/images/product/111.jpg</image>'.
				'<url>http://igood.tw/product/123.php</url><description>商品介紹商品介紹商品介紹商品介紹</description>'.
				'</entry><entry><name>商品名稱</name><price>NT$100</price>'.
				'<image>http://igood.tw/images/product/111.jpg</image>'.
				'<url>http://igood.tw/product/123.php</url>'.
				'<description>商品介紹商品介紹商品介紹商品介紹</description></entry></FEED>';

		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		if ($write instanceof Zend_Db_Adapter_Abstract) {
			echo get_class($write);
		}
	}

	public function testAction() {
		$this->loadLayout();
		$this->renderLayout();
	}

	public function paramsAction() {
		echo '<dl>';
		foreach($this->getRequest()->getParams() as $key=>$value) {
			echo '<dt><strong>Param: </strong>'.$key.'</dt>';
			echo '<dt><strong>Value: </strong>'.$value.'</dt>';
		}
		echo '</dl>';
	}

}