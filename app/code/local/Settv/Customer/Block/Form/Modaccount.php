<?php

class Settv_Customer_Block_Form_Modaccount extends Mage_Core_Block_Template
{
	protected function _prepareLayout()
	{		
		return parent::_prepareLayout();
	}
	
	public function getPostActionUrl()
	{
		$url = Mage::getUrl('customer/modaccount/post');
		return $url;
	}
}
