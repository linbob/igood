<?php
class Settv_Customer_Helper_Order extends Mage_Core_Helper_Abstract
{
	public function getCurrentCustomerOrderById($order_id)
	{
		$order = Mage::getModel('sales/order')
		->getCollection()
		->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
		->addFieldToFilter('entity_id', $order_id)
		->load()->getFirstItem();
	
		return $order;
	}	
}