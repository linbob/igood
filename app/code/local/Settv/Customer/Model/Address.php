<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Customer address model
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Settv_Customer_Model_Address extends Mage_Customer_Model_Address
{
    protected $_customer;

    protected function _construct()
    {
        $this->_init('customer/address');
    }

    /**
     * Retrieve address customer identifier
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->_getData('customer_id') ? $this->_getData('customer_id') : $this->getParentId();
    }

    /**
     * Declare address customer identifier
     *
     * @param integer $id
     * @return Mage_Customer_Model_Address
     */
    public function setCustomerId($id)
    {
        $this->setParentId($id);
        $this->setData('customer_id', $id);
        return $this;
    }

    /**
     * Retrieve address customer
     *
     * @return Mage_Customer_Model_Customer | false
     */
    public function getCustomer()
    {
        if (!$this->getCustomerId()) {
            return false;
        }
        if (empty($this->_customer)) {
            $this->_customer = Mage::getModel('customer/customer')
                ->load($this->getCustomerId());
        }
        return $this->_customer;
    }

    /**
     * Specify address customer
     *
     * @param Mage_Customer_Model_Customer $customer
     */
    public function setCustomer(Mage_Customer_Model_Customer $customer)
    {
        $this->_customer = $customer;
        $this->setCustomerId($customer->getId());
        return $this;
    }

    /**
     * Delete customer address
     *
     * @return Mage_Customer_Model_Address
     */
    public function delete()
    {
        parent::delete();
        $this->setData(array());
        return $this;
    }

    /**
     * Retrieve address entity attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        $attributes = $this->getData('attributes');
        if (is_null($attributes)) {
            $attributes = $this->_getResource()
                ->loadAllAttributes($this)
                ->getSortedAttributes();
            $this->setData('attributes', $attributes);
        }
        return $attributes;
    }

    public function __clone()
    {
        $this->setId(null);
    }

    /**
     * Return Entity Type instance
     *
     * @return Mage_Eav_Model_Entity_Type
     */
    public function getEntityType()
    {
        return $this->_getResource()->getEntityType();
    }

    /**
     * Return Entity Type ID
     *
     * @return int
     */
    public function getEntityTypeId()
    {
        $entityTypeId = $this->getData('entity_type_id');
        if (!$entityTypeId) {
            $entityTypeId = $this->getEntityType()->getId();
            $this->setData('entity_type_id', $entityTypeId);
        }
        return $entityTypeId;
    }
    
    /**
     * Return Region ID
     *
     * @return int
     */
    public function getRegionId()
    {
        return (int)$this->getData('region_id');
    }
    
    /**
     * Set Region ID. $regionId is automatically converted to integer
     *
     * @param int $regionId
     * @return Mage_Customer_Model_Address
     */
    public function setRegionId($regionId)
    {
        $this->setData('region_id', (int)$regionId);
        return $this;
    }
    
    
    /**
     * Validate address attribute values
     * 
     * Delete last name validate
     *
     * @return bool
     */
    public function validate()
    {
    	$denylist=array("　","０","１","２","３","４","５","６","７","８","９","Ａ","Ｂ","Ｃ","Ｄ","Ｅ","Ｆ","Ｇ","Ｈ","Ｉ","Ｊ","Ｋ","Ｌ","Ｍ","Ｎ","Ｏ","Ｐ","Ｑ","Ｒ","Ｓ","Ｔ","Ｕ","Ｖ","Ｗ","Ｘ","Ｙ","Ｚ","ａ","ｂ","ｃ","ｄ","ｅ","ｆ","ｇ","ｈ","ｉ","ｊ","ｋ","ｌ","ｍ","ｎ","ｏ","ｐ","ｑ","ｒ","ｓ","ｔ","ｕ","ｖ","ｗ","ｘ","ｙ","ｚ","～","！","＠","＃","＄","％","＾","＆","＊","（","）","＿","＋","｜","‘","－","＝","＼","｛","｝","〔","〕","：","”","；","’","＜","＞","？","，","．","／");    	
    	$errors = array();
    	$helper = Mage::helper('customer');
    	$this->implodeStreetAddress();
    	if (!Zend_Validate::is($this->getFirstname(), 'NotEmpty')) {
    		$errors[] = $helper->__('Please enter the first name.');
    	}
    
    	/*
    	if (!Zend_Validate::is($this->getLastname(), 'NotEmpty')) {
    		$errors[] = $helper->__('Please enter the last name.');
    	}
 		*/  

    	    
    	if (!Zend_Validate::is($this->getStreet(1), 'NotEmpty')) {
    		$errors[] = $helper->__('Please enter the street.');
    	}
    
    	if (!Zend_Validate::is($this->getCity(), 'NotEmpty')) {
    		$errors[] = $helper->__('Please enter the city.');
    	}
    	
    	if ($this->contains($this->getStreet(1), $denylist)) {
    		$errors[] = $helper->__('Please check the street format.');
    	}
    
    	if (!Zend_Validate::is($this->getTelephone(), 'NotEmpty')) {
    		$errors[] = $helper->__('Please enter the telephone number.');
    	}
    	    	
    	if ($this->contains($this->getTelephone(), $denylist)) {    		
    		$errors[] = $helper->__('Please check the telephone number format.');
    	}
    	
    	if ($this->contains($this->getCellphone(), $denylist)) {
    		$errors[] = $helper->__('Please check the cellphone number format.');
    	}
    	    	    	    
    	$_havingOptionalZip = Mage::helper('directory')->getCountriesWithOptionalZip();
    	if (!in_array($this->getCountryId(), $_havingOptionalZip) && !Zend_Validate::is($this->getPostcode(), 'NotEmpty')) {
    		$errors[] = $helper->__('Please enter the zip/postal code.');
    	}
    
    	if (!Zend_Validate::is($this->getCountryId(), 'NotEmpty')) {
    		$errors[] = $helper->__('Please enter the country.');
    	}
    
    	if ($this->getCountryModel()->getRegionCollection()->getSize()
    			&& !Zend_Validate::is($this->getRegionId(), 'NotEmpty')) {
    		$errors[] = $helper->__('Please enter the state/province.');
    	}
    
    	if (empty($errors) || $this->getShouldIgnoreValidation()) {
    		return true;
    	}
    	return $errors;
    }
    
    private function contains($str, array $arr)
    {
    	foreach($arr as $a) {
    		if (stripos($str,$a) !== false) return true;
    	}
    	return false;
    }
}
