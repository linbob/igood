<?php
class Settv_Customer_Model_Observer {
	public function registerSuccess($evt) {
		//get from evt
		$customer = $evt->getCustomer();
		$customerId = $customer->getId();
		//get from session
		$session = Mage::getSingleton('customer/session');
		$modOrders = $session->getModOrders(); //get from session;

		if ($modOrders!=null)
		{
			Mage::log($customerId.":".implode(",", $modOrders), null, "moveorder_register.log" );
			Mage::helper('cht/order')->moveOrderToCustomer($customerId, $modOrders);
		}
	}
}