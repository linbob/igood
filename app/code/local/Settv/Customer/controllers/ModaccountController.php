<?php
class Settv_Customer_ModAccountController extends Mage_Core_Controller_Front_Action {
	public function indexAction()
	{
		$msg = $this->_getSession()->getMessages(true);
		$this->loadLayout();
		$this->getLayout()->getMessagesBlock()->addMessages($msg);
		$this->_initLayoutMessages('core/session');
		$this->renderLayout();
	}
	
	public function postAction()
	{
		if ($this->getRequest()->isPost()) {
			$session = $this->_getSession();
			$session->setModOrders(null);
			$modaccount = $this->getRequest()->getPost('modaccount');
			$mod_username = $modaccount['username'];
			$mod_cellphone = $modaccount['cellphone'];
			if (!empty($mod_username) && !empty($mod_cellphone))
			{
				$orderIds = Mage::helper('cht/order')->getModOrdersByNameAndPhone($mod_username, $mod_cellphone);
				if ($orderIds != null && count($orderIds) > 0)
				{					
					$session->setModOrders($orderIds);					
					$this->_redirect('*/account/create');
				}
				else
				{
					$session->addError("查無MOD購物訂單，請洽詢客服專線：02-87927357");
					$this->_redirect('*/*/');
				}
			}
			else
			{
				$session->addError("請輸入手機號碼及發票收件人姓名");
				$this->_redirect('*/*/');
			}
		}
	}
	
	public function oldpostAction()
	{
		if ($this->getRequest()->isPost()) {
			$session = $this->_getSession();
			$modaccount = $this->getRequest()->getPost('modaccount');
			$mod_username = $modaccount['username'];
			$mod_cellphone = $modaccount['cellphone'];
			if (!empty($mod_username) && !empty($mod_cellphone))
			{
				if (Mage::helper('cht/order')->IsModOrderExistedByNameAndPhone($mod_username, $mod_cellphone))
				{
					$session->setModUserName($mod_username);
					$session->setModCellphone($mod_cellphone);
					$this->_redirect('*/account/create');
				}
				else 
				{
					$session->addError("查無MOD購物訂單，請洽詢客服專線：02-87927357");
					$this->_redirect('*/*/');
				}				
			}
			else
			{
				$session->addError("請輸入手機號碼及發票收件人姓名");
				$this->_redirect('*/*/');
			}
		}
	}	
						
	protected function _getSession()
	{
		return Mage::getSingleton('customer/session');
	}
}