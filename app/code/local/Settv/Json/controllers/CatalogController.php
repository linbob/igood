<?php
class Settv_Json_CatalogController extends Mage_Core_Controller_Front_Action {

	public function indexAction() {

	}

	public function categoryAction() {
		$DEFAULT_ITEMS_PER_PAGE = 6; // default value

		$page = $this->_request->getQuery('p');
		$category_id = $this->_request->getQuery('cid');
		$_items_per_page = $this->_request->getQuery('ipp');
		$items_per_page = $_items_per_page == null ? $DEFAULT_ITEMS_PER_PAGE : $_items_per_page;

		$prodCollection = Mage::getModel('catalog/category')->load($category_id)
		->getProductCollection()->addAttributeToFilter('status', 1)->setOrder('created_at', 'DESC');
		
		$TotCnt = $prodCollection->count();

		$product_a = new Mage_Catalog_Model_Product();
		
		foreach ($prodCollection as $product) {
			$prdIds[] = $product->getId();
		}
		
        shuffle($prdIds);
		
		// $outStr = 'var myJSONObject = {"products": [';
		$outStr = '{"products": [';

		$begin = $page * $items_per_page;
		$_end = ($page * $items_per_page) + $items_per_page;
		$end = ($_end > count($prdIds)) ? count($prdIds) : $_end;
		//Mage::log('$begin: '.$begin.', $end: '.$end);
		//Mage::log('$count($prdIds): '.count($prdIds).', $_end: '.$_end);
		for ($i = $begin; $i < $end; $i++) {
			try {
				$_model = Mage::getModel('catalog/product');
				$_product = $_model->load($prdIds[$i]);

				$outStr .= '{';
				$outStr .= '"id":'.'"'.$prdIds[$i].'"'.',';
				$outStr .= '"name":'.'"'.htmlspecialchars($_product->getName(), ENT_QUOTES).'"'.',';
				$outStr .= '"price":'.'"'.round($_product->getPrice(), 0).'"'.',';
				$outStr .= '"special_price":'.'"'.round($_product->getFinalPrice(), 0).'"'.',';
				$outStr .= '"url":'.'"'.htmlspecialchars($_product->getUrlPath(), ENT_QUOTES).'?___store='.htmlspecialchars($_product->getAttributeText('sale_store'), ENT_QUOTES).'"'.',';
				$outStr .= '"img_url":'.'"'.htmlspecialchars($_product->getSmallImageUrl(300,225), ENT_QUOTES).'"'.',';
				$outStr .= '"spacial_note":'.'"'.htmlspecialchars($_product->getSpecialNote(), ENT_QUOTES).'"'.',';
				$outStr .= '"short_desc":'.'"'.htmlspecialchars($this->convert_line_breaks(trim($_product->getShortDescription()), ENT_QUOTES)).'"'.',';
				$outStr .= '"TotCnt":'.'"'.$TotCnt.'"';
				// $outStr .= '"store":'.'"'.'?___store='.htmlspecialchars($_product->getAttributeText('sale_store'), ENT_QUOTES).'"'.',';
				
				$outStr .= '}';
				if ($i == $end - 1) {

				} else {
					$outStr .= ',';
				}
			} catch (Exception $e) {
				var_dump($e->getMessage());
			}
		}
		$outStr .= ']}';
		echo $outStr;
	}

	public function categoryPrdTotalAction() {
		$category_id = $this->_request->getQuery('cid');
		$prodCollection = Mage::getModel('catalog/category')->load($category_id)->getProductCollection();

		echo $prodCollection->count();
	}
	
	function convert_line_breaks($string, $line_break=PHP_EOL) {
		$patterns = array(
				"/(<br>|<br \/>|<br\/>)\s*/i",
				"/(\r\n|\r|\n)/"
		);
		$replacements = array(
				PHP_EOL,
				$line_break
		);
		$string = preg_replace($patterns, $replacements, $string);
		return $string;
	}
	
	public function phonecategoryAction() {
		$DEFAULT_ITEMS_PER_PAGE = 10; // default value
		$DEFAULT_ITEMS_SORTBY = "created_at"; // default value
	
		$page = $this->_request->getQuery('p');
		$category_id = $this->_request->getQuery('cid');
		$items_per_page = $DEFAULT_ITEMS_PER_PAGE;
		$_items_sortby = $this->_request->getQuery('sortby');

		$items_sortby = $_items_sortby == null ? $DEFAULT_ITEMS_SORTBY : $_items_sortby;
	
// 		$prodCollection = Mage::getModel('catalog/category')->load($category_id)
// 		->getProductCollection()->addAttributeToFilter('status', 1)->setOrder('created_at', 'DESC');
	
		if($_items_sortby == "created_at"){
			$prodCollection = Mage::getModel('catalog/category')->load($category_id)
			->getProductCollection()->addAttributeToFilter('status', 1)->setOrder($items_sortby, 'DESC');
		}else{
			$prodCollection = Mage::getModel('catalog/category')->load($category_id)
			->getProductCollection()->addAttributeToFilter('status', 1)->setOrder($items_sortby, 'ASC');
		}
		$product_a = new Mage_Catalog_Model_Product();
	
		foreach ($prodCollection as $product) {
			$prdIds[] = $product->getId();
		}
	
		// $outStr = 'var myJSONObject = {"products": [';
		$outStr = '{"products": [';
	
		$begin = $page * $items_per_page;
		$_end = ($page * $items_per_page) + $items_per_page;
		$end = ($_end > count($prdIds)) ? count($prdIds) : $_end;
		//Mage::log('$begin: '.$begin.', $end: '.$end);
		//Mage::log('$count($prdIds): '.count($prdIds).', $_end: '.$_end);
		for ($i = $begin; $i < $end; $i++) {
			try {
				$_model = Mage::getModel('catalog/product');
				$_product = $_model->load($prdIds[$i]);
	
				$outStr .= '{';
				$outStr .= '"id":'.'"'.$prdIds[$i].'"'.',';
				$outStr .= '"name":'.'"'.htmlspecialchars($_product->getName(), ENT_QUOTES).'"'.',';
				$outStr .= '"price":'.'"'.round($_product->getFinalPrice(), 0).'"'.',';
				$outStr .= '"url":'.'"'.Mage::getBaseUrl().htmlspecialchars($_product->getUrlPath(), ENT_QUOTES).'?___store='.htmlspecialchars($_product->getAttributeText('sale_store'), ENT_QUOTES).'"'.',';
				$outStr .= '"img_url":'.'"'.htmlspecialchars($_product->getSmallImageUrl(100,75), ENT_QUOTES).'"'.',';
				$outStr .= '"spacial_note":'.'"'.htmlspecialchars($_product->getSpecialNote(), ENT_QUOTES).'"'.',';
				$outStr .= '"short_desc":'.'"'.htmlspecialchars($this->convert_line_breaks(trim($_product->getShortDescription()), ENT_QUOTES)).'"';
				// $outStr .= '"store":'.'"'.'?___store='.htmlspecialchars($_product->getAttributeText('sale_store'), ENT_QUOTES).'"'.',';
	
				$outStr .= '}';
				if ($i == $end - 1) {
	
				} else {
					$outStr .= ',';
				}
			} catch (Exception $e) {
				var_dump($e->getMessage());
			}
		}
		$outStr .= ']}';
		echo $outStr;
	}
}