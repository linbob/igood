<?php
class Settv_Ordermanage_Block_Sales_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('sales_order_grid');
		$this->setUseAjax(true);
		$this->setDefaultSort('created_at');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	/**
	 * Retrieve collection class
	 *
	 * @return string
	 */
	protected function _getCollectionClass()
	{
		return 'sales/order_grid_collection';
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getResourceModel($this->_getCollectionClass());	
		$collection->getSelect()->joinLeft('settv_sales_order_custom as cu', 'main_table.entity_id = cu.order_id',array('shipstatus', 'supplier_id' ,'logistics_id','order_id'));		

		$collection->getSelect()->joinLeft(
		'sales_flat_order_item as im',
		'main_table.entity_id=im.order_id',
		array('im.name','im.qty_ordered'))
		->where("im.product_type='simple'");

		$collection->getSelect()->joinLeft('sales_flat_order_payment as pm', 'main_table.entity_id = pm.parent_id',array('method'));

		$collection->getSelect()->joinLeft('settv_sales_logistics_custom as sslc', 'cu.logistics_id = sslc.id', array('Max(sslc.created_at) as logistics_created', 'logistics_num'));


		$collection->getSelect()->joinLeft(array('ss'=>'settv_supplier'), 'cu.supplier_id = ss.id', array('ss.name as supplier_name'))
		->group('main_table.entity_id');

		$collection->getSelect()->joinLeft('sales_flat_order as sfo', 'main_table.entity_id = sfo.entity_id',array('shipping_method'));				
		
		$collection->getSelect()->joinLeft('medium_order_item as med', 'main_table.entity_id = med.order_id',array('master_order_id'));		

/*		print_r($collection->getSelect()->__toString()); */
		$this->setCollection($collection);  

		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('real_order_id', array(
				'header'=> Mage::helper('sales')->__('Order #'),
				'width' => '80px',
				'type'  => 'text',
				'index' => 'increment_id',
				'filter_index' => 'main_table.increment_id',
		));

		$this->addColumn('created_at', array(
				'header' => Mage::helper('sales')->__('Purchased On'),
				'index' => 'created_at',
				'type' => 'datetime',
				'width' => '100px',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'main_table.created_at',
		));

		$this->addColumn('name', array(
				'header'=> Mage::helper('sales')->__('Product Name'),
				'type'  => 'text',
				'index' => 'name',
				'filter_index' => 'im.name',
		));

		$this->addColumn('product_price', array(
				'header' => Mage::helper('sales')->__('product price'),
				'index' => 'order_id',
				'type'  => 'currency',
				'currency' => 'order_currency_code',
				'renderer' => new Settv_Common_Block_Render_ProductPrice(),
				'sortable' => false,
				'filter' => false,
		));

		$this->addColumn('qty_ordered', array(
				'header' => Mage::helper('sales')->__('QTY'),
				'index' => 'qty_ordered',
				'type'  => 'number',
				'total' => 'sum',
		));

		$this->addColumn('grand_total', array(
				'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
				'index' => 'grand_total',
				'filter_index' => 'main_table.grand_total',
				'type'  => 'currency',
				'currency' => 'order_currency_code',
		));

		$this->addColumn('supplier_name', array(
				'header' => Mage::helper('supplier')->__('Supplier'),
				'index' => 'supplier_name',
				'filter_index' => 'ss.name',
		));

		$this->addColumn('status', array(
				'header' => Mage::helper('sales')->__('Order Status'),
				'index' => 'status',
				'type'  => 'options',
				'width' => '70px',
				'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
				'filter_index' => 'main_table.status',
		));

		$this->addColumn('shipstatus', array(
				'header' => Mage::helper('sales')->__('Ship Status'),
				'index' => 'shipstatus',
				'type' => 'options',
				'options' => array( 'Shipped' => Mage::helper('sales')->__('Shipped'),'Not Shipped' => Mage::helper('sales')->__('Not Shipped') ,
						'Arrived' => Mage::helper('sales')->__('Arrived'),'Rejected' => Mage::helper('sales')->__('Rejected'),
						'Return Received' => Mage::helper('sales')->__('Return Received'),'Return Rejected' => Mage::helper('sales')->__('Return Rejected')),
				'filter_index' => 'cu.shipstatus'
		));

		$this->addColumn('forward_at', array(
				'header' => Mage::helper('sales')->__('forward at'),
				'index' => 'logistics_created',
				'type' => 'datetime',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'sslc.created_at',
		));

		$payments = Mage::getSingleton('payment/config')->getActiveMethods();
		$options = array();
		foreach ($payments as $_method){
			$options[$_method->getCode()] = $_method->getTitle();
		}

		$this->addColumn('payment_method', array(
				'header' => Mage::helper('sales')->__('Payment Method'),
				'index' => 'method',
				'renderer' => new Settv_Common_Block_Render_RenderPayment(),
				'type' => 'options',
				'options' => $options,
				'filter_index' => 'pm.method',
		));

		$methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
		$options = array();
		foreach($methods as $_code => $_method)
		{
			$options[$_code.'_'] = Mage::getStoreConfig("carriers/$_code/title");
		}
		$options["familymart_"]=Mage::helper('sales')->__('Familymart Shipping');

		$this->addColumn('logistic_method', array(
				'header' => Mage::helper('sales')->__('Logistics Type'),
				'index' => 'shipping_method',
				'renderer' => new Settv_Common_Block_Render_RenderShipping(),
				'type' => 'options',
				'options' => $options,
				'filter_index' => 'sfo.shipping_method',
		));

		if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
			$this->addColumn('action',
					array(
							'header'    => Mage::helper('sales')->__('Action'),
							'width'     => '50px',
							'type'      => 'action',
							'getter'     => 'getId',
							'actions'   => array(
									array(
											'caption' => Mage::helper('sales')->__('View'),
											'url'     => array('base'=>'*/sales_order/view'),
											'field'   => 'order_id'
									)
							),
							'filter'    => false,
							'sortable'  => false,
							'index'     => 'stores',
							'is_system' => true,
					));
		}
		
		$this->addColumn('master_order_id', array(
				'header' => Mage::helper('splitorder')->__('Master Order Id'),
				'index' => 'master_order_id',				
				'type' => 'text',
				'filter_index' => 'med.master_order_id'				
		));
		
		$this->addColumn('logistics_num', array(
				'header' => Mage::helper('sales')->__('logistics number'),
				'index' => 'logistics_num',
				'type' => 'text',
				'filter_index' => 'sslc.logistics_num'
		));
		
		$this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('entity_id');
		$this->getMassactionBlock()->setFormFieldName('order_ids');
		$this->getMassactionBlock()->setUseSelectAll(false);

		if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/cancel')) {
			$this->getMassactionBlock()->addItem('cancel_order', array(
					'label'=> Mage::helper('sales')->__('Cancel'),
					'url'  => $this->getUrl('*/sales_order/massCancel'),
			));
		}

		if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/hold')) {
			$this->getMassactionBlock()->addItem('hold_order', array(
					'label'=> Mage::helper('sales')->__('Hold'),
					'url'  => $this->getUrl('*/sales_order/massHold'),
			));
		}

		if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/unhold')) {
			$this->getMassactionBlock()->addItem('unhold_order', array(
					'label'=> Mage::helper('sales')->__('Unhold'),
					'url'  => $this->getUrl('*/sales_order/massUnhold'),
			));
		}

		$this->getMassactionBlock()->addItem('pdfinvoices_order', array(
				'label'=> Mage::helper('sales')->__('Print Invoices'),
				'url'  => $this->getUrl('*/sales_order/pdfinvoices'),
		));

		$this->getMassactionBlock()->addItem('pdfshipments_order', array(
				'label'=> Mage::helper('sales')->__('Print Packingslips'),
				'url'  => $this->getUrl('*/sales_order/pdfshipments'),
		));

		$this->getMassactionBlock()->addItem('pdfcreditmemos_order', array(
				'label'=> Mage::helper('sales')->__('Print Credit Memos'),
				'url'  => $this->getUrl('*/sales_order/pdfcreditmemos'),
		));

		$this->getMassactionBlock()->addItem('pdfdocs_order', array(
				'label'=> Mage::helper('sales')->__('Print All'),
				'url'  => $this->getUrl('*/sales_order/pdfdocs'),
		));

		$this->getMassactionBlock()->addItem('print_shipping_label', array(
				'label'=> Mage::helper('sales')->__('Print Shipping Labels'),
				'url'  => $this->getUrl('*/sales_order_shipment/massPrintShippingLabel'),
		));

		return $this;
	}

	public function getRowUrl($row)
	{
		if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
			return $this->getUrl('*/sales_order/view', array('order_id' => $row->getId()));
		}
		return false;
	}

	public function getGridUrl()
	{
		return $this->getUrl('*/*/grid', array('_current'=>true));
	}

}
