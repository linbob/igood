<?php
class Settv_Ordermanage_Block_Sales_Order_Gridforexport extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('sales_order_grid');
		$this->setUseAjax(true);
		$this->setDefaultSort('created_at');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	/**
	 * Retrieve collection class
	 *
	 * @return string
	 */
	protected function _getCollectionClass()
	{
		return 'sales/order_grid_collection';
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getResourceModel($this->_getCollectionClass());

		$collection->getSelect()->joinLeft('settv_sales_order_custom as cu', 'main_table.entity_id = cu.order_id',array('shipstatus', 'supplier_id' ,'invoicetype','logistics_id','invoiceubn','order_id'));
		$collection->getSelect()->joinLeft(array('sfoi'=>'sales_flat_order_item'), 'main_table.entity_id = sfoi.order_id',array('sfoi.name as product_name','sfoi.qty_ordered as total_qty_ordered','sku'))
		->where("sfoi.product_type='simple'");
		
		$collection->getSelect()->joinLeft(array('ss'=>'settv_supplier'), 'cu.supplier_id = ss.id', array('ss.name as supplier_name', 'ss.ubn as supplier_ubn'));
		$collection->getSelect()->joinLeft('sales_flat_order', 'main_table.entity_id = sales_flat_order.entity_id',array('customer_email'));
		$collection->getSelect()->joinLeft('sales_flat_order_address', 'main_table.entity_id = sales_flat_order_address.parent_id',array('b_address' => new Zend_Db_Expr('CONCAT(sales_flat_order_address.postcode,sales_flat_order_address.city,sales_flat_order_address.region,sales_flat_order_address.street)')))
		->where("sales_flat_order_address.address_type = 'billing'");
		$collection->getSelect()->joinLeft(array('sfoa'=>'sales_flat_order_address'),
				'main_table.entity_id = sfoa.parent_id AND sfoa.address_type="shipping"',
				array('s_address' => new Zend_Db_Expr('CONCAT(sfoa.postcode,sfoa.city,sfoa.region,sfoa.street)'), 's_cellphone'=>'trim(sfoa.cellphone)'))
				->group('main_table.entity_id');
		$collection->getSelect()->joinLeft('sales_flat_order_payment', 'main_table.entity_id = sales_flat_order_payment.parent_id',array('method'));
		$collection->getSelect()->joinLeft('settv_sales_logistics_custom as sslc', 'cu.logistics_id = sslc.id', array('Max(`sslc`.`created_at`) as logistics_created', 'logistics_num'));

		$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
		$costId = $eavAttribute->getIdByCode('catalog_product', 'product_cost');
		$collection->getSelect()->joinLeft('catalog_product_entity_int', "(sfoi.product_id = catalog_product_entity_int.entity_id and catalog_product_entity_int.attribute_id='{$costId}')", array('catalog_product_entity_int.value as product_cost'));
		$collection->getSelect()->joinLeft('medium_order_item', 'main_table.entity_id = medium_order_item.order_id',array('master_order_id'));
		$this->setCollection($collection);
		
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('real_order_id', array(
				'header'=> Mage::helper('sales')->__('Order #'),
				'width' => '80px',
				'type'  => 'text',
				'index' => 'increment_id',
				'filter_index' => 'main_table.increment_id',
		));

		$this->addColumn('created_at', array(
				'header' => Mage::helper('sales')->__('Purchased On'),
				'index' => 'created_at',
				'type' => 'datetime',
				'width' => '100px',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'main_table.created_at',
		));

		$this->addColumn('name', array(
				'header'=> Mage::helper('sales')->__('Product Name'),
				'type'  => 'text',
				'index' => 'product_name',
				'filter_index' => 'sfoi.name',
		));

		$this->addColumn('sku', array(
				'header'=> Mage::helper('sales')->__('SKU'),
				'type'  => 'text',
				'index' => 'sku',
		));

		$this->addColumn('product_price', array(
				'header' => Mage::helper('sales')->__('product price'),
				'index' => 'order_id',
				'type'  => 'currency',
				'currency' => 'order_currency_code',
				'renderer' => new Settv_Common_Block_Render_ProductPrice(),
		));

		$this->addColumn('qty_ordered', array(
				'header' => Mage::helper('sales')->__('QTY'),
				'index' => 'total_qty_ordered',
				'type'  => 'number',
				'total' => 'sum',
				'filter_index' => 'sfoi.qty_ordered',
		));

		$this->addColumn('grand_total', array(
				'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
				'index' => 'grand_total',
				'filter_index' => 'main_table.grand_total',
				'type'  => 'currency',
				'currency' => 'order_currency_code',
		));

		$this->addColumn('supplier_name', array(
				'header' => Mage::helper('supplier')->__('Supplier'),
				'index' => 'supplier_name',
				'filter_index' => 'ss.name',
		));
		
		$this->addColumn('supplier_ubn', array(
				'header' => Mage::helper('supplier')->__('Supplier Ubn'),
				'index' => 'supplier_ubn',
				'filter_index' => 'ss.ubn',
		));

		$this->addColumn('product_cost', array(
				'header' => Mage::helper('sales')->__('Product Cost'),
				'index' => 'product_cost',
				'type'  => 'currency',
				'currency' => 'order_currency_code',
		));


		
		$this->addColumn('status', array(
				'header' => Mage::helper('sales')->__('Order Status'),
				'index' => 'status',
				'type'  => 'options',
				'width' => '70px',
				'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
				'filter_index' => 'main_table.status',
		));

		
		$this->addColumn('shipstatus', array(
				'header' => Mage::helper('sales')->__('Ship Status'),
				'index' => 'shipstatus',
				'type' => 'options',
				'options' => array( 'Shipped' => Mage::helper('sales')->__('Shipped'),'Not Shipped' => Mage::helper('sales')->__('Not Shipped') ,
						'Arrived' => Mage::helper('sales')->__('Arrived'),'Rejected' => Mage::helper('sales')->__('Rejected'),
						'Return Received' => Mage::helper('sales')->__('Return Received'),'Return Rejected' => Mage::helper('sales')->__('Return Rejected')),
				'filter_index' => 'cu.shipstatus'
		));
		
		

		$this->addColumn('logistics_num', array(
				'header' => Mage::helper('sales')->__('logistics number'),
				'type' => 'number',
				'index' => 'logistics_num',
				'filter_index' => 'sslc.logistics_num'
		));

		$this->addColumn('forward_at', array(
				'header' => Mage::helper('sales')->__('forward at'),
				'index' => 'logistics_created',
				'type' => 'datetime',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'sslc.created_at',
		));

		$payments = Mage::getSingleton('payment/config')->getActiveMethods();
		$options = array();
		foreach ($payments as $_method){
			$options[$_method->getCode()] = $_method->getTitle();
		}
		
		$this->addColumn('payment_method', array(
				'header' => Mage::helper('sales')->__('Payment Method'),
				'type' => 'text',
				'index' => 'method',
				'renderer' => new Settv_Common_Block_Render_RenderPayment(),
		));
		
		$methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
		$options = array();
		foreach($methods as $_code => $_method)
		{
			$options[$_code.'_'] = Mage::getStoreConfig("carriers/$_code/title");
		}
		$options["familymart_"]=Mage::helper('sales')->__('Familymart Shipping');
		
		$this->addColumn('logistic_method', array(
				'header' => Mage::helper('sales')->__('Logistics Type'),
				'index' => 'shipping_method',
				'renderer' => new Settv_Common_Block_Render_RenderShipping(),
				'type' => 'options',
				'options' => $options,
		));

		$this->addColumn('shipping_name', array(
				'header' => Mage::helper('sales')->__('Ship to Name'),
				'type' => 'text',
				'index' => 'shipping_name',
				'filter_index' => 'sslc.created_at'
		));

		
		$this->addColumn('s_cellphone', array(
				'header' => Mage::helper('sales')->__('Ship to Cellphone'),
				'type' => 'text',
				'index' => 's_cellphone',				
		));		
		

		$this->addColumn('s_address', array(
				'header' => Mage::helper('sales')->__('Shipping Address'),
				'type' => 'text',
				'index' => 's_address',
		));

		$this->addColumn('billing_name', array(
				'header' => Mage::helper('sales')->__('Bill to Name'),
				'type' => 'text',
				'index' => 'billing_name',
		));

		$this->addColumn('b_address', array(
				'header' => Mage::helper('sales')->__('Billing Address'),
				'type' => 'text',
				'index' => 'b_address',
		));

		$this->addColumn('customer_email', array(
		        'header' => Mage::helper('sales')->__('Email'),
		        'type' => 'text',
		        'index' => 'customer_email',
		));

		$this->addColumn('invoicetype', array(
				'header' => Mage::helper('sales')->__('invoice type'),
				'type' => 'number',
				'index' => 'invoicetype',
		));

		$this->addColumn('invoiceubn', array(
				'header' => Mage::helper('sales')->__('Invoice UBN'),
				'type' => 'number',
				'index' => 'invoiceubn',
		));
		
		$this->addColumn('master_order_id', array(
				'header' => Mage::helper('splitorder')->__('Master Order Id'),
				'index' => 'master_order_id',
				'type' => 'text'
		));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
			return $this->getUrl('*/sales_order/view', array('order_id' => $row->getId()));
		}
		return false;
	}

	public function getGridUrl()
	{
		return $this->getUrl('*/*/grid', array('_current'=>true));
	}

}
