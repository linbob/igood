<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml sales order view
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Settv_Ordermanage_Block_Sales_Order_View extends Mage_Adminhtml_Block_Widget_Form_Container
{

	public function __construct()
	{
		$this->_objectId    = 'order_id';
		$this->_controller  = 'sales_order';
		$this->_mode        = 'view';

		parent::__construct();

		$this->_removeButton('delete');
		$this->_removeButton('reset');
		$this->_removeButton('save');
		$this->setId('sales_order_view');
		$order = $this->getOrder();
		$customId = Mage::getModel('check/custom_order')->getIdByOrder($order->getId());
		$customOrder = Mage::getModel('check/custom_order')->load($customId);
		//$createdinvoice = Mage::getModel ( 'einvoice/einvoices' )->loadByOrderId ($order->getId());

		if ($this->_isAllowedAction('edit') && $order->canEdit()) {
			$onclickJs = 'deleteConfirm(\''
					. Mage::helper('sales')->__('Are you sure? This order will be canceled and a new one will be created instead')
					. '\', \'' . $this->getEditUrl() . '\');';
			$this->_addButton('order_edit', array(
					'label'    => Mage::helper('sales')->__('Edit'),
					'onclick'  => $onclickJs,
			));
			// see if order has non-editable products as items
			$nonEditableTypes = array_keys($this->getOrder()->getResource()->aggregateProductsByTypes(
					$order->getId(),
					array_keys(Mage::getConfig()
							->getNode('adminhtml/sales/order/create/available_product_types')
							->asArray()
					),
					false
			));
			if ($nonEditableTypes) {
				$this->_updateButton('order_edit', 'onclick',
						'if (!confirm(\'' .
						Mage::helper('sales')->__('This order contains (%s) items and therefore cannot be edited through the admin interface at this time, if you wish to continue editing the (%s) items will be removed, the order will be canceled and a new order will be placed.', implode(', ', $nonEditableTypes), implode(', ', $nonEditableTypes)) . '\')) return false;' . $onclickJs
				);
			}
		}

		if ($this->_isAllowedAction('cancel') && $order->canCancel()) {
			$message = Mage::helper('sales')->__('Are you sure you want to cancel this order?');
			$this->_addButton('order_cancel', array(
					'label'     => Mage::helper('sales')->__('Cancel'),
					'onclick'   => 'deleteConfirm(\''.$message.'\', \'' . $this->getCancelUrl() . '\')',
			));
		}

		if ($this->_isAllowedAction('emails') && !$order->isCanceled()) {
			$message = Mage::helper('sales')->__('Are you sure you want to send order email to customer?');
			$this->addButton('send_notification', array(
					'label'     => Mage::helper('sales')->__('Send Email'),
					'onclick'   => "confirmSetLocation('{$message}', '{$this->getEmailUrl()}')",
			));
		}

		if ($this->_isAllowedAction('creditmemo') && $order->canCreditmemo()) {
			$message = Mage::helper('sales')->__('This will create an offline refund. To create an online refund, open an invoice and create credit memo for it. Do you wish to proceed?');
			$onClick = "setLocation('{$this->getCreditmemoUrl()}')";
			if ($order->getPayment()->getMethodInstance()->isGateway()) {
				$onClick = "confirmSetLocation('{$message}', '{$this->getCreditmemoUrl()}')";
			}
			$this->_addButton('order_creditmemo', array(
					'label'     => Mage::helper('sales')->__('Credit Memo'),
					'onclick'   => $onClick,
					'class'     => 'go'
			));
		}

		// invoice action intentionally
		if ($this->_isAllowedAction('invoice') && $order->canVoidPayment()) {
			$message = Mage::helper('sales')->__('Are you sure you want to void the payment?');
			$this->addButton('void_payment', array(
					'label'     => Mage::helper('sales')->__('Void'),
					'onclick'   => "confirmSetLocation('{$message}', '{$this->getVoidPaymentUrl()}')",
			));
		}

		if ($this->_isAllowedAction('hold') && $order->canHold()) {
			$this->_addButton('order_hold', array(
					'label'     => Mage::helper('sales')->__('Hold'),
					'onclick'   => 'setLocation(\'' . $this->getHoldUrl() . '\')',
			));
		}

		if ($this->_isAllowedAction('unhold') && $order->canUnhold()) {
			$this->_addButton('order_unhold', array(
					'label'     => Mage::helper('sales')->__('Unhold'),
					'onclick'   => 'setLocation(\'' . $this->getUnholdUrl() . '\')',
			));
		}

		if ($this->_isAllowedAction('review_payment')) {
			if ($order->canReviewPayment()) {
				$message = Mage::helper('sales')->__('Are you sure you want to accept this payment?');
				$this->_addButton('accept_payment', array(
						'label'     => Mage::helper('sales')->__('Accept Payment'),
						'onclick'   => "confirmSetLocation('{$message}', '{$this->getReviewPaymentUrl('accept')}')",
				));
				$message = Mage::helper('sales')->__('Are you sure you want to deny this payment?');
				$this->_addButton('deny_payment', array(
						'label'     => Mage::helper('sales')->__('Deny Payment'),
						'onclick'   => "confirmSetLocation('{$message}', '{$this->getReviewPaymentUrl('deny')}')",
				));
			}
			if ($order->canFetchPaymentReviewUpdate()) {
				$this->_addButton('get_review_payment_update', array(
						'label'     => Mage::helper('sales')->__('Get Payment Update'),
						'onclick'   => 'setLocation(\'' . $this->getReviewPaymentUrl('update') . '\')',
				));
			}
		}

		if ($this->_isAllowedAction('invoice') && $order->canInvoice()) {
			$_label = $order->getForcedDoShipmentWithInvoice() ?
			Mage::helper('sales')->__('Invoice and Ship') :
			Mage::helper('sales')->__('Invoice');
			$this->_addButton('order_invoice', array(
					'label'     => $_label,
					'onclick'   => 'setLocation(\'' . $this->getInvoiceUrl() . '\')',
					'class'     => 'go'
			));
		}

		if ($this->_isAllowedAction('ship') && $order->canShip()
		&& !$order->getForcedDoShipmentWithInvoice()) {
			$this->_addButton('order_ship', array(
					'label'     => Mage::helper('sales')->__('Ship'),
					'onclick'   => 'setLocation(\'' . $this->getShipUrl() . '\')',
					'class'     => 'go'
			));
		}

		if ($this->_isAllowedAction('reorder')
		&& $this->helper('sales/reorder')->isAllowed($order->getStore())
		&& $order->canReorder()
		) {
			$this->_addButton('order_reorder', array(
					'label'     => Mage::helper('sales')->__('Reorder'),
					'onclick'   => 'setLocation(\'' . $this->getReorderUrl() . '\')',
					'class'     => 'go'
			));
		}

		$user = Mage::getSingleton('admin/session')->getUser();
		$is_supplier = $user->getIsSupplier();

		if(!$is_supplier){
			$message = Mage::helper('sales')->__('Confirm to force return');
			$this->_addButton('frocereturnorder', array(
					'label'     => Mage::helper('ordermanage')->__('force return'),
					'onclick'   => "confirmSetLocation('{$message}', '{$this->getForcereturnUrl()}')",
					'class'     => 'go'
							));
			if($customOrder->canReturn($order)) {
				$message = Mage::helper('sales')->__('Confirm to allow for return');
				$this->_addButton('returnorder_allow', array(
						'label'     => Mage::helper('ordermanage')->__('allow return'),
						'onclick'   => "confirmSetLocation('{$message}', '{$this->getReturnallowUrl()}')",
						'class'     => 'go'
								));
					
				$message = Mage::helper('sales')->__('Confirm to deny for return');
				$this->_addButton('returnorder_deny', array(
						'label'     => Mage::helper('ordermanage')->__('deny return'),
						'onclick'   => "confirmSetLocation('{$message}', '{$this->getReturndenyUrl()}')",
						'class'     => 'go'
								));


			}
				
			if(!$customOrder->getInvoice_create_isupload())
			{
				$message = Mage::helper('sales')->__('Confirm to reCreate Invoice');
				$this->_addButton('reCreateInvoice_allow', array(
						'label'     => Mage::helper('ordermanage')->__('ReCreate Invoice'),
						'onclick'   => "confirmSetLocation('{$message}', '{$this->getReCreateInvoiceUrl()}')",
						'class'     => 'go'
								));
			}
				
			if(!$customOrder->getInvoice_cancel_isupload())
			{
				$message = Mage::helper('sales')->__('Confirm to reCancel Invoice');
				$this->_addButton('reCancelInvoice_allow', array(
						'label'     => Mage::helper('ordermanage')->__('ReCancel Invoice'),
						'onclick'   => "confirmSetLocation('{$message}', '{$this->getReCancelInvoiceUrl()}')",
						'class'     => 'go'
								));
			}

				
		} else {
			$isFamilyMartShipping = $customOrder["shippingtype"] === "familymart";
			$familyPrintPopWindow = $isFamilyMartShipping ? 'popWin(\'' . Mage::geturl("sales/supplierprint")."familyprint?order_id=" .$order->getId() . '\', \'_blank\');' : "";
				
			if($customOrder->canDeliver($order)){
				if (Mage::helper("ordermanage")->isDisplayRefOrders($order))
				{
					$message = Mage::helper('sales')->__('Confirm combine to deliver');
					$this->_addButton('order_deliver', array(
							'label'     => Mage::helper('supplier')->__('Combine Deliver'),
							'onclick'   => "confirmSetLocationWithConfirmCallback('{$message}', '{$this->getDeliverUrl()}', function(){{$familyPrintPopWindow}})",
							'class'     => 'go'
									));
				}
				else 
				{
					$message = Mage::helper('sales')->__('Confirm to deliver');
					$this->_addButton('order_deliver', array(
							'label'     => Mage::helper('supplier')->__('Deliver'),
							'onclick'   => "confirmSetLocationWithConfirmCallback('{$message}', '{$this->getDeliverUrl()}', function(){{$familyPrintPopWindow}})",
							'class'     => 'go'
									));
				}
			}
				
			if($customOrder->canArrive()) {
				$message = Mage::helper('sales')->__('Confirm to arrive');
				$this->_addButton('order_arrive', array(
						'label'     => Mage::helper('supplier')->__('Arrive'),
						'onclick'   => "confirmSetLocation('{$message}', '{$this->getSetArrivedUrl()}');" ,
						'class'     => 'go'
								));
			}
				
			if ($isFamilyMartShipping && $customOrder->getShipstatus()==="Shipped") {
				$this->_addButton('print_family_logistic', array(
						'label'     => Mage::helper('supplier')->__('PrintFamily'),
						'onclick'   => $familyPrintPopWindow,
						'class'     => 'go'
				));
			}

		}
	}

	/**
	 * Retrieve order model object
	 *
	 * @return Mage_Sales_Model_Order
	 */
	public function getOrder()
	{
		return Mage::registry('sales_order');
	}

	/**
	 * Retrieve Order Identifier
	 *
	 * @return int
	 */
	public function getOrderId()
	{
		return $this->getOrder()->getId();
	}

	public function getOrderIncrementId()
	{
		return $this->getOrder()->getIncrementId();
	}

	public function getHeaderText()
	{
		if ($_extOrderId = $this->getOrder()->getExtOrderId()) {
			$_extOrderId = '[' . $_extOrderId . '] ';
		} else {
			$_extOrderId = '';
		}
		return Mage::helper('sales')->__('Order # %s %s | %s', $this->getOrder()->getRealOrderId(), $_extOrderId, $this->formatDate($this->getOrder()->getCreatedAtDate(), 'short', true));
	}

	public function getUrl($params='', $params2=array())
	{
		$params2['order_id'] = $this-> getOrderId();
		$params2['order_incrementid'] = $this-> getOrderIncrementId();
		return parent::getUrl($params, $params2);
	}

	public function getEditUrl()
	{
		return $this->getUrl('*/sales_order_edit/start');
	}

	public function getEmailUrl()
	{
		return $this->getUrl('*/*/email');
	}

	public function getCancelUrl()
	{
		return $this->getUrl('*/*/cancel');
	}

	public function getInvoiceUrl()
	{
		return $this->getUrl('*/sales_order_invoice/start');
	}

	public function getCreditmemoUrl()
	{
		return $this->getUrl('*/sales_order_creditmemo/start');
	}

	public function getHoldUrl()
	{
		return $this->getUrl('*/*/hold');
	}

	public function getUnholdUrl()
	{
		return $this->getUrl('*/*/unhold');
	}

	public function getShipUrl()
	{
		return $this->getUrl('*/sales_order_shipment/start');
	}

	public function getCommentUrl()
	{
		return $this->getUrl('*/*/comment');
	}

	public function getReorderUrl()
	{
		return $this->getUrl('*/sales_order_create/reorder');
	}

	/**
	 * Payment void URL getter
	 */
	public function getVoidPaymentUrl()
	{
		return $this->getUrl('*/*/voidPayment');
	}

	protected function _isAllowedAction($action)
	{
		return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/' . $action);
	}

	/**
	 * Return back url for view grid
	 *
	 * @return string
	 */
	public function getBackUrl()
	{
		if ($this->getOrder()->getBackUrl()) {
			return $this->getOrder()->getBackUrl();
		}

		return $this->getUrl('*/*/');
	}

	public function getReviewPaymentUrl($action)
	{
		return $this->getUrl('*/*/reviewPayment', array('action' => $action));
	}


	public function getForcereturnUrl()
	{
		return $this->getUrl('*/*/forcereturn');
	}

	public function getSetestablishedUrl()
	{
		return $this->getUrl('*/*/setestablished');
	}

	public function getSetReturnokUrl()
	{
		return $this->getUrl('*/*/setreturnok');
	}
	public function getSetReturnprocessingUrl()
	{
		return $this->getUrl('*/*/setreturnprocessing');
	}
	public function getSetFailedUrl()
	{
		return $this->getUrl('*/*/setfailed');
	}
	public function getSetCancelUrl()
	{
		return $this->getUrl('*/*/setcancel');
	}
	public function getSetShippedUrl()
	{
		return $this->getUrl('*/*/setshipped');
	}
	public function getSetNotShippedUrl()
	{
		return $this->getUrl('*/*/setnotshipped');
	}
	public function getSetArrivedUrl()
	{
		return $this->getUrl('*/*/setarrived');
	}
	public function getReturnallowUrl()
	{
		return $this->getUrl('*/*/returnallow');
	}
	public function getReturndenyUrl()
	{
		return $this->getUrl('*/*/returndeny');
	}
	public function getSetRejectedUrl()
	{
		return $this->getUrl('*/*/setrejected');
	}
	public function getSetReturnReceivedUrl()
	{
		return $this->getUrl('*/*/setreturnreceived');
	}
	public function getSetReturnRejectedUrl()
	{
		return $this->getUrl('*/*/setreturnrejected');
	}
	public function getDeliverUrl()
	{
		return $this->getUrl('*/*/deliver');
	}
	public function getReCreateInvoiceUrl()
	{
		return $this->getUrl('*/*/reCreateC0401');
	}
	public function getReCancelInvoiceUrl()
	{
		return $this->getUrl('*/*/reCreateC0501');
	}

	protected function _prepareLayout()
	{
		$this->getLayout()->getBlock('head')->addItem("skin_js", "settvjs/jquery-1.9.0.min.js");
		$this->getLayout()->getBlock('head')->addItem("skin_js", "settvjs/ordermanage.js");

		return parent::_prepareLayout();
	}

	/**
	 * Produce dropdown HTML
	 *
	 * @param string $area
	 * @return string
	 */
	public function getOrderStatusDropdownsHtml($area = null)
	{
		$user = Mage::getSingleton('admin/session')->getUser();
		$is_supplier = $user->getIsSupplier();

		if(!$is_supplier){
			$out = "<select id=\"orderStatusSelect\" onchange=\"changeOrderStatus('orderStatusSelect');\">";

			$optionValue = "";
			$display = Mage::helper('ordermanage')->__('Set order status');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetestablishedUrl();
			$display = Mage::helper('ordermanage')->__('Set established');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetReturnprocessingUrl();
			$display = Mage::helper('ordermanage')->__('Set Return Processing');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";
				
			$optionValue = $this->getSetReturnokUrl();
			$display = Mage::helper('ordermanage')->__('Set Return Success');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetFailedUrl();
			$display = Mage::helper('ordermanage')->__('Set failed');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetCancelUrl();
			$display = Mage::helper('ordermanage')->__('Set cancel');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			return $out . "</select>";
		}
	}

	/**
	 * Produce dropdown HTML
	 *
	 * @param string $area
	 * @return string
	 */
	public function getShippingStatusDropdownsHtml($area = null)
	{
		$user = Mage::getSingleton('admin/session')->getUser();
		$is_supplier = $user->getIsSupplier();

		if(!$is_supplier){
			$out = "<select id=\"shippingStatusSelect\" onchange=\"changeOrderStatus('shippingStatusSelect');\">";

			$optionValue = "";
			$display = Mage::helper('ordermanage')->__('Set shipping status');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetShippedUrl();
			$display = Mage::helper('ordermanage')->__('Set shipped');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetNotShippedUrl();
			$display = Mage::helper('ordermanage')->__('Set not shipped');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetArrivedUrl();
			$display = Mage::helper('ordermanage')->__('Set arrived');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetRejectedUrl();
			$display = Mage::helper('ordermanage')->__('Set rejected');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetReturnReceivedUrl();
			$display = Mage::helper('ordermanage')->__('Set return received');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			$optionValue = $this->getSetReturnRejectedUrl();
			$display = Mage::helper('ordermanage')->__('Set return rejected');
			$out = $out . "<option value=\"" . $optionValue ."\">" . $display . "</option>";

			return $out . "</select>";
		}
	}	

}
