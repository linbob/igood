<?php
// // 20120711 set 2 link in 1 column by Sean

class Settv_Ordermanage_Block_Widget_Grid_Column_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
	public function render(Varien_Object $row)
	{
		$actions = $this->getColumn()->getActions();
		if ( empty($actions) || !is_array($actions) ) {
			return '&nbsp;';
		}
	
		if(sizeof($actions)==1 && !$this->getColumn()->getNoLink()) {
			foreach ($actions as $action) {
				if ( is_array($action) ) {
					return $this->_toLinkHtml($action, $row);
				}
			}
		}
		
		$sethtml = "";
		if(sizeof($actions)==2 && !$this->getColumn()->getNoLink()) {
			foreach ($actions as $action) {
				if ( is_array($action) ) {
					$sethtml .= $this->_SettvtoLinkHtml($action, $row)."&nbsp;&nbsp;&nbsp;&nbsp;";
				}
			}
			return $sethtml;
		}
	
		$out = '<select class="action-select" onchange="varienGridAction.execute(this);">'
		. '<option value=""></option>';
		$i = 0;
		foreach ($actions as $action){
			$i++;
			if ( is_array($action) ) {
				$out .= $this->_toOptionHtml($action, $row);
			}
		}
		$out .= '</select>';
		return $out;
	}
	
	protected function _SettvtoLinkHtml($action, Varien_Object $row)
	{
		$actionAttributes = new Varien_Object();
	
		$actionCaption = '';
		$this->_transformActionData($action, $actionCaption, $row);
	
		if(isset($action['confirm'])) {
			$action['onclick'] = 'return window.confirm(\''
			. addslashes($this->htmlEscape($action['confirm']))
			. '\')';
			unset($action['confirm']);
		}
	
		$actionAttributes->setData($action);
		return '<a ' . $actionAttributes->serialize() . ' style ="border-width: 1px;border-style: solid;border-color: #ED6502 #A04300 #A04300 #ED6502;padding: 0 7px 1px 7px;background: #FFAC47;color: white;font: bold 12pxarial,helvetica, sans-serif;cursor: pointer;text-align: center !important;white-space: nowrap;text-decoration:none;">' . $actionCaption . '</a>';
	}
}