<?php
class Settv_Ordermanage_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getRefOrderForShipping($order)
	{
		$sales_orders = array();

		$order_id = $order->getId();
		$customer_id = $order->getCustomerId();
		$shiping_address_data = $order->getShippingAddress();
		$shiping_name = $shiping_address_data->getName();
		$shiping_address = $shiping_address_data->getPostcode().$shiping_address_data->getCity().$shiping_address_data->getRegion().$shiping_address_data->getStreetFull();

		$custom_order = Mage::getModel('check/custom_order')->loadByOrderId($order_id);
		$supplier_id = $custom_order->getSupplierId();

		$collection = Mage::getModel('sales/order')->getCollection()
		->addFieldToFilter('state', array('eq' => 'established'))
		->addFieldToFilter('customer_id', array('eq' => $customer_id));

		$collection->getSelect()->joinInner('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id')
		->where('supplier_id=?', $supplier_id)
		->where('shipstatus=?', Settv_Check_Model_Custom_Order::STATE_NOT_SHIPPED);

		$shipping_title = Mage::helper('ordermanage')->getShippingTitleByOrder($order);

		foreach ($collection as $item)
		{
			$s_shipping_title = Mage::helper('ordermanage')->getShippingTitleByOrder($item);
			$sa_data = $item->getShippingAddress();
			$sn = $sa_data->getName();
			$sa = $sa_data->getPostcode().$sa_data->getCity().$sa_data->getRegion().$sa_data->getStreetFull();

			if ($shiping_name == $sn && $shiping_address == $sa
			&& $s_shipping_title == $shipping_title)
			{
				array_push($sales_orders, $item);
			}
		}

		return $sales_orders;
	}

	public function setSummaryOfOrders($orders, $master_order = null)
	{
		if ($master_order == null)
			$master_order = new Varien_Object();

		$qty_count = 0;
		$grand_total = 0;
		$period = 0;
		$payment = "";
		$format_grand_total = "";
		$createTime = null;
		$charge_amount = 0;
		$original_amount = 0;

		foreach ($orders as $order)
		{
			$period = $order->getPayment()->getAdditionalData(); //Chinatrust Installment;
			$payment = $order->getPayment()->getMethodInstance()->getTitle();
			$pay_method = $order->getPayment()->getMethodInstance()->getCode();

			$createTime = $order->getCreatedAt();
			$order_state = $order->getState();
			$order_id = $order->getId();
			$custom_order = Mage::getModel('check/custom_order')->loadByOrderId($order_id);
			$ship_state = $custom_order->getShipstatus();

			if ($order_state != Settv_Check_Model_Sales_Order::STATE_CANCELED
			&& $order_state != Settv_Check_Model_Sales_Order::STATE_FAILED
			&& $order_state != Settv_Check_Model_Sales_Order::STATE_RETURN_SUCCEEDED)
			{
				$grand_total += $order->getGrandTotal();
				$qty_count += $order->getTotalQtyOrdered();

				if ($ship_state == Settv_Check_Model_Custom_Order::STATE_NOT_SHIPPED)
				{
					if($pay_method == "cashondelivery")
						$charge_amount += $order->getGrandTotal();
				}
			}
			$original_amount += $order->getGrandTotal();

			$format_grand_total = $order->formatPrice($grand_total);
		}

		$master_order->setFormatGrandTotal($format_grand_total);
		$master_order->setGrandTotal(round($grand_total,0));
		$master_order->setTotalQtyOrdered(round($qty_count,0));
		$master_order->setPeriod($period);
		$master_order->setPayment($payment);
		$master_order->setChargeAmt($charge_amount);
		$master_order->setCreateTime($createTime);
		$master_order->setOriginalAmount($original_amount); //全部訂單的總額, 不管訂單裡是否有退貨,退訂,失敗 (because Chinatrust refund need this)

		return $master_order;
	}

	public function isDisplayRefOrders($order)
	{
		$diplay_ref_order = false;

		if ($order->getState() == Settv_Check_Model_Sales_Order::STATE_ESTABLISHED)
		{
			$shipping_title = Mage::helper('ordermanage')->getShippingTitleByOrder($order);

			if ($shipping_title == Mage::getStoreConfig('carriers/hct/title', false))
				$diplay_ref_order = true;
		}

		return $diplay_ref_order;
	}

	public function getShippingMethodByCompany($logistics_com)
	{
		$shipping_method = null;
		$methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
		foreach ($methods as $_code => $_method) {
			if ($logistics_com == Mage::getStoreConfig("carriers/$_code/title")){
				$shipping_method = $_code.'_';
			}
		}
		return $shipping_method;
	}

	public function getShippingTitleByOrder($order)
	{
		$shipping_method = $order->getShippingMethod();
		$code = substr($shipping_method, 0, -1);
		$shipping_title = Mage::getStoreConfig("carriers/$code/title");
		return $shipping_title;
	}
}