<?php
require_once 'Mage/Adminhtml/controllers/Report/SalesController.php';
class Settv_Ordermanage_Sales_BestsellerController extends Mage_Adminhtml_Report_SalesController
{	
	public function exportBestsellersExcelAction()
	{
		$fileName   = 'bestsellers.xls';
		$grid       = $this->getLayout()->createBlock('adminhtml/report_sales_bestsellers_grid');
		$this->_initReportAction($grid);
		$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
	}
}
?>