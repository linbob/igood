<?php
require_once 'Mage/Adminhtml/controllers/Report/CustomerController.php';
class Settv_Ordermanage_Sales_CustomerController extends Mage_Adminhtml_Report_CustomerController
{	
	public function exportAccountsExcelAction()
	    {
	        $fileName   = 'accounts.xls';
	        $content    = $this->getLayout()->createBlock('adminhtml/report_customer_accounts_grid')
	            ->getExcel($fileName);
	
	        $this->_prepareDownloadResponse($fileName, $content);
	    }
}
?>