<?php
require_once 'Mage/Adminhtml/controllers/Report/CustomerController.php';
class Settv_Ordermanage_Sales_CustomersordersController extends Mage_Adminhtml_Report_CustomerController
{	
	public function exportOrdersExcelAction()
	    {
	        $fileName   = 'customers_orders.xls';
	        $content    = $this->getLayout()->createBlock('adminhtml/report_customer_orders_grid')
	            ->getExcel($fileName);
	
	        $this->_prepareDownloadResponse($fileName, $content);
	    }
}
?>