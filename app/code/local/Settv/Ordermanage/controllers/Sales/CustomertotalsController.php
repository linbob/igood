<?php
require_once 'Mage/Adminhtml/controllers/Report/CustomerController.php';
class Settv_Ordermanage_Sales_CustomertotalsController extends Mage_Adminhtml_Report_CustomerController
{	
	public function exportTotalsExcelAction()
	    {
	        $fileName   = 'customer_totals.xls';
	        $content    = $this->getLayout()->createBlock('adminhtml/report_customer_totals_grid')
	            ->getExcel($fileName);
	
	        $this->_prepareDownloadResponse($fileName, $content);
	    }
}
?>