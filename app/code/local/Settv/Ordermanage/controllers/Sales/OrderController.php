<?php
require_once 'Mage/Adminhtml/controllers/Sales/OrderController.php';
class Settv_Ordermanage_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{
	/**
	 *  Export order grid to CSV format
	 */
	public function exportCsvAction()
	{
		$fileName   = 'sales_orders.csv';
		$content    = $this->getLayout()->createBlock('ordermanage/sales_order_gridforexport')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
		exit(0);
	}

	/**
	 *  Export order grid to Excel XML format
	 */
	public function exportExcelAction()
	{
		$fileName   = 'sales_orders.xls';
		$content       = $this->getLayout()->createBlock('ordermanage/sales_order_gridforexport')->getExcelFile($fileName);
		$this->_prepareDownloadResponse($fileName, $content);
		exit(0);
	}



	public function forcereturnAction()
	{
		//Save Return Order Deny Flag
		$order_id   = $this->getRequest()->getParam('order_id');
		$order      = Mage::getModel('sales/order')->load($order_id);

		Mage::helper('common/payment')->refundOrder($order); //: Refund

		//- post-refund action
		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$model = Mage::getModel('check/custom_order')->load($CustomId)
		->setData('returnallowflag', true)
		->save();
		
		$modelArr = $model->getData();
		if($modelArr['returnid']){
		    $ChangeOld = Mage::getModel('check/custom_returnorder')->load($modelArr['returnid'])
		    ->setData('order_id',$Form_OrderId)
		    ->setData('active',false)
		    ->setData('update_at',now())
		    ->save();
		}
		
		$shippingId = $order->getShippingAddress()->getCustomerAddressId();

		$Handle_OrderId = $order_id;
		$returnreason = 9;
		$returnaddressid = $shippingId;

		$ReturnOrder = Mage::getModel('check/custom_returnorder');
		$ReturnOrder->setData('order_id',$Handle_OrderId)
		->setData('active',true)
		->save();

		//Set return number (逆物流編號) to empty if the order return logistic is not using hct
		$ReturnNum = "";
		$items1 = $order->getAllItems();
		foreach ($items1 as $itemId1 => $item1) {
			$SproductId=$item1->getProductId();
		}
		$shouldProductUseHct = Mage::helper('common/common')->shouldProductUseHct($SproductId);
		if ( $shouldProductUseHct ) {
			$ReturnNum = $ReturnOrder->getreturnnum($ReturnOrder->getId());
		}

		$ReturnOrder->setData('return_num',$ReturnNum)->save();

		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($Handle_OrderId);
		$model = Mage::getModel('check/custom_order')->load($CustomId);
		$model->setData('returnid', $ReturnOrder->getId())
		->setData('returnnum', $ReturnNum)
		->setData('returnreason', $returnreason)
		->setData('returnaddressid', $returnaddressid)
		->setData('returnallowflag', true)
		->save();

		$order = Mage::getModel('sales/order')->load($Handle_OrderId);
		$order->setState('return_processing', true, '手動')->save();

		if ( $shouldProductUseHct ) {
			Mage::helper('logistichct/Hct')->CreateReverseTransactionFile($Handle_OrderId);
		}

		$this->_redirect('*/*/');
	}

	public function setestablishedAction()
	{

		$order_id  = $this->getRequest()->getParam('order_id');

		$order = Mage::getModel('sales/order')->load($order_id);

		if($order){
			$order->setState('established', true, '手動')->save();
			$items = $order->getAllItems();
			foreach ($items as $itemId => $item)
			{
				$productId=$item->getProductId();
				$qty=$item->getQtyOrdered() * -1;
			}
			$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
			$stockItem->addQty($qty);
			$stockItem->save();
		}

		$this->_redirect('*/*/');
	}

	public function setreturnokAction()
	{

		$order_id  = $this->getRequest()->getParam('order_id');

		$order = Mage::getModel('sales/order')->load($order_id);

		if($order){
			$order->setState('return_succeeded', true, '手動')->save();
		}

		$this->_redirect('*/*/');
	}

	public function setreturnprocessingAction()
	{

		$order_id  = $this->getRequest()->getParam('order_id');

		$order = Mage::getModel('sales/order')->load($order_id);

		if($order){
			$order->setState('return_processing', true, '手動')->save();
		}

		$this->_redirect('*/*/');
	}

	public function setfailedAction()
	{

		$order_id  = $this->getRequest()->getParam('order_id');

		$order = Mage::getModel('sales/order')->load($order_id);

		if($order){
			$order->setState('failed', true, '手動')->save();
		}

		$this->_redirect('*/*/');
	}

	public function setcancelAction()
	{

		$order_id  = $this->getRequest()->getParam('order_id');

		$order = Mage::getModel('sales/order')->load($order_id);

		if($order){
			$order->setState('canceled', true, '手動')->save();
		}

		$this->_redirect('*/*/');
	}

	public function setshippedAction()
	{
		$order_id  = $this->getRequest()->getParam('order_id');

		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$customOrder = Mage::getModel('check/custom_order')->load($CustomId);

		if($customOrder){
			$customOrder->setShipstatus('Shipped', '手動')->save();
		}

		$this->_redirect('*/*/');
	}

	public function setnotshippedAction()
	{
		$order_id  = $this->getRequest()->getParam('order_id');

		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$customOrder = Mage::getModel('check/custom_order')->load($CustomId);

		if($customOrder){
			$customOrder->setShipstatus('Not Shipped', '手動')->save();
		}

		$this->_redirect('*/*/');
	}

	public function setarrivedAction()
	{
		$order_id  = $this->getRequest()->getParam('order_id');

		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$customOrder = Mage::getModel('check/custom_order')->load($CustomId);

		$logistics_id = $customOrder->getLogisticsId();
		$dateTime = now();
		Mage::getModel('check/custom_logistics')
		->load($logistics_id)
		->setArrivedAt($dateTime)
		->save();

		if($customOrder){
			$customOrder
			->setShipstatus('Arrived', '手動')
			->setArrivedAt($dateTime)
			->save();
		}

		$this->_redirect('*/*/');
	}

	public function setrejectedAction()
	{
		$order_id  = $this->getRequest()->getParam('order_id');

		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$customOrder = Mage::getModel('check/custom_order')->load($CustomId);

		if($customOrder){
			$customOrder->setShipstatus('Rejected', '手動')->save();
		}

		$this->_redirect('*/*/');
	}

	public function setreturnreceivedAction()
	{
		$order_id  = $this->getRequest()->getParam('order_id');

		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$customOrder = Mage::getModel('check/custom_order')->load($CustomId);

		if($customOrder){
			$customOrder->setShipstatus('Return Received', '手動')->save();
		}

		$this->_redirect('*/*/');
	}

	public function setreturnrejectedAction()
	{
		$order_id  = $this->getRequest()->getParam('order_id');

		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$customOrder = Mage::getModel('check/custom_order')->load($CustomId);

		if($customOrder){
			$customOrder->setShipstatus('Return Rejected', '手動')->save();
		}

		$this->_redirect('*/*/');
	}

	public function addCommentAction()
	{
		parent::addCommentAction();

		$user = Mage::getSingleton('admin/session')->getUser();
		$is_supplier = $user->getIsSupplier();

		$id = $this->getRequest()->getParam('order_id');
		$order = Mage::getModel('sales/order')->load($id);

		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$data = $this->getRequest()->getPost('history');
		$emailTemplateVariables['comment'] = $data['comment'];

		if ($order && $is_supplier) {

			$email_address = Mage::getStoreConfig('contacts/email/recipient_email');

			Mage::helper('common/common')->sendEmail('Admin Order Comment', $email_address, $emailTemplateVariables);
		} else {
			Mage::helper('common/common')->emailSupplierByOrder($order, 'Admin Order Comment', $emailTemplateVariables);
		}

	}

	public function returnallowAction()
	{
		//Save Return Order Allow Flag
		$order_id = $this->getRequest()->getParam('order_id');
		$order = Mage::getModel('sales/order')->load($order_id);

		Mage::helper('common/payment')->refundOrder($order); //: Refund

		//- post-refund actions
		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$model = Mage::getModel('check/custom_order')->load($CustomId)
		->setData('returnallowflag', true)
		->save();

		$order->setState('return_succeeded', true, '')->save();

		$items = $order->getAllItems();
		foreach ($items as $itemId => $item)
		{
			$productId=$item->getProductId();
			$qty=$item->getQtyOrdered();
		}
		$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
		$stockItem->addQty($qty);
		$stockItem->save();

		$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
		$email_address = $customer->getEmail();

		//**************************************Send Email to Customer
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$emailTemplateVariables['order_id'] = $order->getIncrementId();
		$emailTemplateVariables['totalPrice'] = round($order->getGrandTotal(),0);
		$emailTemplateVariables['returndate'] = date("Y年m月d日");

		$items = $order->getAllItems();
		foreach ($items as $itemId => $item)
		{
			$emailTemplateVariables['productname'] = $item->getName();
		}

		if($model["invoicetype"] == "5" or $model["invoicetype"] == "4")
		{
			if(Mage::helper('goveinvoice/message')->C0501_Action($order))
			{
				Mage::log ( "[C0501] Create einvoice C0501 action SUCCESS. ",null, 'einvoice.log' );
			}
			else
			{
				Mage::log ( "[C0501] Create einvoice C0501 action FAIL.",null, 'einvoice.log' );
			}
		}

		Mage::helper('common/common')->sendEmail('Return Order Allow', $email_address, $emailTemplateVariables);
		//**************************************

		$this->_redirect('*/*/');
	}

	public function returndenyAction()
	{
		//Save Return Order Deny Flag
		$order_id  = $this->getRequest()->getParam('order_id');
		$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($order_id);
		$model = Mage::getModel('check/custom_order')->load($CustomId)
		->setData('returnallowflag', false)
		->save();

		$order = Mage::getModel('sales/order')->load($order_id);
		$order->setState('return_canceled', true, '')->save();

		$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
		$email_address = $customer->getEmail();

		/*
			//Send Email to Customer
		$emailTemplateVariables = array();
		$emailTemplateVariables['order_id'] = $order->getIncrementId();
		$emailTemplateVariables['customer'] = $customer->getFirstname();
		Mage::helper('common/common')->sendEmail('Return Order Deny', $email_address, $emailTemplateVariables);
		*/

		//**************************************Send Email to Customer
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$emailTemplateVariables['order_id'] = $order->getIncrementId();
		$emailTemplateVariables['totalPrice'] = round($order->getGrandTotal(),0);
		$emailTemplateVariables['returndate'] = date("Y年m月d日");

		$items = $order->getAllItems();
		foreach ($items as $itemId => $item)
		{
			$emailTemplateVariables['productname'] = $item->getName();
			//$emailTemplateVariables['unitPrice'] = round($item->getPrice(),0);
			//$emailTemplateVariables['sku'] =$item->getSku();
			//$emailTemplateVariables['productId']=$item->getProductId();
			//$emailTemplateVariables['qty']=$item->getQtyToInvoice();
		}

		Mage::helper('common/common')->sendEmail('Return Order Deny', $email_address, $emailTemplateVariables);
		//**************************************

		$this->_redirect('*/*/');
	}

	public function reCreateC0401Action()
	{
		$order_id  = $this->getRequest()->getParam('order_id');
		$order = Mage::getModel('sales/order')->load($order_id);
		Mage::helper('goveinvoice/message')->ReCreateC0401Action($order);
		$this->_redirect('*/*/');
	}

	public function reCreateC0501Action()
	{
		$order_id  = $this->getRequest()->getParam('order_id');
		$order = Mage::getModel('sales/order')->load($order_id);
		Mage::helper('goveinvoice/message')->ReCreateC0501Action($order);
		$this->_redirect('*/*/');
	}

	protected function _isAllowed()
	{
		$isAllowed = parent::_isAllowed();
		if(!$isAllowed) {
			$user = Mage::getSingleton('admin/session')->getUser();
			$is_supplier = $user->getIsSupplier();

			if($is_supplier) {
				$aclResource = 'admin/supplier_report_menu';
				$isAllowed =  Mage::getSingleton('admin/session')->isAllowed($aclResource);
			}
		}
		return $isAllowed;
	}
}
?>