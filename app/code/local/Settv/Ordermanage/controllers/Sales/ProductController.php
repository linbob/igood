<?php
require_once 'Settv/Supplier/controllers/Adminhtml/ProductController.php';
class Settv_Ordermanage_Sales_ProductController extends Settv_Supplier_Adminhtml_ProductController
{
	public function indexAction() {
		$this->loadLayout()->_setActiveMenu('sales/sales_product');		
		$this->renderLayout();
	}
	
	public function editAction()
	{		
		$id = $this->getRequest()->getParam('id');		
		$url = Mage::helper('adminhtml')->getUrl("adminhtml/catalog_product/edit", array('id'=> $id));		
		Mage::app()->getResponse()->setRedirect($url);
	}
}