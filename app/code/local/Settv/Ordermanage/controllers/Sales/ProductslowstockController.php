<?php
require_once 'Mage/Adminhtml/controllers/Report/ProductController.php';
class Settv_Ordermanage_Sales_ProductslowstockController extends Mage_Adminhtml_Report_ProductController
{	
	public function exportLowstockExcelAction()
	    {
	        $fileName   = 'products_lowstock.xls';
	        $content    = $this->getLayout()->createBlock('adminhtml/report_product_lowstock_grid')
	            ->setSaveParametersInSession(true)
	            ->getExcel($fileName);
	
	        $this->_prepareDownloadResponse($fileName, $content);
	    }
}
?>