<?php
require_once 'Mage/Adminhtml/controllers/Report/ProductController.php';
class Settv_Ordermanage_Sales_ProductsmostviewedController extends Mage_Adminhtml_Report_ProductController
{	
    public function exportViewedExcelAction()
    {
        $fileName   = 'products_mostviewed.xls';
        $content    = $this->getLayout()->createBlock('adminhtml/report_product_viewed_grid')
            ->getExcel($fileName);

        $this->_prepareDownloadResponse($fileName, $content);
    }
}
?>