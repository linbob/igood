<?php
require_once 'Mage/Adminhtml/controllers/Report/ProductController.php';
class Settv_Ordermanage_Sales_ProductsorderedController extends Mage_Adminhtml_Report_ProductController
{	
    public function exportSoldExcelAction()
    {
        $fileName   = 'products_ordered.xls';
        $content    = $this->getLayout()
            ->createBlock('adminhtml/report_product_sold_grid')
            ->getExcel($fileName);

        $this->_prepareDownloadResponse($fileName, $content);
    }
}
?>