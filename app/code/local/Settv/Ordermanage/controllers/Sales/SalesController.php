<?php
require_once 'Mage/Adminhtml/controllers/Report/SalesController.php';
class Settv_Ordermanage_Sales_SalesController extends Mage_Adminhtml_Report_SalesController
{	
	public function exportSalesExcelAction()
	{
		$fileName   = 'sales.xls';
		$grid       = $this->getLayout()->createBlock('adminhtml/report_sales_sales_grid');
		$this->_initReportAction($grid);
		$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
	}
}
?>