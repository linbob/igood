<?php
class Settv_Page_Block_Html_Header extends Mage_Page_Block_Html_Header
{
	public function getMenuHtml()
	{
		$baseURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		$_category = Mage::getModel('catalog/category');
		$outputHtmlArray = array();
		$maxSubLevel = 3;
		foreach (Mage::app()->getWebsites() as $website)
		{
			foreach ($website->getGroups() as $group)
			{
				$stores = $group->getStores();
				foreach ($stores as $store)
				{
					$storeId = $store->getId();
					$catId = Mage::app()->getStore($storeId)->getRootCategoryId();
					$_curCategory = $_category->load($catId);
					if($_curCategory->getIsActive())
					{
						$catChildren = $_curCategory->getChildren();
						if (!empty($catChildren))
						{
							$o = '<li><a href="#">'.$_curCategory->getName().'</a>';
							$o .= $this->getChildLevels($baseURL, $storeId, $catChildren, $maxSubLevel, true);
							$o .= '</li>';
							array_push($outputHtmlArray, array($store->getSortOrder(), $o));
						}
					}
				}
			}
		}

		usort($outputHtmlArray, "cmp");
		$result = null;
		foreach ($outputHtmlArray as $outputHtml)
		{
			$result .= $outputHtml[1];
		}
		return $result;
	}

	function getChildLevels($baseURL, $storeId, $catChildren, $maxSubLevel, $isFirstLevel)
	{
		$maxSubLevel = $maxSubLevel - 1;		
		
		$categoryChildren = explode(",", $catChildren);
		if ($isFirstLevel)
			$o = '<ul class="submenu-ul sf-menu">'; //Level 2
		else
			$o = '<ul>';
		
		$collection = Mage::getModel('catalog/category')->getCollection()
		->addAttributeToFilter('entity_id', array('in' => $categoryChildren))
		->addAttributeToSelect('entity_id');
		$subCatIds = $collection->setOrder('position', 'asc');
		
		foreach ($subCatIds as $categoryChild)
		{			
			$categoryChildId = $categoryChild['entity_id'];			
			$_childCategory = Mage::getModel('catalog/category')->setStoreId($storeId)->load($categoryChildId);
			if ($_childCategory->getIsActive())
			{
				$o .= '<li>';
				$o .= '<a href="'.$baseURL.'catalog/category/view/id/'.$categoryChildId.'?___store='.$storeId.'">'.$_childCategory->getName().'</a>';
				if ($maxSubLevel > 0)
				{
					$catChildren2 = $_childCategory->getChildren();
					if(!empty($catChildren2))
					{						
						$o .= $this->getChildLevels($baseURL, $storeId, $catChildren2, $maxSubLevel, false);
					}
				}
				$o .= '</li>';
			}
		}
		$o .= '</ul>';
		return $o;
	}
}

function cmp($a, $b)
{
	if ($a[0] == $b[0]) {
		return 0;
	}
	return ($a[0] < $b[0]) ? -1 : 1;
}
