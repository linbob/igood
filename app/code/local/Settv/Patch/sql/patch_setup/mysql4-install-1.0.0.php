<?php

$installer = $this;
$installer->startSetup();

// modify default package / theme settings
$configData = Mage::getModel("core/config");
//$configData->saveConfig('design/package/name', "settvdefault", 'default', 0);
//$configData->saveConfig('design/theme/default', "settv_default", 'default', 0);

// remove phone themes
$configData->saveConfig('design/theme/template_ua_regexp', "", 'default', 0);
$configData->saveConfig('design/theme/skin_ua_regexp', "", 'default', 0);
$configData->saveConfig('design/theme/layout_ua_regexp', "", 'default', 0);
$configData->saveConfig('design/theme/default_ua_regexp', "", 'default', 0);

// todo: need to modify cms pages

$installer->endSetup();