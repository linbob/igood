﻿<?php

$installer = $this;
$installer->startSetup();

$model=Mage::getModel('eav/entity_setup','core_setup');

$attribute_code = "supplier";
$attributeID = $model->getAttribute("catalog_product",$attribute_code);

if(!$attributeID) {
	$data=array(
			'type'=>'int',
			'input'=>'select',
			'label'=>'供應商',
			'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
			'required' => true
	);

	$model->addAttribute('catalog_product',$attribute_code,$data);
}

$attribute_code = "supplier_percentage";
$attributeID = $model->getAttribute("catalog_product",$attribute_code);

if(!$attributeID) {
	$data=array(
			'type'=>'int',
			'input'=>'text',
			'label'=>'供應商商品成數百分比 (0-100)',
			'frontend_class'=> 'validate-percents',
			'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
			'required' => true,
			'default'=>'25'
	);

	$model->addAttribute('catalog_product',$attribute_code,$data);
}

$attribute_code = "product_cost";
$attributeID = $model->getAttribute("catalog_product",$attribute_code);

if(!$attributeID) {
	$data=array(
			'type'=>'int',
			'input'=>'text',
			'label'=>'成本',
			'frontend_class'=> 'validate-digits',
			'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
			'required' => false
	);

	$model->addAttribute('catalog_product',$attribute_code,$data);
}

$installer->endSetup();