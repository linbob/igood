﻿<?php

$installer = $this;
$installer->startSetup();

$model=Mage::getModel('eav/entity_setup','core_setup');

$attribute_code = "cashflow";
$attributeID = $model->getAttribute("catalog_product",$attribute_code);

if(!$attributeID) {
	$data=array(
			'type'=>'varchar',
			'backend'=>'eav/entity_attribute_backend_array',
			'input'=>'multiselect',
			'label'=>'金流選項',
			'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
			'source'=>'cashflow/product_attribute_source_cashflow',
			'required'=>true,
	);

	$model->addAttribute('catalog_product',$attribute_code,$data);
}

$configSwitch = new Mage_Core_Model_Config();
$configSwitch ->saveConfig('payment/free/active', "0", 'default', 0);
$configSwitch ->saveConfig('payment/paypal_billing_agreement/active', "0", 'default', 0);

$installer->endSetup();