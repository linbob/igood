<?php

// place holder for CMS block creation

$installer = $this;
$installer->startSetup();

$blockModel = Mage::getModel("cms/block");

$blockIDTxt = "test-block-identifier";
$blockTitle = "Test Title";
$blockContent = "<div>Test Content</div>";
$blockIsActive = 1;

$blockID = $blockModel->load($blockIDTxt,"identifier")->getId();

if ($blockID) {
	$blockModel = $blockModel->load($blockID);
	$blockModel->setTitle($blockTitle);
	$blockModel->setContent($blockContent);
	$blockModel->setIsActive($blockIsActive);
	
	/* TODO: Jeff, store ID does not SET!!! BUG!?!? Workaround in else below. */
	$blockModel->setStoreId(array(0)); /* 0 for global */
	$blockModel->setIdentifier($blockIDTxt);

	$blockModel->save();
} else {
	$cmsBlock = array(
	'title'         => $blockTitle,
	'identifier'    => $blockIDTxt,
	'content'       => $blockContent,
	'is_active'     => $blockIsActive,
	'stores'        => 0
	);
	
	$blockModel->setData($cmsBlock)->save();
}

$installer->endSetup();