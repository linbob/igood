<?php

$installer = $this;
$installer->startSetup();

$configData = Mage::getModel("core/config");

// change pagination frame skip to 5 for pager to display jump navigation
$configData->saveConfig('design/pagination/pagination_frame_skip', "5", 'default', 0);

// todo: need to modify cms pages

$installer->endSetup();