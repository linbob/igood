<?php

// place holder for CMS block creation

$installer = $this;
$installer->startSetup();

$blockModel = Mage::getModel("cms/block");

$blockIDTxt = "footer";
$blockTitle = "footer";
$blockContent = "
<div class=\"footer\">
  <div class=\"footerBar\">
  	<div class=\"sol\"><a href=\"http://www.facebook.com/igoodtw\"><img src=\"{{skin url='settvimgs/btn/fb.png'}}\" width=\"23\" height=\"23\"></a></div>
    <ul class=\"footer-Nav\">
    <li><a href=\"{{store url='shopping-info'}}#to1\">關於愛買客</a></li>
    <li><a href=\"{{store url='shopping-info'}}#to4\">相關條款</a></li>
    <li><a href=\"{{store url='contacts'}}\">聯絡我們</a></li></ul>
    </div>
    <div class=\"pDiv\">愛買客股份有限公司&copy; 2013 版權所有 盜用必究 台北市內湖區舊宗路一段159號 02-8792-7357(客服時間：10:00~19:00) 客服信箱：<a href=\"mailto:service@igood.tw\">service@igood.tw</a></div>
</div>";

$blockIsActive = 1;

$blockID = $blockModel->load($blockIDTxt,"identifier")->getId();

if ($blockID) {
	$blockModel = $blockModel->load($blockID);
	$blockModel->setTitle($blockTitle);
	$blockModel->setContent($blockContent);
	$blockModel->setIsActive($blockIsActive);
	
	/* TODO: Jeff, store ID does not SET!!! BUG!?!? Workaround in else below. */
	$blockModel->setStoreId(array(0)); /* 0 for global */
	$blockModel->setIdentifier($blockIDTxt);

	$blockModel->save();
} else {
	$cmsBlock = array(
	'title'         => $blockTitle,
	'identifier'    => $blockIDTxt,
	'content'       => $blockContent,
	'is_active'     => $blockIsActive,
	'stores'        => 0
	);
	
	$blockModel->setData($cmsBlock)->save();
}

$installer->endSetup();