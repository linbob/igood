<?php

// place holder for CMS block creation

$installer = $this;
$installer->startSetup();

$blockModel = Mage::getModel("cms/block");

$blockIDTxt = "category_top_item_41";
$blockTitle = "category_top_item_41";
$blockContent = "
<a href=\"#\"><img src=\"{{skin url='settvimgs/default/716x403.jpg'}}\" /></a>
<a href=\"#\"><img src=\"{{skin url='settvimgs/default/716x403.jpg'}}\" /></a>
<a href=\"#\"><img src=\"{{skin url='settvimgs/default/716x403.jpg'}}\" /></a>";

$blockIsActive = 1;

$blockID = $blockModel->load($blockIDTxt,"identifier")->getId();

if ($blockID) {
	$blockModel = $blockModel->load($blockID);
	$blockModel->setTitle($blockTitle);
	$blockModel->setContent($blockContent);
	$blockModel->setIsActive($blockIsActive);
	
	/* TODO: Jeff, store ID does not SET!!! BUG!?!? Workaround in else below. */
	$blockModel->setStoreId(array(0)); /* 0 for global */
	$blockModel->setIdentifier($blockIDTxt);

	$blockModel->save();
} else {
	$cmsBlock = array(
	'title'         => $blockTitle,
	'identifier'    => $blockIDTxt,
	'content'       => $blockContent,
	'is_active'     => $blockIsActive,
	'stores'        => 0
	);
	
	$blockModel->setData($cmsBlock)->save();
}

$installer->endSetup();