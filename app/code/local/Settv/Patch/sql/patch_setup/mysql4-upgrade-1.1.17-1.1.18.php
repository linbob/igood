<?php

// place holder for CMS block creation

$installer = $this;
$installer->startSetup();

$blockModel = Mage::getModel("cms/block");

$blockIDTxt = "product_notice";
$blockTitle = "product_notice";
$blockContent = "
<ul>
                <li><a href=\"{{store url='shopping-info'}}#to4\">
                    <h6 class=\"t1\">
                    </h6>
                    <p>
                        每日01:00 ~ 24:00之訂單，於隔日出貨，預計於後天送達。<br>
                        星期日與公告不出貨的日期將暫不提供出貨服務。<br>
                        <span>更多詳情請點此聯結..</span></p>
                </a></li>
                <li><a href=\"{{store url='shopping-info'}}#to4\">
                    <h6 class=\"t2\">
                    </h6>
                    <p>
                        送貨範圍為全台灣地區。<br>
                        收件地址請勿為郵政信箱，將無法送達。<br>
                        離島(外島)地區（暫不提供）。<br>
                        <span>更多詳情請點此聯結..</span></p>
                </a></li>
                <li><a href=\"{{store url='shopping-info'}}#to5\">
                    <h6 class=\"t3\">
                    </h6>
                    <p>
                        七天猶豫期：於愛買客下單購買產品，可依消費者保護法規定享有商品貨到日起七天猶豫期之權益。<br>
                        <span>更多詳情請點此聯結..</span></p>
                </a></li>
                <li class=\"fix\"><a href=\"{{store url='shopping-info'}}#to6\">
                    <h6 class=\"t4\">
                    </h6>
                    <p>
                        iGood保有最終解釋之權利。<br>
                        若有未盡事宜，iGood將另行修正並公布。<br>
                        <span>更多詳情請點此聯結..</span></p>
                </a></li>
            </ul>";

$blockIsActive = 1;

$blockID = $blockModel->load($blockIDTxt,"identifier")->getId();

if ($blockID) {
	$blockModel = $blockModel->load($blockID);
	$blockModel->setTitle($blockTitle);
	$blockModel->setContent($blockContent);
	$blockModel->setIsActive($blockIsActive);
	
	/* TODO: Jeff, store ID does not SET!!! BUG!?!? Workaround in else below. */
	$blockModel->setStoreId(array(0)); /* 0 for global */
	$blockModel->setIdentifier($blockIDTxt);

	$blockModel->save();
} else {
	$cmsBlock = array(
	'title'         => $blockTitle,
	'identifier'    => $blockIDTxt,
	'content'       => $blockContent,
	'is_active'     => $blockIsActive,
	'stores'        => 0
	);
	
	$blockModel->setData($cmsBlock)->save();
}

$installer->endSetup();