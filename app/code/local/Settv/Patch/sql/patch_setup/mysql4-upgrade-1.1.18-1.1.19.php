<?php

// replace bar-end.png as this is causing email buggy display

$installer = $this;
$installer->startSetup();

$strToReplace = "style=\"background-image: url({{skin url=\"imgs/email/bar-end.png\" _area='frontend'}}); background-repeat: no-repeat; background-position: right;\"";
$strToReplace2 = "style=\"background-image: url({{skin url=\"imgs/email/bar-end.png\"}}); background-repeat: no-repeat; background-position: right;\"";

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('Account_New_Comfirmation');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('Account_Password_Reset');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('Welcome_to_New_Account');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('CreditCard_Faild');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('Assign logistics by Order Manager');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace2, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('New Order to Supplier');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace2, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('Return Order Deny');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace2, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('Return Order Allow');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace2, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('Supplier Return Order');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace2, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('Supplier Cancel Order');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace2, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$emailTemplate  = Mage::getModel('core/email_template')->loadByCode('新訂單 產生通知');

if($emailTemplate->getTemplateText()) {
	$templateContent = str_replace($strToReplace2, "", $emailTemplate->getTemplateText());
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}

$installer->endSetup();