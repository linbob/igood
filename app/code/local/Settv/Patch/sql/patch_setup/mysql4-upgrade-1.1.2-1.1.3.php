<?php

// modify 404 page

$installer = $this;
$installer->startSetup();

$pageModel = Mage::getModel("cms/page");

$pageIDTxt = "no-route";
$title = "404 Not Found";

$contentHeading = "";
$content = "
<div class=\"error404\">
	<img src=\"{{skin url=settvimgs/404.png}}\" width=\"966\" height=\"420\">
</div>
		";

$rootTemplate = "settv_columns";
$layoutUpdateXml = "
<reference name=\"head\">
	<action method=\"addCss\"><stylesheet>settvcss/error.css</stylesheet></action>
</reference>
		";

$pageID = $pageModel->checkIdentifier($pageIDTxt);

if ($pageID) {
	$pageModel = $pageModel->load($pageID);
} else {
	$pageModel->setIdentifier($pageIDTxt);
}

$pageModel->setTitle($title);
$pageModel->setContentHeading($contentHeading);
$pageModel->setContent($content);
$pageModel->setRootTemplate($rootTemplate);
$pageModel->setLayoutUpdateXml($layoutUpdateXml);
$pageModel->setStoreId(array(0)); /* 0 for global */

/* no need to set custom theme if there if page is not tied to specific store theme (system->config->design)*/
$pageModel->setCustomTheme(null);
//$pageModel->setCustomRootTemplate("one_column");

$pageModel->save();

$installer->endSetup();