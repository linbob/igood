<?php

// create faq (qna) page

$installer = $this;
$installer->startSetup();

$pageModel = Mage::getModel("cms/page");

$pageIDTxt = "home";
$title = "愛買客";


$alert_bar_name = "home_sticker";
$alert_bar = $blockModel->load($alert_bar_name,"identifier")->getId();

$home_item_name = "home_item";
$home_item = $blockModel->load($home_item_name,"identifier")->getId();

$home_top_item_1_name = "home_top_item_1";
$home_top_item_1 = $blockModel->load($home_top_item_1_name,"identifier")->getId();

$home_top_item_2_name = "home_top_item_2";
$home_top_item_2 = $blockModel->load($home_top_item_2_name,"identifier")->getId();

$home_top_item_3_name = "home_top_item_3";
$home_top_item_3 = $blockModel->load($home_top_item_3_name,"identifier")->getId();

$home_top_item_4_name = "home_top_item_4";
$home_top_item_4 = $blockModel->load($home_top_item_4_name,"identifier")->getId();

$home_top_item_5_name = "home_top_item_5";
$home_top_item_5 = $blockModel->load($home_top_item_5_name,"identifier")->getId();

$home_event_name = "home_event";
$home_event = $blockModel->load($home_event_name,"identifier")->getId();

$contentHeading = "";
$content = "
		<div class=\"alertBar\">
             <div class=\"btn\"><a href=\"#\"></a></div>
             <div class=\" message v-mid\">";

if ($alert_bar) {
	$content  .= "{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"".$alert_bar_name."\"}}";
}		

$content  .= "</div>
		</div>

		<div class=\"adBlockContainer\">
		<div id=\"adblock\" class=\"adblock\">
		<div class=\"adTemplate\">
		";

if ($home_top_item_1) {
	$content .= "{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"".$home_top_item_1_name."\"}}";
}
if ($home_top_item_2) {
	$content .= "{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"".$home_top_item_2_name."\"}}";
}
if ($home_top_item_3) {
	$content .= "{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"".$home_top_item_3_name."\"}}";
}
if ($home_top_item_4) {
	$content .= "{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"".$home_top_item_4_name."\"}}";
}
if ($home_top_item_5) {
	$content .= "{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"".$home_top_item_5_name."\"}}";
}


$content .= "
		</div>
		<ul class=\"showbox\"></ul>
		<ul class=\"link\"></ul>
		</div>
		</div>

		<div class=\"imgAryContainer\">
		<div class=\"imgTemplate\">
		";

if ($home_item) {
	$content .= "{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"".$home_item_name."\"}}";
}

$content .= "
		</div>
		<div id=\"imgArray\" class=\"imgAry masonry\" style=\"position: relative;\">
		</div>
		</div>";

if ($home_event) {
	$content .= "{{widget type=\"cms/widget_block\" template=\"cms/widget/static_block/default.phtml\" block_id=\"".$home_event_name."\"}}";
}

$rootTemplate = "settv_columns";


$layoutUpdateXml = "
<reference name=\"head\">        
    <action method=\"addItem\"><type>skin_js</type><name>settvjs/jqueryui/jquery.ui.core.min.js</name></action>
	<action method=\"addItem\"><type>skin_js</type><name>settvjs/jqueryui/jquery.ui.widget.min.js</name></action>
	<action method=\"addItem\"><type>skin_js</type><name>settvjs/jqueryui/jquery.ui.position.min.js</name></action>
	<action method=\"addItem\"><type>skin_js</type><name>settvjs/jqueryui/jquery.ui.button.min.js</name></action>
	<action method=\"addItem\"><type>skin_js</type><name>settvjs/jqueryui/jquery.ui.dialog.min.js</name></action>	
	<action method=\"addItem\"><type>skin_js</type><name>settvjs/home.js</name></action>
    <action method=\"addCss\"><stylesheet>settvcss/jqueryui/jquery-ui.css</stylesheet></action>  
    <action method=\"addCss\"><stylesheet>settvcss/home.css</stylesheet></action>  
</reference>";

$pageID = $pageModel->checkIdentifier($pageIDTxt);

if ($pageID) {
	$pageModel = $pageModel->load($pageID);
} else {
	$pageModel->setIdentifier($pageIDTxt);
}

$pageModel->setTitle($title);
$pageModel->setContentHeading($contentHeading);
$pageModel->setContent($content);
$pageModel->setRootTemplate($rootTemplate);
$pageModel->setLayoutUpdateXml($layoutUpdateXml);
$pageModel->setStoreId(array(0)); /* 0 for global */

/* no need to set custom theme if there if page is not tied to specific store theme (system->config->design)*/
$pageModel->setCustomTheme(null);
//$pageModel->setCustomRootTemplate("one_column");

$pageModel->save();

$installer->endSetup();