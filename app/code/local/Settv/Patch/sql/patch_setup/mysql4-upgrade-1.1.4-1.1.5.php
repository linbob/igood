<?php

// remove member_img_banner block

$installer = $this;
$installer->startSetup();

$blockModel = Mage::getModel("cms/block");

$blockIDTxt = "member_img_banner";

$blockID = $blockModel->load($blockIDTxt,"identifier")->getId();
$blockModel->delete();

$installer->endSetup();