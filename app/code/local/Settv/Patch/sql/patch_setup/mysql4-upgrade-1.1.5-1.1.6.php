<?php

// remove privacy-term page

$installer = $this;
$installer->startSetup();

$pageModel = Mage::getModel("cms/page");

$pageIDTxt = "privacy-term";
$pageID = $pageModel->checkIdentifier($pageIDTxt);
$pageModel = $pageModel->load($pageID);
$pageModel->delete();

$installer->endSetup();