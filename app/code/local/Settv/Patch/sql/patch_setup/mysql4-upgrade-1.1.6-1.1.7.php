<?php

// create faq (qna) page

$installer = $this;
$installer->startSetup();

$pageModel = Mage::getModel("cms/page");

$pageIDTxt = "qna";
$title = "Q&A";

$contentHeading = "";
$content = "
<div id=\"bodyDiv\">
	<a class=\"hiddenLink\" name=\"q1\"></a>
	<div class=\"productDiv\"> 
	  <!-- 左欄資訊功能----->
	  <div class=\"leftCol floatL\">
		<div id=\"faq-info\">
		  <h6>愛買客FAQ</h6>
		  <ul class=\"faq eff\">
			<li class=\"act\"><a href=\"ink_a\">新手上路</a></li>
			<li><a href=\"#link_b\" >會員功能</a></li>
			<li><a href=\"#link_c\">購物問題</a></li>
			<li><a href=\"#link_d\">訂單問題</a></li>
			<li><a href=\"#link_e\">退換貨問題</a></li>
			<li><a href=\"#link_f\">退款問題</a></li>
		  </ul>
		</div>
	  </div>
	  <!-- 左欄資訊功能 fine-----> 
	  <!-- 右欄資訊----->
	  <div class=\"faq-main-info floatR nor\">
		<div class=\"faq-title\">
		  <div class=\"btntop eff\"><a href=\"#q1\" class=\"to-top\" title=\"To top\" name=\"link_a\">▲</a></div>
		  新手上路 </div>
		<div class=\"faq-intro\">
		  <h5>1.加入會員</h5>
		  <ul>
			<li>(1)	您可至愛買客網站右上方處「加入會員/登入」點選</li>
			<li>(2)	點選「加入會員</li>
			<li>(3)	輸入「會員名稱」、「電子郵件地址」、「密碼」、「確認密碼」</li>
			<li>(4)	請務必勾選已詳讀三立好站會員服務條款後，並選擇送出</li>
			<li>(5)	愛買客將會以系統信方式發送帳號確認信至您註冊時輸入之信箱，請至該信箱開啟信件，並依照信中方式進行帳號確認，點選認證連結後，即可開始購物囉~</li>
		  </ul>
		  <h5>2.購買商品</h5>
		  <ul>
			<li>(1)	您可以透過瀏覽或搜尋方式，找到您所需之商品</li>
			<li>(2)	按下「購買」按鈕</li>
			<li>(3)	進入確認訂單介面，您可以輸入您欲購買之數量，並按「繼續結帳」</li>
			<li>(4)	填寫訂購資訊，並選擇發票聯式，並按「繼續結帳」(建議您，收件人姓名請使用中文名稱，以便商品配送順利)</li>
			<li>(5)	選擇付款方式，並按「繼續」</li>
			<li>(6)	訂購完成，即會收到訂單確認郵件</li>
		  </ul>
		</div>
		<div class=\"faq-title\">
		  <div class=\"btntop eff\"><a href=\"#q1\" class=\"to-top\" title=\"To top\" name=\"link_b\">▲</a></div>
		  會員功能 </div>
		<div class=\"faq-intro\">
		  <h5>1.通訊錄</h5>
		  <p>您可於通訊錄之中，增加新的寄件資訊，方便您日後購買商品時，無需再次輸入寄件地址，可直接勾選通訊錄中之寄件資料。</p>
		  <h5>2.我的訂單</h5>
		  <p>您可於「我的訂單」內查詢到您購買的歷史紀錄，並可查詢訂單是否出貨、出貨、退訂等資訊，以及物流配送編號，方便您了解您的商品配送狀況。</p>
		  <h5>3.退訂</h5>
		  <p>您可於「退訂」功能，選擇您尚未出貨之訂單，選擇退訂。即可取消訂單。</p>
		  <h5>4.退貨</h5>
		  <p>您收到商品後七天猶豫期內，可於退貨選單選取該商品進行退貨。愛買客將會派宅配至您的收件處取回退貨，您無須負擔任何退貨運費。
			提醒您，退貨商品必須保持完整狀態，包含主要商品、贈品、原來的完整外盒、保證書。</p>
		  <h5>5.最愛商品</h5>
		  <p>您可於商品頁面中點選「加入最愛商品」，將該商品加入最愛商品列中，方便您選擇或紀錄。<br>
			儲存在「最愛商品」中商品亦可在該頁面直接選擇購買或刪除。</p>
		</div>
		<div class=\"faq-title\">
		  <div class=\"btntop eff\"><a href=\"#q1\" class=\"to-top\" title=\"To top\" name=\"link_c\">▲</a></div>
		  購物問題 </div>
		<div class=\"faq-intro\">
		  <h5>1.付款方式</h5>
		  <h3>愛買客目前提供以下兩種付款方式</h3>
		  <ul>
			<li>(A)	線上刷卡</li>
			<li>(B)	貨到付款
			  <ol>
				<li>貨到付款-現金：您可於宅配配送商品予您時，將現金交付予宅配人員</li>
				<li>貨到付款-刷卡：您可於宅配配送商品予您時，現在刷卡付款</li>
			  </ol>
			</li>
		  </ul>
		  <h5>2.搜尋商品</h5>
		  <p>您可於網站右上方「搜尋」功能中，輸入您欲找尋之商品名稱，或輸入節目名稱等關鍵字，搜尋愛買客上的商品。</p>
		  <h5>3.訂單確認</h5>
		  <p>您可至「會員功能」→「我的訂單」內查詢您的訂單資料，若已訂購成功，訂單明細即可該筆訂單紀錄。<br>
			若您訂購流程正確仍查不到訂單資料，煩請您與「客服中心」聯絡。</p>
		  <h5>4.配送範圍</h5>
		  <p>愛買客僅提供台灣本島之寄送服務，離島及海外訂購者，請您提供台灣本島親朋好友地址為代收地址。</p>
		  <h5>5.取貨問題</h5>
		  <p>愛買客為網路購物平台，目前無實體店面提供您現場購買或親自取貨服務。</p>
		</div>
		<div class=\"faq-title\">
		  <div class=\"btntop eff\"><a href=\"#q1\" class=\"to-top\" title=\"To top\" name=\"link_d\">▲</a></div>
		  訂單問題 </div>
		<div class=\"faq-intro\">
		  <h5>1.查詢訂單</h5>
		  <p>您可至「會員功能」→「我的訂單」內查詢您的訂單資料，若已訂購成功，訂單明細即可該筆訂單紀錄。</p>
		  <h5>2.訂單退訂</h5>
		  <p>您可於商品出貨前，取消訂單。請您至「會員功能」→「退訂」功能內選擇您需退訂之訂單。</p>
		  <h5>3.更改訂購商品或數量問題</h5>
		  <p>為保障您購物資料的安全性，客服人員無權限異動會員的訂單資料，訂單成立後即無法為您更改數量及商品，煩請您選擇退訂訂單重新訂購，造成您的不便請見諒，謝謝！ </p>
		  <h5>4.單一訂單多筆商品</h5>
		  <p>愛買客商品由眾多優質廠商供應，當您同筆訂單訂購多樣商品時有可能為不同廠商所寄送，無法統一時間收到，不便之處請您見諒。 </p>
		  <h5>5.更改收件資料</h5>
		  <p>訂單成立後，若您需更改訂單資料，您可來信至客服信箱service@igood.tw，或來電客服專線：02-8792-7357。為確保您的購物資料安全性，來信請您提供訂單編號、帳號、原收件資料(收件人/收件處)，及欲更改之資料(收件人/收件處)，以核對您帳務資訊。 </p>
		  <h5>6.指定收貨日</h5>
		  <p>很抱歉，商品配送由宅配公司託寄，因宅配需安排路等因素，無法指定到貨時間，一般商品將於訂單成立後七個工作天內送達，預購商品或特殊大型家電等，會於商品頁上標註之出貨時間陸續寄出。 </p>
		</div>
		<div class=\"faq-title\">
		  <div class=\"btntop eff\"><a href=\"#q1\" class=\"to-top\" title=\"To top\" name=\"link_e\">▲</a></div>
		  退換貨問題 </div>
		<div class=\"faq-intro\">
		  <h5>1.換貨問題</h5>
		  <p>收到商品後於猶豫期七天內(含例假日)，發現商有有瑕疵或缺件，為減少您等候相關流程時間，愛買客目前無提供商品換貨服務，煩請您直接申請退貨，將會由宅配人員收取退件，您無須負擔任何退貨運費。再請您重新訂購該商品。 </p>
		  <h5>2.退貨問題</h5>
		  <p>愛買客提供猶豫期內退貨服務，若您收到商品後，於猶豫期內七天內(含例假日)不滿意或其他原因，可至「會員功能」→「退貨」功能內點選退貨。將會由宅配人員收取退件，您無須負擔任何退貨運費。</p>
		  <h5>3.退貨注意事項</h5>
		  <ol>
			<li> 愛買客並不提供產品之新品試用，因此一旦開始使用產品，即會影響您退貨後可全額退費之權益（新品不良除外）。因此若您不確定是否要購買，請在七天猶豫期內審慎思考及檢視產品：勿破壞新品包裝、相機請勿試用試拍、完整一體包裝產品如 iPod/iPad等請勿拆封、所有配件請保持完好包裝勿拆封、保證卡請勿填寫或上網註冊、軟體請勿開通序號使用等等。產品外包裝視同產品本身，請勿損毀。 </li>
			<li>您於七天猶豫期內退回之貨品，客服人員將檢視退回之產品是否符合前述條件、維持在新品可銷售之狀態。確認無誤後，將會儘速處理你您的退款事宜，您可獲得全額退費。若您在猶豫期內已確定購買，因此拆封並開始使用後，始發現該貨品為新品瑕疵不良。不論您是否已拆封或已開始使用，於七日內皆可無條件退換貨，且不影響您退貨後可全額退費之權益。退貨運費並將由愛買客負擔 </li>
			<li>若您於七天猶豫期內已拆封開箱並開始使用產品（如相機已開始拍照、電腦已開機使用、一體包裝產品已開封使用等等），稍後於產品本身並無瑕疵情況下想要退貨，則由於該產品一經使用後已非新品，無法以新品方式銷售，因此我們將向您收取該產品購買時售價 7% 之金額，做為折價銷售及整貨理貨等恢復原狀之部分費用。此費用將由您原應退費金額中扣取。該物品並將以非新品方式另行折價銷售。請您購買與開始使用產品前務必瞭解此一條件。 </li>
			<li>若您欲退貨的產品，除已開封並使用外，並有人為因素所造成之損傷，包括但不限於刮傷、摔傷、配件損壞、包裝嚴重損傷等，則除以上之7% 費用外，尚需包括為修復/恢復產品原狀所需之其他費用。詳細費用金額將另行報價處理。 </li>
		  </ol>
		</div>
		<div class=\"faq-title\">
		  <div class=\"btntop eff\"><a href=\"#q1\" class=\"to-top\" title=\"To top\" name=\"link_f\">▲</a></div>
		  退款問題 </div>
		<div class=\"faq-intro\">
		  <h5>1.信用卡退款：</h5>
		  <p>愛買客收到您的退貨檢測無誤後完成退貨後進行退款，將於14個工作天內完成退款至您的信用卡帳戶。</p>
		  <h5>2.貨到付款-刷卡：</h5>
		  <p>愛買客收到您的退貨檢測無誤後完成退貨後進行退款，將於21個工作天內完成退款至您的信用卡帳戶。</p>
		  <h5>3.貨到付款-現金：</h5>
		  <p>愛買客收到您的退貨檢測無誤後完成退貨後進行退款，目前僅提供匯款轉入之方式，請您提供相關資訊，傳真至：或EMAIL至客服信箱service@igood.tw，僅限提供訂單購買人或收件人退款帳號。將於收到資訊後21個工作天內退款至您提供的帳戶中。</p>
		  <ol>
			<li>銀行 (如台北富邦)： </li>
			<li>銀行分行(如松山分行)： </li>
			<li>帳 號： </li>
			<li>戶 名： </li>
		  </ol>
		</div>
	  </div>
	  <!-- 右欄資訊 fine----->
	  <div class=\"clearDiv space\"></div>
	</div>
</div>
		";

$rootTemplate = "settv_columns";
$layoutUpdateXml = "
<reference name=\"head\">
	<action method=\"addCss\"><stylesheet>settvcss/faq.css</stylesheet></action>
</reference>	
		";

$pageID = $pageModel->checkIdentifier($pageIDTxt);

if ($pageID) {
	$pageModel = $pageModel->load($pageID);
} else {
	$pageModel->setIdentifier($pageIDTxt);
}

$pageModel->setTitle($title);
$pageModel->setContentHeading($contentHeading);
$pageModel->setContent($content);
$pageModel->setRootTemplate($rootTemplate);
$pageModel->setLayoutUpdateXml($layoutUpdateXml);
$pageModel->setStoreId(array(0)); /* 0 for global */

/* no need to set custom theme if there if page is not tied to specific store theme (system->config->design)*/
$pageModel->setCustomTheme(null);
//$pageModel->setCustomRootTemplate("one_column");

$pageModel->save();

$installer->endSetup();