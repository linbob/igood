<?php

// place holder for CMS block creation

$installer = $this;
$installer->startSetup();

$blockModel = Mage::getModel("cms/block");

$blockIDTxt = "home_item";
$blockTitle = "首頁商品區";
$blockContent = "
<a href=\"#\" title=\"Kendra by Dave DeSandro, on Flickr\">
<img src=\"skin/frontend/settvdefault/settv_default/settvimgs/default/214x215.jpg\" alt=\"Kendra\" class=\"img-large\">
</a>
<a href=\"#/\" title=\"Stanley by Dave DeSandro, on Flickr\">
<img src=\"skin/frontend/settvdefault/settv_default/settvimgs/default/214x215.jpg\" alt=\"Stanley\" class=\"img-small\">
</a>
<a href=\"#/\" title=\"Wonder by Dave DeSandro, on Flickr\">
<img src=\"skin/frontend/settvdefault/settv_default/settvimgs/default/716x403.jpg\" alt=\"Wonder\" class=\"img-small\">
</a>
<a href=\"#/\" title=\"Giraffe by Dave DeSandro, on Flickr\">
<img src=\"skin/frontend/settvdefault/settv_default/settvimgs/default/214x215.jpg\" alt=\"Giraffe\" class=\"img-large\">
</a>
<a href=\"#/\" title=\"Officer by Dave DeSandro, on Flickr\">
<img src=\"skin/frontend/settvdefault/settv_default/settvimgs/default/214x215.jpg\" alt=\"Officer\" class=\"img-small\">
</a>
<a href=\"#/\" title=\"Gavin by Dave DeSandro, on Flickr\">
<img src=\"skin/frontend/settvdefault/settv_default/settvimgs/default/716x403.jpg\" alt=\"Gavin\" class=\"img-small\">
</a>
<a href=\"#/\" title=\"Anita by Dave DeSandro, on Flickr\">
<img src=\"skin/frontend/settvdefault/settv_default/settvimgs/default/476x215.jpg\" alt=\"Anita\" class=\"img-medium\">
</a>
<a href=\"#/\" title=\"Take My Portrait by Dave DeSandro, on Flickr\">
<img src=\"skin/frontend/settvdefault/settv_default/settvimgs/default/214x215.jpg\" alt=\"Take My Portrait\" class=\"img-small\">
</a>
<a href=\"#/\" title=\"Tony by Dave DeSandro, on Flickr\">
<img src=\"skin/frontend/settvdefault/settv_default/settvimgs/default/214x215.jpg\" class=\"img-small\">
</a>";

$blockIsActive = 1;

$blockID = $blockModel->load($blockIDTxt,"identifier")->getId();

if ($blockID) {
	$blockModel = $blockModel->load($blockID);
	$blockModel->setTitle($blockTitle);
	$blockModel->setContent($blockContent);
	$blockModel->setIsActive($blockIsActive);
	
	/* TODO: Jeff, store ID does not SET!!! BUG!?!? Workaround in else below. */
	$blockModel->setStoreId(Mage::app()->getStore()->getId()); /* 0 for global */
	$blockModel->setIdentifier($blockIDTxt);

	$blockModel->save();
} else {
	$cmsBlock = array(
	'title'         => $blockTitle,
	'identifier'    => $blockIDTxt,
	'content'       => $blockContent,
	'is_active'     => $blockIsActive,
	'stores'        => 0
	);
	
	$blockModel->setData($cmsBlock)->save();
}

$installer->endSetup();