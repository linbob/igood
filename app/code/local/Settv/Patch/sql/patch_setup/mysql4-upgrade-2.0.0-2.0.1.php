<?php

$installer = $this;
$installer->startSetup();

$roles = Mage::getModel("admin/roles")->getCollection();

foreach($roles as $role):
if ($role->getRoleName() == "Supplier"){
	$resources = explode(',','__root__,admin/dashboard,admin/system,admin/system/myaccount,admin/supplier_report_menu,admin/supplier_report_menu/order_list_page,admin/supplier_report_menu/product_list_page,admin/deliver_menu,admin/deliver_menu/deliver_page');

	Mage::getModel('admin/rules')->setRoleId($role->getId())->setResources($resources)->saveRel();
}
endforeach;

$installer->endSetup();