<?php

$installer = $this;
$installer->startSetup();

$configData = Mage::getModel("core/config");

// disable free payment method
$configData->saveConfig('payment/free/active', "0", 'default', 0);

// disable paypal billing agreement payment method
$configData->saveConfig('payment/paypal_billing_agreement/active', "0", 'default', 0);

$installer->endSetup();