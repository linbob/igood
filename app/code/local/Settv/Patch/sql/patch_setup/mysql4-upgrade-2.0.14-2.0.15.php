<?php

$installer = $this;
$installer->startSetup();

$connection = $this->getConnection();
$connection->addColumn($this->getTable('check/custom_order'), 'familymart_pincode', "varchar(255)");

$installer->endSetup();