<?php

$installer = $this;
$installer->startSetup();

$configData = Mage::getModel("core/config");

// enable AdminNotification module
$configData->saveConfig('advanced/modules_disable_output/Mage_AdminNotification', "0", 'default', 0);

// disable check/money order payment method
$configData->saveConfig('payment/checkmo/active', "0", 'default', 0);

// disable cc-pay payment method
$configData->saveConfig('payment/pay/active', "0", 'default', 0);

$installer->endSetup();