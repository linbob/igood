<?php

$installer = $this;
$installer->startSetup();

$statusCode = "pending_payment";
$status = Mage::getModel('sales/order_status')
->load($statusCode);
 
$status->setLabel("付款處理中");
$status->save();

$installer->endSetup();