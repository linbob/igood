<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'logistics_com');
if ($attribute->usesSource()) {
	$options = $attribute->getSource()->getAllOptions(false);
}

$newLogistics = array('ezcat');
$iProductEntityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();
$aOption = array();
$aOption['attribute_id'] = $installer->getAttributeId($iProductEntityTypeId, 'logistics_com');

for($i=0;$i<sizeof($options);$i++){
	$tmp[$i] = $options[$i]['label'];
}

for($iCount=0;$iCount<sizeof($newLogistics);$iCount++){
	if (!in_array($newLogistics[$iCount], $tmp)){
		$aOption['value']['option'.$iCount][0] = $newLogistics[$iCount];
	}
}

if ($aOption['value']){
	$installer->addAttributeOption($aOption);
}

$installer->endSetup();