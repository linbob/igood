<?php

// replace bar-end.png as this is causing email buggy display

$installer = $this;
$installer->startSetup();

$templateCode = 'Admin Order Comment';
$templateSubject = '廠商異常回報通知';
$emailTemplate  = Mage::getModel('core/email_template')->loadByCode($templateCode);

$templateContent = "
訂單編號： #{{var order.increment_id}} 有異常回報.<br/>
回報內容: {{var comment}}
";

if($emailTemplate->getTemplateText()) {
	// do nothing
} else {
	$emailTemplate = Mage::getModel('core/email_template');
	$emailTemplate->setTemplateCode($templateCode);
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->setTemplateSubject($templateSubject);
	$emailTemplate->setTemplateType(2);
	$emailTemplate->save();
}


$installer->endSetup();