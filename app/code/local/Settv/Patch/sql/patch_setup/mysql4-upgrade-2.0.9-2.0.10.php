<?php

$installer = $this;
$installer->startSetup();

$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product','cashflow');
if ($attributeId) {
	$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
	$attribute->setSourceModel('cashflow/product_attribute_source_cashflow')->save();
}

$installer->endSetup();