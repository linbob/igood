﻿<?php

$installer = $this;
$installer->startSetup();

$model=Mage::getModel('eav/entity_setup','core_setup');

$attribute_code = "family_logistics";
$attributeID = $model->getAttribute("catalog_product",$attribute_code);

if(!$attributeID) {
	$data=array(
			'type'=>'varchar',
			'input'=>'select',
			'label'=>'開放全家取貨',
			'frontend_class'=> '',
			'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
			'source'=>'eav/entity_attribute_source_table',
			'required' => false,
			'option' => array (
					'value' => array('disabled' => array('否'),
							'enabled' => array('是'),
					)
			),
	);

	$model->addAttribute('catalog_product',$attribute_code,$data);
}

$installer->endSetup();