<?php

$installer = $this;
$installer->startSetup();

$strToReplace = "order.created_at";
$strToReplace2 = "order.getTaiwanCreatedAtFormated()";


$arr = array('新訂單 產生通知', 'Supplier Cancel Order', 'Supplier Return Order', 'Return Order Allow', 'Return Order Deny' ,'New Order to Supplier', 'Assign logistics by Order Manager', 'Return Of Goods');

foreach ($arr as $value) {
	$emailTemplate  = Mage::getModel('core/email_template')->loadByCode($value);
	
	if($emailTemplate->getTemplateText()) {
		$templateContent = str_replace($strToReplace, $strToReplace2, $emailTemplate->getTemplateText());
		$emailTemplate->setTemplateText($templateContent);
		$emailTemplate->save();
	}
}

$installer->endSetup();