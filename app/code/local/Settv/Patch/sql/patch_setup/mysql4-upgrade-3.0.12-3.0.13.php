<?php

$installer = $this;
$installer->startSetup();

$strToReplace = "三立好站";
$strToReplace2 = "愛買客";

$arr = array('新訂單 產生通知');

foreach ($arr as $value) {
	$emailTemplate  = Mage::getModel('core/email_template')->loadByCode($value);
	
	if($emailTemplate->getTemplateText()) {
		$templateContent = str_replace($strToReplace, $strToReplace2, $emailTemplate->getTemplateText());
		$emailTemplate->setTemplateText($templateContent);
		$emailTemplate->save();
	}
}

$installer->endSetup();