<?php

$installer = $this;
$installer->startSetup();

$existSupplier = false;
$roles = Mage::getModel('admin/roles')->getCollection();
foreach($roles as $role):
	if ($role->getRoleName() == "Supplier"){
		$existSupplier = true;
	}
endforeach;

if (!$existSupplier){
	$role = Mage::getModel("admin/role");
	$role->setTree_level(1);
	$role->setRole_type('G');
	$role->setRole_name("Supplier");
	$role->save();
}

$installer->endSetup();