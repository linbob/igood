<?php

$installer = $this;
$installer->startSetup();

$connection = $this->getConnection();

$connection->addColumn($this->getTable('check/custom_order'), 'invoice_carriertype', "VARCHAR(6)");
$connection->addColumn($this->getTable('check/custom_order'), 'invoice_carriernum1', "VARCHAR(60)");
$connection->addColumn($this->getTable('check/custom_order'), 'invoice_carriernum2', "VARCHAR(60)");
$connection->addColumn($this->getTable('check/custom_order'), 'invoice_donateorg', "text");
$connection->addColumn($this->getTable('check/custom_order'), 'invoice_donateorgnum', "VARCHAR(10)");
$connection->addColumn($this->getTable('check/custom_order'), 'invoice_isdonate', "tinyint(1) NOT NULL DEFAULT '0'");

$installer->endSetup();