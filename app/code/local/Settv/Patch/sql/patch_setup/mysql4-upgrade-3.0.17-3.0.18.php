<?php
	
$installer = $this;
$installer->startSetup();

$connection = $this->getConnection();

$connection->addColumn($this->getTable('check/custom_order'), 'invoice_create_isupload', "tinyint(1) NOT NULL DEFAULT '1'");

$connection->addColumn($this->getTable('check/custom_order'), 'invoice_cancel_isupload', "tinyint(1) NOT NULL DEFAULT '1'");

$installer->endSetup();	
