<?php

$installer = $this;
$installer->startSetup();

$strToReplace = "{{skin url=\'imgs/email/logo.png\' _area=\'frontend\'}}";
$strToReplace2 = "{{skin url=\"imgs/email/logo.png\" _area=\"frontend\" _package=\"settvdefault\" _theme=\"settv_default\"}}";

$emailTemplates  = Mage::getModel('core/email_template')->getCollection();

foreach ($emailTemplates as $emailTemplate) {	
	
	if($emailTemplate->getTemplateText()) {
		$templateContent = str_replace($strToReplace, $strToReplace2, $emailTemplate->getTemplateText());
		$emailTemplate->setTemplateText($templateContent);
		$emailTemplate->save();
	}
}

$installer->endSetup();