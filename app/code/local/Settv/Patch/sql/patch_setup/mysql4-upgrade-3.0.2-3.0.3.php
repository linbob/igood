<?php

$installer = $this;
$installer->startSetup();

$model=Mage::getModel('eav/entity_setup','core_setup');

$attribute_code = "logistics_com";
$attributeID = $model->getAttribute("catalog_product",$attribute_code);

if(!$attributeID) {
	$data=array(
			'type'=>'int',
			'input'=>'select',
			'label'=>'物流選項',
			'source'=> 'cashflow/product_attribute_source_logistics',
			'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
			'required' => true
	);

	$model->addAttribute('catalog_product',$attribute_code,$data);
} else {
	$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product','logistics_com');
	if ($attributeId) {
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
		$attribute->setSourceModel('cashflow/product_attribute_source_logistics')->save();
}
}

$installer->endSetup();