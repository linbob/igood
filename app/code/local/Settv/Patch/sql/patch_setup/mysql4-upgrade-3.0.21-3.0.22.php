<?php

$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE {$this->getTable('settv_sales_logistics_custom')} AUTO_INCREMENT = 2000018000;
");

$installer->endSetup();