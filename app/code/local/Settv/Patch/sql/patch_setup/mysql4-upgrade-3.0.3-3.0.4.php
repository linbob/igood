<?php

$installer = $this;
$installer->startSetup();

$configData = Mage::getModel("core/config");

// disable goolecheckout module
$configData->saveConfig('carriers/googlecheckout/active', "0", 'default', 0);

// disable freeshipping shipping method
$configData->saveConfig('carriers/freeshipping/active', "0", 'default', 0);

$installer->endSetup();