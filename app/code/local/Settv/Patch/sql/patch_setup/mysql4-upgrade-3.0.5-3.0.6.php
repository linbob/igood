<?php

$installer = $this;
$installer->startSetup();

$templateCode = 'Return Of Goods';
$templateSubject = "愛買客-請進行商品取回, 訂單號碼# {{var order_id}}";
$emailTemplate  = Mage::getModel('core/email_template')->loadByCode($templateCode);
$templateContent = '
<body style="margin: 0; padding: 0; background-color: #d8e7ea;" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
<!--100% body table-->

    <!--header text--><!--/header text--> 
      
      <!--content section-->
      
      <table width="620" border="0" align="center" cellpadding="0" cellspacing="0">
        <tbody><tr>
          <td height="82" width="11" valign="middle"><img style="margin: 0; padding: 0; display: block;" src="{{skin url="imgs/email/side-corner.png"}}" width="11" height="93"></td>
          <td height="82" valign="middle" bgcolor="#FFFFFF"><table width="594" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td  valign="middle" height="47" bgcolor="#f83e3e"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                      <td style="background-color:#f83e3e;" bgcolor="#f83e3e" width="25" height="37"></td>
                      <td height="37"><img src="{{skin url="imgs/email/logo.png"}}" width="128" height="40"></td>
                    </tr>
                  </tbody></table></td>
              </tr>
            </tbody></table></td>
        </tr>
        <tr>
          <td rowspan="3" valign="top"></td>
          <td valign="top" bgcolor="#FFFFFF"><h4 style=" font-size:21px ;margin:0; color:#333 ;padding:0 20px  20px  20px">商品取回通知</h4></td>
        </tr>
        <tr>
          <td valign="top" bgcolor="#FFFFFF" style="padding:0 0 10px 0">
          
          <table width="570" border="0" align="center" cellpadding="0" style="border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; -khtml-border-radius: 5px; background:#ecf3f9; color:#666">
              <tbody><tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">訂單編號: <span style="color:#F36">#{{var order.increment_id}}</span></td>
              </tr>
              <tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">訂購日期: <span style="color:#F36">{{var order.created_at}}</span></td>
              </tr>
            </tbody></table>
            </td>
        </tr>
        <tr>
          <td valign="top" bgcolor="#FFFFFF" style="padding:0 0 10px 0"><table width="570" border="0" align="center" cellpadding="0" style="border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; -khtml-border-radius: 5px; background:#ecf3f9; color:#666">	 
              <tbody><tr>
                <td style="padding:5px; line-height:2em;font-size:15px ">商品資訊：
                <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#CCCCCC" style="font-size:12px ">
                  <tbody><tr>
                    <th width="170" bgcolor="#FFFFFF">商品名稱</th>
                    <th bgcolor="#FFFFFF">商品號碼</th>
                    <th width="90" bgcolor="#FFFFFF">價格</th>
                    <th width="50" align="center" bgcolor="#FFFFFF">數量</th>
                    <th width="60" align="center" bgcolor="#FFFFFF">小計</th>
                  </tr>
                  <tr>
                    <td align="center" valign="top" bgcolor="#FFFFFF">{{var productname}}</td>
                    <td align="center" valign="top" bgcolor="#FFFFFF">{{var sku}}</td>
                    <td align="center" valign="top" bgcolor="#FFFFFF">{{var unitPrice}}</td>
                    <td align="center" valign="top" bgcolor="#FFFFFF">{{var qty}}</td>
                    <td align="center" valign="top" bgcolor="#FFFFFF">{{var totalPrice}}</td>
                  </tr>
                </tbody>
				</table>
                </td>
              </tr>
            </tbody></table></td>
        </tr>
      </tbody></table>
      
      <!--/content section--> 
      
      <!--footer--><!--footer--> 
      <!--break-->
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td height="25"></td>
        </tr>
      </tbody></table>
      
    <!--/break-->
  

<!--/100% body table-->

</body>
';

if($emailTemplate->getTemplateText()) {
	//donothing
}
else 
{
	$emailTemplate = Mage::getModel('core/email_template');
	$emailTemplate->setTemplateCode($templateCode);
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->setTemplateSubject($templateSubject);
	$emailTemplate->setTemplateType(2);
	$emailTemplate->save();
}

$installer->endSetup();