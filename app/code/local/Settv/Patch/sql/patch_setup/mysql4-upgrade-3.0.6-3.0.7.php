<?php

$installer = $this;
$installer->startSetup();

$connection = $this->getConnection();
$connection->addColumn($this->getTable('check/custom_order'), 'shippingtype', "varchar(255)");
$connection->addColumn($this->getTable('check/custom_order'), 'shippingdescription', "varchar(255)");

$installer->endSetup();