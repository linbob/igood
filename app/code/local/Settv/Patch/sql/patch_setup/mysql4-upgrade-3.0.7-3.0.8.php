<?php

$installer = $this;
$installer->startSetup();

$templateCode = 'Shipping Advice';
$templateSubject = "愛買客-您的訂單{{var order_id}}已出貨";
$emailTemplate  = Mage::getModel('core/email_template')->loadByCode($templateCode);
$templateContent = '
<body style="margin: 0; padding: 0; background-color: #d8e7ea;" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
<!--100% body table-->

    <!--header text--><!--/header text--> 
      
      <!--content section-->
      
      <table width="620" border="0" align="center" cellpadding="0" cellspacing="0">
        <tbody><tr>
          <td height="82" width="11" valign="middle"><img style="margin: 0; padding: 0; display: block;" src="{{skin url="imgs/email/side-corner.png"}}" width="11" height="93"></td>
          <td height="82" valign="middle" bgcolor="#FFFFFF"><table width="594" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td  valign="middle" height="47" bgcolor="#f83e3e"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                      <td style="background-color:#f83e3e;" bgcolor="#f83e3e" width="25" height="37"></td>
                      <td height="37"><img src="{{skin url="imgs/email/logo.png"}}" width="128" height="40"></td>
                    </tr>
                  </tbody></table></td>
              </tr>
            </tbody></table></td>
        </tr>
        <tr>
          <td rowspan="3" valign="top"></td>
          <td valign="top" bgcolor="#FFFFFF"><h4 style=" font-size:21px ;margin:0; color:#333 ;padding:0 20px  20px  20px">訂單出貨通知</h4></td>
        </tr>
        <tr>
          <td valign="top" bgcolor="#FFFFFF" style="padding:0 0 10px 0">
          
          <table width="570" border="0" align="center" cellpadding="0" style="border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; -khtml-border-radius: 5px; background:#ecf3f9; color:#666">
              <tbody><tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">親愛的客戶 　您好</span></td>
              </tr>
			  <tr><td></td></tr>
              <tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">您的商品已經抵達  <span style="color:#F36">{{var fmstorename}}</span> 的全家便利商店嚕！</td>
              </tr>			 
				<tr><td></td></tr>
				<tr><td></td></tr>
			  <tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">取貨時務必注意以下五點</td>
              </tr>
			  <tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">1.請依取件通知簡訊內容前往指定店舖取件。</td>
              </tr>
			  <tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">2.取件人請攜帶身分證件至取件店舖，並告知店員真實姓名，以利取貨作業進行。</td>
              </tr>
			  <tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">3.<span style="color:red">為保障您的權益，取貨作業限本人親自領取，並於簽收單據上親自簽收。</span></td>
              </tr>
			  <tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">4.取貨前請確認貨件資訊，貨件一經領取，即無法接受退回。</td>
              </tr>
			  <tr>
                <td style="padding:10px 5px 5px 5px; font-size:15px">5.退件：貨件送達取件店舖七日內無人領取，<span style="color:red">將退回物流中心後通知寄件人</span>，但於七日內寄件人不得要求提早退回。</td>
              </tr>		
            </tbody></table>
            </td>
        </tr>        
      </tbody></table>
      
      <!--/content section--> 
      
      <!--footer--><!--footer--> 
      <!--break-->
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tbody><tr>
          <td height="25"></td>
        </tr>
      </tbody></table>
      
    <!--/break-->
  

<!--/100% body table-->

</body>
';

if($emailTemplate->getTemplateText()) {
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->save();
}
else 
{
	$emailTemplate = Mage::getModel('core/email_template');
	$emailTemplate->setTemplateCode($templateCode);
	$emailTemplate->setTemplateText($templateContent);
	$emailTemplate->setTemplateSubject($templateSubject);
	$emailTemplate->setTemplateType(2);
	$emailTemplate->save();
}

$installer->endSetup();