<?php

$installer = $this;
$installer->startSetup();

$model=Mage::getModel('eav/entity_setup','core_setup');

$attribute_code = "low_temp_logistics";
$attributeID = $model->getAttribute("catalog_product",$attribute_code);

if(!$attributeID) {
	$data=array(
			'type'=>'int',
			'input'=>'boolean',
			'label'=>'低溫宅配',
			'frontend_class'=> '',
			'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
			'source'=>'eav/entity_attribute_source_boolean',
			'required' => false,
			'default'=>'0'
	);

	$model->addAttribute('catalog_product',$attribute_code,$data);
}

$installer->endSetup();