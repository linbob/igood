<?php
class Settv_Report_Block_Invoice_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('report_invoice_grid');
		// 		$this->setTemplate('widget/report/grid.phtml');
		// 		$this->setUseAjax(true);
		$this->setDefaultSort('logistics_created');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	/**
	 * Retrieve collection class
	 *
	 * @return string
	 */
	protected function _getCollectionClass()
	{
		return 'sales/order_grid_collection';
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getResourceModel($this->_getCollectionClass());
		$collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('invoicetype', 'invoiceubn', 'invoicecom', 'order_id'));
		$collection->getSelect()->join('settv_sales_logistics_custom as sslc', 'settv_sales_order_custom.logistics_id = sslc.id', array('Max(`sslc`.`created_at`) as logistics_created'));
		$collection->getSelect()->joinLeft('sales_flat_order_item', 'main_table.entity_id = sales_flat_order_item.order_id', array('sku', 'name'))
		->where("sales_flat_order_item.product_type = 'simple'");
		$collection->getSelect()->joinLeft('sales_flat_order', 'main_table.entity_id = sales_flat_order.entity_id', array('total_qty_ordered', 'grand_total as gt'));
		$collection->getSelect()->joinLeft('sales_flat_order_address as sfoa', 'main_table.entity_id = sfoa.parent_id',array('firstname','postcode','city','region','street'))
		->where("sfoa.address_type = 'billing'")
		->group('main_table.entity_id');
		
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('logistics_created', array(
				'header' => Mage::helper('sales')->__('forward at'),
				'index' => 'logistics_created',
				'type' => 'datetime',
				'width' => '100px',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'sslc.created_at',
		));

		$this->addColumn('increment_id', array(
				'header'    => Mage::helper('sales')->__('Order #'),
				'index'     => 'increment_id',
				'type'      => 'text',
				'filter_index' => 'main_table.increment_id',
		));

		$this->addColumn('serial_id', array(
				'header'    => Mage::helper('sales')->__('Serial #'),
				'type'      => 'number',
				'align'     => 'right',
				'renderer' => new Settv_Report_Block_Render_RenderSerialID(),
				'filter' => false,
				'sortable' => false,
		));

		$this->addColumn('invoiceubn', array(
				'header'    => Mage::helper('sales')->__('Invoice UBN'),
				'index'     => 'invoiceubn',
				'type'      => 'text',
		));

		$this->addColumn('sku', array(
				'header'=> Mage::helper('sales')->__('SKU'),
				'type'  => 'text',
				'index' => 'sku',
		));

		$this->addColumn('product_name', array(
				'header'=> Mage::helper('sales')->__('Product Name'),
				'type'  => 'text',
				'index' => 'name',
		));

		$this->addColumn('product_price', array(
				'header' => Mage::helper('sales')->__('product price'),
				'index' => 'order_id',
				'type'  => 'currency',
				'currency' => 'order_currency_code',
				'renderer' => new Settv_Common_Block_Render_ProductPrice(),
				'sortable' => false,
				'filter' => false,
		));

		$this->addColumn('total_qty_ordered', array(
				'header' => Mage::helper('sales')->__('QTY'),
				'index' => 'total_qty_ordered',
				'type'  => 'number',
				'total' => 'sum',
		));

		$this->addColumn('is_print', array(
				'header'    => Mage::helper('sales')->__('Is Print'),
				'type'      => 'text',
				'align'     => 'right',
				'renderer' => new Settv_Report_Block_Render_RenderIsPrint(),
				'filter' => false,
				'sortable' => false,
		));

		$this->addColumn('grand_total', array(
				'header'    => Mage::helper('sales')->__('G.T. (Purchased)'),
				'index'     => 'gt',
				'type'      => 'currency',
				'align'     => 'right',
				'currency'  => 'order_currency_code',
				'filter_index' => 'sales_flat_order.gt',
		));

		$this->addColumn('tax_type', array(
				'header'    => Mage::helper('sales')->__('Tax Type'),
				'type'      => 'number',
				'align'     => 'right',
				'renderer' => new Settv_Report_Block_Render_RenderTaxType(),
				'filter' => false,
				'sortable' => false,
		));

		$this->addColumn('invoice_format', array(
				'header'    => Mage::helper('sales')->__('Invoice Format'),
				'index'     => 'invoicetype',
				'type'      => 'number',
				'align'     => 'right',
				'renderer' => new Settv_Report_Block_Render_RenderInvoiceFormat(),
				'filter' => false,
				'sortable' => false,
		));

		$this->addColumn('invoice_title', array(
				'header'=> Mage::helper('sales')->__('Invoice Title'),
				'type'  => 'text',
				'index' => 'invoicecom',
		));

		$this->addColumn('billing_name', array(
				'header'    => Mage::helper('sales')->__('Bill to Name'),
				'index'     => 'firstname',
				'type'      => 'text',
		));
		
		$this->addColumn('billing_address', array(
				'header'    => Mage::helper('sales')->__('Billing Address'),
				'index'     => 'street',
				'type'      => 'text',
				'renderer' => 'Settv_Common_Block_Render_RenderAddress',
				'filter_condition_callback' => array($this, '_addressFilter'),
		));

		$this->addColumn('note', array(
				'header'    => Mage::helper('sales')->__('Invoice Note'),
				'type'      => 'text',
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return false;
	}

	public function getRefreshButtonHtml()
	{
		return $this->getChildHtml('refresh_button');
	}

	protected function _addressFilter($collection, $column)
	{
		if (!$value = $column->getFilter()->getValue()) {
			return $this;
		}

		$this->getCollection()->getSelect()->where(
				"sfoa.city like ?
				OR sfoa.region like ?
				OR sfoa.street like ?
				OR sfoa.postcode like ?"
				, "%$value%");
		return $this;
	}
}
