<?php
class Settv_Report_Block_Render_RenderInvoiceFormat extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
	public function render(Varien_Object $row) {
		$invoice_type = $row->getData ( $this->getColumn ()->getIndex () );
		
		if ($invoice_type == "2" || $invoice_type == "5") { //5: eInvoice
			return "32";
		} else if ($invoice_type == "3") {
			return '31';
		} else if ($invoice_type == "4") {
			return '33';
		}
	}
}