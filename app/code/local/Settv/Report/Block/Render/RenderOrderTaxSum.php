<?php
class Settv_Report_Block_Render_RenderOrderTaxSum extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

	public function render(Varien_Object $row)
	{
		$grand_total = $row->getData($this->getColumn()->getIndex());

		if ($grand_total){
			return round((int)$grand_total/1.05*0.05);
		} else {
			return '0';
		}
	}
}