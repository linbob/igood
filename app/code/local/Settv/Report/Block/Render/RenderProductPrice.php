<?php

class Settv_Report_Block_Render_RenderProductPrice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$productId = $row->getData($this->getColumn()->getIndex());
		
		if ($productId){
			$_product = Mage::getModel('catalog/product')->load($productId);
			return number_format($_product->getFinalPrice(), 2);
		} else {
			return '';
		}
	}
}