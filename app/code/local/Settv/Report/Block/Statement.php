<?php
class Settv_Report_Block_Statement extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'statement';
        $this->_blockGroup = 'report';
        $this->_headerText = Mage::helper('sales')->__('Order Statement');
        parent::__construct();
        $this->_removeButton('add');
    }
}
