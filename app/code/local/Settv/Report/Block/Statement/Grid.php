<?php
class Settv_Report_Block_Statement_Grid extends Mage_Adminhtml_Block_Report_Grid
{
	protected $_subReportSize = 0;

	public function __construct()
	{
		parent::__construct();
		$this->setId('report_statement_grid');
		$this->setTemplate('widget/report/grid.phtml');
		$this->setExportVisibility(false);
	}

	/**
	 * Retrieve collection class
	 *
	 * @return string
	 */
	protected function _getCollectionClass()
	{
		return 'supplier/supplier_collection';
	}


	protected function _prepareCollection()
	{
		$filter = $this->getParam($this->getVarNameFilter(), null);

		if (is_null($filter)) {
			$filter = $this->_defaultFilter;
		}

		if (is_string($filter)) {
			$data = array();
			$filter = base64_decode($filter);
			parse_str(urldecode($filter), $data);

			if (!isset($data['report_from'])) {
				// getting all reports from 2001 year
				$date = new Zend_Date(mktime(0,0,0,1,1,2001));
				$data['report_from'] = $date->toString($this->getLocale()->getDateFormat('short'));
			}

			if (!isset($data['report_to'])) {
				// getting all reports from 2001 year
				$date = new Zend_Date();
				$data['report_to'] = $date->toString($this->getLocale()->getDateFormat('short'));
			}

			$this->_setFilterValues($data);
		} else if ($filter && is_array($filter)) {
			$this->_setFilterValues($filter);
		} else if(0 !== sizeof($this->_defaultFilter)) {
			$this->_setFilterValues($this->_defaultFilter);
		}

		$collection = Mage::getResourceModel($this->_getCollectionClass());

		$collection->setPeriod($this->getFilter('report_period'));

		if ($this->getFilter('report_from') && $this->getFilter('report_to')) {
			/**
			 * Validate from and to date
			 */
			try {
				$from = $this->getLocale()->date($this->getFilter('report_from'), Zend_Date::DATE_SHORT, null, false);	
				$to   = $this->getLocale()->date($this->getFilter('report_to'), Zend_Date::DATE_SHORT, null, false);
				$collection->setInterval($from, $to);
			}
			catch (Exception $e) {
				$this->_errors[] = Mage::helper('reports')->__('Invalid date specified.');
			}
		}

		if ($this->getSubReportSize() !== null) {
			$collection->setPageSize($this->getSubReportSize());
		}

		$this->setCollection($collection);

		Mage::dispatchEvent('adminhtml_widget_grid_filter_collection',
		array('collection' => $this->getCollection(), 'filter_values' => $this->_filterValues)
		);
	}

	protected function _prepareColumns()
	{
		parent::_prepareColumns();
			
		$this->addColumn('supplier_name', array(
				'header'    => Mage::helper('sales')->__('Supplier'),
				'width' 	=> '200px',
				'index'     => 'name',
				'type'      => 'text',
				'sortable'		=> false,
		));

		$this->addColumn('total_income_amount', array(
				'header'        => Mage::helper('sales')->__('Total Amount'),
				'type'          => 'number',
				'index'         => 'total_income_amount',
				'total'         => 'sum',
				'align'			=> 'right',
				'sortable'		=> false,
		));

		$this->addColumn('total_amount_tax', array(
				'header' => Mage::helper('sales')->__('Total Amount Tax'),
				'index' => 'total_amount_tax',
				'type'  => 'number',
				'total' => 'sum',
				'align' => 'right',
				'sortable'		=> false,
		));

		$this->addColumn('total_refunded_amount', array(
				'header'        => Mage::helper('sales')->__('Total Refunded Amount'),
				'type'          => 'number',
				'index'         => 'total_refunded_amount',
				'total'         => 'sum',
				'sortable'		=> false,
		));

		$this->addColumn('total_refunded_tax_amount', array(
				'header'        => Mage::helper('sales')->__('Total Refunded Tax'),
				'type'          => 'number',
				'index'         => 'total_refunded_tax_amount',
				'total'         => 'sum',
				'sortable'		=> false,
		));

		$this->addColumn('total_order_income', array(
				'header'    => Mage::helper('sales')->__('Total Order Income'),
				'index'     => 'total_order_income',
				'type'      => 'number',
				'total'     => 'sum',
				'sortable'		=> false,
		));

		$this->addColumn('total_order_tax', array(
				'header'        => Mage::helper('sales')->__('Total Order Tax'),
				'type'          => 'number',
				'index'         => 'total_order_tax',
				'total'         => 'sum',
				'sortable'		=> false,
		));

		$this->addColumn('total_order_paid', array(
				'header'        => Mage::helper('sales')->__('Total Order Paid'),
				'type'          => 'number',
				'index'         => 'total_order_paid',
				'total'         => 'sum',
				'sortable'		=> false,
		));

		$this->removeColumn('name');
		$this->removeColumn('ordered_qty');
			
		return $this;
	}


	public function getRowUrl($row)
	{
		$from = $this->getFilter('report_from');
		if($from){
			$from_date = explode("/", $from);
			//Rich Add
//		if($from_date[2] < 10) $from_date[2] = "0".$from_date[2];
			//$from = $from_date[2].$from_date[1].$from_date[0];
			//2015.01.09 Edward Edit
			$from = $from_date[0].$from_date[1].$from_date[2];
		}
		
		$to = $this->getFilter('report_to');
		if($to){
			$to_date = explode("/", $to);
			//Rich Add
			//if($to_date[2] < 10) $to_date[2] = "0".$to_date[2];
			//$to = $to_date[2].$to_date[1].$to_date[0];
			//2014.01.09 Edward Edit
			$to = $to_date[0].$to_date[1].$to_date[2];
		}

		return $this->getUrl('*/*/exportExcel', array('supplier_id' => $row->getId(),'report_from' => $from, 'report_to' => $to));
	}

	public function getRefreshButtonHtml()
	{
		return $this->getChildHtml('refresh_button');
	}

	public function getReport($from, $to)
	{
		if ($from == '') {
			$from = $this->getFilter('report_from');
		}
		if ($to == '') {
			$to = $this->getFilter('report_to');
		}
		
		$dateStart  = new Zend_Date($from, "yyyyMMdd");
		$tpStartTime = $dateStart->toString('yyyy-MM-dd 00:00:00');
		$utcStart = $dateStart -> subDay(1);
		$startTime = $utcStart->toString('yyyy-MM-dd 16:00:00');
		
		$dateEnd    = new Zend_Date($to, "yyyyMMdd");
		$tpEndTime = $dateEnd->toString('yyyy-MM-dd 23:59:59');
		$endTime = $dateEnd->toString('yyyy-MM-dd 15:59:59');
		
		$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
		$costId = $eavAttribute->getIdByCode('catalog_product', 'product_cost');

		$collection = $this->getCollection();


		$collection->getSelect()->joinLeft(
				array('ordertable' => new Zend_Db_Expr("(SELECT
						settv_sales_order_custom.supplier_id,
						Round(SUM(catalog_product_entity_int.value*sales_flat_order_item.qty_ordered)/1.05) AS total_income_amount,
						Round(SUM(catalog_product_entity_int.value*sales_flat_order_item.qty_ordered)/21) AS total_amount_tax,
						SUM(catalog_product_entity_int.value*sales_flat_order_item.qty_ordered) AS sum_order_product_cost
						FROM sales_flat_order_grid AS main_table
						LEFT JOIN `settv_sales_order_custom` ON main_table.entity_id = settv_sales_order_custom.order_id
						LEFT JOIN `settv_supplier` ON settv_sales_order_custom.supplier_id = settv_supplier.id
						LEFT JOIN `settv_sales_logistics_custom` ON settv_sales_logistics_custom.id = settv_sales_order_custom.logistics_id
						LEFT JOIN `sales_flat_order_item` ON main_table.entity_id = sales_flat_order_item.order_id
						LEFT JOIN `catalog_product_entity_int` ON sales_flat_order_item.product_id = catalog_product_entity_int.entity_id and catalog_product_entity_int.attribute_id='{$costId}'
						WHERE (sales_flat_order_item.product_type = 'simple' and settv_sales_order_custom.shipstatus <> 'Not Shipped' and settv_sales_logistics_custom.created_at>='{$startTime}' and settv_sales_logistics_custom.created_at<='{$endTime}') 
						GROUP BY supplier_id)")
				), 'ordertable.supplier_id = main_table.id',
				array('(IFNULL(total_income_amount,0)-IFNULL(total_refunded_amount,0)) as total_order_income',
						'(IFNULL(total_amount_tax,0)-IFNULL(total_refunded_tax_amount,0)) as total_order_tax',
						'(IFNULL(sum_order_product_cost,0)-IFNULL(sum_return_product_cost,0)) as total_order_paid',
						'IFNULL(total_income_amount,0) as total_income_amount',
						'IFNULL(total_amount_tax,0) as total_amount_tax',
						'IFNULL(sum_order_product_cost,0) as sum_order_product_cost'))
						-> order( array('total_income_amount DESC') );

		
		$collection->getSelect()->joinLeft(
				array('returntable' => new Zend_Db_Expr("(SELECT
						settv_sales_order_custom.supplier_id,
						Round(SUM(catalog_product_entity_int.value*sales_flat_order_item.qty_ordered)/1.05) AS total_refunded_amount,
						Round(SUM(catalog_product_entity_int.value*sales_flat_order_item.qty_ordered)/21) AS total_refunded_tax_amount,
						SUM(catalog_product_entity_int.value*sales_flat_order_item.qty_ordered) AS sum_return_product_cost
						FROM sales_flat_order_grid AS main_table
						LEFT JOIN `settv_sales_order_custom` ON main_table.entity_id = settv_sales_order_custom.order_id
						LEFT JOIN `settv_supplier` ON settv_sales_order_custom.supplier_id = settv_supplier.id
						LEFT JOIN `settv_sales_returnorder_custom` ON settv_sales_returnorder_custom.order_id = settv_sales_order_custom.order_id
						LEFT JOIN `sales_flat_order_item` ON main_table.entity_id = sales_flat_order_item.order_id
						LEFT JOIN `catalog_product_entity_int` ON sales_flat_order_item.product_id = catalog_product_entity_int.entity_id and catalog_product_entity_int.attribute_id='{$costId}'
						WHERE (sales_flat_order_item.product_type = 'simple' and status = 'return_succeeded' and settv_sales_returnorder_custom.created_at>='{$tpStartTime}' and settv_sales_returnorder_custom.created_at<='{$tpEndTime}' and settv_sales_returnorder_custom.active='1')
						GROUP BY supplier_id)")
				), 'returntable.supplier_id = main_table.id',
				array('IFNULL(total_refunded_amount,0) as total_refunded_amount',
						'IFNULL(total_refunded_tax_amount,0) as total_refunded_tax_amount',
						'IFNULL(sum_return_product_cost,0) as sum_return_product_cost'));
		
		$this->setCollection($collection);
		
		return $this -> getCollection();
	}

}
