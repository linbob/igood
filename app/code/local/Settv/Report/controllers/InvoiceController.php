<?php
class Settv_Report_InvoiceController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction()
	{
		$this->loadLayout()
		->_setActiveMenu('report/invoice');
		return $this;
	}

	public function indexAction() {
		$this->_title($this->__('Report'))->_title($this->__('Invoices'));

		$this->_initAction()
		->_addContent($this->getLayout()->createBlock('report/invoice'));
		$this->renderLayout();
	}
	
	/**
	 * Export invoice grid to CSV format
	 */
	public function exportCsvAction()
	{
		$fileName   = 'invoices.csv';
		$grid       = $this->getLayout()->createBlock('report/invoice_grid');
		$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		exit(0);
	}

	/**
	 *  Export invoice grid to Excel XML format
	 */
	public function exportExcelAction()
	{
		$fileName   = 'invoices.xls';
		$grid       = $this->getLayout()->createBlock('report/invoice_grid');
		$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		exit(0);
	}
}
