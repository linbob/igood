<?php
require_once("phpexcel/Classes/PHPExcel.php");
require_once("phpexcel/Classes/PHPExcel/IOFactory.php");
require_once("phpexcel/Classes/PHPExcel/Reader/Excel5.php");
class Settv_Report_StatementController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction()
	{
		$this->loadLayout()
		->_setActiveMenu('report/statement');
		return $this;
	}

	public function indexAction() {
		$this->_title($this->__('Report'))->_title($this->__('Statement'));

		$this->_initAction()
		->_addContent($this->getLayout()->createBlock('report/statement'));
		$this->renderLayout();
	}

	public function exportExcelAction() {

		$supplierId = $this->getRequest()->getParam('supplier_id');
		$report_from = $this->getRequest()->getParam('report_from');
		$report_to = $this->getRequest()->getParam('report_to');
		//2015.01.09 Edward Edit
		//$dateStart  = new Zend_Date($report_from, "ddMMyyyy");
		$dateStart  = new Zend_Date($report_from, "yyyyMMdd");
		//$dateEnd    = new Zend_Date($report_to, "ddMMyyyy");
		$dateEnd    = new Zend_Date($report_to, "yyyyMMdd");
		$startDay = $dateStart->toString('yyyy-MM-dd');
		$tpStartTime = $dateStart->toString('yyyy-MM-dd 00:00:00');
		//2015.01.09 Edward Edit
		$utcStart = $dateStart -> subDay(1);
		//$utcStart = $dateStart -> subDay(0);
		$startTime = $utcStart->toString('yyyy-MM-dd 16:00:00');
		$tpEndTime = $dateEnd->toString('yyyy-MM-dd 23:59:59');
		$endTime = $dateEnd->toString('yyyy-MM-dd 15:59:59');
		$endDay = $dateEnd->toString('yyyy-MM-dd');
		
		$collection = Mage::getResourceModel('supplier/supplier_collection');
		$collection->getSelect()->where("id={$supplierId}");
		$supplierName = $collection-> getLastItem() -> getData('name');

		$borderStyle = array(
				'borders' => array(
						'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN
						)
				)
		);

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
		$objPHPExcel->setActiveSheetIndex(0);
		$sheet1 = $objPHPExcel->getActiveSheet();

		$sheet1
		->setTitle("對帳單")
		->SetCellValue('A1', "【應付帳款對帳統計表】")
		//->SetCellValue('A1', $report_from.'_'.$dateStart.'_' )
		->SetCellValue('A2', "期間 :")
		->SetCellValue('B2', $startDay." ~ ".$endDay)
		->SetCellValue('A3', "廠商名稱")
		->SetCellValue('B3', Mage::helper('sales')->__('Total Amount'))
		->SetCellValue('C3', "進貨稅額")
		->SetCellValue('D3', "銷退金額")
		->SetCellValue('E3', "銷退稅額")
		->SetCellValue('F3', "總貨款(未稅)")
		->SetCellValue('G3', "貨款稅額")
		->SetCellValue('H3', "應付總淨額")
		->SetCellValue('G5', "本月逆物流扣款(愛買客將另開發票) :")
		->SetCellValue('G6', "本月愛買客實際付款金額總計 :")
		->SetCellValue('A8', "注意事項")
		->SetCellValue('A9', "1.請核對帳款明細，若有疑義請於本月15號前提出，愛買客：李小姐02-8792-8888#83015。\n2.帳款內容核對無誤，請於本月20號前開立發票並寄達。( 114台北市內湖區舊宗路一段159號3樓 電子商務小組 收)\n3.請貨款發票開立「應付總淨額」之金額，逆物流扣款，愛買客會於當月另開發票。實際匯款金額會扣掉逆物流費用。\n4.若有前期應付款，將會另附前期對帳單，煩請分別開立月份相同之發票。(例：1月貨款延遲至2月請款，將有兩份對帳單，請分別開立3月份發票)")
		-> mergeCells('A8:H8')
		-> mergeCells('A9:H9');

		$sheet1->getColumnDimension('A')->setWidth(30);
		$sheet1->getColumnDimension('B')->setAutoSize(true);
		$sheet1->getColumnDimension('C')->setWidth(10);
		$sheet1->getColumnDimension('D')->setWidth(10);
		$sheet1->getColumnDimension('F')->setWidth(10);
		$sheet1->getColumnDimension('G')->setWidth(10);
		$sheet1->getColumnDimension('H')->setWidth(13);
		$sheet1->getRowDimension('9')->setRowHeight(80);

		$sheet1->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$sheet1->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$sheet1->getStyle('A9')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		$sheet1->getStyle('A9')->getAlignment()->setWrapText(true);

		$sheet1->getStyle('A3:H4')->applyFromArray($borderStyle);
		$sheet1->getStyle('A8:H9')->applyFromArray($borderStyle);

		$sheet1->getStyle('A1:B2')->getFont()->setBold(true);
		$sheet1->getStyle('A1:H9')->getFont()->setSize(12);

		//sheet2, 出貨明細
		$collection = Mage::getResourceModel('sales/order_grid_collection');
		$collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id', 'shipstatus'))
		->where("settv_sales_order_custom.shipstatus <> 'Not Shipped'");
		$collection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplierId);
		$collection->getSelect()->joinLeft('sales_flat_order', 'main_table.entity_id = sales_flat_order.entity_id', array('total_qty_ordered'));
		$collection->getSelect()->joinLeft('sales_flat_order_item', 'main_table.entity_id = sales_flat_order_item.order_id', array('name','product_id'))
//Edward Edit
		->joinLeft('settv_sales_logistics_custom', 'settv_sales_order_custom.logistics_id = settv_sales_logistics_custom.id', array('Max(`settv_sales_logistics_custom`.`created_at`) as logistics_created'))
		->where("sales_flat_order_item.product_type = 'simple' and settv_sales_logistics_custom.created_at>='{$startTime}' and settv_sales_logistics_custom.created_at<='{$endTime}'")
		->group('main_table.entity_id');

		$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
		$costId = $eavAttribute->getIdByCode('catalog_product', 'product_cost');
		$collection->getSelect()->joinLeft('catalog_product_entity_int', "(sales_flat_order_item.product_id = catalog_product_entity_int.entity_id and catalog_product_entity_int.attribute_id='{$costId}')", array('value As product_cost'));
		
		$sheet2 = $objPHPExcel -> createSheet();
		$sheet2
		->setTitle("出貨明細")
		->SetCellValue('A1', "【出貨明細清單】")
		->SetCellValue('A2', "廠商名稱")
		->SetCellValue('B2', $supplierName)
		->SetCellValue('A3', "期間 :")
		->SetCellValue('B3', $startDay." ~ ".$endDay)
		->SetCellValue('A4', "訂單編號")
		->SetCellValue('B4', "出貨日期")
		->SetCellValue('C4', "品名")
		->SetCellValue('D4', "供應商")
		->SetCellValue('E4', "訂單狀態")
		->SetCellValue('F4', "物流狀態")
		->SetCellValue('G4', "數量")
		->SetCellValue('H4', "進售金額")
		->SetCellValue('I4', "營業稅")
		->SetCellValue('J4', "總計");
		$sheet2->getColumnDimension('A')->setAutoSize(true);
		$sheet2->getColumnDimension('B')->setAutoSize(true);
		$sheet2->getColumnDimension('C')->setAutoSize(true);
		$sheet2->getColumnDimension('D')->setWidth(20);
		$sheet2->getColumnDimension('E')->setWidth(10);
		$sheet2->getColumnDimension('F')->setWidth(10);
		$sheet2->getColumnDimension('G')->setWidth(5);
		$sheet2->getColumnDimension('H')->setWidth(10);
		$sheet2->getColumnDimension('I')->setWidth(8);
		$sheet2->getColumnDimension('J')->setWidth(10);

		$sheet2->getStyle('A4:J4')->applyFromArray($borderStyle);

		$ship_status = array( 'Shipped' => Mage::helper('sales')->__('Shipped'),'Not Shipped' => Mage::helper('sales')->__('Not Shipped') ,
				'Arrived' => Mage::helper('sales')->__('Arrived'),'Rejected' => Mage::helper('sales')->__('Rejected'),
				'Return Received' => Mage::helper('sales')->__('Return Received'),'Return Rejected' => Mage::helper('sales')->__('Return Rejected'));
		$order_status = Mage::getSingleton('sales/order_config')->getStatuses();

		$ship_num = 5;
		foreach ($collection->getData() as $data){
			$qty = $data['total_qty_ordered'];

			$created_date = new Zend_Date($data['logistics_created'], "yyyyMMdd HH:mm:ss");
			$created_date = $created_date -> addHour(8);
			
			$sheet2
			->SetCellValue('A'.$ship_num, $data['increment_id'])
			->SetCellValue('B'.$ship_num, $created_date->toString("yyyy-MM-dd HH:mm:ss"))
			->SetCellValue('C'.$ship_num, $data['name'])
			->SetCellValue('D'.$ship_num, $supplierName)
			->SetCellValue('E'.$ship_num, $order_status[$data['status']])
			->SetCellValue('F'.$ship_num, $ship_status[$data['shipstatus']])
			->SetCellValue('G'.$ship_num, $qty);

			$sheet2->getCell('J'.$ship_num)->setValueExplicit(round($data['product_cost']*$qty), PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$sheet2->SetCellValue('H'.$ship_num, '=(VALUE(J'.$ship_num.')/1.05)');
			$sheet2->SetCellValue('I'.$ship_num, '=SUM(VALUE(J'.$ship_num.'),-VALUE(H'.$ship_num.'))');

			$ship_num++;
		}

		if ($ship_num>5){
			$j = $ship_num-1;
			$sheet2->getStyle('A5:A'.$j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$sheet2->getStyle('A5:J'.$j)->applyFromArray($borderStyle);
			$sheet2
			->SetCellValue('I'.$ship_num, "總計")
			->SetCellValue('J'.$ship_num, '=SUM(J5:J'.$j.')');
		} else {
			$sheet2
			->SetCellValue('I5', "總計")
			->SetCellValue('J5', '0');
		}

		// sheet3, 銷退明細
		$collection = Mage::getResourceModel('sales/order_grid_collection');
		$collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id', 'shipstatus'))
		->where("main_table.status = 'return_succeeded'");
		$collection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplierId);
		$collection->getSelect()->joinLeft('sales_flat_order', 'main_table.entity_id = sales_flat_order.entity_id', array('total_qty_ordered'));
		$collection->getSelect()->joinLeft('sales_flat_order_item', 'main_table.entity_id = sales_flat_order_item.order_id', array('name','product_id'))
		->joinLeft('settv_sales_returnorder_custom', 'main_table.entity_id = settv_sales_returnorder_custom.order_id', array('created_at'))
		->where("sales_flat_order_item.product_type = 'simple' and settv_sales_returnorder_custom.active = '1' and settv_sales_returnorder_custom.created_at>='{$tpStartTime}' and settv_sales_returnorder_custom.created_at<='{$tpEndTime}'");

		$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
		$costId = $eavAttribute->getIdByCode('catalog_product', 'product_cost');
		$collection->getSelect()->joinLeft('catalog_product_entity_int', "(sales_flat_order_item.product_id = catalog_product_entity_int.entity_id and catalog_product_entity_int.attribute_id='{$costId}')", array('value AS product_cost'));
		
		$sheet3 = $objPHPExcel -> createSheet();
		$sheet3
		->setTitle("銷退明細")
		->SetCellValue('A1', "【銷退明細清單】")
		->SetCellValue('A2', "廠商名稱：")
		->SetCellValue('B2', $supplierName)
		->SetCellValue('A3', "期間 :")
		->SetCellValue('B3', $startDay." ~ ".$endDay)
		->SetCellValue('A'.'4', "訂單編號")
		->SetCellValue('B'.'4', "退貨日期")
		->SetCellValue('C'.'4', "品名")
		->SetCellValue('D'.'4', "供應商")
		->SetCellValue('E'.'4', "訂單狀態")
		->SetCellValue('F'.'4', "物流狀態")
		->SetCellValue('G'.'4', "數量")
		->SetCellValue('H'.'4', "進售金額")
		->SetCellValue('I'.'4', "營業稅")
		->SetCellValue('J'.'4', "總計");

		$sheet3->getColumnDimension('A')->setAutoSize(true);
		$sheet3->getColumnDimension('B')->setAutoSize(true);
		$sheet3->getColumnDimension('C')->setAutoSize(true);
		$sheet3->getColumnDimension('D')->setWidth(20);
		$sheet3->getColumnDimension('E')->setWidth(10);
		$sheet3->getColumnDimension('F')->setWidth(10);
		$sheet3->getColumnDimension('G')->setWidth(5);
		$sheet3->getColumnDimension('H')->setWidth(10);
		$sheet3->getColumnDimension('I')->setWidth(8);
		$sheet3->getColumnDimension('J')->setWidth(10);

		$sheet3->getStyle('A4:J4')->applyFromArray($borderStyle);

		$return_num = 5;
		foreach ($collection->getData() as $data){
			$qty = $data['total_qty_ordered'];

			$updated_date = new Zend_Date($data['created_at'], "yyyyMMdd HH:mm:ss");
			//$updated_date = $updated_date -> addHour(8);
			
			$sheet3
			->SetCellValue('A'.$return_num, $data['increment_id'])
			->SetCellValue('B'.$return_num, $updated_date->toString("yyyy-MM-dd HH:mm:ss"))
			->SetCellValue('C'.$return_num, $data['name'])
			->SetCellValue('D'.$return_num, $supplierName)
			->SetCellValue('E'.$return_num, $order_status[$data['status']])
			->SetCellValue('F'.$return_num, $ship_status[$data['shipstatus']])
			->SetCellValue('G'.$return_num, $qty);

			$sheet3->getCell('J'.$return_num)->setValueExplicit(round($data['product_cost']*$qty), PHPExcel_Cell_DataType::TYPE_NUMERIC);
			$sheet3->SetCellValue('H'.$return_num, '=(VALUE(J'.$return_num.')/1.05)');
			$sheet3->SetCellValue('I'.$return_num, '=SUM(VALUE(J'.$return_num.'),-VALUE(H'.$return_num.'))');

			$return_num++;
		}

		if ($return_num>5){
			$j = $return_num-1;
			$sheet3->getStyle('A5:A'.$j)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$sheet3->getStyle('A5:J'.$j)->applyFromArray($borderStyle);
			$sheet3
			->SetCellValue('I'.$return_num, "總計")
			->SetCellValue('J'.$return_num, '=SUM(J5:J'.$j.')');
		} else {
			$sheet3
			->SetCellValue('I5', "總計")
			->SetCellValue('J5', '0');
		}

		// $sheet1 set reference Data
		$sheet1
		->SetCellValue('A4', $supplierName)
		->SetCellValue('B4', '=ROUND(VALUE(出貨明細!J'.$ship_num.')/1.05,0)')
		->SetCellValue('C4', '=Round(VALUE(B4)*0.05,0)')
		->SetCellValue('D4', '=ROUND(VALUE(銷退明細!J'.$return_num.')/1.05,0)')
		->SetCellValue('E4', '=Round(VALUE(D4)*0.05,0)')
		->SetCellValue('F4', '=SUM(VALUE(B4),-VALUE(D4))')
		->SetCellValue('G4', '=SUM(VALUE(C4),-VALUE(E4))')
		->SetCellValue('H4', '=SUM(VALUE(F4),VALUE(G4))')
		->setCellValue('H6', '=SUM(VALUE(H4),-VALUE(H5))');

		$file_name = iconv('utf-8','big5','對帳單-愛買客-'.$supplierName.'.xls');

		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
		header("Content-Type:application/force-download");
		header("Content-Type:application/vnd.ms-execl");
		header("Content-Type:application/octet-stream");
		header("Content-Type:application/download");
		header("Content-Disposition: attachment; filename=\"".$file_name."\"");
		header("Content-Transfer-Encoding:binary");
		$objWriter->save("php://output");
	}
}
