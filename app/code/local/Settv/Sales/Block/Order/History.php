<?php 
/*
 * 20120622 filter order state = failded by Sean 
 */
?>
<?php
class Settv_Sales_Block_Order_History extends Mage_Sales_Block_Order_History
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('sales/order/history.phtml');

        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
            ->addFieldToFilter('state', array('nin' => "cc_processing"))
            ->setOrder('created_at', 'desc')
        ;        

        $orders->getSelect()->joinleft('medium_order_item','order_id=entity_id',array("*"));
        
        $this->setOrders($orders);

        Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('sales')->__('My Orders'));
    }
    
}
?>