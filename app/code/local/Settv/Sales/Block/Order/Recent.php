<?php 
/*
 * 20120622 filter order state = failded by Sean 
 */
?>
<?php
class Settv_Sales_Block_Order_Recent extends Mage_Sales_Block_Order_Recent
{

	public function __construct()
	{
		parent::__construct();

		//TODO: add full name logic
		$orders = Mage::getResourceModel('sales/order_collection')
    		->addAttributeToSelect('*')
    		->joinAttribute('shipping_firstname', 'order_address/firstname', 'shipping_address_id', null, 'left')
    		->joinAttribute('shipping_lastname', 'order_address/lastname', 'shipping_address_id', null, 'left')
    		->addAttributeToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
    		->addAttributeToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
    		->addAttributeToFilter('state', array('nin' => "cc_processing"))
    		->addAttributeToSort('created_at', 'desc')
		;
		
		$orders->getSelect()->joinleft('medium_order_item','order_id=entity_id',array("*"));					

		$this->setOrders($orders);
	}
}
?>