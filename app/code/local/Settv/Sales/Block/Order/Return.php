<?php
/**
 * 
 *  20120619 return order form by Sean
 * 
 */

class Settv_Sales_Block_Order_Return extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('sales/order/return.phtml');
        
        //get shipping status data from customize table by Sean
        $Lday7 = strtotime('-1 week');
        $str = date("Y-m-d H:i:s", $Lday7);

        $orderids = Mage::getModel('check/custom_order')->getCollection()
        ->addFieldToSelect('order_id')
        ->addFieldToFilter('shipstatus', 'Arrived')
        ->addFieldToFilter('arrived_at', array('gt' => $str))
        ->getData();   
        
        // use shipping status to filter order collection 
        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('entity_id', array('in' => $orderids))
            ->addFieldToFilter('state', 'established')
            ->setOrder('created_at', 'desc')
        ;
        
        $orders->getSelect()->joinleft('medium_order_item','order_id=entity_id',array("*"));

        $this->setOrders($orders);

        Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('sales')->__('My Orders'));
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'sales.order.history.pager')
            ->setCollection($this->getOrders());
        $this->setChild('pager', $pager);
        $this->getOrders()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getViewUrl($order)
    {
        return $this->getUrl('*/*/view', array('order_id' => $order->getId()));
    }

    public function getTrackUrl($order)
    {
        return $this->getUrl('*/*/track', array('order_id' => $order->getId()));
    }

    public function getReorderUrl($order)
    {
        return $this->getUrl('*/*/reorder', array('order_id' => $order->getId()));
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
