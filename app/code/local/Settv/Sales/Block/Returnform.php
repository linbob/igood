<?php
/**
 * 
 * @author VM User
 *
 */
class Settv_Sales_Block_Returnform extends Mage_Core_Block_Template
{
    /**
     * @deprecated after 1.4.0.1
     */
    private $_order;
    //protected $_address;
    /**
     * Retrieve identifier of created order
     *
     * @return string
     * @deprecated after 1.4.0.1
     */
    
    /* 20120729 Sean save address cache
    protected function _prepareLayout()
    {
    	parent::_prepareLayout();
    	$this->_retrunaddress = Mage::getModel('customer/address');
        
    	if ($postedData = Mage::getSingleton('customer/session')->getAddressFormData(true)) {
    		$this->_address->setData($postedData['return']);
    	}
    }
    */
    
    public function getInOrderId()
    {
    	return ($this->getRequest()->getParam('order_id'));
    }
    
    public function getOrderId()
    {
        //return $this->_getData('order_id');
        $orderid = $this->getRequest()->getParam('order_id');
        if($orderid){
        	$order = Mage::getModel('sales/order')->load($orderid);       	
        }else{
        	if ($postedData = Mage::getSingleton('customer/session')->getReturnFormData()){
        		$order = Mage::getModel('sales/order')->load($postedData['form_order_id']);
        	}
        }
        return $order->getRealOrderId();
    }

    /**
     * Check order print availability
     *
     * @return bool
     * @deprecated after 1.4.0.1
     */
    public function canPrint()
    {
        return $this->_getData('can_view_order');
    }

    /**
     * Get url for order detale print
     *
     * @return string
     * @deprecated after 1.4.0.1
     */
    public function getPrintUrl()
    {
        return $this->_getData('print_url');
    }

    /**
     * Get url for view order details
     *
     * @return string
     * @deprecated after 1.4.0.1
     */
    public function getViewOrderUrl()
    {
        return $this->_getData('view_order_id');
    }

    /**
     * See if the order has state, visible on frontend
     *
     * @return bool
     */
    public function isOrderVisible()
    {
        return (bool)$this->_getData('is_order_visible');
    }

    /**
     * Getter for recurring profile view page
     *
     * @param $profile
     */
    public function getProfileUrl(Varien_Object $profile)
    {
        return $this->getUrl('sales/recurring_profile/view', array('profile' => $profile->getId()));
    }

    /**
     * Initialize data and prepare it for output
     */
    protected function _beforeToHtml()
    {
        $this->_prepareLastOrder();
        $this->_prepareLastBillingAgreement();
        $this->_prepareLastRecurringProfiles();
        return parent::_beforeToHtml();
    }

    /**
     * Get last order ID from session, fetch it and check whether it can be viewed, printed etc
     */
    protected function _prepareLastOrder()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        if ($orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getId()) {
                $isVisible = !in_array($order->getState(),
                    Mage::getSingleton('sales/order_config')->getInvisibleOnFrontStates());
                $this->addData(array(
                    'is_order_visible' => $isVisible,
                    'view_order_id' => $this->getUrl('sales/order/view/', array('order_id' => $orderId)),
                    'print_url' => $this->getUrl('sales/order/print', array('order_id'=> $orderId)),
                    'can_print_order' => $isVisible,
                    'can_view_order'  => Mage::getSingleton('customer/session')->isLoggedIn() && $isVisible,
                    'order_id'  => $order->getIncrementId(),
                ));
            }
        }
    }

    /**
     * Prepare billing agreement data from an identifier in the session
     */
    protected function _prepareLastBillingAgreement()
    {
        $agreementId = Mage::getSingleton('checkout/session')->getLastBillingAgreementId();
        $customerId = Mage::getSingleton('customer/session')->getCustomerId();
        if ($agreementId && $customerId) {
            $agreement = Mage::getModel('sales/billing_agreement')->load($agreementId);
            if ($agreement->getId() && $customerId == $agreement->getCustomerId()) {
                $this->addData(array(
                    'agreement_ref_id' => $agreement->getReferenceId(),
                    'agreement_url' => $this->getUrl('sales/billing_agreement/view',
                        array('agreement' => $agreementId)
                    ),
                ));
            }
        }
    }

    /**
     * Prepare recurring payment profiles from the session
     */
    protected function _prepareLastRecurringProfiles()
    {
        $profileIds = Mage::getSingleton('checkout/session')->getLastRecurringProfileIds();
        if ($profileIds && is_array($profileIds)) {
            $collection = Mage::getModel('sales/recurring_profile')->getCollection()
                ->addFieldToFilter('profile_id', array('in' => $profileIds))
            ;
            $profiles = array();
            foreach ($collection as $profile) {
                $profiles[] = $profile;
            }
            if ($profiles) {
                $this->setRecurringProfiles($profiles);
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $this->setCanViewProfiles(true);
                }
            }
        }
    }
    
    /*
     * 20120628 Set address select by Sean
     */
    public function customerHasAddresses()
    {
    	$customer = Mage::getSingleton('customer/session')->getCustomer();
    	return count($customer->getAddresses());
    }
    
    /*
    public function getAddressId($type = 'billing')
    {
    	$customer = Mage::getSingleton('customer/session')->getCustomer();
    	if ($type=='billing') {
    		$address = $customer->getPrimaryBillingAddress();
    	} else {
    		$address = $customer->getPrimaryShippingAddress();
    	}
    	return $address->getId();
    }
    */
    
    public function getAddressesHtmlSelect($type = 'billing')
    {
    	
    		$options = array();
    		$customer = Mage::getSingleton('customer/session')->getCustomer();
    		foreach ($customer->getAddresses() as $address) {
    			$options[] = array(
    					'value' => $address->getId(),
    					'label' => $address->getName().",".$address->getPostcode().$address->getCity().$address->getRegion().$address->getStreet(-1)
    			);
    		}
    
  			if ($type=='billing') {
    			$address = $customer->getPrimaryBillingAddress();
    		} else {
    			$address = $customer->getPrimaryShippingAddress();
    		}
    		
    		$PostData = Mage::getSingleton('customer/session')->getReturnFormData();
    		if($PostData){
    			$addressId = $PostData['return_address_id'];
    		}else{
    			$addressId = $address->getId();
    		}
    		

    
    		$select = $this->getLayout()->createBlock('core/html_select')
    		->setName('return_address_id')
    		->setId('return-address-select')
    		->setClass('address-select')
    		->setExtraParams('onchange="ChangeLayout(this.value)"')
    		->setValue($addressId)
    		->setOptions($options);
    
    		$select->addOption('', Mage::helper('checkout')->__('New Address'));
    
    		return $select->getHtml();
    }
    
}
