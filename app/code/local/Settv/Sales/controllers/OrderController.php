<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales orders controller
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Settv_Sales_OrderController extends Mage_Sales_Controller_Abstract
{

	/**
	 * Action predispatch
	 *
	 * Check customer authentication for some actions
	 */


	public function preDispatch()
	{
		parent::preDispatch();
		$action = $this->getRequest()->getActionName();
		$loginUrl = Mage::helper('customer')->getLoginUrl();

		if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
			$this->setFlag('', self::FLAG_NO_DISPATCH, true);
		}
	}

	/**
	 * Customer order return 退貨
	 * Fai 20120514
	 */
	public function returnAction()
	{

		$this->loadLayout();
		$this->_initLayoutMessages('catalog/session');

		$this->getLayout()->getBlock('head')->setTitle($this->__('My Return Orders'));

		if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
			$block->setRefererUrl($this->_getRefererUrl());
		}
		$this->renderLayout();

	}

	/**
	 * Customer order cancel 退訂
	 * Fai 20120514
	 */
	public function cancelAction()
	{

		$this->loadLayout();
		$this->_initLayoutMessages('catalog/session');

		$this->getLayout()->getBlock('head')->setTitle($this->__('My Cancel Orders'));

		if ($block = $this->getLayout()->getBlock('customer.account.link.back')) {
			$block->setRefererUrl($this->_getRefererUrl());
		}
		$this->renderLayout();

	}

	public function cancelbillingAction(){
		//set Order States to cancelled
		$this->loadLayout();
		$order_id = $this->getRequest()->getParam('order_id');
		$order = Mage::helper('customer/order')->getCurrentCustomerOrderById($order_id);

		if ($order->getId() && $order->canCancel())
		{
			$isRefunded = Mage::helper('common/payment')->refundOrder($order); //: Refund

			//- post-refund actions
			$order->setState('canceled', true, '')->save();

			//Product add Inventory by Dabo 20120619
			$items = $order->getAllItems();
			$product_id = null;
			foreach ($items as $itemId => $item)
			{
				$name = $item->getName();
				$unitPrice=$item->getPrice();
				$sku=$item->getSku();
				$productId=$item->getProductId();
				//$qty=$item->getQtyToInvoice();
				$qty=$item->getQtyOrdered();
			}
			$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
			$stockItem->addQty($qty);
			$stockItem->save();

			// load supplier data, then notify supplier
			$_model = Mage::getModel('catalog/product');
			$_product = $_model->load($productId);
			$supplier_id = $_product->getSupplier();
			$collection = Mage::getModel('supplier/supplier')->getCollection()
			->addFieldToSelect('*')->addFieldToFilter('attribute_id', $supplier_id);
			$supplier = current($collection->load()->getIterator());

			$emailTemplateVariables = array();
			$emailTemplateVariables['order'] = $order;
			$emailTemplateVariables['order_id'] = $order->getIncrementId();
			$emailTemplateVariables['totalPrice'] = round($order->getGrandTotal(),0);

			$items1 = $order->getAllItems();
			$itemIndex = 0;
			foreach ($items1 as $itemId1 => $item1)
			{
				if ($itemIndex == 0)
				{
					$emailTemplateVariables['unitPrice'] = round($item1->getPrice(),0);
					$emailTemplateVariables['qty']=$item1->getQtyOrdered()*1;
				}
				$emailTemplateVariables['productname'] = $item1->getName();
				$emailTemplateVariables['sku'] =$item1->getSku();
				$itemIndex++;
			}
			//send to supplier
			$email_address = $supplier->getContactEmail();
			Mage::helper('common/common')->sendEmail('Supplier Cancel Order', $email_address, $emailTemplateVariables);
			//send to order manager
			$email_address = Mage::getStoreConfig('sales_email/settv_order_mgmt/manager_email');
			Mage::helper('common/common')->sendEmail('Supplier Cancel Order', $email_address, $emailTemplateVariables);
		
		}
		else {
			$this->_redirectError(Mage::getUrl('sales/order/cancel'));
		}

		$this->renderLayout();
	}

	public function returngoodsAction(){
		$this->loadLayout();
		$order_id = $this->getRequest()->getParam('order_id');
		$order = Mage::helper('customer/order')->getCurrentCustomerOrderById($order_id);

		if ($order->getId())
		{
			$order->setState('return_processing', true, '')->save();

			//**************************************
			$email_address = Mage::getStoreConfig('sales_email/settv_order_mgmt/manager_email');
			$emailTemplateVariables = array();
			$emailTemplateVariables['order'] = $order;
			$emailTemplateVariables['order_id'] = $order->getIncrementId();
			$emailTemplateVariables['totalPrice'] = round($order->getGrandTotal(),0);

			$items1 = $order->getAllItems();
			$itemIndex = 0;
			foreach ($items1 as $itemId1 => $item1)
			{
				if ($itemIndex == 0)
				{
					$emailTemplateVariables['unitPrice'] = round($item1->getPrice(),0);
					$emailTemplateVariables['qty']=$item1->getQtyOrdered()*1;
				}
				$emailTemplateVariables['productname'] = $item1->getName();
				$emailTemplateVariables['sku'] =$item1->getSku();
				$SproductId=$item1->getProductId();
				$itemIndex++;
			}
			//send to order manager
			$email_address = Mage::getStoreConfig('sales_email/settv_order_mgmt/manager_email');
			Mage::helper('common/common')->sendEmail('Supplier Return Order', $email_address, $emailTemplateVariables);

			//Rule: if it is low temperature product and logistic == ezcat, then hct will not be used
			if (Mage::helper('common/common')->shouldProductUseHct($SproductId)) {
				Mage::helper('logistichct/Hct')->CreateReverseTransactionFile($order->getId());
			} else {
				//Do nothing
			}
		}
		else 
		{
			//TODO: add error msg;
		}
		$this->renderLayout();
	}

	public function returnformAction(){

		/* 20120630 show error msg by Sean S */
		$msg = $this->_getSession()->getMessages(true);

		$this->loadLayout();
		$this->getLayout()->getMessagesBlock()->addMessages($msg);
		$this->_initLayoutMessages('core/session');

		//$this->loadLayout();
		/* 20120630 show error msg by Sean E */

		$this->renderLayout();
	}

	protected function _getSession()
	{
		return Mage::getSingleton('customer/session');
	}

	public function returnpostAction()
	{
		// Save data
		if ($this->getRequest()->isPost()) {
			$customer = $this->_getSession()->getCustomer();
			/* @var $address Mage_Customer_Model_Address */


			$addressId = $this->getRequest()->getPost('return_address_id');
			$Form_OrderId = $this->getRequest()->getPost('form_order_id');
			$return_address = $this->getRequest()->getPost('return',array());


			if ($addressId) {
				$errors =array();
				$errors = $this->returnvalidate($return_address,1);

				if(count($errors) === 0){
					$reason = $return_address['reason'];

					$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($Form_OrderId);
					$model = Mage::getModel('check/custom_order')->load($CustomId);

					$modelArr = $model->getData();
					if($modelArr['returnid']){
						$ChangeOld = Mage::getModel('check/custom_returnorder')->load($modelArr['returnid'])
						->setData('order_id',$Form_OrderId)
						->setData('active',false)
						->setData('update_at',now())
						->save();
					}

					$NewReturn = Mage::getModel('check/custom_returnorder')
					->setData('order_id',$Form_OrderId)
					->save();
					$NewReturn_id = $NewReturn->getId();

					//Set return number (逆物流編號) to empty if the order return logistic is not using hct
					$ReturnNum = "";
					$order = Mage::getModel('sales/order')->load($Form_OrderId);
					$items1 = $order->getAllItems();
					foreach ($items1 as $itemId1 => $item1) {
						$SproductId=$item1->getProductId();
					}
					if ( Mage::helper('common/common')->shouldProductUseHct($SproductId) ) {
						Mage::log("Should use hct return number", null, "returnpost.log"); //TODO: deleteme
						Mage::log("product id: $SproductId", null, "returnpost.log"); //TODO: deleteme
						$ReturnNum = $NewReturn->getreturnnum($NewReturn_id);
					}

					Mage::log("FormOrderId : $Form_OrderId", null, "returnpost.log"); //TODO: deleteme
					Mage::log("ReturnNum : $ReturnNum", null, "returnpost.log"); //TODO: deleteme
					Mage::log("NewReturnId : $NewReturn_id", null, "returnpost.log"); //TODO: deleteme


					$NewReturn->setData('return_num',$ReturnNum)->save();
					$model->setData('returnid', $NewReturn->getId())
					->setData('returnnum', $ReturnNum)
					->setData('returnreason', $reason)
					->setData('returnaddressid', $addressId)
					->save();

					Mage::log("return model saved", null, "returnpost.log"); //TODO: deleteme

					$this->_getSession()->setReturnFormData();

					$this->_redirectSuccess(Mage::getUrl('sales/order/returngoods',array('order_id'=>$Form_OrderId)));
					//$this->getResponse()->setRedirect('sales/order/returngoods').'/order_id/'.$Form_OrderId.'/');
					return;
					//$this->getResponse()->setRedirect(Mage::getUrl('sales/order/returngoods', array('order_id'=>$Form_OrderId)));
					//return;
				}else{
					foreach ($errors as $errorMessage) {
						$this->_getSession()->addError($errorMessage);
					}
					$this->_getSession()->setReturnFormData($this->getRequest()->getPost());
				}
			}
			else{
				//try {
				$errors =array();
				$errors = $this->returnvalidate($return_address,2);

				if(count($errors) === 0){
					$address = Mage::getModel("customer/address");
					$address->setCustomerId($customer->getId());
					$address->firstname = $return_address['firstname'];
					$address->telephone = $return_address['telephone'];
					$address->cellphone = $return_address['cellphone'];
					$address->country_id = "TW";
					$address->city = $return_address['city'];
					$address->region = $return_address['region'];
					$address->postcode = $return_address['postcode'];
					$address->street = $return_address['street'];
					$address->setIsDefaultBilling(false);
					$address->setIsDefaultShipping(false);
					$address->save();

					$addressId = $address->getId();

					$reason = $return_address['reason'];

					$CustomId = Mage::getModel('check/custom_order')->getIdByOrder($Form_OrderId);
					$model = Mage::getModel('check/custom_order')->load($CustomId);

					$modelArr = $model->getData();
					if($modelArr['returnid']){
						$ChangeOld = Mage::getModel('check/custom_returnorder')->load($modelArr['returnid'])
						->setData('order_id',$Form_OrderId)
						->setData('active',false)
						->save();
					}

					$NewReturn = Mage::getModel('check/custom_returnorder')
					->setData('order_id',$Form_OrderId)
					->save();
					$NewReturn_id = $NewReturn->getId();
					$ReturnNum = $NewReturn->getreturnnum($NewReturn_id);
					$NewReturn->setData('return_num',$ReturnNum)->save();

					$model->setData('returnid', $NewReturn->getId())
					->setData('returnnum', $ReturnNum)
					->setData('returnreason', $reason)
					->setData('returnaddressid', $addressId)
					->save();


					$this->_getSession()->setReturnFormData();

					$this->_redirectSuccess(Mage::getUrl('sales/order/returngoods',array('order_id'=>$Form_OrderId)));
					return;

				}else{
					foreach ($errors as $errorMessage) {
						$this->_getSession()->addError($errorMessage);
					}
					$this->_getSession()->setReturnFormData($this->getRequest()->getPost());
				}
				/*
				 } catch (Mage_Core_Exception $e) {
				$this->_getSession()->setAddressFormData($this->getRequest()->getPost())
				->addException($e, $e->getMessage());
				} catch (Exception $e) {
				$this->_getSession()->setAddressFormData($this->getRequest()->getPost())
				->addException($e, $this->__('Cannot save address.'));
				}
				*/
			}
		}

		$this->_redirectError(Mage::getUrl('sales/order/returnform',array('order_id'=>$Form_OrderId)));
		return;
	}

	public function returnvalidate($return_address,$validatetype)
	{
		$errors = array();
		$helper = Mage::helper('customer');

		if ($return_address['reason'] == "0") {
			$errors[] = $helper->__('Please select the return reason.');
		}

		if($validatetype == 2){
			if (!Zend_Validate::is($return_address['firstname'], 'NotEmpty')) {
				$errors[] = $helper->__('Please enter the first name.');
			}

			if (!Zend_Validate::is($return_address['telephone'], 'NotEmpty')) {
				$errors[] = $helper->__('Please enter the telephone number.');
			}

			if (!Zend_Validate::is($return_address['cellphone'], 'NotEmpty')) {
				$errors[] = $helper->__('Please enter the cellphone number.');
			}

			if (!Zend_Validate::is($return_address['city'], 'NotEmpty')) {
				$errors[] = $helper->__('Please enter the city.');
			}

			if (!Zend_Validate::is($return_address['region'], 'NotEmpty')) {
				$errors[] = $helper->__('Please enter the region.');
			}

			if (!Zend_Validate::is($return_address['street'], 'NotEmpty')) {
				$errors[] = $helper->__('Please enter the street.');
			}
		}
		/*
		 if (empty($errors)) {
		return true;
		}
		*/
		return $errors;
	}

	public function requireEeinvoiceAction() {
		$order_id = $this->getRequest ()->getParam ( 'order_id' );
		$createdinvoice = Mage::getModel ( 'einvoice/einvoices' )->loadByOrderId ( $order_id );
		if (! is_null ( $createdinvoice->getId () )) {
			$createdinvoice->setUser_request ( 1 )->save ();
		}

		$order = Mage::getModel('sales/order')->load($order_id);
		$email_address = Mage::getStoreConfig('sales_email/settv_order_mgmt/manager_email');
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$_items = $order->getItemsCollection();
		foreach ($_items as $_item):
		if ($_item->getParentItem()) continue;
		$emailTemplateVariables['product'] .= $_item->getName();
		endforeach;

		Mage::helper('common/common')->sendEmail("Request_einvoice_notification", $email_address, $emailTemplateVariables);

		$this->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
		return;
	}

	//     public function notifysupplier($template_code, $email, $email_subject, array $emailTemplateVariables = array()) {
	//     	$emailTemplate  = Mage::getModel('core/email_template')->loadByCode($template_code);
	// //     	$emailTemplateVariables = array();
	// //     	$emailTemplateVariables['myvar1'] = 'Branko';
	// //     	$emailTemplateVariables['myvar2'] = 'Ajzele';
	// //     	$emailTemplateVariables['myvar3'] = 'ActiveCodeline';

	//     	$emailTemplate->setSenderName('Settv');
	//     	$emailTemplate->setSenderEmail('settv@settv.com.tw');
	//     	$emailTemplate->setTemplateSubject($email_subject);
	//     	$emailTemplate->send($email, null ,$emailTemplateVariables );
	//     	//var_dump($emailTemplate->send('dabochang@rivercrm.com', null ,$emailTemplateVariables ));
	//     	//var_dump($emailTemplate);
	//     	//exit();
	//     }


	/**
	 * Check osCommerce order view availability
	 *
	 * @deprecate after 1.6.0.0
	 * @param   array $order
	 * @return  bool
	 */
	/*
	 protected function _canViewOscommerceOrder($order)
	 {
	 return false;
	 }
	 */

	/**
	 * osCommerce Order view page
	 *
	 * @deprecate after 1.6.0.0
	 *
	 */
	/*
	 public function viewOldAction()
	 {
	 $this->_forward('noRoute');
	 return;
	 }
	 */


	/**
	 * Print Order Action , customize for print function.
	 */
	public function printAction()
	{
		if(!$this->validMasterOrder())
		{
			return;
		}
		$this->loadLayout('print');
		$this->renderLayout();
	}

	protected function validMasterOrder()
	{
		
		$orderId = (int) $this->getRequest()->getParam('order_id');
		
		if (!$orderId) {
			$this->_forward('noRoute');
			return false;
		}

		$orders = Mage::getModel('splitorder/items')->getOrdersByMasterOrderId($orderId);

		foreach ($orders as $order)
		{
			if ($this->_canViewOrder($order)) {
				Mage::register('current_order', $order);
				return true;
			} else {
				$this->_redirect('*/*/history');
			}
			return false;
		}

		$this->_redirect('*/*/history');

	}
}
