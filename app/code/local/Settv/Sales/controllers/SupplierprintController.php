﻿<?php

class Settv_Sales_SupplierprintController extends Mage_Core_Controller_Front_Action
{

    public function indexAction ()
    {
        $order_id = $this->getRequest()->getParam('order_id');
        $_order = Mage::getModel('sales/order')->load($order_id);
        $order_increment_id = $_order->getIncrementId();
        
        $SAddress = $_order->getShippingAddress();
        $BAddress = $_order->getBillingAddress();
        $_items = $_order->getItemsCollection();
        
        $Order_Date = Mage::app()->getLocale()->date(
                $_order->getCreatedAtStoreDate(), Zend_Date::DATE_SHORT);
        $Order_DMY = $Order_Date->toString('dd/MM/YYYY');
        
        $HtmlStr = "
			        <table width='620' border='0' align='center' cellpadding='0' cellspacing='0'>
			        <tbody><tr>
			        <td height='82' width='11' valign='middle'>&nbsp;</td>
			        <td height='82' valign='middle'><table width='594' border='0' cellspacing='0' cellpadding='0'>
			        <tbody><tr>
			        <td style=' ;' valign='middle' height='47'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
			        <tbody><tr>
			        <td style=' ' width='25' height='37'></td>
			        <td height='37'><img src='" .
                 Mage::getDesign()->getSkinUrl('imgs/default/logo_print.png') .
                 "' width='128' height='40'></td>
			        </tr>
			        </tbody></table></td>
			        </tr>
			        </tbody></table></td>
			        </tr>
			        <tr>
			        <td rowspan='2' valign='top'></td>
			        <td valign='top'><h4 style=' font-size:21px ;margin:0; color:#333 ;padding:0 20px  20px  20px'>訂單明細</h4></td>
			        </tr>
			        
					<tr>
			          <td valign='top' style='padding:0 0 10px 0'><table width='' border='0' align='center' cellpadding='0' style=' '>
			              <tbody><tr>
			                <td width='290' valign='top' style='padding:10px 5px 5px 5px; font-size:15px'>訂單編號: <span style='color:#F36'>#" .
                 $_order->getRealOrderId() .
                 "</span></td>
			                <td width='288' valign='top' style='padding:10px 5px 5px 5px; font-size:15px'>訂購日期: <span style='color:#F36'>" .
                 $Order_DMY . "</span></td>
			              </tr>
			              <tr valign='top'>
			                <td colspan='2' style='padding:10px 5px 5px 5px; font-size:15px'>收件地址: 
			                  <table width='570' border='0' cellpadding='0' cellspacing='0'>
			                    <tbody><tr>
			                      <td><span style='color:#F36'>" .
                 $SAddress->getName() . ' , ' . $SAddress->getPostcode() .
                 $SAddress->getCity() . $SAddress->getRegion() .
                 $SAddress->getStreetFull() . "</span></td>
			                    </tr>
			                    <tr>
			                      <td>&nbsp;</td>
			                    </tr>
			                </tbody></table></td>
			              </tr>
			              <tr valign='top'>
			                <td colspan='2' style='padding:10px 5px 5px 5px; font-size:15px'>帳單地址:
			                  <table width='570' border='0' cellpadding='0' cellspacing='0'>
			                    <tbody><tr>
			                      <td><span style='color:#F36'>" .
                 $BAddress->getName() . ' , ' . $BAddress->getPostcode() .
                 $BAddress->getCity() . $BAddress->getRegion() .
                 $BAddress->getStreetFull() .
                 "</span></td>
			                    </tr>
			                    <tr>
			                      <td>&nbsp;</td>
			                    </tr>
			                </tbody></table></td>
			              </tr>
			              <tr>
			                <td colspan='2' valign='top' style='padding:10px 5px 5px 5px; font-size:15px'>支付方式: <br/><span style='color:#F36'>";
        
        $CustomId = Mage::getModel('check/custom_order')->getIdByOrder(
                $order_id);
        $model = Mage::getModel('check/custom_order')->load($CustomId);
        $CustomField = $model->getData();
        
        $payment_method = $_order->getPayment()
            ->getMethodInstance()
            ->getTitle();
        $pincode = $model->getFamilymartPincode();
        
        if ($payment_method) {
            $HtmlStr .= $payment_method;
            if ($CustomField['cashdelivery'] != null) {
                if ($CustomField['cashdelivery'] == "0") {
                    $HtmlStr .= Mage::helper('sales')->__('- Cash');
                } else 
                    if ($CustomField['cashdelivery'] == "1") {
                        $HtmlStr .= Mage::helper('sales')->__('- Credit Card');
                    }
            }
            
            if (! empty($pincode)) {
                $order_created_at = $_order->getCreatedAt();
                
                $dateTime = new DateTime($order_created_at);
                $dateTime->add(date_interval_create_from_date_string('8 hours'));
                $dateTime->add(date_interval_create_from_date_string('3 days'));
                
                $HtmlStr .= "<form id='form1' name='form1' method='post' action='" . Mage::getStoreConfig(
                        'payment/paymentfamilymart/barcodeprintposturl', false) . "' target='_blank'>
                <input type='hidden' name='VD_ACCOUNT' value='" .
                         Mage::getStoreConfig(
                                'payment/paymentfamilymart/paccountno', false) . "'>
                					 	<input type='hidden' name='VD_ORDERNO' value=" .
                         $order_increment_id . ">
                					 	<input type='hidden' name='VD_PINCODE' value='" .
                         $pincode .
                         "'>
                					 	<input type='submit' id='sent_btn' value='' style='display:none'/>
                					 	繳費代碼：<a href=\"javascript:;\" onclick=\"document.getElementById('form1').submit();\">" .
                         $pincode . "</a>
                					 	</form>
                					 	<p>
                					 		繳費截止時間：" .
                         $dateTime->format("Y-m-d H:i:s") . "</p>";
            }
        }
        
        $HtmlStr .= "</span></td>
			              </tr>
			              <tr>
			                <td colspan='2' style='padding:10px 5px 5px 5px; font-size:15px'><table width='570' border='0' align='center' cellpadding='0' style=' '>
			                  <tbody><tr>
			                    <td style='padding:5px; line-height:2em;font-size:15px '>商品資訊：
			                      <table width='100%' border='0' align='center' cellpadding='1' cellspacing='1' bgcolor='#CCCCCC' style='font-size:12px '>
			                        <tbody><tr>
			                          <th width='170' align='left' valign='top' bgcolor='#FFFFFF'>商品名稱</th>
			                          <th align='left' valign='top' bgcolor='#FFFFFF'>商品號碼</th>
			                          <th width='90' align='left' valign='top' bgcolor='#FFFFFF'>價格</th>
			                          <th width='50' align='left' valign='top' bgcolor='#FFFFFF'>數量</th>
			                          <th width='60' align='left' valign='top' bgcolor='#FFFFFF'>小計</th>
			                        </tr>";
        
        $_count = $_items->count();
        $i = 0;
        foreach ($_items as $_item) {
            if ($_item->getParentItem())
                continue;
            if ($i == 0) {
                $HtmlStr .= "
				    						<tr>
				                          		<td align='left' valign='top' bgcolor='#FFFFFF'>" .
                         $_item->getName() .
                         "</td>
				                          		<td align='left' valign='top' bgcolor='#FFFFFF'>" .
                         $_item->getSku() .
                         "</td>
				                          		<td align='left' valign='top' bgcolor='#FFFFFF'>" .
                         round($_item->getPrice(), 0) .
                         "</td>
				                          		<td align='left' valign='top' bgcolor='#FFFFFF'>" .
                         $_item->getQtyToInvoice() .
                         "</td>
				                          		<td align='left' valign='top' bgcolor='#FFFFFF'>" . round(
                                ($_item->getPrice() * 1 *
                                 $_item->getQtyToInvoice()), 0) . "</td>
				                        	</tr>";
                $i ++;
            }
        }
        $HtmlStr .= "  </tbody></table></td>
							                  </tr>
							                </tbody></table></td>
							              </tr>
							            </tbody></table></td>
							        </tr>        
							        </tbody>
							    </table>
							    <table width='620' border='0' align='center' cellpadding='0' cellspacing='0'>
							        <tbody><tr>
							          <td width='11' valign='top'></td>
							          <td bgcolor='#FFFFFF' valign='top'><table width='560' border='0' align='center' cellpadding='0' cellspacing='0'>
							              <tbody><tr>
							                <td valign='top'><table width='100%' border='0' cellspacing='20' cellpadding='0'>
							                    <tbody><tr>
							                      <td valign='top' width='50%'></td>
							                      <td valign='top' style='font-size:13px ; color: #666'><p style='padding:0 ; margin: 0 0 2em 0'>您有任何有關帳號問題與相問疑問，請利用以下客服信箱或電話與我們聯絡</p>
							                        客服信箱：<a href='#' style='font-size:13px ; color: #03F'>service@igood.tw</a></td>
							                    </tr>
							                  </tbody></table>
							                  <h5 style='display:block; padding:10px ;  margin:0px;text-align:center ; color:#333; border-top: 1px dotted #ccc;font-size:15px'>愛買客感謝您的購買</h5></td>
							              </tr>
							            </tbody></table></td>
							        </tr>
							      </tbody></table>
							      <table width='100%' border='0' cellspacing='0' cellpadding='0'>
							        <tbody><tr>
							          <td height='25'></td>
							        </tr>
							      </tbody></table>";
        
        echo $HtmlStr;
    }

    public function familyprintAction ()
    {
        $order_id = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($order_id);
        $block = $this->getLayout()->createBlock('Mage_Core_Block_Template', 
                'familymart_block_name', 
                array(
                        'template' => 'sales/familyprint.phtml'
                ));
        
        $CustomId = Mage::getModel('check/custom_order')->getIdByOrder(
                $order->getId());
        $Custommodel = Mage::getModel('check/custom_order')->load($CustomId);
        $CustomArr = $Custommodel->getData();
        $fminfo = unserialize($CustomArr["shippingdescription"]);
        
        $fmcompanynum = Mage::getStoreConfig('carriers/familymart/fmcompanynum', 
                false);
        
        $address = $order->getShippingAddress();
        $data = array(
                "receiver" => $address->getName(),
                "realOrderId" => $order->getRealOrderId(),
                "fmcompanynum" => $fmcompanynum,
                "fmstorename" => $fminfo["fmstorename"],
                "fmstorecode" => $fminfo["fmstorecode"]
        );
        $block->setData("data", $data);
        
        $this->getResponse()->setBody($block->toHtml());
    }
}

?>