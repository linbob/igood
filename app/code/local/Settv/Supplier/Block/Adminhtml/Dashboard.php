<?php

class Settv_Supplier_Block_Adminhtml_Dashboard extends Mage_Adminhtml_Block_Dashboard
{
	protected function _prepareLayout()
	{
		parent::_prepareLayout();

		$user = Mage::getSingleton('admin/session')->getUser();
		$is_supplier = $user->getIsSupplier();

		if($is_supplier) {
			$this->unsetChild("sales");
			$this->unsetChild("lastSearches");
			$this->unsetChild("topSearches");

			$this->setChild('underliverdOrders',
					$this->getLayout()->createBlock('supplier/adminhtml_supplierdeliver_dashboard_grid')
			);
		}
	}
}
