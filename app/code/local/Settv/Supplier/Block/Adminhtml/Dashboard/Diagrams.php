<?php

class Settv_Supplier_Block_Adminhtml_Dashboard_Diagrams extends Mage_Adminhtml_Block_Dashboard_Diagrams
{
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->removeTab("amounts");
        
        return $this;
    }
}
