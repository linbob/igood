<?php

class Settv_Supplier_Block_Adminhtml_Dashboard_Orders_Grid extends Mage_Adminhtml_Block_Dashboard_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('lastOrdersGrid');
	}
	
	protected function _prepareCollection()
	{
		if (!Mage::helper('core')->isModuleEnabled('Mage_Reports')) {
			return $this;
		}

		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		// get supplier_id by user_id
		$supplierCollection = Mage::getModel('supplier/supplier')->getCollection()
		->addFieldToSelect('*')->addFieldToFilter('user_id', $user_id);

		$supplier_id = -1;
		if ($supplierCollection->getSize() > 0) {
			$supplier_id = $supplierCollection->getFirstItem()->getId();;
		}

		$collection = Mage::getResourceModel('reports/order_collection')
		->addItemCountExpr()
		->joinCustomerName('customer')
		->orderByCreatedAt();

		if($supplier_id && $supplier_id > 0) {
			$collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id'));
			$collection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplier_id);
		}

		if($this->getParam('store') || $this->getParam('website') || $this->getParam('group')) {
			if ($this->getParam('store')) {
				$collection->addAttributeToFilter('store_id', $this->getParam('store'));
			} else if ($this->getParam('website')){
				$storeIds = Mage::app()->getWebsite($this->getParam('website'))->getStoreIds();
				$collection->addAttributeToFilter('store_id', array('in' => $storeIds));
			} else if ($this->getParam('group')){
				$storeIds = Mage::app()->getGroup($this->getParam('group'))->getStoreIds();
				$collection->addAttributeToFilter('store_id', array('in' => $storeIds));
			}

			$collection->addRevenueToSelect();
		} else {
			$collection->addRevenueToSelect(true);
		}

		$this->setCollection($collection);

		return parent::_prepareCollection();
	}

	/**
	 * Prepares page sizes for dashboard grid with las 5 orders
	 *
	 * @return void
	 */
	protected function _preparePage()
	{
		$this->getCollection()->setPageSize($this->getParam($this->getVarNameLimit(), $this->_defaultLimit));
		// Remove count of total orders $this->getCollection()->setCurPage($this->getParam($this->getVarNamePage(), $this->_defaultPage));
	}
	
	protected function _prepareColumns()
	{
		$this->addColumn('customer', array(
				'header'    => $this->__('Customer'),
				'sortable'  => false,
				'index'     => 'customer',
				'default'   => $this->__('Guest'),
		));
	
		$this->addColumn('items', array(
				'header'    => $this->__('Items'),
				'align'     => 'right',
				'type'      => 'number',
				'sortable'  => false,
				'index'     => 'items_count'
		));
	
		$baseCurrencyCode = Mage::app()->getStore((int)$this->getParam('store'))->getBaseCurrencyCode();
	
		$this->addColumn('total', array(
				'header'    => $this->__('Grand Total'),
				'align'     => 'right',
				'sortable'  => false,
				'type'      => 'currency',
				'currency_code'  => $baseCurrencyCode,
				'index'     => 'revenue'
		));
	
		$this->setFilterVisibility(false);
		$this->setPagerVisibility(false);
	
		return parent::_prepareColumns();
	}
	
	public function getRowUrl($row)
	{
		$user = Mage::getSingleton('admin/session')->getUser();
		$is_supplier = $user->getIsSupplier();

		if($is_supplier) {
			$url = $this->getUrl('supplier/adminhtml_supplierorder/view', array('order_id'=>$row->getId()));;
		}
		else {
			$url = $this->getUrl('*/sales_order/view', array('order_id'=>$row->getId()));;
		}
		return $url;
	}
}
