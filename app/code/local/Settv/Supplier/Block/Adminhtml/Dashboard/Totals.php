<?php

class Settv_Supplier_Block_Adminhtml_Dashboard_Totals extends Mage_Adminhtml_Block_Dashboard_Totals
{

	protected function _prepareLayout()
	{
		if (!Mage::helper('core')->isModuleEnabled('Mage_Reports')) {
			return $this;
		}

		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		// get supplier_id by user_id
		$supplierCollection = Mage::getModel('supplier/supplier')->getCollection()
		->addFieldToSelect('*')->addFieldToFilter('user_id', $user_id);

		$supplier_id = -1;
		if ($supplierCollection->getSize() > 0) {
			$supplier_id = $supplierCollection->getFirstItem()->getId();;
		}

		$isFilter = $this->getRequest()->getParam('store') || $this->getRequest()->getParam('website') || $this->getRequest()->getParam('group');
		$period = $this->getRequest()->getParam('period', '24h');

		/* @var $collection Mage_Reports_Model_Mysql4_Order_Collection */
		$collection = Mage::getResourceModel('reports/order_collection')
		->addCreateAtPeriodFilter($period)
		->calculateTotals($isFilter);

		if($supplier_id && $supplier_id > 0) {
			//TODO: Jeff, please make the collection retrievals OO...
			$undeliverOrdercollection = Mage::getResourceModel('sales/order_grid_collection');
			 
			$StateArr= Mage::getModel('sales/order')->getCollection()
			->addFieldToSelect('entity_id')
			->addFieldToFilter('state', 'established')
			->getData();
			 
			$undeliverOrdercollection = Mage::getResourceModel('sales/order_grid_collection');
			$undeliverOrdercollection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id', 'shipstatus'));
			 
			$undeliverOrdercollection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplier_id)
			->addFieldToFilter('settv_sales_order_custom.shipstatus', 'Not Shipped')
			->addFieldToFilter('entity_id', array('in' => $StateArr));
			 
			// intermediate querty to fetch size of underlivered orders
			$undeliverOrdercollection->getSelect()->joinLeft('sales_flat_order_item', 'main_table.entity_id = sales_flat_order_item.order_id', array('name'))->where("sales_flat_order_item.product_type = 'simple'")->order("main_table.created_at ASC");
			$this->addTotal($this->__('Undelivered total orders'), $undeliverOrdercollection->getSize(), true);
			
 			// complete query to filter within 3 days of underlivered orders
			$undeliverOrdercollection->addFieldToFilter('main_table.created_at', array('from' => date('Y-m-d', strtotime("-3 days"))));
			$this->addTotal($this->__('Undelivered Orders within 3 days'), sizeof($undeliverOrdercollection->getData()), true);
			
			$collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id'));
			$collection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplier_id);
		}

		if ($this->getRequest()->getParam('store')) {
			$collection->addFieldToFilter('store_id', $this->getRequest()->getParam('store'));
		} else if ($this->getRequest()->getParam('website')){
			$storeIds = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
			$collection->addFieldToFilter('store_id', array('in' => $storeIds));
		} else if ($this->getRequest()->getParam('group')){
			$storeIds = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
			$collection->addFieldToFilter('store_id', array('in' => $storeIds));
		} elseif (!$collection->isLive()) {
			$collection->addFieldToFilter('store_id',
					array('eq' => Mage::app()->getStore(Mage_Core_Model_Store::ADMIN_CODE)->getId())
			);
		}

		$collection->load();

		$totals = $collection->getFirstItem();

		$this->addTotal($this->__('Quantity'), $totals->getQuantity()*1, true);
	}
}
