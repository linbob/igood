<?php
class Settv_Supplier_Block_Adminhtml_Notice_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		$this->_objectId = 'id';
		$this->_blockGroup = 'supplier';
		$this->_controller = 'adminhtml_notice';
		
		parent::__construct();

		$this->_updateButton('save', 'label', Mage::helper('supplier')->__('Save'));
		$this->_removeButton('delete');
		$this->_removeButton('back');
	}

	public function getHeaderText()
	{
		return Mage::helper('supplier')->__('Supplier Notice');
	}
}