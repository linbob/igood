<?php
class Settv_Supplier_Block_Adminhtml_Notice_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$model = Mage::registry('adminhtml_supplier');

		$form = new Varien_Data_Form();

		$fieldset = $form->addFieldset('supplier_form', array('legend'=>Mage::helper('supplier')->__('Supplier Notice')));

		$fieldset->addField('notice_area', 'textarea', array(
				'label'     => Mage::helper('supplier')->__('Supplier Notice'),
				'name'      => 'notice_area',
		));

		if (! empty($model)) {
			$data = $model->getData();
			$form->setValues($data);
		}
		$this->setForm($form);

		return parent::_prepareForm();
	}
}