<?php
class Settv_Supplier_Block_Adminhtml_Notification_Window extends Mage_Adminhtml_Block_Notification_Window
{
	protected function _construct()
	{
		parent::_construct();
	}

	/**
	 * Can we show notification window
	 *
	 * @return bool
	 */
	public function canShow()
	{
		if (!is_null($this->_available)) {
			return $this->_available;
		}

		if (!Mage::getSingleton('admin/session')->isFirstPageAfterLogin()) {
			$this->_available = false;
			return false;
		}

		if (!$this->isOutputEnabled('Mage_AdminNotification')) {
			$this->_available = false;
			return false;
		}

		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		$role_data = Mage::getModel('admin/user')->load($user_id)->getRole()->getData();
		$role_name = $role_data['role_name'];

		if (strcasecmp($role_name,"supplier")!=0){
			return false;
		}

		if (!$this->_isAllowed()) {
			$this->_available = false;
			return false;
		}

		if (is_null($this->_available)) {
			$this->_available = $this->isShow();
		}

		return $this->_available;
	}
}
