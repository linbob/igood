<?php
class Settv_Supplier_Block_Adminhtml_Product extends Mage_Adminhtml_Block_Widget_Grid_Container {
	public function __construct()
	{
		$this->_controller = 'adminhtml_product';
		$this->_blockGroup = 'supplier';
		$this->_headerText = Mage::helper('supplier')->__('Supplier Product');
		parent::__construct();
		$this->_removeButton('add');
	}
}