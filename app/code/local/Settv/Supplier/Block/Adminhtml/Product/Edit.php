<?php
class Settv_Supplier_Block_Adminhtml_Product_Edit extends Mage_Adminhtml_Block_Catalog_Product_Edit
{
	public function __construct()
	{
		parent::__construct();
	}

	protected function _prepareLayout()
	{
		$return= parent::_prepareLayout();

		$this->unsetChild('reset_button');
		$this->unsetChild('save_button');
		$this->unsetChild('save_and_edit_button');
		$this->unsetChild('delete_button');
		$this->unsetChild('duplicate_button');

		return $return;
	}
}
