<?php

class Settv_Supplier_Block_Adminhtml_Product_Grid extends Mage_Adminhtml_Block_Report_Grid
{
	protected $_subReportSize = 0;

	public function __construct() {
		parent::__construct();
		$this->setId('settvProductGrid');
		$this->setTemplate('widget/report/grid.phtml');
	}

	public function _prepareCollection()
	{
		$filter = $this->getParam($this->getVarNameFilter(), null);

		if (is_null($filter)) {
			$filter = $this->_defaultFilter;
		}

		if (is_string($filter)) {
			$data = array();
			$filter = base64_decode($filter);
			parse_str(urldecode($filter), $data);

			if (!isset($data['report_from'])) {
				// getting all reports from 2001 year
				$date = new Zend_Date(mktime(0,0,0,1,1,2001));
				$data['report_from'] = $date->toString($this->getLocale()->getDateFormat('short'));
			}

			if (!isset($data['report_to'])) {
				// getting all reports from 2001 year
				$date = new Zend_Date();
				$data['report_to'] = $date->toString($this->getLocale()->getDateFormat('short'));
			}

			$this->_setFilterValues($data);
		} else if ($filter && is_array($filter)) {
			$this->_setFilterValues($filter);
		} else if(0 !== sizeof($this->_defaultFilter)) {
			$this->_setFilterValues($this->_defaultFilter);
		}

		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		// get supplier_id by user_id
		$collection = Mage::getModel('supplier/supplier')->getCollection()
		->addFieldToSelect('*')->addFieldToFilter('user_id', $user_id);

		$role_name = Mage::getModel('admin/user')->load($user_id)->getRole()->getRoleName();

		$supplier_id = -1;
		if ($collection->getSize() > 0) {
			$supplier_model = current($collection->load()->getIterator());
			$supplier_attribute_id = $supplier_model->getAttributeId();
		}

		if ($role_name=="Administrators"){
			$collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect('sku')
			->addAttributeToSelect('name')
			->addAttributeToSelect('supplier');
		}else{
			$collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect('sku')
			->addAttributeToSelect('name')
			->addAttributeToSelect('supplier')
			->addFieldToFilter('supplier', $supplier_attribute_id);
		}

		if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
			$collection->joinField('qty',
					'cataloginventory/stock_item',
					'qty',
					'product_id=entity_id',
					'{{table}}.stock_id=1',
					'left');
		}

		$store = $this->_getStore();
		if ($store->getId()) {
			$adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
			$collection->addStoreFilter($store);
			$collection->joinAttribute('name', 'catalog_product/name', 'entity_id', null, 'inner', $adminStore);
			$collection->joinAttribute('custom_name', 'catalog_product/name', 'entity_id', null, 'inner', $store->getId());
			$collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner', $store->getId());
			$collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner', $store->getId());
			$collection->joinAttribute('price', 'catalog_product/price', 'entity_id', null, 'left', $store->getId());
		}
		else {
			$collection->addAttributeToSelect('price');
			$collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
			$collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
		}

		$collection->setPeriod($this->getFilter('report_period'));

		if ($this->getFilter('report_from') && $this->getFilter('report_to')) {
			/**
			 * Validate from and to date
			 */
			try {
				$from = $this->getLocale()->date($this->getFilter('report_from'), Zend_Date::DATE_SHORT, null, false);
				$to   = $this->getLocale()->date($this->getFilter('report_to'), Zend_Date::DATE_SHORT, null, false);

				$collection->setInterval($from, $to);
			}
			catch (Exception $e) {
				$this->_errors[] = Mage::helper('reports')->__('Invalid date specified.');
			}
		}

		if ($this->getSubReportSize() !== null) {
			$collection->setPageSize($this->getSubReportSize());
		}

		$this->setCollection($collection);

		Mage::dispatchEvent('adminhtml_widget_grid_filter_collection',
		array('collection' => $this->getCollection(), 'filter_values' => $this->_filterValues)
		);


	}

	protected function _prepareColumns() {

		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		$role_name = Mage::getModel('admin/user')->load($user_id)->getRole()->getRoleName();

		Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();

		$this->addColumn('entity_id', array(
				'header'=> Mage::helper('catalog')->__('ID'),
				'width' => '50px',
				'type'  => 'number',
				'index' => 'entity_id'
		));

		$this->addColumn('name', array(
				'header'=> Mage::helper('catalog')->__('Name'),
				'type'  => 'text',
				'index' => 'name'
		));

		if ($role_name=="Administrators"){
			$this->addColumn('supplier_name', array(
					'header'=> Mage::helper('supplier')->__('Supplier'),
					'type'  => 'text',
					'index' => 'supplier',
					'renderer' => 'Settv_Common_Block_Render_RenderSupplier',
			));
		}

		$this->addColumn('sku', array(
				'header'=> Mage::helper('sales')->__('SKU'),
				'type'  => 'text',
				'index' => 'sku'
		));

		$this->addColumn('order_count', array(
				'header' => Mage::helper('sales')->__('Total Orders'),
				'index' => 'success_count',
				'type'  => 'number',
				'filter' => false,
				'sortable'  => false
		));

		$this->addColumn('return_count', array(
				'header' => Mage::helper('sales')->__('Total Refunded Orders'),
				'index' => 'return_count',
				'type'  => 'number',
				'filter' => false,
				'sortable'  => false
		));

		if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
			$this->addColumn('qty',
					array(
							'header'=> Mage::helper('sales')->__('Stock Qty'),
							'type'  => 'number',
							'index' => 'qty'
					));
		}

		$store = $this->_getStore();
		$this->addColumn('price', array(
				'header'=> Mage::helper('catalog')->__('Price'),
				'index' => 'order_id',
				'type'  => 'currency',
				'currency_code' => $store->getBaseCurrency()->getCode(),
				'renderer' => new Settv_Common_Block_Render_ProductPrice(),
		));

		$this->addColumn('total_qty_ordered', array(
				'header' => Mage::helper('sales')->__('QTY'),
				'index' => 'total_qty_ordered',
				'type'  => 'number',
				'total' => 'sum',
				'filter' => false,
				'sortable'  => false
		));

		$this->addColumn('sum_grand_total', array(
				'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
				'index' => 'sum_grand_total',
				'type'  => 'price',
				'currency_code' => $store->getBaseCurrency()->getCode(),
				'filter' => false,
				'sortable'  => false
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)	{
		return $this->getUrl('*/*/edit', array(
				'store'=>$this->getRequest()->getParam('store'),
				'id'=>$row->getId())
		);
	}

	protected function _getStore()
	{
		$storeId = (int) $this->getRequest()->getParam('store', 0);
		return Mage::app()->getStore($storeId);
	}

	public function getRefreshButtonHtml()
	{
		return $this->getChildHtml('refresh_button');
	}

	public function getReport($from, $to)
	{
		if ($from == '') {
			$from = $this->getFilter('report_from');
		}
		if ($to == '') {
			$to = $this->getFilter('report_to');
		}

		$collection = $this->getCollection();

		$collection->getSelect()
		-> where ("success_item.success_count IS NOT NULL")
		-> order( array('sum_grand_total DESC') );

		$collection->getSelect()->joinLeft(
				array('success_item' => new Zend_Db_Expr("(SELECT
						sales_flat_order_item.product_id,
						Count(sales_flat_order_item.product_id) as success_count
						FROM sales_flat_order
						LEFT JOIN `sales_flat_order_item` ON sales_flat_order.entity_id = sales_flat_order_item.order_id
						WHERE (sales_flat_order.state <> 'canceled' and sales_flat_order.state <> 'failed' and sales_flat_order.created_at>='{$from}' and sales_flat_order.created_at<='{$to}')
						Group by sales_flat_order_item.product_id)")
				), 'e.entity_id = success_item.product_id',
				array('success_count'));

		$collection->getSelect()->joinLeft(
				array('return_item' => new Zend_Db_Expr("(SELECT
						sales_flat_order_item.product_id,
						Count(sales_flat_order_item.product_id) as return_count
						FROM sales_flat_order
						LEFT JOIN `sales_flat_order_item` ON sales_flat_order.entity_id = sales_flat_order_item.order_id
						WHERE (sales_flat_order.state = 'return_succeeded' and sales_flat_order.created_at>='{$from}' and sales_flat_order.created_at<='{$to}')
						Group by sales_flat_order_item.product_id)")
				), 'e.entity_id = return_item.product_id',
				array('IFNULL(return_count,0) as return_count'));

		$collection->getSelect()->joinLeft(
				array('total_item' => new Zend_Db_Expr("(SELECT
						sales_flat_order_item.product_id,
						sales_flat_order_item.order_id,
						IFNULL(SUM(sales_flat_order_item.qty_ordered), 0) AS total_qty_ordered,
						IFNULL(SUM(sales_flat_order.grand_total), 0) AS sum_grand_total
						FROM sales_flat_order
						LEFT JOIN `sales_flat_order_item` ON sales_flat_order.entity_id = sales_flat_order_item.order_id
						WHERE (sales_flat_order.state <> 'canceled' and sales_flat_order.state <> 'failed' and sales_flat_order.state <> 'return_succeeded' and sales_flat_order.created_at>='{$from}' and sales_flat_order.created_at<='{$to}')
						Group by sales_flat_order_item.product_id)")
				), 'e.entity_id = total_item.product_id',
				array('IFNULL(total_qty_ordered,0) as total_qty_ordered','IFNULL(sum_grand_total,0) as sum_grand_total','order_id'));

		$this->setCollection($collection);

		return $this -> getCollection();
	}

	/**
	 * Retrieve grid as CSV
	 *
	 * @return unknown
	 */
	public function getCsv()
	{
		$csv = '';
		$this->_prepareGrid();

		$data = array();
		foreach ($this->_columns as $column) {
			if (!$column->getIsSystem()) {
				$data[] = '"'.$column->getHeader().'"';
			}
		}
		$csv.= implode(',', $data)."\n";

		foreach ($this->getCollection()->getIntervals() as $_index=>$_item) {
			$report = $this->getReport($_item['start'], $_item['end']);
			foreach ($report as $_subIndex=>$_subItem) {
				$data = array();
				foreach ($this->_columns as $column) {
					if (!$column->getIsSystem()) {
						$data[] = '"' . str_replace(
								array('"', '\\'),
								array('""', '\\\\'),
								$column->getRowField($_subItem)
						) . '"';
					}
				}
				$csv.= implode(',', $data)."\n";
			}
		}

		return $csv;
	}

	/**
	 * Retrieve grid as Excel Xml
	 *
	 * @return unknown
	 */
	public function getExcel($filename = '')
	{
		$this->_prepareGrid();

		$data = array();
		$row = array();
		foreach ($this->_columns as $column) {
			if (!$column->getIsSystem()) {
				$row[] = $column->getHeader();
			}
		}
		$data[] = $row;

		foreach ($this->getCollection()->getIntervals() as $_index=>$_item) {
			$report = $this->getReport($_item['start'], $_item['end']);
			foreach ($report as $_subIndex=>$_subItem) {
				$row = array();
				foreach ($this->_columns as $column) {
					if (!$column->getIsSystem()) {
						$row[] = $column->getRowField($_subItem);
					}
				}
				$data[] = $row;
			}
		}

		$xmlObj = new Varien_Convert_Parser_Xml_Excel();
		$xmlObj->setVar('single_sheet', $filename);
		$xmlObj->setData($data);
		$xmlObj->unparse();

		return $xmlObj->getData();
	}
}