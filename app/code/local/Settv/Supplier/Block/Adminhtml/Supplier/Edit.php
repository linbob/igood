<?php
class Settv_Supplier_Block_Adminhtml_Supplier_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		$this->_objectId = 'id';
		$this->_blockGroup = 'supplier';
		$this->_controller = 'adminhtml_supplier';
		
		parent::__construct();

		$this->_updateButton('save', 'label', Mage::helper('supplier')->__('Save'));
		// 20120713 remove delete button by Sean S
		$this->_removeButton('delete');
		// 20120713 remove delete button by Sean E
		
		//$this->_updateButton('delete', 'label', Mage::helper('supplier')->__('Delete'));

// 		$this->_addButton('saveandcontinue', array(
// 				'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
// 				'onclick'   => 'saveAndContinueEdit()',
// 				'class'     => 'save',
// 		), -100);
	}

	public function getHeaderText()
	{
		return Mage::helper('supplier')->__('Supplier Basic Information');
	}
}