<?php
class Settv_Supplier_Block_Adminhtml_Supplier_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$model = Mage::registry('adminhtml_supplier');

		$form = new Varien_Data_Form();

		$fieldset = $form->addFieldset('supplier_form', array('legend'=>Mage::helper('supplier')->__('Item information')));

		if (! empty($model)) {
			if ($model->getId()) {
				$fieldset->addField('id', 'hidden', array(
						'name' => 'id',
				));
			} else {
				if (! $model->hasData('is_active')) {
					$model->setIsActive(1);
				}
			}
		}

		$fieldset->addField('name', 'text', array(
				'label'     => Mage::helper('supplier')->__('Name'),
				'class'     => 'required-entry',
				'required'  => true,
				'name'      => 'name',
		));

		$fieldset->addField('ubn', 'text', array(
				'label'     => Mage::helper('supplier')->__('UBN :'),
				'name'      => 'ubn',
		));


		$fieldset->addField('postcode', 'text', array(
				'label'     => Mage::helper('supplier')->__('Zip/Postal Code'),
				'name'      => 'postcode',
		));
		$fieldset->addField('region', 'text', array(
				'label'     => Mage::helper('supplier')->__('Supplier Province'),
				'name'      => 'region',
		));
		$fieldset->addField('city', 'text', array(
				'label'     => Mage::helper('supplier')->__('Supplier City'),
				'name'      => 'city',
		));
		$fieldset->addField('street', 'text', array(
				'label'     => Mage::helper('supplier')->__('Street Address'),
				'name'      => 'street',
		));

		$fieldset->addField('telephone', 'text', array(
				'label'     => Mage::helper('supplier')->__('Telephone'),
				'name'      => 'telephone',
		));
		$fieldset->addField('fax', 'text', array(
				'label'     => Mage::helper('supplier')->__('Fax'),
				'name'      => 'fax',
		));


		$fieldset->addField('contact_name', 'text', array(
				'label'     => Mage::helper('supplier')->__('Contact Name'),
				'name'      => 'contact_name',
				'required'  => true,
		));
		$fieldset->addField('contact_telephone', 'text', array(
				'label'     => Mage::helper('supplier')->__('Contact Telephone'),
				'name'      => 'contact_telephone',
				'required'  => true,
		));
		$fieldset->addField('contact_email', 'text', array(
				'label'     => Mage::helper('supplier')->__('Contact Email'),
				'name'      => 'contact_email',
				'required'  => true,
		));

		$fieldset->addField('supplier_code', 'text', array(
				'label'     => Mage::helper('supplier')->__('Supplier Code'),
				'name'      => 'supplier_code',
				'required'  => true, 
		));
		
		$fieldset->addField('customer_service', 'text', array(
				'label'     => Mage::helper('supplier')->__('Customer Service'),
				'name'      => 'customer_service',
		));
			
		$fieldset->addField('customer_service_telephone', 'text', array(
				'label'     => Mage::helper('supplier')->__('Customer Service Telephone'),
				'name'      => 'customer_service_telephone',
				'class'		=> 'validate-number',
		));
			
		$fieldset->addField('customer_service_email', 'text', array(
				'label'     => Mage::helper('supplier')->__('Customer Service Email'),
				'name'      => 'customer_service_email',
				'class'		=> 'validate-email',
		));
			
		$fieldset->addField('accountant', 'text', array(
				'label'     => Mage::helper('supplier')->__('Accountant'),
				'name'      => 'accountant',
		));
			
		$fieldset->addField('accountant_telephone', 'text', array(
				'label'     => Mage::helper('supplier')->__('Accountant Telephone'),
				'name'      => 'accountant_telephone',
				'class'		=> 'validate-number',
		));
			
		$fieldset->addField('accountant_email', 'text', array(
				'label'     => Mage::helper('supplier')->__('Accountant Email'),
				'name'      => 'accountant_email',
				'class'		=> 'validate-email',
		));

		if (! empty($model)) {
			$fieldset->addField('password', 'password', array(
					'name'  => 'new_password',
					'label' => Mage::helper('adminhtml')->__('New Password'),
					'id'    => 'new_pass',
					'title' => Mage::helper('adminhtml')->__('New Password'),
					'class' => 'input-text validate-admin-password',
			));

			$fieldset->addField('confirmation', 'password', array(
					'name'  => 'password_confirmation',
					'label' => Mage::helper('adminhtml')->__('Password Confirmation'),
					'id'    => 'confirmation',
					'class' => 'input-text validate-cpassword',
			));
		} else {
			$fieldset->addField('password', 'password', array(
					'name'  => 'password',
					'label' => Mage::helper('adminhtml')->__('Password'),
					'id'    => 'customer_pass',
					'title' => Mage::helper('adminhtml')->__('Password'),
					'class' => 'input-text required-entry validate-admin-password',
					'required' => true,
			));
			$fieldset->addField('confirmation', 'password', array(
					'name'  => 'password_confirmation',
					'label' => Mage::helper('adminhtml')->__('Password Confirmation'),
					'id'    => 'confirmation',
					'title' => Mage::helper('adminhtml')->__('Password Confirmation'),
					'class' => 'input-text required-entry validate-cpassword',
					'required' => true,
			));
		}
		//}

		//if (Mage::getSingleton('admin/session')->getUser()->getId() != $model->getUserId()) {
		$fieldset->addField('is_active', 'select', array(
				'name'  	=> 'is_active',
				'label' 	=> Mage::helper('adminhtml')->__('Account Status'),
				'id'    	=> 'is_active',
				'title' 	=> Mage::helper('adminhtml')->__('Account Status'),
				'class' 	=> 'input-select',
				'style'		=> 'width: 80px',
				'options'	=> array('1' => Mage::helper('adminhtml')->__('Active'), '0' => Mage::helper('adminhtml')->__('Inactive')),
		));

		if (! empty($model)) {
			$data = $model->getData();
			$form->setValues($data);
		}
		$this->setForm($form);

		return parent::_prepareForm();
	}
}