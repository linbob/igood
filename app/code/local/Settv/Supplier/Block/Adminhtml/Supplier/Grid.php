<?php

class Settv_Supplier_Block_Adminhtml_Supplier_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct() {
		parent::__construct();
		$this->setId('supplierGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$collection = Mage::getModel('supplier/supplier')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('id', array(
				'header'    => Mage::helper('supplier')->__('ID'),
				'align'     =>'right',
				'width'     => '10px',
				'index'     => 'id',
		));
		


		$this->addColumn('name', array(
				'header'    => Mage::helper('supplier')->__('Name'),
				'align'     =>'left',
				'index'     => 'name',
		));

		$this->addColumn('contact_name', array(
				'header'    => Mage::helper('supplier')->__('Contact Name'),
				'width'     => '120px',
				'index'     => 'contact_name',
		));
		
		$this->addColumn('contact_telephone', array(
				'header'    => Mage::helper('supplier')->__('Contact Telephone'),
				'width'     => '100px',
				'index'     => 'contact_telephone',
		));
		
		$this->addColumn('contact_email', array(
				'header'    => Mage::helper('supplier')->__('Contact Email'),
				'width'     => '120px',
				'index'     => 'contact_email',
		));
		
		$this->addColumn('supplier_code', array(
				'header'    => Mage::helper('supplier')->__('Supplier Code'),
				'width'     => '80px',
				'index'     => 'supplier_code',
		));
		
		$this->addColumn('is_active', array(
				'header'    => Mage::helper('supplier')->__('Status'),
				'width'     => '50px',
				'index'     => 'is_active',
				'type'      => 'options',
				'options'   => array('1' => Mage::helper('supplier')->__('Active'), '0' => Mage::helper('supplier')->__('Inactive'))
		));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));
		
		return parent::_prepareColumns();
	}

	public function getRowUrl($row)	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

}