<?php
class Settv_Supplier_Block_Adminhtml_Supplierdeliver extends Mage_Adminhtml_Block_Widget_Grid_Container {
	public function __construct()
	{
		$this->_controller = 'adminhtml_supplierdeliver';
		$this->_blockGroup = 'supplier';
		$this->_headerText = Mage::helper('supplier')->__('Supplier Undelivered Order');
		// $this->_addButtonLabel = Mage::helper('supplier')->__('Add Supplier');
		parent::__construct();
		$this->_removeButton('add');
	}
}