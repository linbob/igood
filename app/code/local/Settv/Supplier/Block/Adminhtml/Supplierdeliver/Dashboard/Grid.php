<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml dashboard recent orders grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Settv_Supplier_Block_Adminhtml_Supplierdeliver_Dashboard_Grid extends Mage_Adminhtml_Block_Dashboard_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('lastOrdersGrid');
	}

	protected function _prepareCollection()
	{
		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		// get supplier_id by user_id
		$collection = Mage::getModel('supplier/supplier')->getCollection()
		->addFieldToSelect('*')->addFieldToFilter('user_id', $user_id);

		$supplier_id = -1;
		if ($collection->getSize() > 0) {
			$supplier_model = current($collection->load()->getIterator());
			$supplier_id = $supplier_model->getId();
		}

		$StateArr= Mage::getModel('sales/order')->getCollection()
		->addFieldToSelect('entity_id')
		->addFieldToFilter('state', 'established')
		->addFieldToFilter('created_at', array('from' => date("Y-m-d", strtotime("-3 days"))))
		->getData();

		$collection = Mage::getResourceModel('sales/order_grid_collection');
		$collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id', 'shipstatus'));

		$collection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplier_id)
		->addFieldToFilter('settv_sales_order_custom.shipstatus', 'Not Shipped')
		->addFieldToFilter('entity_id', array('in' => $StateArr));

		$collection->getSelect()->joinLeft('sales_flat_order_item', 'main_table.entity_id = sales_flat_order_item.order_id', array('name'))->where("sales_flat_order_item.product_type = 'simple'")->order("main_table.created_at ASC");



		$this->setCollection($collection);

		return parent::_prepareCollection();
	}

	/**
	 * Prepares page sizes for dashboard grid
	 *
	 * @return void
	 */
	protected function _preparePage()
	{
		$this->setDefaultLimit(20);
		$this->getCollection()->setPageSize($this->getParam($this->getVarNameLimit(), $this->_defaultLimit));
	}

	protected function _prepareColumns()
	{

		$this->addColumn('real_order_id', array(
				'header'=> Mage::helper('sales')->__('Order #'),
				'sortable'  => false,
				'type'  => 'text',
				'index' => 'increment_id',
		));

		$this->addColumn('created_at', array(
				'header' => Mage::helper('sales')->__('Purchased On'),
				'sortable'  => false,
				'index' => 'created_at',
				'type' => 'datetime',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
		));

		$this->addColumn('billing_name', array(
				'header' => Mage::helper('sales')->__('Bill to Name'),
				'sortable'  => false,
				'index' => 'billing_name',
		));

		$this->addColumn('shipping_name', array(
				'header' => Mage::helper('sales')->__('Ship to Name'),
				'sortable'  => false,
				'index' => 'shipping_name',
		));

		$this->setFilterVisibility(false);
		$this->setPagerVisibility(false);

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('supplier/adminhtml_supplierdeliver/view', array('order_id'=>$row->getId()));
	}
}
