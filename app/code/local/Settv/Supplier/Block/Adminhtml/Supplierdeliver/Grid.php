<?php

class Settv_Supplier_Block_Adminhtml_Supplierdeliver_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct() {
		parent::__construct();
		$this->setDefaultSort('created_at');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		// get supplier_id by user_id
		$collection = Mage::getModel('supplier/supplier')->getCollection()
		->addFieldToSelect('*')->addFieldToFilter('user_id', $user_id);

		$supplier_id = -1;
		if ($collection->getSize() > 0) {
			$supplier_model = current($collection->load()->getIterator());
			$supplier_id = $supplier_model->getId();
		}

		$collection = Mage::getResourceModel('sales/order_grid_collection');
		$collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id', 'shipstatus','order_id'));

		$collection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplier_id)
		->addFieldToFilter('settv_sales_order_custom.shipstatus', 'Not Shipped');

		$collection->getSelect()->joinLeft(array('sfoa'=>'sales_flat_order_address'), 'main_table.entity_id = sfoa.parent_id',array('postcode','city','region','street'))
		->where("sfoa.address_type = 'shipping'");
		$collection->getSelect()->joinLeft('sales_flat_order', 'main_table.entity_id = sales_flat_order.entity_id', array('total_qty_ordered','shipping_address_id','shipping_method'))
		->where("sales_flat_order.state = 'established'");
		$collection->getSelect()->joinLeft('sales_flat_order_item', 'main_table.entity_id = sales_flat_order_item.order_id', array('name'))->where("sales_flat_order_item.product_type = 'simple'");;

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {

		$this->addColumn('real_order_id', array(
				'header'=> Mage::helper('sales')->__('Order #'),
				'width' => '80px',
				'type'  => 'text',
				'index' => 'increment_id',
				'filter_index' => 'main_table.increment_id',
		));

		$this->addColumn('created_at', array(
				'header' => Mage::helper('sales')->__('Purchased On'),
				'index' => 'created_at',
				'type' => 'datetime',
				'width' => '100px',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'main_table.created_at',
		));

		$this->addColumn('product_name', array(
				'header'=> Mage::helper('sales')->__('Product Name'),
				'type'  => 'text',
				'index' => 'name',
		));

		$this->addColumn('billing_name', array(
				'header' => Mage::helper('sales')->__('Bill to Name'),
				'index' => 'billing_name',
		));

		$this->addColumn('shipping_name', array(
				'header' => Mage::helper('sales')->__('Ship to Name'),
				'index' => 'shipping_name',
		));

		$this->addColumn('shipping_address', array(
				'header' => Mage::helper('sales')->__('Shipping Address'),
				'index' => 'street',
				'renderer' => 'Settv_Common_Block_Render_RenderAddress',
				'filter_condition_callback' => array($this, '_addressFilter'),
		));

		$this->addColumn('base_grand_total', array(
				'header' => Mage::helper('sales')->__('G.T. (Base)'),
				'type'  => 'currency',
				'currency' => 'base_currency_code',
				'filter_index' => 'main_table.base_grand_total',
				'index' => 'order_id',
				'renderer' => new Settv_Common_Block_Render_ProductPrice(),
				'sortable' => false,
				'filter' => false,
		));

		$this->addColumn('total_qty_ordered', array(
				'header' => Mage::helper('sales')->__('QTY'),
				'index' => 'total_qty_ordered',
				'type'  => 'number',
				'total' => 'sum',
		));

		$this->addColumn('grand_total', array(
				'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
				'index' => 'grand_total',
				'type'  => 'currency',
				'currency' => 'order_currency_code',
				'filter_index' => 'main_table.grand_total',
		));

		$this->addColumn('status', array(
				'header' => Mage::helper('sales')->__('Status'),
				'index' => 'status',
				'type'  => 'options',
				'width' => '70px',
				'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
				'filter' => false,
		));

		$this->addColumn('shipstatus', array(
				'header' => Mage::helper('sales')->__('Ship Status'),
				'index' => 'shipstatus',
				'type' => 'options',
				'options' => array( 'Shipped' => Mage::helper('sales')->__('Shipped'),'Not Shipped' => Mage::helper('sales')->__('Not Shipped'),
						'Arrived' => Mage::helper('sales')->__('Arrived'),'Rejected' => Mage::helper('sales')->__('Rejected'),
						'Return Received' => Mage::helper('sales')->__('Return Received'),'Return Rejected' => Mage::helper('sales')->__('Return Rejected')),
				'filter' => false,
		));

		$methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
		$options = array();
		foreach($methods as $_code => $_method)
		{
			$options[$_code.'_'] = Mage::getStoreConfig("carriers/$_code/title");
		}
		$options["familymart_"]=Mage::helper('sales')->__('Familymart Shipping');

		$this->addColumn('logistic_method', array(
				'header' => Mage::helper('sales')->__('Logistics Type'),
				'index' => 'shipping_method',
				'renderer' => new Settv_Common_Block_Render_RenderShipping(),
				'type' => 'options',
				'options' => $options,
		));

		if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
			$this->addColumn('action',
					array(
							'header'    => Mage::helper('sales')->__('Action'),
							'width'     => '50px',
							'type'      => 'action',
							'getter'     => 'getId',
							'actions'   => array(
									array(
											'caption' => Mage::helper('sales')->__('View'),
											'url'     => array('base'=>'*/sales_order/view'),
											'field'   => 'order_id'
									)
							),
							'filter'    => false,
							'sortable'  => false,
							'index'     => 'stores',
							'is_system' => true,
					));
		}

		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)	{
		return $this->getUrl('*/*/view', array('order_id' => $row->getId()));
	}

	protected function _addressFilter($collection, $column)
	{
		if (!$value = $column->getFilter()->getValue()) {
			return $this;
		}

		$this->getCollection()->getSelect()->where(
				"sfoa.city like ?
				OR sfoa.region like ?
				OR sfoa.street like ?
				OR sfoa.postcode like ?"
				, "%$value%");
		return $this;
	}
}