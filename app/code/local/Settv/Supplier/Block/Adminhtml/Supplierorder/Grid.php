<?php

class Settv_Supplier_Block_Adminhtml_Supplierorder_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct() {
		parent::__construct();
		$this->setDefaultSort('created_at');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		// get supplier_id by user_id
		$collection = Mage::getModel('supplier/supplier')->getCollection()
		->addFieldToSelect('*')->addFieldToFilter('user_id', $user_id);

		$supplier_id = -1;
		if ($collection->getSize() > 0) {
			$supplier_model = current($collection->load()->getIterator());
			$supplier_id = $supplier_model->getId();
		}

		$collection = Mage::getResourceModel('sales/order_grid_collection')
		->addFieldToSelect('increment_id')
		->addFieldToSelect('created_at')
		->addFieldToSelect('billing_name')
		->addFieldToSelect('shipping_name')
		->addFieldToSelect('grand_total')
		->addFieldToSelect('status');
		$collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id', 'shipstatus'));
		$collection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplier_id);

		$collection->getSelect()->joinLeft(array('sfoa'=>'sales_flat_order_address'), 'main_table.entity_id = sfoa.parent_id',array('postcode','city','region','street'))
		->where("sfoa.address_type = 'shipping'");
		$collection->getSelect()->joinLeft('sales_flat_order', 'main_table.entity_id = sales_flat_order.entity_id', array('shipping_address_id','shipping_method'))
		->where("sales_flat_order.state <> 'failed'")
		->where("sales_flat_order.state <> 'cc_processing'");
		$collection->getSelect()->joinLeft('sales_flat_order_item', 'main_table.entity_id = sales_flat_order_item.order_id', array('name','qty_ordered'))
		->where("sales_flat_order_item.product_type = 'simple'");
		$collection->getSelect()->joinLeft('sales_flat_order_payment', 'main_table.entity_id = sales_flat_order_payment.parent_id',array('method'));
		$collection->getSelect()->joinLeft('settv_sales_logistics_custom as sslc', 'settv_sales_order_custom.logistics_id = sslc.id', array('Max(`sslc`.`created_at`) as logistics_created','logistics_num'))
		->group('main_table.increment_id');

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {

		$this->addColumn('real_order_id', array(
				'header'=> Mage::helper('sales')->__('Order #'),
				'width' => '80px',
				'type'  => 'text',
				'index' => 'increment_id',
				'filter_index' => 'main_table.increment_id',
		));

		$this->addColumn('created_at', array(
				'header' => Mage::helper('sales')->__('Purchased On'),
				'index' => 'created_at',
				'type' => 'datetime',
				'width' => '100px',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'main_table.created_at',
		));

		$this->addColumn('product_name', array(
				'header'=> Mage::helper('sales')->__('Product Name'),
				'type'  => 'text',
				'index' => 'name',
		));

		$this->addColumn('billing_name', array(
				'header' => Mage::helper('sales')->__('Bill to Name'),
				'index' => 'billing_name',
		));

		$this->addColumn('shipping_name', array(
				'header' => Mage::helper('sales')->__('Ship to Name'),
				'index' => 'shipping_name',
		));

		$this->addColumn('shipping_address', array(
				'header' => Mage::helper('sales')->__('Shipping Address'),
				'index' => 'street',
				'renderer' => 'Settv_Common_Block_Render_RenderAddress',
				'filter_condition_callback' => array($this, '_addressFilter'),
		));

		$this->addColumn('total_qty_ordered', array(
				'header' => Mage::helper('sales')->__('QTY'),
				'index' => 'qty_ordered',
				'type'  => 'number',
				'total' => 'sum',
		));

		$store = $this->_getStore();
		$this->addColumn('grand_total', array(
				'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
				'index' => 'grand_total',
				'type'  => 'currency',
				'currency_code' => $store->getBaseCurrency()->getCode(),
				'filter_index' => 'main_table.grand_total',
		));

		$this->addColumn('status', array(
				'header' => Mage::helper('sales')->__('Status'),
				'index' => 'status',
				'type'  => 'options',
				'width' => '70px',
				'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
				'filter_index' => 'main_table.status',
		));

		$this->addColumn('shipstatus', array(
				'header' => Mage::helper('sales')->__('Ship Status'),
				'index' => 'shipstatus',
				'type' => 'options',
				'options' => array( 'Shipped' => Mage::helper('sales')->__('Shipped'),'Not Shipped' => Mage::helper('sales')->__('Not Shipped'),
						'Arrived' => Mage::helper('sales')->__('Arrived'),'Rejected' => Mage::helper('sales')->__('Rejected'),
						'Return Received' => Mage::helper('sales')->__('Return Received'),'Return Rejected' => Mage::helper('sales')->__('Return Rejected')),
		));

		$this->addColumn('forward_at', array(
				'header' => Mage::helper('sales')->__('forward at'),
				'index' => 'logistics_created',
				'type' => 'datetime',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'sslc.created_at',
		));

		$payments = Mage::getSingleton('payment/config')->getActiveMethods();
		$options = array();
		foreach ($payments as $_method){
			$options[$_method->getCode()] = $_method->getTitle();
		}

		$this->addColumn('payment_method', array(
				'header' => Mage::helper('sales')->__('Payment Method'),
				'index' => 'method',
				'renderer' => new Settv_Common_Block_Render_RenderPayment(),
				'type' => 'options',
				'options' => $options,
		));

		$methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
		$options = array();
		foreach($methods as $_code => $_method)
		{
			$options[$_code.'_'] = Mage::getStoreConfig("carriers/$_code/title");
		}
		$options["familymart_"]=Mage::helper('sales')->__('Familymart Shipping');

		$this->addColumn('logistic_method', array(
				'header' => Mage::helper('sales')->__('Logistics Type'),
				'index' => 'shipping_method',
				'renderer' => new Settv_Common_Block_Render_RenderShipping(),
				'type' => 'options',
				'options' => $options,
		));
		
		$this->addColumn('logistics_num', array(
				'header' => Mage::helper('sales')->__('logistics number'),
				'index' => 'logistics_num',
				'type' => 'text',
				'filter_index' => 'sslc.logistics_num'
		));

		if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
			$this->addColumn('action',
					array(
							'header'    => Mage::helper('sales')->__('Action'),
							'width'     => '50px',
							'type'      => 'action',
							'getter'     => 'getId',
							'actions'   => array(
									array(
											'caption' => Mage::helper('sales')->__('View'),
											'url'     => array('base'=>'*/sales_order/view'),
											'field'   => 'order_id'
									)
							),
							'filter'    => false,
							'sortable'  => false,
							'index'     => 'stores',
							'is_system' => true,
					));
		}
		// $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

		$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)	{
		$orders = Mage::getModel('sales/order')
		->getCollection()
		->addAttributeToFilter('increment_id', $row->getIncrementId())
		->getFirstItem();

		return $this->getUrl('*/*/view', array('order_id' => $orders->getId()));
	}

	protected function _addressFilter($collection, $column)
	{
		if (!$value = $column->getFilter()->getValue()) {
			return $this;
		}

		$this->getCollection()->getSelect()->where(
				"sfoa.city like ?
				OR sfoa.region like ?
				OR sfoa.street like ?
				OR sfoa.postcode like ?"
				, "%$value%");
		return $this;
	}

	protected function _getStore()
	{
		$storeId = (int) $this->getRequest()->getParam('store', 0);
		return Mage::app()->getStore($storeId);
	}
}