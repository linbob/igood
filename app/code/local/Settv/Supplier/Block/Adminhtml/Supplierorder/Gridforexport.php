<?php

class Settv_Supplier_Block_Adminhtml_Supplierorder_Gridforexport extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct() {
		parent::__construct();
		$this->setDefaultSort('created_at');
		$this->setDefaultDir('DESC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		// get supplier_id by user_id
		$collection = Mage::getModel('supplier/supplier')->getCollection()
		->addFieldToSelect('*')->addFieldToFilter('user_id', $user_id);

		$supplier_id = -1;
		if ($collection->getSize() > 0) {
			$supplier_model = current($collection->load()->getIterator());
			$supplier_id = $supplier_model->getId();
		}

		$collection = Mage::getResourceModel('sales/order_grid_collection')
		->addFieldToSelect('increment_id')
		->addFieldToSelect('created_at')
		->addFieldToSelect('billing_name')
		->addFieldToSelect('shipping_name')
		->addFieldToSelect('grand_total')
		->addFieldToSelect('status');
		$collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id', 'shipstatus', 'logistics_id', 'order_id'));
		$collection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplier_id);

		$collection->getSelect()->joinLeft('sales_flat_order_address', 'main_table.entity_id = sales_flat_order_address.parent_id',array('shipping_address' => new Zend_Db_Expr('CONCAT(sales_flat_order_address.postcode,sales_flat_order_address.city,sales_flat_order_address.region,sales_flat_order_address.street)')))
		->where("sales_flat_order_address.address_type = 'shipping'");
		$collection->getSelect()->joinLeft('sales_flat_order', 'main_table.entity_id = sales_flat_order.entity_id', array('total_qty_ordered','shipping_address_id'))
		->where("sales_flat_order.state <> 'failed'")
		->where("sales_flat_order.state <> 'cc_processing'");
		$collection->getSelect()->joinLeft('sales_flat_order_item', 'main_table.entity_id = sales_flat_order_item.order_id', array('sku','sales_flat_order_item.name as product_name','qty_ordered','sales_flat_order_item.price as product_price','product_id'))
		->where("sales_flat_order_item.product_type = 'simple'");
		$collection->getSelect()->joinLeft('settv_sales_logistics_custom as sslc', 'settv_sales_order_custom.logistics_id = sslc.id', array('Max(`sslc`.`created_at`) as logistics_created'));
		$collection->getSelect()->joinLeft('sales_flat_order_payment', 'main_table.entity_id = sales_flat_order_payment.parent_id',array('method'));

		$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();
		$costId = $eavAttribute->getIdByCode('catalog_product', 'product_cost');
		$collection->getSelect()->joinLeft('catalog_product_entity_int', "(sales_flat_order_item.product_id = catalog_product_entity_int.entity_id and catalog_product_entity_int.attribute_id='{$costId}')", array('value AS product_cost'))
		->group('main_table.increment_id');

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {

		$this->addColumn('real_order_id', array(
				'header'=> Mage::helper('sales')->__('Order #'),
				'width' => '80px',
				'type'  => 'text',
				'index' => 'increment_id',
				'filter_index' => 'main_table.increment_id',
		));

		$this->addColumn('created_at', array(
				'header' => Mage::helper('sales')->__('Purchased On'),
				'index' => 'created_at',
				'type' => 'datetime',
				'width' => '100px',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'main_table.created_at',
		));

		$this->addColumn('product_name', array(
				'header'=> Mage::helper('sales')->__('Product Name'),
				'type'  => 'text',
				'index' => 'product_name',
		));

		$this->addColumn('sku', array(
				'header'=> Mage::helper('sales')->__('SKU'),
				'type'  => 'text',
				'index' => 'sku',
		));

		$store = $this->_getStore();
		$this->addColumn('product_price', array(
				'header' => Mage::helper('sales')->__('product price'),
				'type'  => 'currency',
				'currency_code' => $store->getBaseCurrency()->getCode(),
				'index' => 'order_id',
				'renderer' => new Settv_Common_Block_Render_ProductPrice(),
		));

		$this->addColumn('total_qty_ordered', array(
				'header' => Mage::helper('sales')->__('QTY'),
				'index' => 'qty_ordered',
				'type'  => 'number',
				'total' => 'sum',
		));

		$this->addColumn('grand_total', array(
				'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
				'index' => 'grand_total',
				'type'  => 'currency',
				'currency_code' => $store->getBaseCurrency()->getCode(),
				'filter_index' => 'main_table.grand_total',
		));

		$this->addColumn('product_cost', array(
				'header' => Mage::helper('sales')->__('Product Cost'),
				'index' => 'product_cost',
				'type'  => 'currency',
				'currency_code' => $store->getBaseCurrency()->getCode(),
		));

		$this->addColumn('status', array(
				'header' => Mage::helper('sales')->__('Status'),
				'index' => 'status',
				'type'  => 'options',
				'width' => '70px',
				'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
				'filter_index' => 'main_table.status',
		));

		$this->addColumn('shipstatus', array(
				'header' => Mage::helper('sales')->__('Ship Status'),
				'index' => 'shipstatus',
				'type' => 'options',
				'options' => array( 'Shipped' => Mage::helper('sales')->__('Shipped'),'Not Shipped' => Mage::helper('sales')->__('Not Shipped'),
						'Arrived' => Mage::helper('sales')->__('Arrived'),'Rejected' => Mage::helper('sales')->__('Rejected'),
						'Return Received' => Mage::helper('sales')->__('Return Received'),'Return Rejected' => Mage::helper('sales')->__('Return Rejected')),
		));

		$this->addColumn('logistics_id', array(
				'header' => Mage::helper('sales')->__('logistics number'),
				'index' => 'logistics_id',
				'type' => 'number',
		));

		$this->addColumn('forward_at', array(
				'header' => Mage::helper('sales')->__('forward at'),
				'index' => 'logistics_created',
				'type' => 'datetime',
				'renderer' => new Settv_Common_Block_Render_RenderDate(),
				'filter_index' => 'sslc.created_at',
		));

		$payments = Mage::getSingleton('payment/config')->getActiveMethods();
		$options = array();
		foreach ($payments as $_method){
			$options[$_method->getCode()] = $_method->getTitle();
		}

		$this->addColumn('payment_method', array(
				'header' => Mage::helper('sales')->__('Payment Method'),
				'index' => 'method',
				'renderer' => new Settv_Common_Block_Render_RenderPayment(),
				'type' => 'options',
				'options' => $options,
		));

		$this->addColumn('shipping_name', array(
				'header' => Mage::helper('sales')->__('Ship to Name'),
				'index' => 'shipping_name',
		));

		$this->addColumn('shipping_address', array(
				'header' => Mage::helper('sales')->__('Shipping Address'),
				'index' => 'shipping_address',
		));

		return parent::_prepareColumns();
	}

	protected function _getStore()
	{
		$storeId = (int) $this->getRequest()->getParam('store', 0);
		return Mage::app()->getStore($storeId);
	}
}