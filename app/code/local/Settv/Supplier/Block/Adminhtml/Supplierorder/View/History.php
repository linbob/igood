<?php
class Settv_Supplier_Block_Adminhtml_Supplierorder_View_History extends Mage_Adminhtml_Block_Sales_Order_View_History
{
	public function canAddComment()
	{
		$isAllowed = Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/comment') &&
		$this->getOrder()->canComment();

		if(!$isAllowed) {
			$user = Mage::getSingleton('admin/session')->getUser();
			$is_supplier = $user->getIsSupplier();
				
			if($is_supplier) {
				$aclResource = 'admin/supplier_report_menu';
				$isAllowed =  Mage::getSingleton('admin/session')->isAllowed($aclResource);
			}
		}
		return $isAllowed;
	}
	
	public function getSubmitUrl()
	{
		return $this->getUrl('adminhtml/sales_order/addComment', array('order_id'=>$this->getOrder()->getId()));
	}
}
