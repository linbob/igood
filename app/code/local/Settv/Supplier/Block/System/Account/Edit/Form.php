<?php

class Settv_Supplier_Block_System_Account_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
	    $user = Mage::getSingleton('admin/session')->getUser();
		$id = $user->getId();
		$model = Mage::getModel('supplier/supplier')->load($id,'user_id');
		$supplier_id = $model->getId();
		if (!isset($id) && !isset($supplier_id)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('This supplier no longer exists.'));
			$this->_redirect('*/*/');
			return;
		}

		$is_supplier = $user->getIsSupplier();
		$user->unsetData('password');

		$form = new Varien_Data_Form();

		$fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('adminhtml')->__('Account Information')));

		$fieldset->addField('user_id', 'hidden', array(
				'name'  => 'user_id',
		)
		);

		if ($is_supplier) {
			$fieldset->addField('supplier_code', 'text', array(
					'label'     => Mage::helper('supplier')->__('Supplier Code'),
					'name'      => 'supplier_code',
					'readonly' => true,
					'style'   => "border:0px",
			));
				
			$fieldset->addField('name', 'text', array(
					'label'     => Mage::helper('supplier')->__('Name'),
					'class'     => 'required-entry',
					'readonly' => true,
					'style'   => "border:0px",
					'name'      => 'name',
			));

			$fieldset->addField('ubn', 'text', array(
					'label'     => Mage::helper('supplier')->__('UBN :'),
					'name'      => 'ubn',
					'readonly' => true,
					'style'   => "border:0px",
			));

			$fieldset->addField('postcode', 'text', array(
					'label'     => Mage::helper('supplier')->__('Zip/Postal Code'),
					'name'      => 'postcode',
					'class'		=> 'validate-number',
			));

			$fieldset->addField('region', 'text', array(
					'label'     => Mage::helper('supplier')->__('Supplier Province'),
					'name'      => 'region',
			));

			$fieldset->addField('city', 'text', array(
					'label'     => Mage::helper('supplier')->__('Supplier City'),
					'name'      => 'city',
			));

			$fieldset->addField('street', 'text', array(
					'label'     => Mage::helper('supplier')->__('Street Address'),
					'name'      => 'street',
			));

			$fieldset->addField('telephone', 'text', array(
					'label'     => Mage::helper('supplier')->__('Telephone'),
					'name'      => 'telephone',
					'class'		=> 'validate-number',
			));

			$fieldset->addField('fax', 'text', array(
					'label'     => Mage::helper('supplier')->__('Fax'),
					'name'      => 'fax',
					'class'		=> 'validate-number',
			));


			$fieldset->addField('contact_name', 'text', array(
					'label'     => Mage::helper('supplier')->__('Contact Name'),
					'name'      => 'contact_name',
					'required'  => true,
			));

			$fieldset->addField('contact_telephone', 'text', array(
					'label'     => Mage::helper('supplier')->__('Contact Telephone'),
					'name'      => 'contact_telephone',
					'class'		=> 'validate-number',
					'required'  => true,
			));

			$fieldset->addField('contact_email', 'text', array(
					'label'     => Mage::helper('supplier')->__('Contact Email'),
					'name'      => 'contact_email',
					'class'		=> 'validate-email',
					'required'  => true,
			));
				
			$fieldset->addField('customer_service', 'text', array(
					'label'     => Mage::helper('supplier')->__('Customer Service'),
					'name'      => 'customer_service',
			));
				
			$fieldset->addField('customer_service_telephone', 'text', array(
					'label'     => Mage::helper('supplier')->__('Customer Service Telephone'),
					'name'      => 'customer_service_telephone',
					'class'		=> 'validate-number',
			));
				
			$fieldset->addField('customer_service_email', 'text', array(
					'label'     => Mage::helper('supplier')->__('Customer Service Email'),
					'name'      => 'customer_service_email',
					'class'		=> 'validate-email',
			));
				
			$fieldset->addField('accountant', 'text', array(
					'label'     => Mage::helper('supplier')->__('Accountant'),
					'name'      => 'accountant',
			));
				
			$fieldset->addField('accountant_telephone', 'text', array(
					'label'     => Mage::helper('supplier')->__('Accountant Telephone'),
					'name'      => 'accountant_telephone',
					'class'		=> 'validate-number',
			));
				
			$fieldset->addField('accountant_email', 'text', array(
					'label'     => Mage::helper('supplier')->__('Accountant Email'),
					'name'      => 'accountant_email',
					'class'		=> 'validate-email',
			));
		} else {
			$fieldset->addField('username', 'text', array(
					'name'  => 'username',
					'label' => Mage::helper('adminhtml')->__('User Name'),
					'title' => Mage::helper('adminhtml')->__('User Name'),
					'required' => true,
			)
			);
				
			$fieldset->addField('firstname', 'text', array(
					'name'  => 'firstname',
					'label' => Mage::helper('adminhtml')->__('First Name'),
					'title' => Mage::helper('adminhtml')->__('First Name'),
					'required' => true,
			)
			);

			$fieldset->addField('lastname', 'text', array(
					'name'  => 'lastname',
					'label' => Mage::helper('adminhtml')->__('Last Name'),
					'title' => Mage::helper('adminhtml')->__('Last Name'),
					'required' => true,
			)
			);
				
			$fieldset->addField('email', 'text', array(
					'name'  => 'email',
					'label' => Mage::helper('adminhtml')->__('Email'),
					'title' => Mage::helper('adminhtml')->__('User Email'),
					'required' => true,
			)
			);
		}

		$fieldset->addField('password', 'password', array(
				'name'  => 'new_password',
				'label' => Mage::helper('adminhtml')->__('New Password'),
				'title' => Mage::helper('adminhtml')->__('New Password'),
				'class' => 'input-text validate-admin-password',
		)
		);

		$fieldset->addField('confirmation', 'password', array(
				'name'  => 'password_confirmation',
				'label' => Mage::helper('adminhtml')->__('Password Confirmation'),
				'class' => 'input-text validate-cpassword',
		)
		);

		if ($is_supplier) {
			$form->setValues($model);
			$form->setAction($this->getUrl('*/../supplier/adminhtml_account/save'));
		} else {
			$form->setValues($user->getData());
			$form->setAction($this->getUrl('*/system_account/save'));
		}
		
		$form->setMethod('post');
		$form->setUseContainer(true);
		$form->setId('edit_form');

		$this->setForm($form);

		return parent::_prepareForm();
	}
}
