<?php

class Settv_Supplier_Helper_Adminhtml_Dashboard_Order extends Mage_Adminhtml_Helper_Dashboard_Order
{

	protected function _initCollection()
	{
		$user_id = Mage::getSingleton('admin/session')->getUser()->getId();
		// get supplier_id by user_id
		$supplierCollection = Mage::getModel('supplier/supplier')->getCollection()
		->addFieldToSelect('*')->addFieldToFilter('user_id', $user_id);

		$supplier_id = -1;
		if ($supplierCollection->getSize() > 0) {
			$supplier_id = $supplierCollection->getFirstItem()->getId();;
		}

		$isFilter = $this->getParam('store') || $this->getParam('website') || $this->getParam('group');

		$this->_collection = Mage::getResourceSingleton('reports/order_collection');

		if($supplier_id && $supplier_id > 0) {
			$this->_collection->getSelect()->joinLeft('settv_sales_order_custom', 'main_table.entity_id = settv_sales_order_custom.order_id', array('supplier_id'));
			$this->_collection->addFieldToFilter('settv_sales_order_custom.supplier_id', $supplier_id);
		}

		$this->_collection->prepareSummary($this->getParam('period'), 0, 0, $isFilter);

		if ($this->getParam('store')) {
			$this->_collection->addFieldToFilter('store_id', $this->getParam('store'));
		} else if ($this->getParam('website')){
			$storeIds = Mage::app()->getWebsite($this->getParam('website'))->getStoreIds();
			$this->_collection->addFieldToFilter('store_id', array('in' => implode(',', $storeIds)));
		} else if ($this->getParam('group')){
			$storeIds = Mage::app()->getGroup($this->getParam('group'))->getStoreIds();
			$this->_collection->addFieldToFilter('store_id', array('in' => implode(',', $storeIds)));
		} elseif (!$this->_collection->isLive()) {
			$this->_collection->addFieldToFilter('store_id',
					array('eq' => Mage::app()->getStore(Mage_Core_Model_Store::ADMIN_CODE)->getId())
			);
		}

		$this->_collection->load();
	}

}
