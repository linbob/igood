<?php
class Settv_Supplier_Model_Mysql4_Supplier extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
	{
		$this->_init('supplier/supplier', 'id');
	}

	public function loadByField($field, $value) {
		$table = $this->getMainTable();
		$where = $this->_getReadAdapter()->quoteInto("$field = ?", $value);
		$select = $this->_getReadAdapter()->select()->from($table, array('id'))->where($where);
		$id = $this->_getReadAdapter()->fetchOne($sql);
		return $id;
	}
}