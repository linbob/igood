<?php
class Settv_Supplier_Model_Resource_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
	protected $_from;
	
	protected $_to;
	
	protected $_period;
	
	protected $_intervals;
	
	public function getSelectCountSql()
	{
		$this->_renderFilters();

		$countSelect = $this->_getClearSelect()
		->reset(Zend_Db_Select::GROUP)
		->columns('COUNT(DISTINCT e.entity_id)')
		->resetJoinLeft();
		return $countSelect;
	}

	public function setPeriod($period)
	{
		$this->_period = $period;
		return $this;
	}

	public function setInterval($from, $to)
	{
		$this->_from = $from;
		$this->_to   = $to;

		return $this;
	}

	public function getIntervals()
	{
		// 		if (!$this->_intervals) {
		$this->_intervals = array();
		if (!$this->_from && !$this->_to) {
			return $this->_intervals;
		}
		$dateStart  = new Zend_Date($this->_from);
		$dateEnd    = new Zend_Date($this->_to);

		$t = array();
		if ($dateStart->compare($dateEnd) <= 0) {
			$t['title'] =  $dateStart->toString('MM/yyyy');
			$t['start'] = $dateStart->toString('yyyy-MM-dd 00:00:00');
			$t['end'] = $dateEnd->toString('yyyy-MM-dd 23:59:59');
			$this->_intervals[$t['title']] = $t;
		}
		// 		}
		return  $this->_intervals;
	}

	public function getPeriods()
	{
		return array(
				'day'   => Mage::helper('reports')->__('Day'),
				'month' => Mage::helper('reports')->__('Month'),
				'year'  => Mage::helper('reports')->__('Year')
		);
	}
}
