<?php
class Settv_Supplier_Model_Supplier extends Mage_Core_Model_Abstract
{
	function _construct()
	{
		$this->_init('supplier/supplier'); // this is location of the resource file.
	}

	/**
	 * @param string $field
	 * @param unknown_type $value
	 */
	public function loadByField($field, $value) {
		// $id = $this->getResource()->loadByField($field, $value);
		$this->load($value, $field);
	}
	
	public function getSupplierByProduct($product)
	{
		$supplier_id = $product->getSupplier();
	
		$collection = $this
		->getCollection()
		->addFieldToSelect('*')
		->addFieldToFilter('attribute_id', $supplier_id);
	
		$supplier = current($collection->load()->getIterator());
	
		return $supplier;
	}
}