<?php

class Settv_Supplier_Adminhtml_AccountController extends Mage_Adminhtml_Controller_Action
{
	/**
	 * Saving edited user information
	 */
	public function saveAction()
	{
		$userId = Mage::getSingleton('admin/session')->getUser()->getId();
		$supplier = Mage::getModel('supplier/supplier')->load($userId,'user_id');
		$pwd    = null;

		$supplier -> setPostcode($this->getRequest()->getParam('postcode', false))
		-> setRegion($this->getRequest()->getParam('region', false))
		-> setCity($this->getRequest()->getParam('city', false))
		-> setStreet($this->getRequest()->getParam('street', false))
		-> setTelephone($this->getRequest()->getParam('telephone', false))
		-> setFax($this->getRequest()->getParam('fax', false))
		-> setContactName($this->getRequest()->getParam('contact_name', false))
		-> setContactTelephone($this->getRequest()->getParam('contact_telephone', false))
		-> setContactEmail($this->getRequest()->getParam('contact_email', false))
		-> setCustomerService($this->getRequest()->getParam('customer_service', false))
		-> setCustomerServiceTelephone($this->getRequest()->getParam('customer_service_telephone', false))
		-> setCustomerServiceEmail($this->getRequest()->getParam('customer_service_email', false))
		-> setAccountant($this->getRequest()->getParam('accountant', false))
		-> setAccountantTelephone($this->getRequest()->getParam('accountant_telephone', false))
		-> setAccountantEmail($this->getRequest()->getParam('accountant_email', false));

		$user = Mage::getModel("admin/user")->load($userId);

		if ( $this->getRequest()->getParam('new_password', false) ) {
			$user->setNewPassword($this->getRequest()->getParam('new_password', false));
		}

		if ($this->getRequest()->getParam('password_confirmation', false)) {
			$user->setPasswordConfirmation($this->getRequest()->getParam('password_confirmation', false));
		}

		try {
			$supplier->save();
			$user->save();
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The account has been saved.'));
		}
		catch (Mage_Core_Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}
		catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('An error occurred while saving account.'));
		}
		$this->getResponse()->setRedirect($this->getUrl("adminhtml/system_account/"));
	}

	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('system/myaccount');
	}
}
