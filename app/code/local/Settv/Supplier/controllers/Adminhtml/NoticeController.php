<?php

class Settv_Supplier_Adminhtml_NoticeController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction() {
		$this->loadLayout()->_setActiveMenu('supplier_menu/supplier_notice_page');
		$this->_addContent($this->getLayout()->createBlock('supplier/adminhtml_notice_edit'))
		->_addLeft($this->getLayout()->createBlock('supplier/adminhtml_notice_edit_tabs'));
		$this->renderLayout();
	}

	/**
	 * Saving edited notices information
	 */
	public function saveAction()
	{
		$text = (string)$this->getRequest()->getParam('notice_area', false);

		if ($text=="") {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Warning! No notice exists.'));
			$this->_redirect('*/*/');
			return;
		}

		try {
			$date = date('Y-m-d H:i:s');
			Mage::getModel('adminnotification/inbox')->parse(array(
			array(
			'severity'      => 4,
			'date_added'    => $date,
			'title'         => $text,
			'description'   => $text,
			'url'           => '',
			'internal'      => true
			)
			));
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The supplier notice has been saved.'));
		}
		catch (Mage_Core_Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
		}
		catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('An error occurred while saving account.'));
		}

		$this->getResponse()->setRedirect($this->getUrl("*/*/"));
	}
}
