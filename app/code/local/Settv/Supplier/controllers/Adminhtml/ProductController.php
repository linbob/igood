<?php

class  Settv_Supplier_Adminhtml_ProductController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction() {
		$this->loadLayout()
		->_setActiveMenu('supplier_report_menu/product_list_page')
		->_addBreadcrumb($this->__('Supplier Report'), $this->__('Supplier Report'))
		->_addBreadcrumb($this->__('Supplier Product'), $this->__('Supplier Product'));
		$this->renderLayout();
	}

	protected function _initProduct()
	{
		$this->_title($this->__('Catalog'))
		->_title($this->__('Manage Products'));

		$productId  = (int) $this->getRequest()->getParam('id');
		$product    = Mage::getModel('catalog/product')
		->setStoreId($this->getRequest()->getParam('store', 0));

		if (!$productId) {
			if ($setId = (int) $this->getRequest()->getParam('set')) {
				$product->setAttributeSetId($setId);
			}

			if ($typeId = $this->getRequest()->getParam('type')) {
				$product->setTypeId($typeId);
			}
		}

		$product->setData('_edit_mode', true);
		if ($productId) {
			try {
				$product->load($productId);
			} catch (Exception $e) {
				$product->setTypeId(Mage_Catalog_Model_Product_Type::DEFAULT_TYPE);
				Mage::logException($e);
			}
		}

		$attributes = $this->getRequest()->getParam('attributes');
		if ($attributes && $product->isConfigurable() &&
				(!$productId || !$product->getTypeInstance()->getUsedProductAttributeIds())) {
			$product->getTypeInstance()->setUsedProductAttributeIds(
					explode(",", base64_decode(urldecode($attributes)))
			);
		}

		// Required attributes of simple product for configurable creation
		if ($this->getRequest()->getParam('popup')
				&& $requiredAttributes = $this->getRequest()->getParam('required')) {
			$requiredAttributes = explode(",", $requiredAttributes);
			foreach ($product->getAttributes() as $attribute) {
				if (in_array($attribute->getId(), $requiredAttributes)) {
					$attribute->setIsRequired(1);
				}
			}
		}

		if ($this->getRequest()->getParam('popup')
				&& $this->getRequest()->getParam('product')
				&& !is_array($this->getRequest()->getParam('product'))
				&& $this->getRequest()->getParam('id', false) === false) {

			$configProduct = Mage::getModel('catalog/product')
			->setStoreId(0)
			->load($this->getRequest()->getParam('product'))
			->setTypeId($this->getRequest()->getParam('type'));

			/* @var $configProduct Mage_Catalog_Model_Product */
			$data = array();
			foreach ($configProduct->getTypeInstance()->getEditableAttributes() as $attribute) {

				/* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
				if(!$attribute->getIsUnique()
						&& $attribute->getFrontend()->getInputType()!='gallery'
						&& $attribute->getAttributeCode() != 'required_options'
						&& $attribute->getAttributeCode() != 'has_options'
						&& $attribute->getAttributeCode() != $configProduct->getIdFieldName()) {
					$data[$attribute->getAttributeCode()] = $configProduct->getData($attribute->getAttributeCode());
				}
			}

			$product->addData($data)
			->setWebsiteIds($configProduct->getWebsiteIds());
		}

		Mage::register('product', $product);
		Mage::register('current_product', $product);
		Mage::getSingleton('cms/wysiwyg_config')->setStoreId($this->getRequest()->getParam('store'));
		return $product;
	}

	/**
	 * View product detail
	 */
	public function editAction()
	{
		$productId  = (int) $this->getRequest()->getParam('id');
		$product = $this->_initProduct();

		if ($productId && !$product->getId()) {
			$this->_getSession()->addError(Mage::helper('catalog')->__('This product no longer exists.'));
			$this->_redirect('*/*/');
			return;
		}

		$this->_title($product->getName());

		Mage::dispatchEvent('catalog_product_edit_action', array('product' => $product));

		$_additionalLayoutPart = '';
		if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE
				&& !($product->getTypeInstance()->getUsedProductAttributeIds()))
		{
			$_additionalLayoutPart = '_new';
		}

		$this->loadLayout(array(
				'default',
				strtolower($this->getFullActionName()),
				'adminhtml_catalog_product_'.$product->getTypeId() . $_additionalLayoutPart
		));

		$this->_setActiveMenu('supplier_report_menu/product_list_page');

		if (!Mage::app()->isSingleStoreMode() && ($switchBlock = $this->getLayout()->getBlock('store_switcher'))) {
			$switchBlock->setDefaultStoreName($this->__('Default Values'))
			->setWebsiteIds($product->getWebsiteIds())
			->setSwitchUrl(
					$this->getUrl('*/*/*', array('_current'=>true, 'active_tab'=>null, 'tab' => null, 'store'=>null))
			);
		}

		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

		$block = $this->getLayout()->getBlock('catalog.wysiwyg.js');
		if ($block) {
			$block->setStoreId($product->getStoreId());
		}

		$this->renderLayout();
	}

	public function exportCsvAction()
	{
		$fileName   = 'products.csv';
		$content    = $this->getLayout()->createBlock('supplier/adminhtml_product_grid')
		->setSaveParametersInSession(true)
		->getCsv();

		$this->_prepareDownloadResponse($fileName, $content);
	}

	/**
	 *  Export order grid to Excel XML format
	 */
	public function exportExcelAction()
	{
		$fileName   = 'products.xls';
		$content    = $this->getLayout()->createBlock('supplier/adminhtml_product_grid')
		->getExcel($fileName);

		$this->_prepareDownloadResponse($fileName, $content);
	}
}