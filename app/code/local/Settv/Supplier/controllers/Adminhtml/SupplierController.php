<?php
class Settv_Supplier_Adminhtml_SupplierController extends Mage_Adminhtml_Controller_action
{
	public function indexAction() {
		$this->loadLayout()->_setActiveMenu('supplier_menu/supplier_account_page');
		$this->renderLayout();
		//$this->createCustomerSupplier();		
	}
	public function createCustomerSupplier(){
		
		$arg_attribute = 'supplier';

		$attribute_model        = Mage::getModel('eav/entity_attribute');
		
		
		/* delete 
		$attribute_code         = $attribute_model->getIdByCode('customer', $arg_attribute);
		$attribute              = $attribute_model->load($attribute_code);
		$optionCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
		->setAttributeFilter($attribute->getAttributeId())
		->setPositionOrder('desc', true)
		->load();
		foreach($optionCollection as $option){
			$option->delete();
		}
		$attribute->save();
		return ;
		*/
		$attribute_code         = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
		$attribute              = $attribute_model->load($attribute_code);
		$attribute_options_model= Mage::getModel('eav/entity_attribute_source_table');
		$attribute_table        = $attribute_options_model->setAttribute($attribute);
		$options                = $attribute_options_model->getAllOptions(false);
		
		foreach($options as $option) {
			$attribute_code         = $attribute_model->getIdByCode('customer', $arg_attribute);
			$attribute              = $attribute_model->load($attribute_code);
			$value['option'] = array($option['label'],$option['label']);
			$result = array('value' => $value);
			$attribute->setData('option', $result);
			$attribute->save();
		}
	}

	public function newAction(){
		$this->loadLayout()
		->_setActiveMenu('supplier_menu/supplier_account_page')
		->_addContent($this->getLayout()->createBlock('supplier/adminhtml_supplier_edit'))
		->_addLeft($this->getLayout()->createBlock('supplier/adminhtml_supplier_edit_tabs'));
		$this->renderLayout();
	}

	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			$id = $this->getRequest()->getParam('id');

			$model = Mage::getModel('supplier/supplier')->load($id);
			if (!$model->getId() && $id) {
				Mage::getSingleton('adminhtml/session')->addError($this->__('This supplier no longer exists.'));
				$this->_redirect('*/*/');
				return;
			}

			/* refresh supplier attribute options */
			$attribute_id="";
			// if($model->getAttributeId() == '') {
			if (!$this->attributeKeyExists('supplier', $model->getAttributeId())) {
				// new supplier
				// Mage::log('NEW SUPPLIER');
				$attribute_id = $this->addAttributeValue('supplier', $data['name']);
			} else {
				// edit supplier data
				// Mage::log('EDIT SUPPLIER');
				$attribute_id = $this->renameAttributeValue('supplier', $model->getAttributeId(), $data['name']);

			}
			
			// check supplier name
			$check_name_col = Mage::getModel('supplier/supplier')->getCollection()
			->addFieldToFilter('name', $data['name']);
 			foreach ($check_name_col as $check_name_model) {
 				if ($check_name_model->getId() != $id) {
 					// Mage::log('Supplier name already exists.');
 					Mage::getSingleton('adminhtml/session')->addError($this->__('Supplier name already exists.'));
 					Mage::getSingleton('adminhtml/session')->setUserData($data);
 					$this->_redirect('*/*/edit');
 					return;
 				}
			}

			if (!$this->userKeyExists($model->getUserId())) {
				
				// no user id, new user
				$user_model = Mage::getModel('admin/user');
				$user_data = array();
				$user_data['username'] = $data['supplier_code'];
				$user_data['firstname'] = $data['contact_name'];
				$user_data['lastname'] = $data['contact_name'];
				$user_data['email'] = $data['contact_email'];
				$user_data['is_active'] = $data['is_active'];
				$user_data['password'] = $data['password'];
				$user_data['is_supplier'] = 1;
					
				$user_model->setData($user_data);
				
				if ($user_model->userExists()) {
					Mage::getSingleton('adminhtml/session')->addError($this->__('This supplier login account has existed.'));
					Mage::getSingleton('adminhtml/session')->setUserData($data);
					$this->_redirect('*/*/edit');
					return;
				}
				
				
				$user_model->save();

				$data['user_id'] = $user_model->getId();

				// setup role
				$supplier_roles = Mage::getModel('admin/role')
				->getCollection()
				->addFieldToFilter('role_name', 'Supplier')
				->addFieldToFilter('role_type', 'G')->getIterator();

				foreach($supplier_roles as $supplier_role) {
					$rs = array();
					$rs[0] = $supplier_role->getData('role_id');
					$user_model->setRoleIds( $rs )->setRoleUserId( $user_model->getUserId() )->saveRelations();
				}
			} else {
				// user id exists, edit user data
				$user_model = Mage::getModel('admin/user')->load($model->getUserId());
				$user_data = $user_model->getData();
				
				$user_data['username'] = $data['supplier_code'];
				$user_data['firstname'] = $data['contact_name'];
				$user_data['lastname'] = $data['contact_name'];
				$user_data['email'] = $data['contact_email'];
				$user_data['is_active'] = $data['is_active'];
				$user_data['is_supplier'] = 1;
				if (!empty($data['new_password'])) {
					$user_data['password'] = $data['new_password'];
				}

				$user_model->setData($user_data);
				// 20120712 check username exist  by Sean S
				if ($user_model->userExists()) {
					Mage::getSingleton('adminhtml/session')->addError($this->__('This supplier login account has existed.'));
					Mage::getSingleton('adminhtml/session')->setUserData($data);
					$this->_redirect('*/*/edit');
					return;
				}
				// 20120712 check username exist  by Sean E
				
				$user_model->save();

				// setup role
				$supplier_roles = Mage::getModel('admin/role')
				->getCollection()
				->addFieldToFilter('role_name', 'Supplier')
				->addFieldToFilter('role_type', 'G')->getIterator();

				foreach($supplier_roles as $supplier_role) {
					$rs = array();
					$rs[0] = $supplier_role->getData('role_id');
					$user_model->setRoleIds( $rs )->setRoleUserId( $user_model->getUserId() )->saveRelations();
				}
			}

			// Mage::log('ATTRIBUTE ID: '.$attribute_id);
			$data['attribute_id'] = $attribute_id;
			
			$model->setData($data);
			$model->save();
		}

		$this->_redirect('*/*/');
	}

	public function editAction() {
		$id = $this->getRequest()->getParam('id');
		$model = Mage::getModel('supplier/supplier');

		if ($id) {
			$model->load($id);
			if (! $model->getId()) {
				Mage::getSingleton('adminhtml/session')->addError($this->__('This user no longer exists.'));
				$this->_redirect('*/*/');
				return;
			}
		}

		$this->_title($model->getId() ? $model->getName() : $this->__('New User'));
		
		$data = Mage::getSingleton('adminhtml/session')->getUserData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register('adminhtml_supplier', $model);

		// ->_addBreadcrumb($id ? $this->__('Edit User') : $this->__('New User'), $id ? $this->__('Edit User') : $this->__('New User'))
		$this->loadLayout()
		->_setActiveMenu('supplier_menu/supplier_account_page')
		->_addContent($this->getLayout()->createBlock('supplier/adminhtml_supplier_edit'))
		->_addLeft($this->getLayout()->createBlock('supplier/adminhtml_supplier_edit_tabs'));
		$this->renderLayout();
	}

	public function addAttributeValue($arg_attribute, $arg_value) {
		$attribute_model        = Mage::getModel('eav/entity_attribute');


		$attribute_code         = $attribute_model->getIdByCode('customer', $arg_attribute);
		$attribute              = $attribute_model->load($attribute_code);
		if(!$this->attributeValueExists($arg_attribute, $arg_value)) {
			$value['option'] = array($arg_value,$arg_value);
			$result = array('value' => $value);
			$attribute->setData('option', $result);
			$attribute->save();
		}
		
		
		$attribute_code         = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
		$attribute              = $attribute_model->load($attribute_code);

		if(!$this->attributeValueExists($arg_attribute, $arg_value)) {
			$value['option'] = array($arg_value,$arg_value);
			$result = array('value' => $value);
			$attribute->setData('option', $result);
			$attribute->save();
		}
		

		$attribute_options_model= Mage::getModel('eav/entity_attribute_source_table');
		$attribute_table        = $attribute_options_model->setAttribute($attribute);
		$options                = $attribute_options_model->getAllOptions(false);

		foreach($options as $option) {
			if ($option['label'] == $arg_value) {
				return $option['value'];
			}
		}
		return false;
	}

	public function renameAttributeValue($arg_attribute, $arg_key, $arg_value) {
		$attribute_model        = Mage::getModel('eav/entity_attribute');
		

		$id = $this->getRequest()->getParam('id');
		
		$model = Mage::getModel('supplier/supplier')->load($id);

		$attribute_code         = $attribute_model->getIdByCode('customer', $arg_attribute);
		$attribute              = $attribute_model->load($attribute_code);

		$option_iterator = Mage::getModel('attributeoption/attributeoption')
		->getCollection()
		->addFieldToFilter('value', $model->getName())->getIterator();
		
		foreach($option_iterator as $option_model) {
			$option_model->setValue($arg_value);
			$option_model->save();
		}
				
		$attribute_code         = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
		$attribute              = $attribute_model->load($attribute_code);

		$option_iterator = Mage::getModel('attributeoption/attributeoption')
		->getCollection()
		->addFieldToFilter('option_id', $arg_key)->getIterator();

		foreach($option_iterator as $option_model) {
			$option_model->setValue($arg_value);
			$option_model->save();
		}

		return $arg_key;
	}

	public function attributeValueExists($arg_attribute, $arg_value) {
		$attribute_model        = Mage::getModel('eav/entity_attribute');
		$attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;

		$attribute_code         = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
		$attribute              = $attribute_model->load($attribute_code);

		$attribute_table        = $attribute_options_model->setAttribute($attribute);
		$options                = $attribute_options_model->getAllOptions(false);

		foreach($options as $option) {
			if ($option['label'] == $arg_value)	{
				return $option['value'];
			}
		}

		return false;
	}

	private function attributeKeyExists($arg_attribute, $arg_key) {
		$attribute_model        = Mage::getModel('eav/entity_attribute');
		$attribute_options_model= Mage::getModel('eav/entity_attribute_source_table') ;

		$attribute_code         = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
		$attribute              = $attribute_model->load($attribute_code);

		$attribute_table        = $attribute_options_model->setAttribute($attribute);
		$options                = $attribute_options_model->getAllOptions(false);

		foreach($options as $option) {
			if ($option['value'] == $arg_key)	{
				return true;
			}
		}

		return false;
	}

	private function userKeyExists($user_key) {
		$user_model = Mage::getModel('admin/user');
		$name = $user_model->load($user_key)->getUsername();

		if ($name) {
			return true;
		}

		return false;
	}
	
	/**
	 *  Export order grid to CSV format
	 */
	public function exportCsvAction()
	{
		$fileName   = 'supplier_list.csv';
		$content    = $this->getLayout()->createBlock('supplier/adminhtml_supplier_grid')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
		exit(0);
	}
	
	/**
	 *  Export order grid to Excel XML format
	 */
	public function exportExcelAction()
	{
		$fileName   = 'supplier_list.xls';
		$content       = $this->getLayout()->createBlock('supplier/adminhtml_supplier_grid')->getExcelFile($fileName);
		$this->_prepareDownloadResponse($fileName, $content);
		exit(0);
	}
}
