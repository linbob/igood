<?php
class Settv_Supplier_Adminhtml_SupplierdeliverController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout()
		->_setActiveMenu('deliver_menu/deliver_page')
		->_addBreadcrumb($this->__('Supplier Undelivered Order'), $this->__('Supplier Undelivered Order'));
		return $this;
	}

	protected function indexAction() {
		$this->loadLayout()
		->_setActiveMenu('deliver_menu/deliver_page')
		->_addBreadcrumb($this->__('Supplier Undelivered Order'), $this->__('Supplier Undelivered Order'));
		$this->renderLayout();
	}

	/**
	 * Initialize order model instance
	 *
	 * @return Mage_Sales_Model_Order || false
	 */
	protected function _initOrder()
	{
		$id = $this->getRequest()->getParam('order_id');
		$order = Mage::getModel('sales/order')->load($id);

		if (!$order->getId()) {
			$this->_getSession()->addError($this->__('This order no longer exists.'));
			$this->_redirect('*/*/');
			$this->setFlag('', self::FLAG_NO_DISPATCH, true);
			return false;
		}
		Mage::register('sales_order', $order);
		Mage::register('current_order', $order);
		return $order;
	}

	public function deliverAction() 
	{
		$request_order_id = $this->getRequest()->getParam('order_id');
		$request_order = Mage::getModel('sales/order')->load($request_order_id);
		$custom_order = Mage::getModel('check/custom_order')->loadByOrderId($request_order->getId());		
		$shipping_title = Mage::helper('ordermanage')->getShippingTitleByOrder($request_order);
		$ship_status = $custom_order->getShipstatus();
		if ($ship_status == Settv_Check_Model_Custom_Order::STATE_NOT_SHIPPED)
		{
			if ($shipping_title == Mage::getStoreConfig('carriers/familymart/title', false))  //全家
			{
				Mage::helper('logisticfamilymart/familymart')->deliverOrder($request_order->getIncrementId());
				Mage::helper('goveinvoice')->createInvoice($request_order);
			}
			else if($shipping_title == Mage::getStoreConfig('carriers/selfdistribute/title', false) //"供應商自行配送 or 黑貓宅配"
			|| $shipping_title == Mage::getStoreConfig('carriers/ezcat/title', false))
			{
				$logistics = Mage::getModel('check/custom_logistics')->createLogistics();
				Mage::getModel('check/custom_order')->setLogisticsData($request_order->getId(), $logistics->getId());
				Mage::helper('goveinvoice')->createInvoice($request_order);
				$this->sendMailToManager($request_order);
			}
			else if ($shipping_title == Mage::getStoreConfig('carriers/hct/title', false)) //新竹物流
			{
				$orders = Mage::helper('ordermanage')->getRefOrderForShipping($request_order);
				Mage::helper('logistichct/hct')->deliverOrder($request_order, $orders);
				foreach ($orders as $order)
					Mage::helper('goveinvoice')->createInvoice($order);
			}
			$this->_redirect('*/*/');
		}
		else
		{
			echo "無法出貨, 訂單狀態:".$ship_status;
		}
		
	}

	

	private function sendMailToManager($order)
	{
		$email_address = Mage::getStoreConfig('sales_email/settv_order_mgmt/manager_email');
		$emailTemplateVariables = array();
		$emailTemplateVariables['order'] = $order;
		$emailTemplateVariables['order_id'] = $order->getIncrementId();
		$emailTemplateVariables['totalPrice'] = round($order->getGrandTotal(),0);

		$items1 = $order->getAllItems();
		$itemIndex = 0;
		foreach ($items1 as $item1)
		{
			if ($itemIndex==0)
			{
				$emailTemplateVariables['unitPrice'] = round($item1->getPrice(),0);
				$emailTemplateVariables['qty'] =$item1->getQtyOrdered()*1;
			}
			$emailTemplateVariables['productname'] = $item1->getName();
			$emailTemplateVariables['sku'] =$item1->getSku();
			$itemIndex++;
		}

		Mage::helper('common/common')->sendEmail('Assign logistics by Order Manager', $email_address, $emailTemplateVariables);
	}


	/**
	 * View order detail
	 */
	public function viewAction()
	{
		$this->_title($this->__('Sales'))->_title($this->__('Orders'));

		if ($order = $this->_initOrder()) {
			$this->_initAction();

			$this->_title(sprintf("#%s", $order->getRealOrderId()));

			$this->renderLayout();
		}
	}

	/**
	 *  Export order grid to CSV format
	 */
	public function exportCsvAction()
	{
		$fileName   = 'supplier_notshipped_orders.csv';
		$content    = $this->getLayout()->createBlock('supplier/adminhtml_supplierdeliver_grid')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
		exit(0);
	}

	/**
	 *  Export order grid to Excel XML format
	 */
	public function exportExcelAction()
	{
		$fileName   = 'supplier_notshipped_orders.xls';
		$content       = $this->getLayout()->createBlock('supplier/adminhtml_supplierdeliver_grid')->getExcelFile($fileName);
		$this->_prepareDownloadResponse($fileName, $content);
		exit(0);
	}
}