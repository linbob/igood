<?php
class Settv_Supplier_Adminhtml_SupplierorderController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction() {
		$this->loadLayout()
		->_setActiveMenu('supplier_report_menu/order_list_page')
		->_addBreadcrumb($this->__('Supplier Report'), $this->__('Supplier Report'))
		->_addBreadcrumb($this->__('Supplier Order List'), $this->__('Supplier Order List'));
		return $this;
	}

	protected function indexAction() {
		$this->loadLayout()
		->_setActiveMenu('supplier_report_menu/order_list_page')
		->_addBreadcrumb($this->__('Supplier Report'), $this->__('Supplier Report'))
		->_addBreadcrumb($this->__('Supplier Order List'), $this->__('Supplier Order List'));
		$this->renderLayout();
	}

	/**
	 * Initialize order model instance
	 *
	 * @return Mage_Sales_Model_Order || false
	 */
	protected function _initOrder()
	{
		$id = $this->getRequest()->getParam('order_id');
		$order = Mage::getModel('sales/order')->load($id);

		if (!$order->getId()) {
			$this->_getSession()->addError($this->__('This order no longer exists.'));
			$this->_redirect('*/*/');
			$this->setFlag('', self::FLAG_NO_DISPATCH, true);
			return false;
		}
		Mage::register('sales_order', $order);
		Mage::register('current_order', $order);
		return $order;
	}

	public function exportCsvAction()
	{
		$fileName   = 'supplier_orders.csv';
		$grid       = $this->getLayout()->createBlock('supplier/adminhtml_supplierorder_gridforexport');
		$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		exit(0);
	}

	/**
	 *  Export order grid to Excel XML format
	 */
	public function exportExcelAction()
	{
		$fileName   = 'supplier_orders.xls';
		$grid       = $this->getLayout()->createBlock('supplier/adminhtml_supplierorder_gridforexport');
		$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		exit(0);
	}

	/**
	 * View order detail
	 */
	public function viewAction()
	{
		$this->_title($this->__('Sales'))->_title($this->__('Orders'));

		if ($order = $this->_initOrder()) {
			$this->_initAction();

			$this->_title(sprintf("#%s", $order->getRealOrderId()));

			$this->renderLayout();
		}
	}
}