<?php 
$installer = $this;  
$installer->startSetup();
/*
 $installer->run("
 		ALTER TABLE `admin_user1` ADD COLUMN `supplier_flag` smallint(5) CHARACTER SET utf8 DEFAULT 0;
 		");
*/
$installer->run("
		CREATE TABLE IF NOT EXISTS {$this->getTable('supplier/supplier')} (
		`id` int(11) unsigned NOT NULL auto_increment,
		`supplier_code` varchar(255) NOT NULL default '' ,
		`name` varchar(255) NOT NULL default '',
		`postcode` varchar(255) NOT NULL default '',
		`region` text NOT NULL default '',
		`city` varchar(255) NOT NULL default '',
		`country` varchar(255) NOT NULL default '',
		`street` varchar(255) NOT NULL default '',
		`ubn` varchar(255) NOT NULL default '',
		`telephone` varchar(255) NOT NULL default '',
		`fax` varchar(255) NOT NULL default '',
		`contact_name` varchar(255) NOT NULL default '',
		`contact_telephone` varchar(255) NOT NULL default '',
		`contact_email` varchar(255) NOT NULL default '',
		`is_active` smallint(6) NOT NULL default '0',
		`created_time` datetime NULL,
		`update_time` datetime NULL,
		`attribute_id` smallint(6) DEFAULT NULL,
  		`user_id` smallint(6) DEFAULT NULL,
		PRIMARY KEY (`id`),
		UNIQUE KEY `supplier_code_UNIQUE` (`supplier_code`),
		UNIQUE KEY `attribute_id_UNIQUE` (`attribute_id`),
  		UNIQUE KEY `user_id_UNIQUE` (`user_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();