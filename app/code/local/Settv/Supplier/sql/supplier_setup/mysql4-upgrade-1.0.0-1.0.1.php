<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn(
		$this->getTable('supplier/supplier'),
		'customer_service',
		'varchar(255) NOT NULL DEFAULT ""'
);

$installer->getConnection()->addColumn(
		$this->getTable('supplier/supplier'),
		'customer_service_telephone',
		'varchar(255) NOT NULL DEFAULT ""'
);

$installer->getConnection()->addColumn(
		$this->getTable('supplier/supplier'),
		'customer_service_email',
		'varchar(255) NOT NULL DEFAULT ""'
);

$installer->getConnection()->addColumn(
		$this->getTable('supplier/supplier'),
		'accountant',
		'varchar(255) NOT NULL DEFAULT ""'
);

$installer->getConnection()->addColumn(
		$this->getTable('supplier/supplier'),
		'accountant_telephone',
		'varchar(255) NOT NULL DEFAULT ""'
);

$installer->getConnection()->addColumn(
		$this->getTable('supplier/supplier'),
		'accountant_email',
		'varchar(255) NOT NULL DEFAULT ""'
);

$installer->endSetup();