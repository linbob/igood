<?php
require_once 'Mage/Wishlist/controllers/IndexController.php';

class Settv_Wishlist_IndexController extends Mage_Wishlist_IndexController
{
	
	public function cartAction()
	{			
		//ajax wishlist cart feature.    
        $isAjax = $this->getRequest()->getParam('isAjax');
		                               
		if(count($isAjax) != 0 && $isAjax == 1){
			
			$wishlist   = $this->_getWishlist();
			
			$itemId = (int) $this->getRequest()->getParam('item');
			
			/* @var $item Mage_Wishlist_Model_Item */
			$item = Mage::getModel('wishlist/item')->load($itemId);
							
			
			$session    = Mage::getSingleton('wishlist/session');
			$cart       = Mage::getSingleton('checkout/cart');
			
			// Set qty
			$qty = $this->getRequest()->getParam('qty');
			if (is_array($qty)) {
				if (isset($qty[$itemId])) {
					$qty = $qty[$itemId];
				} else {
					$qty = 1;
				}
			}
			$qty = $this->_processLocalizedQty($qty);
			if ($qty) {
				$item->setQty($qty);
			}			
			
			$response = array();
			try {
				$options = Mage::getModel('wishlist/item_option')->getCollection()
				->addItemFilter(array($itemId));
				$item->setOptions($options->getOptionsByItem($itemId));
				
				$buyRequest = Mage::helper('catalog/product')->addParamsToBuyRequest(
						$this->getRequest()->getParams(),
						array('current_config' => $item->getBuyRequest())
				);
				
				$item->mergeBuyRequest($buyRequest);
				$item->addToCart($cart, true);
				$cart->save()->getQuote()->collectTotals();
				$wishlist->save();
				
				Mage::helper('wishlist')->calculate();										
									
				$productId = reset($options->getOptionsByItem($itemId))->getProductId();
				$product = Mage::getModel('catalog/product')->load($productId);
				
				if (!$session->getNoCartRedirect(true)) {
					if (!$cart->getQuote()->getHasError()){
						$message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
						$response['status'] = 'SUCCESS';
						$response['message'] = $message;
					}
				}
			} catch (Mage_Core_Exception $e) {		
				if ($e->getCode() == Mage_Wishlist_Model_Item::EXCEPTION_CODE_NOT_SALABLE) {
					$msg = Mage::helper('wishlist')->__('This product(s) is currently out of stock');
				}
				else {
					$msg = "";
					$messages = array_unique(explode("\n", $e->getMessage()));
					foreach ($messages as $message) {
						$msg .= $message.'<br/>';
					}					
				}
				
				$response['status'] = 'ERROR';
				$response['message'] = $msg;
			} catch (Exception $e) {
				$response['status'] = 'ERROR';
				$response['message'] = $this->__('Cannot add the item to shopping cart.');
				Mage::logException($e);
			}
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;
		}
		else 
		{		
			parent::cartAction();
		}
	}		
}
