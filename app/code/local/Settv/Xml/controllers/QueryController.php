<?php
class Settv_Xml_QueryController extends Mage_Core_Controller_Front_Action {
	public function testAction() {
		$this->loadLayout();
		$this->renderLayout();
	}

	public function paramsAction() {
		echo '<dl>';
		foreach($this->getRequest()->getParams() as $key=>$value) {
			echo '<dt><strong>Param: </strong>'.$key.'</dt>';
			echo '<dt><strong>Value: </strong>'.$value.'</dt>';
		}
		echo '</dl>';
	}

	public function catalogAction() {
		$store = $this->getRequest()->getParam('st');
		$sort_order = $this->getRequest()->getParam('so');
		$query = Mage::helper('catalogsearch')->getQuery();
		/* @var $query Mage_CatalogSearch_Model_Query */

		$query->setStoreId(Mage::app()->getStore()->getId());

		if ($query->getQueryText()) {
			if (Mage::helper('catalogsearch')->isMinQueryLength()) {
				$query->setId(0)
				->setIsActive(1)
				->setIsProcessed(1);
			}
			else {
				if ($query->getId()) {
					$query->setPopularity($query->getPopularity()+1);
				}
				else {
					$query->setPopularity(1);
				}

				if ($query->getRedirect()){
					$query->save();
					$this->getResponse()->setRedirect($query->getRedirect());
					return;
				}
				else {
					$query->prepare();
				}
			}

			Mage::helper('catalogsearch')->checkNotes();

			// $this->loadLayout();
			// $this->_initLayoutMessages('catalog/session');
			// $this->_initLayoutMessages('checkout/session');
			// $this->renderLayout();


			if (!Mage::helper('catalogsearch')->isMinQueryLength()) {
				$query->save();
			}

			echo $store;
			echo '<FEED>';

			$result_col = $query->getResultCollection();
			if (strlen($sort_order) > 0 && $sort_order == 1) {
				$result_col->addAttributeToSort('created_at', 'ASC');
			} else {
				$result_col->addAttributeToSort('created_at', 'DESC');
			}

			$result_array = $result_col->getData();

			if (strlen($store) > 0) {
				foreach ($result_array as $result) {
					$_model = Mage::getModel('catalog/product');
					$_product = $_model->load($result['entity_id']);

					$store_code = $_product->getAttributeText('sale_store');
					if ($store_code == $store) {

						echo $_product->getSaleStore();
						echo '<entry>';
						echo '<name>'.htmlspecialchars($_product->getName(), ENT_QUOTES).'</name>';
						echo '<price>'.round($_product->getFinalPrice(), 0).'</price>';
						echo '<url>'.htmlspecialchars($_product->getUrlPath(), ENT_QUOTES).'?___store='.htmlspecialchars($_product->getAttributeText('sale_store'), ENT_QUOTES).'</url>';
						echo '<image>'.htmlspecialchars($_product->getSmallImageUrl(300,225), ENT_QUOTES).'</image>';
						echo '<spacialnote>'.htmlspecialchars($_product->getSpecialNote(), ENT_QUOTES).'</specialnote>';
						echo '<description>'.htmlspecialchars(trim($_product->getShortDescription(), ENT_QUOTES)).'</description>';
						echo '</entry>';
					}
				}
			} else {
				foreach ($result_array as $result) {
					$_model = Mage::getModel('catalog/product');
					$_product = $_model->load($result['entity_id']);

					echo '<entry>';
					echo '<name>'.htmlspecialchars($_product->getName(), ENT_QUOTES).'</name>';
					echo '<price>'.round($_product->getFinalPrice(), 0).'</price>';
					echo '<url>'.htmlspecialchars($_product->getUrlPath(), ENT_QUOTES).'?___store='.htmlspecialchars($_product->getAttributeText('sale_store'), ENT_QUOTES).'</url>';
					echo '<image>'.htmlspecialchars($_product->getSmallImageUrl(300,225), ENT_QUOTES).'</image>';
					echo '<spacialnote>'.htmlspecialchars($_product->getSpecialNote(), ENT_QUOTES).'</specialnote>';
					echo '<description>'.htmlspecialchars(trim($_product->getShortDescription(), ENT_QUOTES)).'</description>';
					echo '</entry>';
						
				}
			}
			echo '</FEED>';
		}
		else {
			$this->_redirectReferer();
		}
	}
}