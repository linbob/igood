<?php
class Yoyo8_Sms_Helper_SMSBridge extends Mage_Core_Helper_Abstract {
	function SendSMS($mobileNo, $smsMsg, $sourceProdID, $sourceMsgID, $charSet, &$output)
	{
		$url = Mage::getStoreConfig('yoyo8/sms/serverurl', false);
		$memberID = Mage::getStoreConfig('yoyo8/sms/memberid', false);
		$password = Mage::getStoreConfig('yoyo8/sms/password', false);		
		
		$data = array('MemberID' => $memberID //廠商登入帳號
				, 'Password' => $this->PwdMd5($memberID, $password, $sourceProdID, $sourceMsgID) //已加密之登入密碼
				, 'MobileNo' => $mobileNo //接收簡訊手機號碼
				, 'CharSet' => $charSet //簡訊文字編碼（B 為Big5，U 為UTF-8，E 為英文簡訊）
				, 'LongSms' => null //長簡訊指標（如要發長簡訊，值請放大寫Y，否則省略此參數）
				, 'GlobalSms' => null //國際簡訊指標（如要發國際簡訊，值請放大寫Y，否則省略此參數）
				, 'SMSMessage' => $smsMsg //簡訊內容（如是發WapPush則為標題，請做URLencode）
				, 'WapPush' => null //WapPush 指標（如要發WapPush，值請放大寫Y，否則省略此參數）
				, 'WapLink' => null
				, 'SourceProdID' => $sourceProdID//廠商自訂字串1，長度勿超過32bytes「varchar(32)」
				, 'SourceMsgID' => $sourceMsgID //廠商自訂字串2，長度勿超過32bytes「varchar(32)」
				, 'ValidSec' => null //簡訊有效時間秒數。
		);

		/*關於自訂字串的說明
		 請廠商自行決定2個字串分別放入SourceProdID與SourceMsgID，
		重複發送時檢查用，接收時會先檢查SourceProdID + SourceMsgID。
		如SourceProdID + SourceMsgID經資料庫比對有此筆視為重複發送，
		Yoyo8系統不管其他參數為何(MobileNo,SMSMessage…等)，直接回傳status=0與
		retstr=Retry_Success，用戶不會重複收到此則簡訊，否則當作另外一則新簡訊發送。*/

		// use key 'http' even if you send the request to https://...
		$options = array(
				'http' => array(
						'header'  => "Content-type: application/x-www-form-urlencoded",
						'method'  => 'POST',
						'content' => http_build_query($data),
				),
		);
		$context  = stream_context_create($options);
		$output = file_get_contents($url, false, $context);

		//status=0&MemberID=登入帳號&MessageID=Yoyo8 帳務編號&UsedCredit=此則簡訊所花點數
		//&Credit=剩餘點數&MobileNo=接收簡訊手機號碼&retstr=回傳字串
		Mage::log("$mobileNo:::$smsMsg:::$sourceProdID:::$sourceMsgID:::$charSet:::$output" , null,"yoyo8_sms.log" );
		parse_str($output);
		
		if ($status==0)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}

	function PwdMd5($memberID, $password, $sourceProdID, $sourceMsgID)
	{
		$beforeMd5 = "$memberID:$password:$sourceProdID:$sourceMsgID";
		$afterMd5 = md5($beforeMd5);
		return $afterMd5;
	}

}
