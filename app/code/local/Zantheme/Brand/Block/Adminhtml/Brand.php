<?php
class Zantheme_Brand_Block_Adminhtml_Brand extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
		
	 
        $this->_controller = 'adminhtml_brand';
        $this->_blockGroup = 'zantheme_brand';
        $this->_headerText = Mage::helper('zantheme_brand')->__('Brands Manager');
		
        parent::__construct();

        $this->setTemplate('zantheme_brand/brand.phtml');
		
		
    }

    protected function _prepareLayout() {
	
        $this->setChild('add_new_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                'label'     => Mage::helper('zantheme_brand')->__('Add Record'),
                'onclick'   => "setLocation('".$this->getUrl('*/*/add')."')",
                'class'   => 'add'
                ))
        );
		
		$this->setChild('mass_rewrite_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                'label'     => Mage::helper('zantheme_brand')->__('Mass Generate Rewrite URLs'),
                'onclick'   => "setLocation('".$this->getUrl('*/*/massRewrite')."')",
                'class'   => 'mass'
                ))
        );
		$this->setChild('mass_resize_button',
                $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                'label'     => Mage::helper('zantheme_brand')->__('Mass Resize Logo'),
                'onclick'   => "setLocation('".$this->getUrl('*/*/massResize')."')",
                'class'   => 'mass'
                ))
        );

        $this->setChild('grid', $this->getLayout()->createBlock('zantheme_brand/adminhtml_brand_grid', 'brand.grid'));
        return parent::_prepareLayout();
    }

    public function getAddNewButtonHtml() {
        return $this->getChildHtml('add_new_button');
    }
	
	public function getMassRewriteCatButtonHtml(){
		  return $this->getChildHtml('mass_rewrite_button');
	}
	public function getMassResizeButtonHtml(){
		  return $this->getChildHtml('mass_resize_button');
	}
    public function getGridHtml() {
        return $this->getChildHtml('grid');
    }
}