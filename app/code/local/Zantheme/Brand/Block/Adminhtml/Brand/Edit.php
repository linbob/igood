<?php
class Zantheme_Brand_Block_Adminhtml_Brand_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup  = 'zantheme_brand';
        $this->_controller  = 'adminhtml_brand';

        $this->_updateButton('save', 'label', Mage::helper('zantheme_brand')->__('Save Record'));
        $this->_updateButton('delete', 'label', Mage::helper('zantheme_brand')->__('Delete Record'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    protected function _prepareLayout() {
        if (!Mage::app()->isSingleStoreMode()) {
            $this->setChild('store_switcher',
                   $this->getLayout()->createBlock('adminhtml/store_switcher')
                   ->setUseConfirm(false)
                   ->setSwitchUrl($this->getUrl('*/*/*/id/'.Mage::registry('brand_data')->get('brand_id'), array('store'=>null)))
           );
        }

        return parent::_prepareLayout();
    }
	
    public function getStoreSwitcherHtml() {
       return $this->getChildHtml('store_switcher');
    }
	
    public function getHeaderText()
    {
        if( Mage::registry('brand_data')->getId() ) {
			return Mage::helper('zantheme_brand')->__("Edit Record '%s'", $this->htmlEscape(Mage::registry('brand_data')->getTitle()));
		}else {
			return Mage::helper('zantheme_brand')->__("Add New Brand");
		}
	}
}