<?php
class Zantheme_Brand_Block_Adminhtml_Brand_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('brand_form');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('zantheme_brand')->__('Brand Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('zantheme_brand')->__('General Information'),
            'title'     => Mage::helper('zantheme_brand')->__('General Information'),
            'content'   => $this->getLayout()->createBlock('zantheme_brand/adminhtml_brand_edit_tab_form')->toHtml(),
        ));
		$this->addTab('form_section_seo', array(
            'label'     => Mage::helper('zantheme_brand')->__('SEO'),
            'title'     => Mage::helper('zantheme_brand')->__('SEO'),
            'content'   => $this->getLayout()->createBlock('zantheme_brand/adminhtml_brand_edit_tab_meta')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}