<?php
class Zantheme_Brand_Block_Brand_List extends Mage_Catalog_Block_Product_List {
	
	public function getGeneralConfig( $val ){ 
		return Mage::getStoreConfig( "zantheme_brand/general_setting/".$val );
	}
	
	public function getConfig( $val ){ 
		return Mage::getStoreConfig( "zantheme_brand/module_setting/".$val );
	}
	
    protected function _prepareLayout()
    {        
        return parent::_prepareLayout();
    }
	
	public function getBrands(){

		return Mage::getModel('zantheme_brand/brand')->getCollection();
	}
 
    public function getBrand() {
        return Mage::registry('current_brand');
    }   
}
?>