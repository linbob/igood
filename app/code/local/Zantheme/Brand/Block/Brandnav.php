<?php 
class Zantheme_Brand_Block_Brandnav extends Zantheme_Brand_Block_List 
{

	
	/**
	 * Contructor
	 */
	public function __construct($attributes = array()){
		// Mage::helper('zantheme_brand/media')->addMediaFile( "js", "zantheme_brand/menu.js" );
		parent::__construct( $attributes );
	}
	
	public function _toHtml(){
		 
		$this->setTemplate( "zantheme/brand/block/brandnav.phtml" );
		
		$collection = Mage::getModel( "zantheme_brand/brand" )->getCollection();
		
		$this->assign( "brands", $collection );
		return parent::_toHtml();	
	}
	 
}
?>