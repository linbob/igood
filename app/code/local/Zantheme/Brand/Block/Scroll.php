<?php 
class Zantheme_Brand_Block_Scroll extends Zantheme_Brand_Block_List 
{
	/**
	 * Contructor
	 */
	public function __construct($attributes = array())
	{
		
		parent::__construct( $attributes );
	}
	
	public function _toHtml(){
		$collection = Mage::getModel( 'zantheme_brand/brand' )
						->getCollection();
		

		$this->assign( 'brands', $collection );	
		$this->setTemplate( "zantheme/brand/block/scroll.phtml" );
		  
		return parent::_toHtml();
		
	}
}	