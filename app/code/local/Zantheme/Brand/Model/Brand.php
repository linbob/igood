<?php
class Zantheme_Brand_Model_Brand extends Mage_Core_Model_Abstract
{
    protected function _construct() {	
        $this->_init('zantheme_brand/brand');
    }
	
	public function getLink(){
		return  Mage::getBaseUrl().Mage::getModel('core/url_rewrite')->loadByIdPath('venusbrand/brand/'.$this->getId())->getRequestPath();
	}
	
	public function getImageUrl($type='l') {
		$tmp = explode("/", $this->getFile());
		$imageName = $type."-".$tmp[count($tmp)-1];
		return Mage::getBaseUrl( Mage_Core_Model_Store::URL_TYPE_MEDIA)."resized/".$imageName;
	}
	
	public function getIconUrl( ) {
		return Mage::getBaseUrl( Mage_Core_Model_Store::URL_TYPE_MEDIA)."/".$this->getIcon();
	}
	
	public function getFileUrl(){
		return Mage::getBaseUrl( Mage_Core_Model_Store::URL_TYPE_MEDIA)."/".$this->getFile();
	}
}