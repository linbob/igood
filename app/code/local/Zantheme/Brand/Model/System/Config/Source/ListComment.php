<?php 
class Zantheme_Brand_Model_System_Config_Source_ListComment
{	
 
    public function toOptionArray()
    {
		$output = array();
		$output[] = array("value"=>"" , "label" => Mage::helper('adminhtml')->__("Default Engine"));
		$output[] = array("value"=>"disqus" , "label" => Mage::helper('adminhtml')->__("Disqus"));
		$output[] = array("value"=>"facebook" , "label" => Mage::helper('adminhtml')->__("Facebook"));
		
        return $output ;
    }    
}
