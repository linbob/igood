<?php 
class Zantheme_Brand_BrandController extends Mage_Core_Controller_Front_Action
{  	
	public function indexAction(){
		 
	 
		$this->loadLayout();
		$this->renderLayout();
	}
	
	public function viewAction(){
		$id = (int) $this->getRequest()->getParam( 'id', false);
        $brand = Mage::getModel('zantheme_brand/brand')->load( $id );
        Mage::register('current_brand', $brand);

		$this->loadLayout();
		$this->renderLayout();
	}
}
?>