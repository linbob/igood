<?php
class Zantheme_Sideproduct_Block_Adminhtml_Banner_Add extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
		
        parent::__construct();

        $this->_objectId    = 'id';
        $this->_blockGroup  = 'zantheme_sideproduct';
        $this->_controller  = 'adminhtml_banner';

        $this->_updateButton('save', 'label', Mage::helper('zantheme_sideproduct')->__('Save Record'));		
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'content');
                }
            }            
        ";

    }

    public function getHeaderText()
    {
        Mage::helper('zantheme_sideproduct')->__("Add Record");
    }
}