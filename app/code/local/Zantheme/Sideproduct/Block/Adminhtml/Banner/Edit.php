<?php

class Zantheme_Sideproduct_Block_Adminhtml_Banner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId    = 'id';
        $this->_blockGroup  = 'zantheme_sideproduct';
        $this->_controller  = 'adminhtml_banner';

        $this->_updateButton('save', 'label', Mage::helper('zantheme_sideproduct')->__('Save Record'));
        $this->_updateButton('delete', 'label', Mage::helper('zantheme_sideproduct')->__('Delete Record'));
    }

    public function getHeaderText()
    {
        return Mage::helper('zantheme_sideproduct')->__("Edit Record '%s'", $this->htmlEscape(Mage::registry('banner_data')->getLabel()));
    }
}