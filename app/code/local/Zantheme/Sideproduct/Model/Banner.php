<?php

class Zantheme_Sideproduct_Model_Banner extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('zantheme_sideproduct/banner');
    }
}