<?php
class Zantheme_Sideproduct_Model_Config extends Mage_Catalog_Model_Product_Media_Config {

    public function getBaseMediaPath() {
        return Mage::getBaseDir('media') .DS. 'sideproduct';
    }

    public function getBaseMediaUrl() {
        return Mage::getBaseUrl('media') . 'sideproduct';
    }

    public function getBaseTmpMediaPath() {
        return Mage::getBaseDir('media') .DS. 'tmp' .DS. 'sideproduct';
    }

    public function getBaseTmpMediaUrl() {
        return Mage::getBaseUrl('media') . 'tmp/sideproduct';
    }

}