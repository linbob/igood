<?php 
class Zantheme_Slideshow_Block_Slideshow extends Zantheme_Slideshow_Block_List 
{
	public function _toHtml(){
		$source = $this->getConfig( "source" );
		if( $source ){ 
			return $this->getLayout()->createBlock("zantheme_slideshow/source_".$source)->toHtml();
		}
	 
	}
}
?>