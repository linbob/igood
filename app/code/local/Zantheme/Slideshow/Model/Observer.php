<?php

class Zantheme_Slideshow_Model_Observer
{
   public function beforeRender( Varien_Event_Observer $observer ){
		$action = Mage::app()->getRequest()->getActionName();
		if($action == 'noRoute' || $this->isAdmin() ){ return ;	}
		
		$this->_loadMedia();  
   }
   public function isAdmin() {
        if(Mage::app()->getStore()->isAdmin()) {
            return true;
        }
		if(Mage::getDesign()->getArea() == 'adminhtml')  {
            return true;
        }
        return false;
    }
   function _loadMedia( $config = array()){
		$theme = Mage::getStoreConfig("zantheme_slideshow/zantheme_slideshow/theme");
		if( Mage::getStoreConfig("zantheme_slideshow/zantheme_slideshow/show") ) { 
			$mediaHelper = Mage::helper("zantheme_slideshow/media");
			$mediaHelper->addMediaFile("js",'zantheme_slideshow/'.$theme.'/script.js');
			if($theme == "elastic"){
				$mediaHelper->addMediaFile("js",'zantheme_slideshow/'.$theme.'/jquery.eislideshow.js');
				$mediaHelper->addMediaFile("js",'zantheme_slideshow/'.$theme.'/jquery.easing.1.3.js');
			}

		}
   }
}
