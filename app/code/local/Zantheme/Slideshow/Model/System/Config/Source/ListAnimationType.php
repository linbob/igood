<?php

class Zantheme_Slideshow_Model_System_Config_Source_ListAnimationType
{
    public function toOptionArray()
    {
        return array(
			array('value'=>'random', 'label'=>Mage::helper('zantheme_slideshow')->__('Random')),
			array('value'=>'boxRandom', 'label'=>Mage::helper('zantheme_slideshow')->__('BoxRandom')),
            array('value'=>'boxRain', 'label'=>Mage::helper('zantheme_slideshow')->__('BoxRain')),
            array('value'=>'boxRainReverse', 'label'=>Mage::helper('zantheme_slideshow')->__('Box RainReverse')),
            array('value'=>'boxRainGrow', 'label'=>Mage::helper('zantheme_slideshow')->__('Box RainGrow')),
            array('value'=>'boxRainGrowReverse', 'label'=>Mage::helper('zantheme_slideshow')->__('Box RainGrowReverse')),
        	array('value'=>'sliceDownRight', 'label'=>Mage::helper('zantheme_slideshow')->__('Slice DownRight')),
            array('value'=>'sliceDownLeft', 'label'=>Mage::helper('zantheme_slideshow')->__('Slice DownLeft')),
            array('value'=>'sliceUpRight', 'label'=>Mage::helper('zantheme_slideshow')->__('Slice UpRight')),
			array('value'=>'sliceUpLeft', 'label'=>Mage::helper('zantheme_slideshow')->__('Slice UpLeft')),
            array('value'=>'sliceUpDown', 'label'=>Mage::helper('zantheme_slideshow')->__('Slice UpDown')),
            array('value'=>'sliceUpDownLeft', 'label'=>Mage::helper('zantheme_slideshow')->__('Slice UpDownLeft')),
            array('value'=>'fold', 'label'=>Mage::helper('zantheme_slideshow')->__('Fold')),
            array('value'=>'fade', 'label'=>Mage::helper('zantheme_slideshow')->__('Fade'))

        );
    }    
}
