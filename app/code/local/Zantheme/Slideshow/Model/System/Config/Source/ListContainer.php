<?php


class Zantheme_Slideshow_Model_System_Config_Source_ListContainer
{
    public function toOptionArray()
    {
        return array(
        	array('value'=>'0', 'label'=>Mage::helper('zantheme_slideshow')->__('Fit to main image')),
            array('value'=>'1', 'label'=>Mage::helper('zantheme_slideshow')->__('Full size'))
        );
    }    
}
