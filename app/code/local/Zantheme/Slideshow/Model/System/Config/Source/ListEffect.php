<?php


class Zantheme_Slideshow_Model_System_Config_Source_ListEffect
{
    public function toOptionArray()
    {
        return array(
        	array('value'=>'linear', 'label'=>Mage::helper('zantheme_slideshow')->__('Linear')),
            array('value'=>'quadOut', 'label'=>Mage::helper('zantheme_slideshow')->__('Medium to Slow')),
            array('value'=>'cubicOut', 'label'=>Mage::helper('zantheme_slideshow')->__('Fast to Slow')),
            array('value'=>'quartOut', 'label'=>Mage::helper('zantheme_slideshow')->__('Very Fast to Slow')),
            array('value'=>'quintOut', 'label'=>Mage::helper('zantheme_slideshow')->__('Uber Fast to Slow')),
            array('value'=>'expoOut', 'label'=>Mage::helper('zantheme_slideshow')->__('Exponential Speed')),
            array('value'=>'elasticOut', 'label'=>Mage::helper('zantheme_slideshow')->__('Elastic')),
            array('value'=>'backIn', 'label'=>Mage::helper('zantheme_slideshow')->__('Back In')),
            array('value'=>'backOut', 'label'=>Mage::helper('zantheme_slideshow')->__('Back Out')),
            array('value'=>'backInOut', 'label'=>Mage::helper('zantheme_slideshow')->__('Back In and Out')),
            array('value'=>'bounceOut', 'label'=>Mage::helper('zantheme_slideshow')->__('Bouncing')),
        );
    }    
}
