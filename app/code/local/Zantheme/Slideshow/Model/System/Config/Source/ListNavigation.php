<?php


class Zantheme_Slideshow_Model_System_Config_Source_ListNavigation
{
    public function toOptionArray()
    {
        return array(
        	array('value'=>'', 'label'=>Mage::helper('zantheme_slideshow')->__('No')),
            array('value'=>'number', 'label'=>Mage::helper('zantheme_slideshow')->__('Number')),
            array('value'=>'thumbs', 'label'=>Mage::helper('zantheme_slideshow')->__('Thumbnails'))
        );
    }    
}
