<?php

class Zantheme_Slideshow_Model_System_Config_Source_ListNavigationAlignment
{
    public function toOptionArray()
    {
        return array(
        	array('value'=>'horizontal', 'label'=>Mage::helper('zantheme_slideshow')->__('Horizontal')),
            array('value'=>'vertical', 'label'=>Mage::helper('zantheme_slideshow')->__('Vertical')),
            array('value'=>'0', 'label'=>Mage::helper('zantheme_slideshow')->__('Disable'))
        );
    }    
}
