<?php


class Zantheme_Slideshow_Model_System_Config_Source_ListThumbnailMode
{
    public function toOptionArray()
    {
        return array(
        	array('value'=>'none', 'label'=>Mage::helper('zantheme_slideshow')->__('Using the source image')),
            array('value'=>'resize', 'label'=>Mage::helper('zantheme_slideshow')->__('Using Image Resizing')),
            array('value'=>'crop', 'label'=>Mage::helper('zantheme_slideshow')->__('Using Image Croping'))
        );
    }    
}
