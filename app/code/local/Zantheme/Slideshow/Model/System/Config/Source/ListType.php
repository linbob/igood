<?php

class Zantheme_Slideshow_Model_System_Config_Source_ListType
{
    public function toOptionArray()
    {
        return array(
        	array('value'=>'', 'label'=>Mage::helper('zantheme_slideshow')->__('-- Please select --')),
            array('value'=>'latest', 'label'=>Mage::helper('zantheme_slideshow')->__('Latest')),
            array('value'=>'best_buy', 'label'=>Mage::helper('zantheme_slideshow')->__('Best Buy')),
            array('value'=>'most_viewed', 'label'=>Mage::helper('zantheme_slideshow')->__('Most Viewed')),
            array('value'=>'most_reviewed', 'label'=>Mage::helper('zantheme_slideshow')->__('Most Reviewed')),
            array('value'=>'top_rated', 'label'=>Mage::helper('zantheme_slideshow')->__('Top Rated')),
            array('value'=>'attribute', 'label'=>Mage::helper('zantheme_slideshow')->__('Featured Product'))
        );
    }    
}
