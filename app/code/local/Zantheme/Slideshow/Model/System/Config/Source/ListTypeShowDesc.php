<?php

class Zantheme_Slideshow_Model_System_Config_Source_ListTypeShowDesc
{
    public function toOptionArray()
    {
        return array(
        	array('value'=>'0', 'label'=>Mage::helper('zantheme_slideshow')->__('No')),
            array('value'=>'1', 'label'=>Mage::helper('zantheme_slideshow')->__('Yes'))
        );
    }    
}
