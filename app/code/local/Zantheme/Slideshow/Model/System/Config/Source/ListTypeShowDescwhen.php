<?php

class Zantheme_Slideshow_Model_System_Config_Source_ListTypeShowDescwhen
{
    public function toOptionArray()
    {
        return array(
        	array('value'=>'always', 'label'=>Mage::helper('zantheme_slideshow')->__('Always')),
            array('value'=>'mouseover', 'label'=>Mage::helper('zantheme_slideshow')->__('Mouse Over Image'))
        );
    }    
}
