<?php

class Zantheme_Themeconfig_Helper_Data extends Mage_Core_Helper_Abstract
{
	
	public function getCfgGroup($group, $storeId = NULL)
    {
		if ($storeId)
			return Mage::getStoreConfig('zantheme/' . $group, $storeId);
		else
			return Mage::getStoreConfig('zantheme/' . $group);
    }

	public function getDesignCfgSection($storeId = NULL)
    {
		if ($storeId)
			return Mage::getStoreConfig('theme_design', $storeId);
		else
			return Mage::getStoreConfig('theme_design');
    }
	
	public function getLayoutCfgSection($storeId = NULL)
    {
		if ($storeId)
			return Mage::getStoreConfig('theme_layout', $storeId);
		else
			return Mage::getStoreConfig('theme_layout');
    }
	
	public function getCfg($optionString)
    {
        return Mage::getStoreConfig('zantheme/' . $optionString);
    }
	
	public function getDesignCfg($optionString)
    {
        return Mage::getStoreConfig('theme_design/' . $optionString);
    }
	
	public function getLayoutCfg($optionString, $storeCode = NULL)
    {
        return Mage::getStoreConfig('theme_layout/' . $optionString, $storeCode);
    }	
	
	public function getLogoSrc()
    {
        return Mage::getBaseUrl('media').'upload/logo/'.Mage::getStoreConfig('theme_design/header/logo');
    }
}
