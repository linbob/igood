<?php
class Zantheme_Themeconfig_Helper_Labels extends Mage_Core_Helper_Abstract
{
	public function isOnSale($product)
	{
		$specialPrice = number_format($product->getFinalPrice(), 2);
		$regularPrice = number_format($product->getPrice(), 2);
		$lbl_display = Mage::getStoreConfig('theme_design/pro_label/lbl_display');
		$currency =  Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
		$rt = "";	
		switch ($lbl_display) {
			case 'text':
				$rt = "Sale";
				break;
			case 'percent':
				$rt = '-'.round(((($regularPrice - $specialPrice)/$regularPrice)*100), 0, PHP_ROUND_HALF_UP).'%';
				break;
			case 'price':
				$rt = "-".($regularPrice - $specialPrice).$currency;
				break;
			default:
			   $rt = "price";
		}
		return $rt;
		
	}

}
