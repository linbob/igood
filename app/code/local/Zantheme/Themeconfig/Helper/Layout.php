<?php

class Zantheme_Themeconfig_Helper_Layout extends Mage_Core_Helper_Abstract
{
	public function getMaxWidth($storeCode = NULL)
	{
		$w = Mage::helper('zantheme')->getLayoutCfg('layout/maxwidth', $storeCode);			
		//Estimate max break point
		if ($w == 1280 || $w == 1024)
			$maxWidth = 960;
		elseif ($w == 1360)
			$maxWidth = 1170;
		elseif ($w == 1440)
			$maxWidth = 1390;
		elseif ($w == 1680)
			$maxWidth = 1470;
		else
			$maxWidth = 1680;		
		return $maxWidth;
	}
}
