<?php

class Zantheme_Themeconfig_Model_System_Config_Backend_Color extends Mage_Core_Model_Config_Data
{
	public function save()
	{
		$value = $this->getValue();
		if ($value == 'rgba(0, 0, 0, 0)')
		{
			$this->setValue('transparent');
		}
		return parent::save();
	}
}
