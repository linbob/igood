<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Css_Background_Positionx
{
    public function toOptionArray()
    {
		return array(
			array('value' => 'left',	'label' => Mage::helper('zantheme')->__('left')),
            array('value' => 'center',	'label' => Mage::helper('zantheme')->__('center')),
            array('value' => 'right',	'label' => Mage::helper('zantheme')->__('right'))
        );
    }
}