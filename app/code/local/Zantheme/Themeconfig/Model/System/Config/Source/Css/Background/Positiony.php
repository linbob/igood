<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Css_Background_Positiony
{
    public function toOptionArray()
    {
		return array(
			array('value' => 'top',		'label' => Mage::helper('zantheme')->__('top')),
            array('value' => 'center',	'label' => Mage::helper('zantheme')->__('center')),
            array('value' => 'bottom',	'label' => Mage::helper('zantheme')->__('bottom'))
        );
    }
}