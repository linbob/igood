<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Css_Background_Repeat
{
    public function toOptionArray()
    {
		return array(
			array('value' => 'no-repeat',	'label' => Mage::helper('zantheme')->__('no-repeat')),
            array('value' => 'repeat',		'label' => Mage::helper('zantheme')->__('repeat')),
            array('value' => 'repeat-x',	'label' => Mage::helper('zantheme')->__('repeat-x')),
			array('value' => 'repeat-y',	'label' => Mage::helper('zantheme')->__('repeat-y'))
        );
    }
}