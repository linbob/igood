<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Element_Display
{
    public function toOptionArray()
    {
		return array(
			array('value' => 'hidden', 'label' => Mage::helper('zantheme')->__('Don\'t Display')),
            array('value' => 'hover', 'label' => Mage::helper('zantheme')->__('Display On Hover')),
            array('value' => 'visible', 'label' => Mage::helper('zantheme')->__('Display'))
        );
    }
}