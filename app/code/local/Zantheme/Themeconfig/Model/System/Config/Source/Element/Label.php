<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Element_Label
{
    public function toOptionArray()
    {
		return array(
			array('value' => 'text', 'label' => Mage::helper('zantheme')->__('Text')),
            array('value' => 'percent', 'label' => Mage::helper('zantheme')->__('Percent')),
            array('value' => 'price', 'label' => Mage::helper('zantheme')->__('Price'))
        );
    }
}