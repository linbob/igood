<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Element_Navcolumn
{
    public function toOptionArray()
    {
        return array(
			array('value' => 2, 'label' => Mage::helper('zantheme')->__('2 Columns')),
			array('value' => 3, 'label' => Mage::helper('zantheme')->__('3 Columns')),
			array('value' => 4, 'label' => Mage::helper('zantheme')->__('4 Columns')),
            array('value' => 5, 'label' => Mage::helper('zantheme')->__('5 Columns')),
			array('value' => 6, 'label' => Mage::helper('zantheme')->__('6 Columns')),
			array('value' => 7, 'label' => Mage::helper('zantheme')->__('7 Columns')),
			array('value' => 8, 'label' => Mage::helper('zantheme')->__('8 Columns')),
			array('value' => 9, 'label' => Mage::helper('zantheme')->__('9 Columns')),
			array('value' => 10, 'label' => Mage::helper('zantheme')->__('10 Columns'))
        );
    }
}