<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Layout_Layout
{
    public function toOptionArray()
    {
		return array(
			array('value' => 'wide','label' => Mage::helper('zantheme')->__('wide')),
			array('value' => 'boxed','label' => Mage::helper('zantheme')->__('boxed'))

        );
    }
}
