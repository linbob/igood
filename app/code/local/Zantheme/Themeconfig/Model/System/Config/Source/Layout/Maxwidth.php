<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Layout_Maxwidth
{
    public function toOptionArray()
    {
		return array(
			array('value' => '1024','label' => Mage::helper('zantheme')->__('1024px')),
			array('value' => '1280','label' => Mage::helper('zantheme')->__('1280px')),
			array('value' => '1360','label' => Mage::helper('zantheme')->__('1360px')),
			array('value' => '1440','label' => Mage::helper('zantheme')->__('1440px')),
			array('value' => '1600','label' => Mage::helper('zantheme')->__('1600px')),
			array('value' => '1920','label' => Mage::helper('zantheme')->__('1920px'))
        );
    }
}
