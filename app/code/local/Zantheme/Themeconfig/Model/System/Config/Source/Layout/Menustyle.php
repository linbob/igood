<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Layout_Menustyle
{
    public function toOptionArray()
    {
		return array(
			array('value' => 'style1','label' => Mage::helper('zantheme')->__('Default')),
			array('value' => 'style2','label' => Mage::helper('zantheme')->__('Columns'))
        );
    }
}
