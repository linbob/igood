<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Layout_Productstyle
{
    public function toOptionArray()
    {
		return array(
			array('value' => 'pt-default','label' => Mage::helper('zantheme')->__('default')),
			array('value' => 'pt-style2','label' => Mage::helper('zantheme')->__('style2'))

        );
    }
}
