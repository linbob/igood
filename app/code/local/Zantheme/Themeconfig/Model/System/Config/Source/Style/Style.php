<?php

class Zantheme_Themeconfig_Model_System_Config_Source_Style_Style
{
    public function toOptionArray()
    {
		return array(
			array('value' => 'style1','label' => Mage::helper('zantheme')->__('style1')),
			array('value' => 'style2','label' => Mage::helper('zantheme')->__('style2'))

        );
    }
}
