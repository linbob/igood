<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php
ini_set("display_errors", "On");
require("app/Mage.php");
Mage::init();

$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');

$sql = "SELECT * FROM ichannels_order";
$result = $readConnection->fetchAll($sql);
?>
<table>
    <tr>
        <td>gid</td>
        <td>訂單編號</td>
        <td>金額</td>
        <td>狀態</td>
        <td>產生時間</td>
    </tr>
    <?php foreach ($result as $ichannelOrder) { ?>
    <tr>
        <td><?php echo $ichannelOrder['gid']; ?></td>
        <td><?php echo $ichannelOrder['master_order_id']; ?></td>
        <td><?php echo $ichannelOrder['amount']; ?></td>
        <td><?php if ($ichannelOrder['status'] == "unconfirmed") { echo "未確認"; } elseif ($ichannelOrder['status'] == "canceled") { echo "已取消"; } elseif ($ichannelOrder['status'] == "established") { echo "已完成交易"; } ?></td>
        <td><?php echo date("Y-m-d H:i:s", $ichannelOrder['created_time']); ?></td>
    </tr>
    <?php }?>
</table>
</body>
</html>
