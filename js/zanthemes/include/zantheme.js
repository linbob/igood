jQuery(function($) {	
	$(window).load(function() {
		/* Dropdown */
		$(".drop-dow > .ico > a").click(function (e) {
			if ($(this).parent().hasClass('selected')) {
				$(".drop-dow .selected .drop-content").stop().fadeOut(0);
				$(".drop-dow .selected").removeClass("selected");
			} else {
				$(".drop-dow .selected .drop-content").stop().fadeOut(0);
				$(".drop-dow .selected").removeClass("selected");
				if ($(this).next(".subs").length) {
					$(this).parent().addClass("selected");
					$(this).next(".subs").children().stop().fadeIn(400);
				}
			}
			e.stopPropagation();
		});
		$('body').click(function(event) {
			var $target = $(event.target);
			if ($target.parents('.drop-dow').length == 0) {
			  $(".drop-dow .selected .drop-content").slideUp(100);
			  $(".drop-dow .selected").removeClass("selected");
			}
		});

	});
	$(document).ready(function() {	
		/* Top menu */
		$('#nav li.parent').hover(function(){
			$(this).find('> ul').stop().fadeIn(300);
		},function(){
			$(this).find('> ul').stop().fadeOut(0);
		});
		
		$('.block-layered-nav dt').click(function(){
			$(this).toggleClass("active");
			$(this).next().stop().slideToggle("fast");
		});			
		
		/* Hover */
		$('.product-block').hover(function(){
			$(this).find('.hover').stop().fadeIn(300);
		},function(){
			$(this).find('.hover').stop().fadeOut(000);
		});
	});
});