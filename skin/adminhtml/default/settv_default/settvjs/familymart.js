$.noConflict();

var popupWindowHandler = {
openWindow : "",
openFamilyMart : function() {
	this.openWindow = window.open(jQuery("#familymart-url").val()
    		, "_blank"
    		, "status=1, width=1024, height=600, resizable=1"
           );
    },
    setResult: function(fmcvsspot, fmname, fmaddr) {
    	jQuery("#fm_pickup_storecode").val(fmcvsspot);
    	jQuery("#fm_pickup_storename").val(fmname);
    	jQuery("#fm_pickup_address").val(fmaddr);
    }
}