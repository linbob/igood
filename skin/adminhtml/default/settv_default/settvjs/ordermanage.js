$.noConflict();

function changeOrderStatus(selectId) {
	selectedOption = jQuery("#" + selectId + " option:selected");
	if (selectedOption.val() != null && selectedOption.val().length > 0) {
		confirmSetLocation("將此筆" + selectedOption.text(), selectedOption.val());
	}
}

function confirmSetLocationWithConfirmCallback(message, url, callback){
    if( confirm(message) ) {
		callback();
        setLocation(url);
    }
    return false;
}