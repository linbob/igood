$.noConflict();

function productApprovalConfirm() {
	// status of 1 is active
	if(jQuery("#status").val() == 1) {
		return confirm("商品儲存後將會直接上線, 請確認是否要儲存?");
	} else {
		return true;
	}
}

function disableAllProductInput() {
	jQuery(":input").attr("disabled","disabled");
	jQuery(".flex").remove();
	jQuery(":button").remove();
}