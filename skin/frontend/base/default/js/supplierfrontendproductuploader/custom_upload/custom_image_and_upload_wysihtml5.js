var xhrFetchingImages;

(function($){

wysiHelpers = {
  getImageTemplate: function() {
    /* this is what goes in the wysiwyg content after the image has been chosen */
    var tmpl;
    var imgEntry = "<img src='<%= url %>''>";
    tmpl = _.template("<div class='shrink_wrap'>" +
                      imgEntry +
                      "</div>");
    return tmpl;
  }
};

bootWysiOverrides = {
  initInsertImage: function(toolbar) {
    var self = this;
    var insertImageModal = toolbar.find('.bootstrap-wysihtml5-insert-image-modal');
    var urlInput = insertImageModal.find('.bootstrap-wysihtml5-insert-image-url');
    var insertButton = insertImageModal.find('a.btn-primary');
    var initialValue = urlInput.val();
    
    var chooser = insertImageModal.find('.image_chooser.images');
    /* this is the template we put in the image dialog */
    var optionTemplate = _.template(
        "<img class='span1' src='<%= file %>' width='200'  data-type='image' data-url='<%= file %>'>"
        );
    
    var helpers = wysiHelpers;
    
    // populate chooser
    // TODO: this get's called once for each wysiwyg on the page.  we could 
    //       be smarter and cache the results after call 1 and use them later.
    if (!xhrFetchingImages) {
      $.ajax({
        url:custom_image_list_uri,
        success: function(data) {
          xhrFetchingImages = false;
          // populate dropdowns
          _.each(data, function(img) {
        	  if(!img.file){
        		  return;
        	  }
            chooser.append(optionTemplate(img));
          });
        }
      });
    }

    var insertImage = function(imageData) {
      if(imageData.url) {
        var clz = 'image_container';
        var doc = self.editor.composer.doc;
        var tmpl = helpers.getImageTemplate();
        var chunk = tmpl(imageData);
        self.editor.composer.commands.exec("insertHTML", chunk);
      }
    };
    
    chooser.on('click', 'img', function(ev) {
      var $row = $(ev.currentTarget);
      insertImage($row.data());
      insertImageModal.modal('hide');
    });
    
    insertImageModal.on('hide', function() {
    	var file = $('#file1');
    	file.replaceWith( file = file.clone( true ) );
    	$('#uploadresult').html('');
      self.editor.currentView.element.focus();
    });
    
    toolbar.find('a[data-wysihtml5-command=insertImage]').click(function() {
      var activeButton = $(this).hasClass("wysihtml5-command-active");
      
      if (!activeButton) {
        insertImageModal.modal('show');
        
        $('#file1').unbind('change').change(function() {
    	    $(this).uploadimage(custom_upload_uri, function(res) {
    	      if(res.status)
    	      {
    		      chooser.append(optionTemplate({"file":res.file}));
    		      $('#uploadresult').html('上傳成功').removeClass().addClass('alert alert-success');
    		    }
    		    else
    		    {
    		      $('#uploadresult').html('上傳失敗').removeClass().addClass('alert alert-error');
    	      }		
    	      }, 'json');
    	  });
        insertImageModal.on('click.dismiss.modal', '[data-dismiss="modal"]', function(e) {
          e.stopPropagation();
        });
        return false;
      }
      else {
        return true;
      }
    });
    

  }
};

$.extend($.fn.wysihtml5.Constructor.prototype, bootWysiOverrides);

$(function() {

  // override options
  var wysiwygOptions = {
    customTags: {
      "em": {},
      "strong": {},
      "hr": {}
    },
    customStyles: {
      // keys with null are used to preserve items with these classes, but not show them in the styles dropdown
      'shrink_wrap': null,
      'credit': null,
      'tombstone': null,
      'chat': null,
      'height':'800px',
    },
    customTemplates: {
      /* this is the template for the image button in the toolbar */
      image: function(locale) {
        return "<li>" +
          "<div class='bootstrap-wysihtml5-insert-image-modal modal fade'>" +
          "<div class='modal-header'>" +
          "<input type=\"hidden\" name=\"product_id\" id=\"custom_upload_product_id\"><a class='close' data-dismiss='modal'>&times;</a>" +
          "<h3>插入圖片</h3>" +
          "</div>" +
          "<div class='modal-body'>" +
          "<div class='chooser_wrapper'>" +
          "<h4>點選圖片插入附加資訊</h4>" +
          "<table class='image_chooser images'></table>" +
          "<h4>上傳圖片</h4>" +
          "<input name=\"file1\" id=\"file1\" type=\"file\">" +
          "<div id=\"uploadresult\"></div>" +
          "</div>" +
          "</div>" +
          "<div class='modal-footer'>" +
          "<a href='#' class='btn' data-dismiss='modal'>取消</a>" +
          "</div>" +
          "</div>" +
          "<a class='btn' data-wysihtml5-command='insertImage' title='插入圖片'><i class='icon-picture'></i></a>" +
          "</li>";
      }
    }
  };

  $('.tip').tooltip();
  jQuery('.wysiwyg').each(function(){
      if(this.name == 'in_depth'){
    	  $(this).wysihtml5($.extend(wysiwygOptions, {html:true, color:false,image:true, stylesheets:[]}));
      }else{
    	  $(this).wysihtml5($.extend(wysiwygOptions, {html:true, color:false,image: false, stylesheets:[]}));
      }
  });
  $('#custom_upload_product_id').val( $('input[name="product_id"]:hidden').val() );
  $('.dropdown-toggle').dropdown();
  /*
  $('textarea.wysiwyg').each(function() {
    $(this).wysihtml5($.extend(wysiwygOptions, {html:true, color:false, stylesheets:[]}));
  });
  */
});

})(jQuery);