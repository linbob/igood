function setFeaturedProduct(url, cate_id, page) {
	var number_per_page = 6;
	var category_id = cate_id;
	var page_number = page;
	var url_string = url + "settvjson/catalog/category?p=" + page_number
			+ "&cid=" + category_id + "&ipp=" + number_per_page;

	new Ajax.Request(url_string, {
		method : 'get',
		onSuccess : function(transport) {
			// var notice = $('notice');
			if (transport.responseText != null) {
				// alert(div_id);
				var prdResult = eval('(' + transport.responseText + ')');
				var prdArray = prdResult['products'];

				var innerString = "";
				for ( var i = 0; i < prdArray.length; i++) {
					innerString += '<li><a href="' + prdArray[i]['url'] + '">';
					innerString += '<img src="' + prdArray[i]['img_url']
							+ '" width="300" height="225">';
					
					innerString += '<span><em>$</em>' + prdArray[i]['special_price'] + '</span>';
					
					/*
					if(prdArray[i]['price'] == prdArray[i]['special_price']){
						innerString += '<span><em>$</em>' + prdArray[i]['price']
									+ '</span>';
					}else{
						innerString += '<span><p class="ex"><em>$</em>' + prdArray[i]['price'] + '</p><em>$</em>' + prdArray[i]['special_price']
						+ '</span>';
					}
					*/
					innerString += '<div class="txtArea">';
					innerString += '<h2>' + prdArray[i]['name'] + '</h2>';
					innerString += '<p>' + prdArray[i]['short_desc'] + '</p>';
					innerString += '</div>';
					if (prdArray[i]['spacial_note'].length > 0) {
						innerString += '<div class="tab"><em>'
								+ prdArray[i]['spacial_note'] + '</em></div>';
					}
					innerString += '</a></li>'
				}
				document.getElementById("CharaInner").innerHTML = innerString;

				// update page selector
				var ulElement = document.getElementById("CharaCtrlInner");
				var kids = ulElement.childNodes;

				for ( var i = 0; i < kids.length; i++) {
					if (i == page_number) {
						kids[i].childNodes[0].className = "act";
					} else {
						kids[i].childNodes[0].className = "";
					}
				}
			} else {
				alert("error");
			}

		}
	});
}

function setFeaturedProductControl(url, cate_id) {
	var number_per_page = 6;
	var category_id = cate_id;
	var url_string = url + "settvjson/catalog/categoryPrdTotal?&cid="
			+ category_id;
	new Ajax.Request(
			url_string,
			{
				method : 'get',
				onSuccess : function(transport) {
					if (transport.responseText != null) {
						var total = transport.responseText;
						var innerString = "";

						for ( var i = 0; i < (total / number_per_page); i++) {
							innerString += '<li class="notext"><a href="javascript:setFeaturedProduct(\''
									+ url
									+ '\', '
									+ category_id
									+ ', '
									+ i
									+ ')">' + (i + 1) + '</a></li>';
						}

						document.getElementById("CharaCtrlInner").innerHTML = innerString;
						var ulElement = document
								.getElementById("CharaCtrlInner");
						ulElement.childNodes[0].childNodes[0].className = "act";
					}
				}
			});
}

function setNormalProduct(url, cate_id, page) {
	var number_per_page = 8;
	var category_id = cate_id;
	var page_number = page;
	var url_string = url + "settvjson/catalog/category?p=" + page_number
			+ "&cid=" + category_id + "&ipp=" + number_per_page;

	new Ajax.Request(url_string, {
		method : 'get',
		onSuccess : function(transport) {
			// var notice = $('notice');
			if (transport.responseText != null) {
				// alert(div_id);
				var prdResult = eval('(' + transport.responseText + ')');
				var prdArray = prdResult['products'];

				var innerString = "";
				for ( var i = 0; i < prdArray.length; i++) {
					innerString += '<li><a href="' + prdArray[i]['url'] + '">';

					innerString += '<div class="txtArea">';
					innerString += '<img src="' + prdArray[i]['img_url']
							+ '" width="220" height="165">';
					innerString += '<h2>' + prdArray[i]['name'] + '</h2>';
					innerString += '<p>' + prdArray[i]['short_desc'] + '</p>';
					innerString += '</div>';
					
					innerString += '<span><em>$</em>' + prdArray[i]['special_price'] + '</span>';
					/*
					if(prdArray[i]['price'] == prdArray[i]['special_price']){
						innerString += '<span><em>$</em>' + prdArray[i]['price']
									+ '</span>';
					}else{
						innerString += '<span><p class="ex"><em>$</em>' + prdArray[i]['price'] + '</p><em>$</em>' + prdArray[i]['special_price']
						+ '</span>';
					}
					*/
					if (prdArray[i]['spacial_note'].length > 0) {
						innerString += '<div class="tab"><em>'
								+ prdArray[i]['spacial_note'] + '</em></div>';
					}
					innerString += '</a></li>'
				}
				document.getElementById("general").innerHTML = innerString;

				// update page selector
				var ulElement = document.getElementById("NormalCtrlInner");
				var kids = ulElement.childNodes;

				for ( var i = 0; i < kids.length; i++) {
					if (i == page_number) {
						kids[i].childNodes[0].className = "act";
					} else {
						kids[i].childNodes[0].className = "";
					}
				}
			} else {
				alert("error");
			}

		}
	});
}

function setNormalProductControl(url, cate_id) {
	var number_per_page = 8;
	var category_id = cate_id;
	var url_string = url + "settvjson/catalog/categoryPrdTotal?&cid="
			+ category_id;
	new Ajax.Request(
			url_string,
			{
				method : 'get',
				onSuccess : function(transport) {
					if (transport.responseText != null) {
						var total = transport.responseText;
						var innerString = "";

						for ( var i = 0; i < (total / number_per_page); i++) {
							innerString += '<li class="notext"><a href="javascript:setNormalProduct(\''
									+ url
									+ '\', '
									+ category_id
									+ ', '
									+ i
									+ ')">' + (i + 1) + '</a></li>';
						}

						document.getElementById("NormalCtrlInner").innerHTML = innerString;
						var ulElement = document
								.getElementById("NormalCtrlInner");
						ulElement.childNodes[0].childNodes[0].className = "act";
					}
				}
			});
}