function setFeaturedProduct(url, cate_id, page) {
	
	var nowcategory = document.getElementById("nowcategory").value;
	var page_number = parseInt(document.getElementById("nowpage").value);
	var category_id = document.getElementById("FeaturedId").value;
	var url = document.getElementById("NowUrl").value;
	var StoreCode = document.getElementById("StoreCode").value;
	var TotCnt = 0;
	
	if(nowcategory == "Featured"){
		document.getElementById("Chara").innerHTML = "";
		document.getElementById("common").innerHTML = "";
		document.getElementById("pageTab").innerHTML = "";
		document.getElementById("showbox").innerHTML = "";
				
		document.getElementById("phonelinker").innerHTML = '<div><a href="javascript: return false;" class="notext s1-act">特色商品</a></div><div><a href="javascript:setChangeControl(\'Normal\')" class="notext s2" >一般商品</a></div>';
			
		var number_per_page = 5;
		var url_string = url + "settvjson/catalog/category?p=" + page_number
				+ "&cid=" + category_id + "&ipp=" + number_per_page;
	
		new Ajax.Request(url_string, {
			method : 'get',
			onSuccess : function(transport) {
				if (transport.responseText != null) {
					var prdResult = eval('(' + transport.responseText + ')');
					var prdArray = prdResult['products'];
					
					var innerString = "";
					for ( var i = 0; i < prdArray.length; i++) {
						innerString += '<div class="imgBox"><a href="' + prdArray[i]['url'] + '">'; 
						if(prdArray[i]['spacial_note'].length > 0){
							innerString+='<div class="tab"><em>'+prdArray[i]['spacial_note']+'</em></div>';
							}
						innerString += '<div><img src="' + prdArray[i]['img_url'] + '"></div>';
						innerString += '<div class="txtArea"><h2>' + prdArray[i]['name'] + '</h2><p>' + prdArray[i]['short_desc'] + '</p></div>'; 
						innerString += '</a></div>';
						TotCnt = parseInt(prdArray[i]['TotCnt']);
					};
					
					if(page_number == 0){
						document.getElementById("phonefeatured").innerHTML = innerString;
					}else{
						var Ihtml = document.getElementById("phonefeatured").innerHTML;
						document.getElementById("phonefeatured").innerHTML = Ihtml + innerString;
					}
					
					var NowCnt = 5 * page_number + prdArray.length;
					document.getElementById("NowCnt").value = NowCnt;
					document.getElementById("nowpage").value = 1 + page_number;
					
					//var TotCnt = parseInt(document.getElementById("FeaturedTot").value);
					if(NowCnt < TotCnt){
						document.getElementById("phonefeaturedctrl").innerHTML = '<div class="moreDiv notext"><a href="javascript: setFeaturedProduct(\'\', \'\', \'\')">more</a></div>';
					}else{
						if(StoreCode == "default"){
							document.getElementById("phonefeaturedctrl").innerHTML = "";
						}else{
							document.getElementById("phonefeaturedctrl").innerHTML = '<div class="moreDiv notext"><a href="javascript: SetGo()">more</a></div>';
						}
					}
//					// update page selector
//					var ulElement = document.getElementById("CharaCtrlInner");
//					var kids = ulElement.childNodes;
//	
//					for ( var i = 0; i < kids.length; i++) {
//						if (i == page_number) {
//							kids[i].childNodes[0].className = "act";
//						} else {
//							kids[i].childNodes[0].className = "";
//						}
//					}
				} else {
					alert("error");
				}
	
			}
		});
	}
}

function setFeaturedProductControl(url, cate_id) {
//	var category_id = cate_id;
//	var url_string = url + "settvjson/catalog/categoryPrdTotal?&cid="
//			+ category_id;
//	new Ajax.Request(
//			url_string,
//			{
//				method : 'get',
//				onSuccess : function(transport) {
//					if (transport.responseText != null) {
//						var total = transport.responseText;
//						document.getElementById("FeaturedTot").value = total;
//					}
//				}
//			});
}

function setNormalProduct(url, cate_id, page) {
	var nowcategory = document.getElementById("nowcategory").value;
	var page_number = parseInt(document.getElementById("nowpage").value);
	var category_id = document.getElementById("NormalId").value;
	var url = document.getElementById("NowUrl").value;
	var StoreCode = document.getElementById("StoreCode").value;
	var TotCnt = 0;

	if(nowcategory == "Normal"){
		document.getElementById("Chara").innerHTML = "";
		document.getElementById("common").innerHTML = "";
		
		document.getElementById("phonelinker").innerHTML = '<div><a href="javascript:setChangeControl(\'Featured\')" class="notext s1">特色商品</a></div><div><a href="javascript: return false;" class="notext s2-act">一般商品</a></div>';
		
		var number_per_page = 5;
		var url_string = url + "settvjson/catalog/category?p=" + page_number
				+ "&cid=" + category_id + "&ipp=" + number_per_page;
	
		new Ajax.Request(url_string, {
			method : 'get',
			onSuccess : function(transport) {
				// var notice = $('notice');
				if (transport.responseText != null) {
					// alert(div_id);
					var prdResult = eval('(' + transport.responseText + ')');
					var prdArray = prdResult['products'];
	
					var innerString = "";
					for ( var i = 0; i < prdArray.length; i++) {
						innerString += '<div class="imgBox"><a href="' + prdArray[i]['url'] + '">'; 
						if(prdArray[i]['spacial_note'].length > 0){
							innerString+='<div class="tab"><em>'+prdArray[i]['spacial_note']+'</em></div>';
							}
						innerString += '<div><img src="' + prdArray[i]['img_url'] + '"></div>';
						innerString += '<div class="txtArea"><h2>' + prdArray[i]['name'] + '</h2><p>' + prdArray[i]['short_desc'] + '</p></div>'; 
						innerString += '</a></div>';	
						TotCnt = parseInt(prdArray[i]['TotCnt']);
					};
					
					if(page_number == 0){
						document.getElementById("phonenormal").innerHTML = innerString;
					}else{
						var Ihtml = document.getElementById("phonenormal").innerHTML;
						document.getElementById("phonenormal").innerHTML = Ihtml + innerString;
					}

					var NowCnt = 5 * page_number + prdArray.length;
					document.getElementById("NowCnt").value = NowCnt;
					document.getElementById("nowpage").value = 1 + page_number;
					
					//var TotCnt = parseInt(document.getElementById("NormalTot").value);
					if(NowCnt < TotCnt){
						document.getElementById("phonenormalctrl").innerHTML = '<div class="moreDiv notext"><a href="javascript: setNormalProduct(\'\', \'\', \'\')">more</a></div>';
					}else{
						if(StoreCode == "default"){
							document.getElementById("phonenormalctrl").innerHTML = "";
						}else{
							document.getElementById("phonenormalctrl").innerHTML = '<div class="moreDiv notext"><a href="javascript: SetGo()">more</a></div>';
						}
					}
										
					// update page selector
//					var ulElement = document.getElementById("NormalCtrlInner");
//					var kids = ulElement.childNodes;
//	
//					for ( var i = 0; i < kids.length; i++) {
//						if (i == page_number) {
//							kids[i].childNodes[0].className = "act";
//						} else {
//							kids[i].childNodes[0].className = "";
//						}
//					}
				} else {
					alert("error");
				}
	
			}
		});
	}
}

function setNormalProductControl(url, cate_id) {
//	var category_id = cate_id;
//	var url_string = url + "settvjson/catalog/categoryPrdTotal?&cid="
//			+ category_id;
//	new Ajax.Request(
//			url_string,
//			{
//				method : 'get',
//				onSuccess : function(transport) {
//					if (transport.responseText != null) {
//						var total = transport.responseText;
//						document.getElementById("NormalTot").value = total;
//					}
//				}
//			});
}

function setChangeControl(CategoryName){
	document.getElementById("nowcategory").value = CategoryName;
	document.getElementById("nowpage").value = 0;

	
	if(CategoryName == "Normal"){
		document.getElementById("phonefeatured").innerHTML ="";
		document.getElementById("phonefeaturedctrl").innerHTML = "";
		setNormalProduct("","","")
	} else {
	//if(CategoryName == "Featured"){
		document.getElementById("phonenormal").innerHTML ="";
		document.getElementById("phonenormalctrl").innerHTML = "";
		setFeaturedProduct("","","")
	}
}

function SetGo(){
	var url = document.getElementById("NowUrl").value;
	var nowcategory = document.getElementById("nowcategory").value;
	if(nowcategory == "Normal"){
		location.href = url + 'normal.html';
	}else{
		location.href = url + 'featured.html';
	}	
}