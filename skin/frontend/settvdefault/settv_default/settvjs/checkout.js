jQuery.noConflict();

jQuery(function (jQuery) {
    jQuery(".btnbox a").click(checkFamilymartLogistic);

    jQuery("#shipping-address-select").change(checkSameAbove);

    jQuery('.invoicetype').find('input[name="check[invoicetype]"]').change(invoicevalidation);

    jQuery('.invoicetype').find('input[name="check[hascarriernumber]"]').change(function () {
        if (jQuery(this).val() == "1") {
            jQuery('table.carrierinfo').show();
            jQuery('input[name="val_carriernumber"]').addClass('validate-carriercode');
            jQuery('input[name="carriernumber"],input[name="val_carriernumber"]').addClass('required-entry');
        }
        else {
            jQuery('table.carrierinfo').hide();
            jQuery('input[name="val_carriernumber"]').removeClass('validate-carriercode');
        }
    });


    Validation.add('validate-carriercode', '載具條碼不正確.', function (v) {
        var result = false;
        if (jQuery('input[name="carriernumber"]').val() == ''
            || jQuery('input[name="val_carriernumber"]').val() == '') {
            return true;
        }        
        jQuery.ajax({
            type: "POST",
            url: './validateCarrierBarcode/',
            data: { carriernumber: jQuery('input[name="val_carriernumber"]').val() },
            async: false,
            cache: false
        })
        .done(function (data) {
            if (data == 'true') {                
                result = true;
            }
            else {                
                result = false;
            }
        });
        return result;
    });



    Validation.add('validate-cstring', '輸入結果不一致.', function (v) {
        var conf = jQuery('.validate-cstring')[0];
        var val_string = false;
        if (jQuery('.validate-cstring')) {
            val_string = jQuery('.validate-cstring');
        }
        var val_strings = jQuery('.validate-string');
        for (var i = 0; i < val_strings.size() ; i++) {
            var val_string = val_strings[i];
            if (val_string.up('form').id == conf.up('form').id) {
                val_string = val_string;
            }
        }
        if (jQuery('.validate-string').size()) {
            val_string = jQuery('.validate-string')[0];
        }
        return (val_string.value == conf.value);
    });

});

function checkFamilymartLogistic() {
    // remove 貨到付款 if logistic is familymart
    var logistic = jQuery("input[name='billing[logistic]']:checked").val();
    if (logistic == "familymart") {
        jQuery("#p_method_cashondelivery").parent().remove();
    }
}

function checkSameAbove() {
    // show sameAbove address checkbox only if new address is chosen above
    if (jQuery("#billing-address-select option:selected").text() == "新地址") {
        jQuery("#sameAbove").show();
    }
}

function copyAddress() {
    if (jQuery("#sameAboveCheckBox").is(':checked') == true) {
        jQuery("#shipping\\:firstname").val(
				jQuery("#billing\\:firstname").val());
        jQuery("#shipping\\:telephone").val(
				jQuery("#billing\\:telephone").val());
        jQuery("#shipping\\:cellphone").val(
				jQuery("#billing\\:cellphone").val());
        jQuery("#Scity").val(jQuery("#City").val()).change();
        jQuery("#Sregion_id").val(jQuery("#region_id").val());
        jQuery("#shipping\\:street1").val(jQuery("#billing\\:street1").val());
    }
}

function invoicevalidation() {
    var invoiceType = jQuery(this).val();
    if (invoiceType == '5') {
        //二聯式電子發票
    	jQuery('.invoicetypeblock').hide();
    	jQuery(this).next('.invoicetypeblock').show();
    	
        jQuery('#donateorgLOV').removeClass('validate-select');
        
        jQuery('input[name="check[invoiceubn]"],input[name="check[invoicecom]"]').removeClass('required-entry');
    }
    else if (invoiceType == '4') {
        //捐贈二聯式發票
    	jQuery('.invoicetypeblock').hide();
    	jQuery(this).next('.invoicetypeblock').show();
    	
        jQuery('input[name="carriernumber"],input[name="val_carriernumber"]').removeClass('required-entry');
        jQuery('input[name="val_carriernumber"]').removeClass('validate-carriercode');
        jQuery('input[name="check[invoiceubn]"],input[name="check[invoicecom]"]').removeClass('required-entry');
        
        jQuery('#donateorgLOV').addClass('validate-select');
    }
    else if(invoiceType == '3') {    
    	//三聯式紙本發票
    	jQuery('.invoicetypeblock').hide();
    	jQuery(this).next('.invoicetypeblock').show();
    	
    	jQuery('#donateorgLOV').removeClass('validate-select');
    	jQuery('input[name="carriernumber"],input[name="val_carriernumber"]').removeClass('required-entry');
    	jQuery('input[name="val_carriernumber"]').removeClass('validate-carriercode');
    	
    	jQuery('input[name="check[invoiceubn]"],input[name="check[invoicecom]"]').addClass('required-entry');
    }
    jQuery('.validation-advice').hide();
}
