jQuery.noConflict();
var _adBlockTImer = null;
var _adBlockMoveDown = true;

jQuery(function(jQuery) {
	AlertBar();
	AdBlock();
	ImageArray();
	PopupBlock();
});

function PopupBlock() {
	var popupItem = jQuery("#home_pop_up");
	if (popupItem.length > 0 && popupItem.css("display") == "block") {
		var title = popupItem.attr("title");
		var winWidth = popupItem.attr("width");
		var winHeight = popupItem.attr("height");
		popupItem.dialog({
			height : winHeight,
			width : winWidth,
			modal : true,
			show : {
				effect : "fadeIn",
				duration : 1500
			},
			hide : {
				effect : "fadeOut",
				duration : 1000
			}
		});
		jQuery(".ui-dialog-title").hide();
		jQuery(".ui-widget-overlay").click(function() {
			jQuery("#home_pop_up").dialog("close");
		})
	}
}

function AlertBar() {
	var alertBar = jQuery(".alertBar:first");
	if (alertBar.find(".widget").html() == "") {
		alertBar.hide();
	} else {
		alertBar.find(".btn:first").click(function() {
			jQuery(".alertBar:first").animate({
				height : 0,
				opacity : 0.5
			}, 500, "linear", function() {
				jQuery(".alertBar:first").hide();
			});
		});
	}
}

function AdBlock() {
	// adBlock S
	var showbox = jQuery('#adblock .showbox');
	var showboxTitle = jQuery('#adblock .link');
	jQuery('#adblock .adTemplate > div > div').each(
			function() {
				var title = jQuery(this).attr("title");
				jQuery(this).find("a").attr("target", "_blank");
				var html = jQuery(this).html();
				showboxTitle.append("<li><a href='#'>" + title
						+ "<div class='arrow-left'>&nbsp;</div></a></li>");
				showbox.append("<li>" + html + "</li>");
			});
	jQuery('#adblock .adTemplate').remove();
	jQuery('#adblock ul.link li a').each(function(i) {
		// 把選單排好位置
		jQuery(this).css('top', i * 80);
	}).mouseover(function() {
		AdBlockMouseover(jQuery(this));
	}).focus(function() {
		jQuery(this).blur();
	}).eq(0).mouseover();
	// adBlock E

	_adBlockTImer = setInterval(RunAdBlock, 3000);

	jQuery('#adblock').mouseover(function() {
		clearInterval(_adBlockTImer);
		_adBlockMoveDown = true;
	}).mouseout(function() {
		_adBlockTImer = setInterval(RunAdBlock, 3000);
	});
}

function AdBlockMouseover(item) {
	var adHeight = 400, animateSpeed = 400;
	// 找出目前 li 是在選單中的第幾個(jQuery 1.4)
	no = item.parent().index();
	// 先移除有 .selected 的超連結的樣式
	jQuery('#adblock ul.link li a.selected').removeClass('selected');
	// 再把目前點擊到的超連結加上 .selected
	item.addClass('selected');

	// 把 ul.showbox 的 top 移到相對應的高度
	jQuery('#adblock ul.showbox').stop().animate({
		top : adHeight * no * -1
	}, animateSpeed);
}
function RunAdBlock() {
	var selectedItem = jQuery('#adblock ul.link li a.selected');
	if (jQuery('#adblock ul.link li').first().find("a.selected").length > 0)
		_adBlockMoveDown = true;
	else if (jQuery('#adblock ul.link li').last().find("a.selected").length > 0)
		_adBlockMoveDown = false;

	if (_adBlockMoveDown)
		AdBlockMouseover(selectedItem.parent().next().find("a"));
	else
		AdBlockMouseover(selectedItem.parent().prev().find("a"));
}

var _imgDiv = jQuery('<div class="box photo col3 masonry-brick"></div>');
var _imageArrayFirstPageSize = 6;
var _imageArrayAppendPageSize = 6;
var _imageArrayCurPager = 1;
var _imageArrayCurItemIndex = 0;

function AppendImages() {
	var imgAry = jQuery('#imgArray');
	jQuery.each(jQuery('.imgAryContainer .imgTemplate > div > a'), function(
			idx, value) {
		if (idx >= _imageArrayCurItemIndex
				&& idx < _imageArrayFirstPageSize + _imageArrayCurPager
						* _imageArrayAppendPageSize) {
			var temp = _imgDiv.clone();
			temp.html(jQuery(value).outerHTML());
			jQuery(temp).fadeIn();
			imgAry.append(jQuery(temp)).masonry('appended', jQuery(temp));
			_imageArrayCurItemIndex++;
		}
	});
	_imageArrayCurPager++;
}

function ImageArray() {
	// imgAry S
	var imgAry = jQuery('#imgArray');
	jQuery.each(jQuery('.imgAryContainer .imgTemplate > div > a'), function(
			idx, value) {
		if (idx < _imageArrayFirstPageSize) {
			var temp = _imgDiv.clone();
			temp.html(jQuery(value).outerHTML());
			imgAry.append(temp);
			_imageArrayCurItemIndex++;
		}
	});

	jQuery('.imgAryContainer .imgTemplate').hide();

	imgAry.imagesLoaded(function() {
		imgAry.masonry({
			itemSelector : '.box',
			columnWidth : function(containerWidth) {
				return containerWidth / 4;
			}
		});
	});
	// imgAry E

	jQuery(window).scroll(
			function() {
				if (jQuery(window).scrollTop() >= jQuery(document).height()
						- jQuery(window).height() - 1) {
					AppendImages();
				}
			});
}
