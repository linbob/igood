jQuery(document).ready(function () {

    var slideSmall = jQuery("#slideshow-main").find(".slidesshow-small");
    jQuery("#carousel").append(slideSmall);
    jQuery("#slideshow-main").find(".slidesshow-small").remove();

    //jCarousel Plugin
    jQuery('#carousel').jcarousel({
        vertical: true,
        scroll: 2 ,
        auto: 2 ,
        //wrap: 'last',
        initCallback: mycarousel_initCallback
    });

    //Front page Carousel - Initial Setup
    jQuery('div#slideshow-carousel a img').css({ 'opacity': '0.5' });
    jQuery('div#slideshow-carousel a img:first').css({ 'opacity': '1.0' });
    jQuery('div#slideshow-carousel li a:first').append('<span class="arrow"></span>')


    //Combine jCarousel with Image Display
    jQuery('div#slideshow-carousel li a').hover(
       	function () {

       	    if (!jQuery(this).has('span').length) {
       	        jQuery('div#slideshow-carousel li a img').stop(true, true).css({ 'opacity': '0.5' });
       	        jQuery(this).stop(true, true).children('img').css({ 'opacity': '1.0' });
       	    }
       	},
       	function () {

       	    jQuery('div#slideshow-carousel li a img').stop(true, true).css({ 'opacity': '0.5' });
       	    jQuery('div#slideshow-carousel li a').each(function () {

       	        if (jQuery(this).has('span').length) jQuery(this).children('img').css({ 'opacity': '1.0' });

       	    });

       	}
	).click(function () {

	    jQuery('span.arrow').remove();
	    jQuery(this).append('<span class="arrow"></span>');	    
	    jQuery('div#slideshow-main').find(".active").find("img").fadeOut("slow");
	    jQuery('div#slideshow-main').find(".active").removeClass('active');
	    jQuery('div#slideshow-main li.' + jQuery(this).attr('rel')).addClass('active');  
	    jQuery('div#slideshow-main').find(".active").find("img").hide().fadeIn("slow");
	    	    
	    	    
	    return false;
	});


});

//Carousel Tweaking

function mycarousel_initCallback(carousel) {

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function () {
        carousel.stopAuto();
    }, function () {
        carousel.startAuto();
    });
}