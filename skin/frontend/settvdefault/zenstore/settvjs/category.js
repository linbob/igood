jQuery(document).ready(function() {
	categoryAdBlock();
	categoryTagBlock();
});

function categoryAdBlock() {
	var showbox = jQuery('#categoryAD');
	var i = 0;
	jQuery('#categoryTemp a').each(
			function() {
				if (i < 3) {
					var href = jQuery(this).attr("href");
					var src = jQuery(this).find("img:first").attr("src");
					showbox.append('<li' + ((i == 2) ? ' class=fix' : '')
							+ '><a href="' + href + '"><img src="' + src
							+ '" class="img-category-ad"></a></li>');
				}
				i++;
			});
	jQuery("#categoryTemp").remove();
}
function categoryTagBlock() {
	var showbox = jQuery('.product-category-title:first');
	var baseURL = jQuery("#baseURL").val();
	var i = 0;
	jQuery.each(jQuery('#categoryTagTemp').text().split(" "), function(index,
			value) {
		jQuery.each(jQuery.trim(value).split("\n"), function(index2, value2) {
			var linkHTML = '<a href="' + baseURL
				+ 'catalogsearch/result/?q=' + encodeURI(jQuery.trim(value2))
				+ '">' + jQuery.trim(value2) + '</a>';
			if (i == 0)
				showbox.append('<div class="firstTag">' + linkHTML + '</div>');
			else
				showbox.append('<div>' + linkHTML + '</div>');
			i++;
		})
	});
	jQuery("#categoryTagTemp").remove();
}