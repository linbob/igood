jQuery.noConflict();

jQuery.fn.outerHTML = function(s) {
	return s ? this.before(s).remove() : jQuery("<p>").append(
			this.eq(0).clone()).html();
};

jQuery(function(jQuery) {
	Menu();
	CustomizeMenu();
	CustomizeClickEvent();
});

function Menu() {
	jQuery(".container").click(CloseHeaderLevel3);

	jQuery(".headerBar .logo").click(function() {
		jQuery(".menu>ul>li").find(">a").removeClass("selected");
		jQuery(".headerBar").stop().animate({
			height : 66
		}, 200);
		jQuery(".submenu-ul").slideUp(200);
		jQuery(".arrow-down").remove();
	});

	jQuery(".menu>ul>li>a").click(
			function() {
				if (!jQuery(this).hasClass("selected")) {
					jQuery(".headerBar").stop().animate({
						height : 130
					}, 200);
					jQuery(this).parent().find(">a").addClass("selected").end()
							.find(">ul").before(
									'<div class="arrow-down"></div>')
							.slideDown(0).end().siblings().find(">a.selected")
							.removeClass("selected").end().find(">ul").slideUp(
									0).end().find(".arrow-down").remove();
				} else {
					jQuery(this).removeClass("selected");
					jQuery(".headerBar").stop().animate({
						height : 66
					}, 200);
					jQuery(".submenu-ul").slideUp(200);
					jQuery(".arrow-down").remove();
				}
				return false;
			});

	// Menu S
	jQuery('.submenu-ul').superfish({
		useClick : false,
		pathClass : 'current',
		delay: 100,
		speed: 'fast'
	});
	// Menu E
}

function CustomizeMenu() {
	var menu = jQuery("#customizeMenu");
	var lis = jQuery(".menu>ul>li");
	var ul = jQuery(".menu>ul");

	jQuery.each(menu.find("a"), function(index, value) {
		var href = jQuery(this).attr("href");
		var target = jQuery(this).attr("target");		
		var indexAfterInsert = jQuery(this).attr("indexAfterInsert");
		var title = jQuery(this).html();
		
		if (target == undefined)
			target = "_self";
		
		var html = "<li><a href='" + href +"' target='" + target + "'>"+ title +"</a></li>";
		
		if (indexAfterInsert == undefined) {
			ul.append(html);
		} else {
			jQuery.each(lis, function(idx, val) {
				if (idx == indexAfterInsert) {
					jQuery(html).insertBefore(jQuery(this));
				}
			});
		}
	});
	menu.remove();
}

function CloseHeaderLevel3() {
	jQuery(".menu>ul>li>ul>li>ul").find(">a").removeClass("sfHover").end()
			.hide();
}

function CustomizeClickEvent()
{
	jQuery('.glyphicon').click(function(){
	    if(jQuery(this).hasClass('icon-collapse'))
	    {            
	        jQuery(this).parents('table').find('tr.show').addClass('hide').removeClass('show');
	        jQuery(this).addClass('icon-expand').removeClass('icon-collapse');	        
	    }
	    else
	    {                              
	        jQuery(this).parents('table').find('tr.hide').removeClass('hide').addClass('show');
	        jQuery(this).addClass('icon-collapse').removeClass('icon-expand');	        
	    }
	});
}
